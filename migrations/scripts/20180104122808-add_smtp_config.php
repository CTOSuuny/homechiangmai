<?php

class AddSmtpConfig extends Migration
{

    public function handle()
    {
        foreach (['conf_smtp_host', 'conf_smtp_username', 'conf_smtp_password', 'conf_smtp_port'] as $conf) {
            if (!$this->valueExists('bsi_configure', 'conf_key', $conf)) {
                $this->getConnection()->query("INSERT INTO bsi_configure (conf_key, conf_value) VALUES ('$conf', '')");
            }
        }

    }

}
