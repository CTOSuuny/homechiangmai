<?php

class AddFieldShowToHomepage extends Migration
{

    public function handle()
    {
        if ($this->hasColumn('bsi_gallery', 'show_in_homepage')) {
            return false;
        }

        $this->addColumn('bsi_gallery', 'show_in_homepage', 'TINYINT', 0, true);
    }

}
