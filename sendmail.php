<?php
session_start();
include('includes/csrf.class.php');

	if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
	include("includes/db.conn.php");
	include("includes/conf.class.php");
	include("includes/mail.class.php");
	include 'vendor/autoload.php';
	include 'includes/smtp-mail.class.php';

	$emailBody  = '';

	$emailBody .= "Contact from ".$_POST['firstname'].' '.$_POST['surnale'].",<br><br>";

	$firstname  = $_POST['firstname'];
	$surnale  	= $_POST['surnale'];
	$telephone  = $_POST['telephone'];
	$email   	= $_POST['email'];
	$type   	= $_POST['type'];
	$abuot   	= $_POST['abuot'];
	$message  	= $_POST['message'];

	$table		= '<br />'.
					'<table  style="font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;" cellpadding="4" cellspacing="1">'.
						'<tr>'.
							'<td align="left" width="30%" style="font-weight:bold; font-variant:small-caps; background:#eeeeee;">'.
							'Name :'.
							'</td>'.
							'<td align="left" style="background:#ffffff;">'.
							$firstname.' '.$surnale.
							'</td>'.
						'</tr>'.
						'<tr>'.
							'<td align="left" width="30%" style="font-weight:bold; font-variant:small-caps;background:#ffffff;">'.
							'Telelphone :'.
							'</td>'.
							'<td align="left" style="background:#ffffff;">'.
							$telephone.
							'</td>'.
						'</tr>'.
						'<tr>'.
							'<td align="left" width="30%" style="font-weight:bold; font-variant:small-caps;background:#ffffff;">'.
							'Email :'.
							'</td>'.
							'<td align="left" style="background:#ffffff;">'.
							''.$email.''.
							'</td>'.
						'</tr>'.
						'<tr>'.
							'<td align="left" width="30%" style="font-weight:bold; font-variant:small-caps;background:#ffffff;">'.
							'Enquiry Type :'.
							'</td>'.
							'<td align="left" style="background:#ffffff;">'.
							''.$type.''.
							'</td>'.
						'</tr>'.
						'<tr>'.
							'<td align="left" width="30%" style="font-weight:bold; font-variant:small-caps;background:#ffffff;">'.
							'How did you hear about us :'.
							'</td>'.
							'<td align="left" style="background:#ffffff;">'.
							''.$abuot.''.
							'</td>'.
						'</tr>'.
						'<tr>'.
							'<td align="left" width="30%" style="font-weight:bold; font-variant:small-caps;background:#ffffff;">'.
							'Message :'.
							'</td>'.
							'<td align="left" style="background:#ffffff;">'.
							''.$message.''.
							'</td>'.
						'</tr>'.
					'</table>';

	$emailBody .= $table;

	$smtpMail = new SMTPMail();

	$smtpConfig = [
		'smtp_host' => $bsiCore->config['conf_smtp_host'],
		'smtp_username' => $bsiCore->config['conf_smtp_username'],
		'smtp_password' => $bsiCore->config['conf_smtp_password'],
		'smtp_port' => $bsiCore->config['conf_smtp_port'],
		'smtp_encryption' => $bsiCore->config['conf_smtp_encryption']
	];

	$returnMsg = $smtpMail->setSubject($abuot)
        ->setSystemConfig($smtpConfig)
        ->sender([$bsiCore->config['conf_hotel_email']])
        ->receiver([$bsiCore->config['conf_notification_email']])
        ->setBody($emailBody)
        ->send();


	if ($returnMsg) {

		header('Location: contacts.php?code=1');
		die;
	}else {
		header('Location: contacts.php?code=0');
		die;
	}
?>
