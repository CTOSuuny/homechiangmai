<?php

use \Mpdf\Mpdf;

class PDF
{
    private $options;

    protected $content;

    protected $pdf;

    public function __construct()
    {
        $this->setOption();
        $this->pdf = new Mpdf($this->options);
    }

    /**
     * Set PDF Option
     *
     * @param PDF
     */
    public function setOption($options = [])
    {
        $tmpDir = dirname(__DIR__) . '/storage/tmp';

        if (empty($options)) {
            $this->options = [
                'tempDir' => $tmpDir
            ];

            return $this;
        }

        $this->options = $options;

        return $this;
    }

    /**
     * Set html to PDF
     *
     * @param string $html html
     */
    public function setHTML($html)
    {
        $this->content = $html;

        return $this;
    }

    /**
     * Generate PDF
     *
     * @param mixed $filename file name
     *
     * @return \Mpdf\Mpdf
     */
    public function generate($filename)
    {
        $this->pdf->WriteHTML($this->content);

        if (!empty($filename)) {
            return $this->pdf->Output($filename, 'D');
        }

        return $this->pdf->Output();
    }
}
