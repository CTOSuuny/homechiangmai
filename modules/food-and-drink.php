<div class="row show-room-dialog room-modules">
    <h2><?php trans('FOOD_AND_DRINKS') ?></h2>
    <p><?php trans('THERE_ARE_THREE') ?></p>
    <h3><?php trans('LEMONT_COFFEE_HOUSE') ?></h3>
    <p class="food-drink-paragraph"><?php trans('OPEN_DAILY') ?></p>
    <div class="row">
        <div class="col-sm-4">
            <div class="food-drink-place" style="
                background-image: url('<?php echo asset("images/food-and-drink/1517455815419.jpg"); ?>');
                background-repeat: no-repeat;
                background-size:     cover;
                background-position: center;
                height:300px;
            ">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="food-drink-place" style="
                background-image: url('<?php echo asset("images/food-and-drink/lemont-restaurant.JPG"); ?>');
                background-repeat: no-repeat;
                background-size:     cover;
                background-position: center;
                height:300px;
            ">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="food-drink-place" style="
                background-image: url('<?php echo asset("images/food-and-drink/1517455810189.jpg"); ?>');
                background-repeat: no-repeat;
                background-size:     cover;
                background-position: center;
                height:300px;
            ">
            </div>
        </div>
    </div>
    <div class="row ">
        <div class="col-sm-4">
            <div class="food-drink-place" style="
                background-image: url('<?php echo asset("images/food-and-drink/7446870512338.jpg"); ?>');
                background-repeat: no-repeat;
                background-size:     cover;
                background-position: center;
                height:300px;
            ">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="food-drink-place" style="
                background-image: url('<?php echo asset("images/food-and-drink/1517455807402.jpg"); ?>');
                background-repeat: no-repeat;
                background-size:     cover;
                background-position: center;
                height:300px;
            ">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="food-drink-place" style="
                background-image: url('<?php echo asset("images/food-and-drink/1517455808768.jpg"); ?>');
                background-repeat: no-repeat;
                background-size:     cover;
                background-position: center;
                height:300px;
            ">
            </div>
        </div>
    </div>
    <br>
    <h3><?php trans('EAT_THAI_ENGLAND') ?></h3>
    <p class="food-drink-paragraph"><?php trans('TO_OFFER_THAI_FOOD') ?></p>
    <div class="row">
        <div class="col-xs-12">
            <div class="eat-thai-image" style="background-image: url('<?php echo asset('images/food-and-drink/7409946102302.jpg') ?>')">
            </div>
        </div>
    </div>

</div>
