<div
data-zs-src='
["<?php echo asset('images/8.JPG') ?>",
"<?php echo asset('images/1.jpg') ?>",
"<?php echo asset('images/2.jpg') ?>",
"<?php echo asset('images/4.jpg') ?>",
"<?php echo asset('images/7.jpg') ?>"]'
data-zs-overlay="dots" data-zs-bullets="false" class="inner">
    <?php
        include("modules/navbar.php");
        $isDisableBookingEngine = $bsiCore->config['conf_booking_turn_off'];
    ?>
    <div class="inner-content">
        <br><br>
        <p class="quote">
            "<?php echo trans('WELCOME_QOUTE') ?>"
        </p>
        <p class="title"><?php trans('WELCOME_TO') ?></p>
        <h1><?php echo WELCOME_MESSAGE ?></h1>
        <br>
        <div class="content-header">
            <?php echo SUBTITLE_MESSAGE ?>
        </div>
        <br><br>
        <div class="hidden-xs book-form">
            <form class="form-inline booking-form" action="booking-search.php" method="post">
                <?php
                    $checkIn = "check-in";
                    $checkOut = "check-out";
                    include("modules/book-form.php");
                ?>
                <button type="submit" <?php echo $isDisableBookingEngine == '1' ? 'disabled' : '' ?> class="btn btn-default btn-book" ><?php echo BOOK_YOUR_ROOM_NOW; ?></button>
            </form>
        </div>
    </div>

<!--     <div class="row" style="display: none;">
        <div class="col-lg-6" style="padding-bottom: 15px">
            <a class="auto" data-fancybox="gallery" href="<?php echo asset('images/pro1.jpg') ?>"><a class="linker" href="page/promotions"><img src="<?php echo asset('images/pro1.jpg') ?>" style="width: 100%" src="page/promotions" onclick="window.open(this.src)"></a></a>
        </div>
        <div class="col-lg-6" style="padding-bottom: 15px">
            <a data-fancybox="gallery" href="<?php echo asset('images/pro2.jpg') ?>"><a href="page/promotions"><img src="<?php echo asset('images/pro2.jpg') ?>" style="width: 100%" src="page/promotions" onclick="window.open(this.src)"></a></a>
        </div>
                <div class="col-lg-6" style="padding-bottom: 15px">
            <a data-fancybox="gallery" href="<?php echo asset('images/pro1.jpg') ?>"><a href="page/promotions"><img src="<?php echo asset('images/pro2.jpg') ?>" style="width: 100%" src="page/promotions" onclick="window.open(this.src)"></a></a>
        </div>
                <div class="col-lg-6" style="padding-bottom: 15px">
            <a data-fancybox="gallery" href="<?php echo asset('images/pro1.jpg') ?>"><a href="page/promotions"><img src="<?php echo asset('images/pro2.jpg') ?>" style="width: 100%" src="page/promotions" onclick="window.open(this.src)"></a></a>
        </div>
                <div class="col-lg-6" style="padding-bottom: 15px">
            <a data-fancybox="gallery" href="<?php echo asset('images/pro1.jpg') ?>"><a href="page/promotions"><img src="<?php echo asset('images/pro2.jpg') ?>" style="width: 100%" src="page/promotions" onclick="window.open(this.src)"></a></a>
        </div>
    </div> -->
</div>
