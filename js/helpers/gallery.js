var Gallery = function(selector) {
    this.selector = selector;
}

Gallery.prototype = {
    constructor: Gallery,

    init: function() {
        this.initSmoothSlide()
    },

    initSmoothSlide: function() {
        $(this.selector).smoothSlides({
            effectModifier: 2,
            effect: 'zoomOut',
            effectEasing: 'linear',
            navigation: false,
            matchImageSize: false,
            pagination: false
        });
    }
}
