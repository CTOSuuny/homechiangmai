<?php

$scriptPath = __DIR__ . '/migrations/scripts/';
$command = $argv[1] ?? 'migrate';

function underscoreToCamel(string $str)
{
    $str = explode('_', $str);
    $str = array_map(function ($splited) {
        return ucfirst($splited);
    }, $str);

    return implode('', $str);
}

if ($command == 'create') {

    if (empty($argv[2])) {
        die("Please enter script name. \n");
    }

    $scriptName = $argv[2];

    $templateFile = file_get_contents($scriptPath . 'template.php');

    $templateFile = str_replace('{ClassName}', underscoreToCamel($scriptName), $templateFile);
    $filename = $scriptPath . date('YmdHis') . '-' . $scriptName . '.php';
    file_put_contents($filename, $templateFile);
    chmod($filename, 0777);
}

if ($command == 'migrate') {

    require_once 'includes/db.conn.php';
    require_once 'migrations/scripts/migration.php';

    $migrationScripts = scandir($scriptPath);

    foreach ($migrationScripts as $script) {
        if (in_array($script, ['.', '..', 'template.php', 'migration.php'])) {
            continue;
        }

        echo "\033[0;32m" . 'Migrated: ' . $script . "\033[0m" . "\n";

        list($date, $class) = explode('-', $script);
        $class = str_replace('.php', '', underscoreToCamel($class));

        include $scriptPath . $script;

        $migrationClass = new $class;
        $migrationClass->handle();
    }
}
