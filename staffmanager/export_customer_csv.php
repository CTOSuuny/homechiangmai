<?php

include("access.php");
include("../includes/db.conn.php");
include("../includes/admin.class.php");

if (empty($_POST['export_csv'])) {
    header('Location: customerlookup.php');
    exit;
}

$fields = [
    'client_id',
    'first_name',
    'surname',
    'title',
    'street_addr',
    'city',
    'province',
    'zip',
    'country',
    'phone',
    'fax',
    'email',
    'id_type',
    'id_number',
    'additional_comments'
];

$fields = array_map(function ($field) {
    return 'bsi_clients.' . $field;
}, $fields);

$customersQuery = $mysqli->query('SELECT ' . implode(',', $fields) . ', count(bsi_bookings.booking_id) as booking_count
    FROM bsi_clients
    LEFT JOIN bsi_bookings
    ON bsi_clients.client_id = bsi_bookings.client_id
    GROUP BY bsi_clients.client_id
');

$customers = $customersQuery->fetch_all(MYSQLI_ASSOC);

if (empty($customers)) {
    header('Location: customerlookup.php');
    exit;
}

$csvSep = ',';

$keys = array_keys($customers[0]);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=customer.csv");
header("Pragma: no-cache");
header("Expires: 0");
echo implode($csvSep, $keys) . "\n";

foreach ($customers as $customer) {

    $customer = array_map(function($c) use ($csvSep) {
        return '"' . str_replace($csvSep, '', $c) . '"';
    }, $customer);

    $customer['id_number'] = $customer['id_number'];

    echo implode($csvSep, $customer) . "\n";
}
