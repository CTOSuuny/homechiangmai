<?php
include("access.php");
if(isset($_POST['submitRoomtype'])){
	include("../includes/db.conn.php");
	include("../includes/conf.class.php");
	include("../includes/admin.class.php");
	$bsiAdminMain->add_edit_roomtype();
	header("location:roomtype.php");
	exit;
}
include("header.php");
include("../includes/conf.class.php");
include("../includes/admin.class.php");
if(isset($_GET['id']) && $_GET['id'] != ""){
	$id = $bsiCore->ClearInput($_GET['id']);
	if($id){
		$result = $mysqli->query($bsiAdminMain->getRoomtypesql($id));
		$row    = $result->fetch_assoc();
		$readonly = 'readonly="readonly"';
	}else{
		$row    = NULL;
		$readonly = '';
	}
}else{
	header("location:admin_capacity.php");
	exit;
}

$languages = $bsiAdminMain->getLanguages();
?>
<link rel="stylesheet" type="text/css" href="css/jquery.validate.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin-style.css') ?>" />
<script src="js/editor/ckeditor.js"></script>

<div id="container-inside">
	<span style="font-size:16px; font-weight:bold"><?php echo ROOM_TYPE_ADD_AND_EDIT;?></span>
	<hr />
	<form action="<?=$_SERVER['PHP_SELF']?>" method="post" id="form1" enctype="multipart/form-data">
		<table cellpadding="5" cellspacing="2" border="0" style="width: 100%;">
			<tr>
				<td style="width: 20%;"><strong><?php echo ROOM_TYPE_TITLE;?>:</strong></td>
				<td valign="middle"><input type="text" name="roomtype_title" id="roomtype_title" class="required" value="<?=$row['type_name']?>" style="width:250px;" /> &nbsp;&nbsp;
					<?php echo EXAMPLE_DELUXE_AND_STANDARD;?>
				</td>
			</tr>
			<tr>
				<td><strong><?php trans('LANGUAGE') ?></strong></td>
				<td>
					<select id="lang-switcher">
						<?php foreach ($languages as $lang): ?>
							<option value="<?php echo $lang['lang_code'] ?>"><?php echo $lang['lang_title']; ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td valign="top"><strong><?php echo DESCRIPTION_FACILITIES_TEXT; ?>:</strong></td>
				<td>
					<?php $content = json_decode($row['description'], true); ?>

					<?php foreach ($languages as $lang): ?>

							<?php
								$description = $row['description'];

								if (isset($content[$lang['lang_code']])) {
									$description = $content[$lang['lang_code']];
								}
							?>

							<div data-language="<?php echo $lang['lang_code'] ?>" class="multilang-textarea-container <?php echo ($lang['lang_default'] == 1 ? '' : 'hide')?>">
								<textarea
									data-target="#description"
									data-group="description"
									class="multilang-textarea"
									data-language="<?php echo $lang['lang_code'] ?>"
									rows="5"
									cols="3"
									name="description[<?php echo $lang['lang_code'] ?>]"
									id="<?php echo $lang['lang_code'] ?>_description" style="width:700px; height:150px;"><?php echo $description;?></textarea>
							</div>

					<?php endforeach; ?>
				</td>
			</tr>
			<tr>
				<td valign="top"><strong><?php echo ROOM_FACILITIES; ?>:</strong></td>
				<td>
					<?php $content = json_decode($row['facilities'], true); ?>

					<?php foreach ($languages as $lang): ?>

						<?php
							$facilities = $row['facilities'];

							if (isset($content[$lang['lang_code']])) {
								$facilities = $content[$lang['lang_code']];
							}
						?>

						<div data-language="<?php echo $lang['lang_code'] ?>" class="multilang-textarea-container <?php echo ($lang['lang_default'] == 1 ? '' : 'hide')?>">
							<textarea
								data-target="#facilities"
								data-group="facilities"
								class="multilang-textarea"
								data-language="<?php echo $lang['lang_code'] ?>"
								rows="5"
								cols="3"
								name="facilities[<?php echo $lang['lang_code'] ?>]"
								id="<?php echo $lang['lang_code'] ?>_facilities" style="width:700px; height:150px;"><?php echo $facilities;?></textarea>
						</div>

					<?php endforeach; ?>
				</td>
			</tr>
			<tr>
				<td><input type="hidden" name="addedit" value="<?=$id?>"></td>
				<td><input type="submit" value="<?php echo ROOM_TYPE_SUBMIT;?>" name="submitRoomtype" style="background:#e5f9bb; cursor:pointer; cursor:hand;" /></td>
			</tr>
		</table>
	</form>
</div>

<script type="text/javascript">
	$().ready(function() {
		$("#form1").validate();
	});
	var LANGS = <?php echo json_encode($languages); ?>;
</script>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<script src="js/modules/room-type-form.js"></script>
<script>
	var roomTypeForm = new RoomTypeForm()
	roomTypeForm.init();
</script>
<?php include("footer.php"); ?>
