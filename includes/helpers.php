<?php

/**
 * Redirect to error page
 *
 * @param  int $code error code
 *
 * @return void
 */
function abort($code = 404)
{
    header("Location: /errors/$code.php");
    die();
}

/**
 * Call asset path
 *
 * @param mixed $path asset path
 */
function asset(string $path = null)
{
    $scheme = $_SERVER['HTTP_X_FORWARDED_PROTO'] ?? $_SERVER['REQUEST_SCHEME'];

    return $scheme . '://' . $_SERVER['HTTP_HOST'] . '/' . $path;
}

/**
 * Trans message
 *
 * @param  mixed  $key    translated key
 * @param  bool $return return value
 *
 * @return mixed
 */
function trans($key, $return = false)
{
    if (defined($key)) {
        if ($return) {
            return constant($key);
        }

        echo constant($key);

        return;
    }

    echo '[' . $key . ']';
}

/**
 * Check current language is specific language
 *
 * @param  string  $lang langauge code
 *
 * @return bool
 */
function is_lang(string $lang)
{
    return !empty($_COOKIE['language']) && $_COOKIE['language'] == $lang;
}

/**
 * Map language code with language file
 *
 * @param  string $shortCode short code
 *
 * @return string
 */
function map_lang(string $shortCode)
{
    $languages = [
        'th' => 'thai',
        'en' => 'english',
        'ch' => 'china'
    ];

    return array_key_exists($shortCode, $languages) ? $languages[$shortCode] : $languages['en'];
}

/**
 * Parse money to int
 *
 * @param mixed $money money
 *
 * @return float
 */
function parse_money_to_int($money)
{
    return preg_replace(['/([^0-9\.])/i'], '', $money);
}

/**
 * Parse string to money
 *
 * @param mixed $money money
 *
 * @return string
 */
function parse_to_money($money)
{
    return number_format($money, 2, '.', ',');
}

/**
 * Redirect to page
 *
 * @param string $path path
 *
 * @return void
 */
function redirect($path)
{
    header('Location: ' . $path);
    die;
}

/**
 * Set flash session
 *
 * @param string $key   session key
 * @param mixed $value session value
 *
 * @return void
 */
function set_flash(string $key, $value)
{
    $_SESSION[$key] = is_array($value) ? json_encode($value) : $value;
}

/**
 * Get flash value
 *
 * @param  string $key session key
 *
 * @return  mixed
 */
function get_flash($key)
{
    if (empty($_SESSION[$key])) {
        return;
    }

    $sessionValue = $_SESSION[$key];
    $decodeValue = json_decode($sessionValue, true);
    unset($_SESSION[$key]);

    if (empty($decodeValue)) {
        return $sessionValue;
    }

    return $decodeValue;
}

/**
 * Debug and die
 *
 * @param mixed $debug debug value
 *
 * @return void
 */
function dd($debug)
{
    echo '<pre>';

    if (is_array($debug)) {
        print_r($debug);
    } else if (is_string($debug)) {
        echo $debug;
    } else {
        var_dump($debug);
    }

    echo '</pre>';

    die;
}

/**
 * Check if request is allowed request
 *
 * @param mixed  $requestType
 *
 * @return boolean
 */
function is_request($methods)
{
    if (is_array($methods)) {

        $methods = array_map(function($method) {
            return strtolower($method);
        }, $methods);

        return !empty($_POST['method']) && in_array(strtolower($_POST['method']), $methods);
    }

    return !empty($_POST['method']) && strtolower($_POST['method']) === strtolower($methods);
}

/**
 * Convert params to string
 *
 * @param  array  $params parameter
 *
 * @return string
 */
function stringify_params(array $params)
{
    $paramStr = [];

    foreach ($params as $key => $value) {
        $paramStr[] = $key . '=' . $value;
    }

    return implode('&', $paramStr);
}

/**
 * Create route name
 * @param  string $fileName file name
 * @param  array  $params   parameters
 *
 * @return string
 */
function route($fileName, $params = [])
{
    return $fileName . (empty($params) ? '' : '?') . stringify_params($params);
}

/**
 * Create method field
 *
 * @param string $method method
 *
 * @return string
 */
function method_field(string $method)
{
    return "<input type=\"hidden\" name=\"method\" value=\"{$method}\">";
}

/**
 * Check allowd method
 *
 * @param  boolean $notCheckHasMethod flag for set not check post, put and delete method
 *
 * @return void
 */
function allow_http_method($notCheckHasMethod = true)
{
    if (empty($_POST) || ($notCheckHasMethod && !in_array(strtolower($_POST['method']), ['put', 'post', 'delete']))) {
        abort(405);
    }
}

/**
 * Set old session
 *
 * @param mixed $value session value
 */
function set_old_session($value)
{
    if (is_array($value)) {
        return $_SESSION['old'] = json_encode($value);
    }

    return $_SESSION['old'] = $value;
}

/**
 * Get old session
 *
 * @return mixed
 */
function old()
{
    if (empty($_SESSION['old'])) {
        return null;
    }

    $oldAsArray = json_decode($_SESSION['old'], true);

    if (!empty($oldAsArray)) {
        unset($_SESSION['old']);
        return $oldAsArray;
    }

    $oldValue = $_SESSION['old'];
    unset($_SESSION['old']);

    return $oldValue;
}
