<?php

include_once "env.class.php";
include "helpers.php";

$env = new Env();
$env->getEnvFile();

define("MYSQL_SERVER", $env->getValue('MYSQL_SERVER'));
define("MYSQL_USER", $env->getValue('MYSQL_USER'));
define("MYSQL_PASSWORD", $env->getValue('MYSQL_PASSWORD'));
define("MYSQL_DATABASE", $env->getValue("MYSQL_DATABASE"));
define('MYSQL_PORT', $env->getValue('MYSQL_PORT'));

$mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
if (mysqli_connect_errno()) {  printf("Connect failed: %s
", mysqli_connect_error());  exit(); }
?>
