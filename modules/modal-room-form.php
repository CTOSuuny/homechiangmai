<?php
include("../includes/db.conn.php");
include("../includes/conf.class.php");
include("../includes/room.class.php");
include("../language.php");
include('../includes/csrf.class.php');
$isDisableBookingEngine = $bsiCore->config['conf_booking_turn_off'];
?>
<style>
    .datepicker{
        z-index: 99999999;
    }
</style>
<div class="book-form-search" style="padding-bottom:15px;">
    <form class="form-inline booking-form" action="<?php echo asset('booking-search.php') ?>" method="post">
        <?php
            $checkIn = "check-in-room-type";
            $checkOut = "check-out-room-type";
            include("book-form.php");
        ?>
        <input type="hidden" name="room_type_id_index" value="<?php echo $_GET['room_type_id_index']; ?>">
        <div align="center">
            <button type="submit" <?php echo $isDisableBookingEngine == '1' ? 'disabled' : '' ?> class="btn btn-default btn-book" ><?php echo BOOK_YOUR_ROOM_NOW; ?></button>
        </div>
    </form>
</div>
<script>

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    //Checkin and Checkout date
    var maxBookingDate = new Date();
    maxBookingDate = maxBookingDate.setDate(maxBookingDate.getDate() + <?php echo $bsiCore->config['conf_maximum_global_years'] ?>)
    maxBookingDate = new Date(maxBookingDate);

    var checkinRoomType = $('#check-in-room-type').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() || date.valueOf() > maxBookingDate ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {

        if (ev.date.valueOf() > checkoutRoomType.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + <?php echo  $bsiCore->config['conf_min_night_booking']; ?>);
            checkoutRoomType.setValue(newDate);
        }

        checkinRoomType.hide();
        $('#check-out-room-type')[0].focus();
    }).data('datepicker');

    var checkoutRoomType = $('#check-out-room-type').datepicker({
        onRender: function(date) {
            var checkoutdt= parseInt(checkinRoomType.date.valueOf())+(60*60*24*1000*<?php echo  ($bsiCore->config['conf_min_night_booking']-1); ?>);

            return date.valueOf() <= checkoutdt || date.valueOf() > maxBookingDate ? 'disabled' : '';

        }
    }).on('changeDate', function(ev) {
        checkoutRoomType.hide();
    }).data('datepicker');
</script>
