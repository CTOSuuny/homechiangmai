<?php

class PayPaid
{
    private $merchantId;

    private $websiteCode;

    private $secretKey;

    private $selectedBank;

    private $service;

    private $host;

    private $currency;

    private $banks = [
        'scb' => 1,
        'ktb' => 2,
        'bay' => 3,
        'bbl' => 4,
        'tbank' => 12
    ];

    public function __construct()
    {
        $this->service = 1; //Internet Banking
        $this->currency = 'THB';
    }

    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    public function setCredential($credential)
    {
        $this->merchantId = $credential['merchant_id'];
        $this->websiteCode = $credential['website_code'];
        $this->secretKey = $credential['secret_key'];

        return $this;
    }

    public function setBank($bank = 'scb')
    {
        if (!array_key_exists($bank, $this->banks)) {
            $this->selectedBank = $this->banks['scb'];

            return $this;
        }

        $this->selectedBank = $this->banks[$bank];

        return $this;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }
    
    public function setService($id)
    {
        $this->service = $id;

        return $this;
    }


    private function createTransactionId($orderId)
    {
        return md5($this->merchantId . $this->secretKey . $orderId);
    }

    public function buildForm($order)
    {
        echo '<form id="form" method="post" action="' . $this->host . '">';
            echo '<input type="hidden" name="MerchantID" value="' . $this->merchantId . '">';
            echo '<input type="hidden" name="WebsiteCode" value="' . $this->websiteCode . '">';
            echo '<input type="hidden" name="Service" value="' . $this->service . '">';
            echo '<input type="hidden" name="BankID" value="' . $this->selectedBank . '">';
            echo '<input type="hidden" name="RefTransactionID" value="' . $order['id'] . '">';
            echo '<input type="hidden" name="CustomerName" value="' . $order['name'] . '">';
            echo '<input type="hidden" name="DeliveryAddress" value="' . $order['address'] . '">';
            echo '<input type="hidden" name="Description" value="' . $order['description'] . '">';
            echo '<input type="hidden" name="PricePerUnit" value="' . $order['price_per_unit'] . '">';
            echo '<input type="hidden" name="Quantity" value="' . $order['quantity'] . '">';
            echo '<input type="hidden" name="Amount" value="' . $order['amount'] . '">';
            echo '<input type="hidden" name="TotalAmount" value="' . $order['total_amount'] . '">';
            echo '<input type="hidden" name="DeliveryFee" value="' . $order['delivery_fee'] . '">';
            echo '<input type="hidden" name="NetTotal" value="' . $order['net_total'] . '">';
            echo '<input type="hidden" name="Currency" value="' . $this->currency . '">';
            echo '<input type="hidden" name="TimeZone" value="41">';
            echo '<input type="hidden" name="MobilePhone" value="' . $order['phone'] . '">';
            echo '<input type="hidden" name="EncryptText" value="' . $this->createTransactionId($order['id']) . '">';
        echo '</form>';
        echo '<script>';
            echo 'document.getElementById("form").submit()';
        echo '</script>';
    }
}