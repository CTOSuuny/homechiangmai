<?php
include("access.php");
if(isset($_POST['submitCapacity'])){
	include("../includes/db.conn.php");
	include("../includes/conf.class.php");
	include("../includes/admin.class.php");
	$bsiAdminMain->add_edit_cms();
	if ($_POST['type'] == '1') {
		header('Location: /staffmanager/cms_home.php');
	}else if ($_POST['type'] == '2') {
		header('Location: /staffmanager/cms_about.php');
	}else if ($_POST['type'] == '3') {
		header('Location: /staffmanager/cms_room.php');
	}else if ($_POST['type'] == '4') {
		header('Location: /staffmanager/cms_gallery.php');
	}else if ($_POST['type'] == '5') {
		header('Location: /staffmanager/cms_bar.php');
	}else if ($_POST['type'] == '6') {
		header('Location: /staffmanager/cms_work.php');
	}else if ($_POST['type'] == '7') {
		header('Location: /staffmanager/cms_event.php');
	}else if ($_POST['type'] == '8') {
		header('Location: /staffmanager/cms_contact.php');
	}else if ($_POST['type'] == '0') {
		header('Location: /staffmanager/cms_maf.php');
	}else{
		header('Location: /staffmanager');
	}
	exit;
}

$lang = $_GET['lang'];
$type = $_GET['type'];

include("header.php");
include("../includes/conf.class.php");
include("../includes/admin.class.php");
if(isset($_GET['id']) && $_GET['id'] != ""){

	$id = $bsiCore->ClearInput($_GET['id']);
	if($id){
		$result = $mysqli->query("select * from bsi_cms where id=".$id);
		$row    = $result->fetch_assoc();
		$lang = $row["language"];
		$type = $row["type"];
		// $dflt=($row['lang_default'])? 'checked="checked"':'';
	}else{
		$row    = NULL;
		$readonly = '';
		$dflt='';
	}

}else{

	header("location:event.php");

	exit;

}

?>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.validate.css" />
	<script type="text/javascript" src="../js//dtpicker/jquery.ui.datepicker-<?=$langauge_selcted?>.js"></script>
	<style>
    	input{
    		margin: 10px;
    	}
    </style> 

<div id="container-inside"> <span style="font-size:16px; font-weight:bold">Add/Edit new event</span>
	<hr />
	<form action="<?=$_SERVER['PHP_SELF']?>" method="post" id="form1" enctype="multipart/form-data" >
		<table cellpadding="5" cellspacing="2" border="0">
			<tr>
				<td><strong>Title :</strong></td>
				<td valign="middle"><input type="text" name="title" class="required" value="<?=$row['key_index']?>" style="width:350px;" />
				</td>
			</tr>
			<tr>
				<td><strong>Detail :</strong></td>
				<td><textarea type="text" name="detail" id="detail" class="required " style="width:700px;height: 300px;" /><?=$row['detail']?></textarea></td>
			</tr>

			<input type="hidden" name="type" value="<?=$type?>">
			<input type="hidden" name="lang" value="<?=$lang?>">
			<input type="hidden" name="addedit" value="<?=$id?>">
			<td></td>
			<td><input type="submit" value="<?php echo ADD_EDIT_CAPACITY_SUBMIT;?>" name="submitCapacity" style="background:#e5f9bb; cursor:pointer; cursor:hand;" /></td>
			
		</table>
	</form><br />
</div>
<script type="text/javascript">
	$().ready(function() {

		$("#form1").validate();
	});

	$(document).ready(function() {
			// $.datepicker.setDefaults($.datepicker.regional["<?=$langauge_selcted?>"]);
			// $.datepicker.setDefaults({
			// 	dateFormat: '<?=$bsiCore->config['conf_dateformat']?>'
			// });
			// $("#txtFromDate").datepicker({
			// 	minDate: 0,
			// 	maxDate: "+<?php echo $bsiCore->config['conf_maximum_global_years']; ?>D",
			// 	numberOfMonths: 2,
			// 	onSelect: function(selected) {
			// 		var date = $(this).datepicker('getDate');
			// 		if (date) {
			// 			date.setDate(date.getDate() + <?=$bsiCore->config['conf_min_night_booking']?>);
			// 		}
			// 		$("#txtToDate").datepicker("option", "minDate", date)
			// 	}
			// });
			// $("#txtToDate").datepicker({
			// 	minDate: 0,
			// 	maxDate: "+<?php echo $bsiCore->config['conf_maximum_global_years']; ?>D",
			// 	numberOfMonths: 2,
			// 	onSelect: function(selected) {
			// 		$("#txtFromDate").datepicker("option", "maxDate", selected)
			// 	}
			// });

			$("#txtFromDate").datepicker({
            format: "dd-mm-yyyy",
            todayBtn: true,
            autoclose: true,
	          })
	          .on("changeDate", function(e) {
	            var checkInDate = e.date, $checkOut = $("#txtToDate");    
	            checkInDate.setDate(checkInDate.getDate() + 1);
	            $checkOut.datepicker("setStartDate", checkInDate);
	            $checkOut.datepicker("setDate", checkInDate).focus();
	          });
	        $("#txtToDate").datepicker({
	          format: "dd-mm-yyyy",
	          todayBtn: true,
	          autoclose: true
	        });  

			// $("#txtFromDate").datepicker();
			$("#datepickerImage").click(function() {
				$("#txtFromDate").datepicker("show");
			});

			// $("#txtToDate").datepicker();
			$("#datepickerImage1").click(function() {
				$("#txtToDate").datepicker("show");
			});
		});
</script>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<?php include("footer.php"); ?>
