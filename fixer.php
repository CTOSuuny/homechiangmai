<?php


if (empty($argv[1])) {
    die("Please enter file or directory path \n");
}

$path = $argv[1];

$rules = [
    '@Symfony' => true,
    'psr0' => false,
    'concat_space' => [
        'spacing' => 'one'
    ],
    'trailing_comma_in_multiline_array' => false,
    'phpdoc_no_empty_return' => false,
    'new_with_braces' => false,
    'phpdoc_align' => false,
    'phpdoc_summary' => false,
    'no_multiline_whitespace_around_double_arrow' => false,
    'pre_increment' => false,
    'no_multiline_whitespace_before_semicolons' => true,
    'array_syntax' => [
        'syntax' => 'short'
    ],
    'binary_operator_spaces' =>  [
        'align_equals' => true
    ],
    'return_type_declaration' => [
        'space_before' => 'one'
    ],
    'php_unit_fqcn_annotation' => false,
    'indentation_type' => true
];

$rules = json_encode($rules);

shell_exec("php-cs-fixer fix $path --rules='$rules'");
