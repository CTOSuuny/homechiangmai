<?php

class AddFieldSmtpEncryptionToConf extends Migration
{

    public function handle()
    {
        if (!$this->valueExists('bsi_configure', 'conf_key', 'conf_smtp_encryption')) {
            $this->getConnection()->query("INSERT INTO bsi_configure (conf_key, conf_value) VALUES ('conf_smtp_encryption', '')");
        }
    }

}
