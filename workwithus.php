<?php
  include("language_set.php");
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Home chiang mai Hotel</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Rubik:300,400,700" rel="stylesheet">
    <link rel="icon" href="favicon.ico" sizes="16x16 32x32" type="image/png">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>

    <!-- Theme Style -->
    <link rel="stylesheet" href="css/style.css">

    <style>
      body::-webkit-scrollbar { 
                display: none; 
            } 
      .inactive{
            color: #FFFFFF;
          text-align: center;

          -webkit-transition: opacity 0.5s ease-in-out;
          -moz-transition: opacity 0.5s ease-in-out;
          -ms-transition: opacity 0.5s ease-in-out;
          -o-transition: opacity 0.5s ease-in-out;
           opacity: 0;
      }
      /* Make the image fully responsive */
      .carousel-inner img {
          width: 100%;
          height: 100%;
      }
      #mixedSlider {
        position: relative;
      }
      #mixedSlider .MS-content {
        white-space: nowrap;
        overflow: hidden;
        margin: 0 5%;
      }
      #mixedSlider .MS-content .item {
        display: inline-block;
        width: 50%;
        position: relative;
        vertical-align: top;
        overflow: hidden;
        height: 100%;
        white-space: normal;
        padding: 0 10px;
      }
      /*@media (max-width: 991px) {
        #mixedSlider .MS-content .item {
          width: 50%;
        }
      }*/
      @media (max-width: 767px) {
        #mixedSlider .MS-content .item {
          width: 100%;
        }
      }
      #mixedSlider .MS-controls button {
        position: absolute;
        border: none;
        background-color: transparent;
        outline: 0;
        font-size: 50px;
        top: 95px;
        color: rgba(0, 0, 0, 0.4);
        transition: 0.15s linear;
      }
      #mixedSlider .MS-controls button:hover {
        color: rgba(0, 0, 0, 0.8);
      }
      @media (max-width: 992px) {
        #mixedSlider .MS-controls button {
          font-size: 30px;
        }
      }
      @media (max-width: 767px) {
        #mixedSlider .MS-controls button {
          font-size: 20px;
        }
      }
      #mixedSlider .MS-controls .MS-left {
        left: 0px;
      }
      @media (max-width: 767px) {
        #mixedSlider .MS-controls .MS-left {
          left: -10px;
        }
      }
      #mixedSlider .MS-controls .MS-right {
        right: 0px;
      }
      @media (max-width: 767px) {
        #mixedSlider .MS-controls .MS-right {
          right: -10px;
        }
      }
    </style>
  </head>
  <body>

  <!-- Sidebar -->
  <div class="w3-sidebar w3-bar-block w3-animate-left" style="display:none;z-index: 4;" id="mySidebar">
    <a href="#" class="w3-bar-item w3-button" style="margin-top: 100px;"></a>
    <a href="index.php" class="w3-bar-item w3-button"><?php echo $manu["home"]; ?></a>
    <a class="w3-bar-item w3-button" href="abouts.php"><?php echo $manu["about"]; ?></a>
    <a class="w3-bar-item w3-button" href="room.php"><?php echo $manu["room"]; ?></a>
    <a class="w3-bar-item w3-button" href="gallery.php"><?php echo $manu["gallery"]; ?></a>
    <a class="w3-bar-item w3-button" href="bar.php"><?php echo $manu["bar"]; ?></a>
    
    <a class="w3-bar-item w3-button" href="workwithus.php"><?php echo $manu["work"]; ?></a>
    <a class="w3-bar-item w3-button" href="calendar.php"><?php echo $manu["event"]; ?></a>
    <a class="w3-bar-item w3-button" href="contacts.php"><?php echo $manu["contact"]; ?></a>    
    <div class="w3-row" style="width: 30%;margin-left: 5%;padding-top: 50px;">
      <div class="w3-third">
        <div><img src="/images/thai.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=thai'"></div>
      </div>
      <div class="w3-third">
        <div><img src="/images/engv.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=eng'"></div>
      </div>
      <div class="w3-third">
        <div><img src="/images/chinav.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=china'"></div>
      </div>
    </div>


    <!-- <a class="w3-bar-item w3-button" href=""><span>Book Now</span></a> -->
  </div>
    
    <header role="banner" style="position: fixed;">
     
      <nav class="navbar navbar-expand-md navbar-dark bg-light">
        <div class="container">
          <a href="index.php"><img src="images/logo.jpg" class="logo" id="logo"></a>
          <div>
            
            <ul class="navbar-nav ml-auto pl-lg-5 pl-0 activate-nav">
              <li class="nav-item">
                <a class="nav-link active" href="index.html">Home</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="rooms.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Rooms</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="rooms.html">Room Videos</a>
                  <a class="dropdown-item" href="rooms.html">Presidential Room</a>
                  <a class="dropdown-item" href="rooms.html">Luxury Room</a>
                  <a class="dropdown-item" href="rooms.html">Deluxe Room</a>
                </div>

              </li>
              <li class="nav-item">
                <a class="nav-link" href="">Event</a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="">Contact</a>
              </li>

               <li class="nav-item cta">
                <a class="nav-link" href=""><span>Book Now</span></a>
              </li>
            </ul>


            <!-- <a id="trigger" ></a> -->
            
            <!-- <button class="w3-button w3-xlarge" id="trigger" onclick="w3_open()"></button> -->
            <div class="container1" onclick="myFunction(this)">
              <div class="bar1" id="bar1"></div>
              <div class="bar3" id="bar3"></div>
            </div>
          </div>
        </div>
        </div>
        <div class="book">
          <a id="book" href="booking.php"><?php echo $manu["book"]; ?></a>
        </div>
      </nav>
    </header>
    <!-- END header -->

    <section class="site-hero overlay" data-stellar-background-ratio="0.5" style="overflow: hidden; height: 200vh;">
      <div class="fullscreen-bg" id="fullscreen" style="background-image: url(images/Hotel/IMG_3401.jpg);height: 100%;position: fixed;background-repeat: no-repeat;
    background-position: center center;
    background-size: 100% 100%;">
                <!-- <video loop muted autoplay class="fullscreen-bg__video" style="  
                      top: 50%;
                      left: 50%;
                      min-width: 100%;
                      min-height: 100%;
                      width: auto;
                      height: auto;
                      z-index: -100;
                      transform: translate(-50%, -50%);
                      position: fixed;">
                    <source src="video/background.mp4" type="video/mp4">
                </video> -->
            </div>
    <div class="about-us">
      <div class="first" id="section07">
        <div class="text" style="    padding-left: inherit;
    padding-right: inherit;">
          <p style="font-size: 100px;    line-height: 100px;"><?php echo $lang["Header_colume1"]; ?></p>
          <p style="font-size: 20px; " id="csss">
            <?php echo $lang["content_colume1"]; ?></p>
          <div class="scrolling" id="secter7" style="top: 5vh">
            <a><span></span><span></span><span></span><p style="margin-top: 30px;"><?php echo $lang["scroller "]; ?></p></a>
          </div>
        </div>
      </div>
<!--       <div class="second">
        <div class="text">
          <p></p>
        </div>
      </div> -->
      <div class="third" style="height: 80vh">
        <div class="text">
          <p>
            <?php echo $lang["content_1"]; ?> </p>
        </div>
      </div>
    </div>
    </section>



   
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
                    <h3>Reservations</h3>
                    <p class="lead"><a href="tel://+ 66 53 218 856">+ 66 53 218 856</a></p>
          </div>
          <div class="col-md-4">
            <h3>Connect With Us</h3>
            <p>We are socialized. Follow us</p>
            <p>
              <a href="https://www.facebook.com/HomeChiangMaiHotel/" class="pl-0 p-3"><span class="fa fa-facebook"></span></a>
              <a href="https://www.instagram.com/homechiangmaihotel/" class="p-3"><span class="fa fa-instagram"></span></a>
            </p>
          </div>
          <div class="col-md-4">
            <h3>Connect With Us</h3>
            <p>
              <a href="https://www.google.com/maps/place/'@homechiangmai'/@18.8029473,98.9800778,18z/data=!4m5!3m4!1s0x30da3a9296803ec7:0xada6e29a86472be!8m2!3d18.8031711!4d98.9807778?hl=en">3 Rasemeechan Alle, Sermsuk Rd. <br>Chang Phueak, Chiang Mai, Thailand 50300</a><br>
              Phone:+66(0)53 218 856<br>
              Email:gsa@homechiangmaihotel.com<br>
              Fax:+66(0)53 218 856<br>
Website: www.homechiangmaihotel.com
            </p>
            <form action="#" class="subscribe">
              <div class="form-group">
                <button type="submit"><span class="ion-ios-arrow-thin-right"></span></button>
                <input type="email" class="form-control" placeholder="Enter Email">
              </div>
              
            </form>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-7 text-center">
            &copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | 
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </div>
        </div>
      </div>
    </footer>
    <!-- END footer -->
    
    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.js"></script>
    <script src="js/jquery-migrate-3.0.0.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>

    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>

    <script src="js/main3.js"></script>
    <script src="js/multislider.js"></script>
    <script type="text/javascript" src="slick/slick.min.js"></script>
    <script>
      $(window).on('scroll', function () {
            var pixs = $(document).scrollTop()
            pixs = pixs / 100;
            $("#fullscreen").css({"-webkit-filter": "blur("+(pixs+5)+"px)","filter": "blur("+(pixs+5)+"px)" })  
        });

$(function() {
      $(window).scroll(function() {    
          var scroll = $(window).scrollTop();

          if (scroll >= 100) {
              $(".scrolling").addClass("inactive");
          } else {
              $(".scrolling").removeClass("inactive");
          }
      });
});

$('#secter7').click(function() {
   //optionally remove the 500 (which is time in milliseconds) of the
   //scrolling animation to remove the animation and make it instant
   $.scrollTo($('.third'), 500);
});
    </script>
  </body>
</html>