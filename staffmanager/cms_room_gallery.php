<?php
include("access.php");
if(isset($_GET['delimg'])){
	include("../includes/db.conn.php");
	include("../includes/conf.class.php");
	include("../includes/admin.class.php");
	$bsiAdminMain->deletegallerycmsroom();
	header("location:cms_room_gallery.php");
	exit;
}
include("../includes/admin.class.php");
include("header.php");
?>
<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen">
<link rel="stylesheet" type="text/css" href="css/gallery.css" media="screen">
<div id="container-inside"> <span style="font-size:16px; font-weight:bold"> Small </span>
  <input type="button" value="<?php echo ADD_PHOTO_TEXT; ?>" onClick="window.location.href='add_cms_gallery_room.php?type=1'" style="background: #EFEFEF; float:right"/>
  <hr />
  <p id="set-image-loading" class="hide"><?php echo trans('SETTING_LOADING') ?>...</p>
  <div class="indent gallery" id="gallery1"></div>
</div>
<div id="container-inside"> <span style="font-size:16px; font-weight:bold"> Medium </span>
  <input type="button" value="<?php echo ADD_PHOTO_TEXT; ?>" onClick="window.location.href='add_cms_gallery_room.php?type=2'" style="background: #EFEFEF; float:right"/>
  <hr />
  <p id="set-image-loading" class="hide"><?php echo trans('SETTING_LOADING') ?>...</p>
  <div class="indent gallery" id="gallery2"></div>
</div>
<div id="container-inside"> <span style="font-size:16px; font-weight:bold"> Large </span>
  <input type="button" value="<?php echo ADD_PHOTO_TEXT; ?>" onClick="window.location.href='add_cms_gallery_room.php?type=3'" style="background: #EFEFEF; float:right"/>
  <hr />
  <p id="set-image-loading" class="hide"><?php echo trans('SETTING_LOADING') ?>...</p>
  <div class="indent gallery" id="gallery3"></div>
</div>
<div id="container-inside"> <span style="font-size:16px; font-weight:bold"> ALMOST Large </span>
  <input type="button" value="<?php echo ADD_PHOTO_TEXT; ?>" onClick="window.location.href='add_cms_gallery_room.php?type=4'" style="background: #EFEFEF; float:right"/>
  <hr />
  <p id="set-image-loading" class="hide"><?php echo trans('SETTING_LOADING') ?>...</p>
  <div class="indent gallery" id="gallery4"></div>
</div>
<div id="container-inside"> <span style="font-size:16px; font-weight:bold"> EXTRA Large </span>
  <input type="button" value="<?php echo ADD_PHOTO_TEXT; ?>" onClick="window.location.href='add_cms_gallery_room.php?type=5'" style="background: #EFEFEF; float:right"/>
  <hr />
  <p id="set-image-loading" class="hide"><?php echo trans('SETTING_LOADING') ?>...</p>
  <div class="indent gallery" id="gallery5"></div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#hotelid').change(function(){
		if($('#hotelid').val() != "0"){
			getImage1();
			getImage2();
			getImage3();
			getImage4();
			getImage5();
		}if($('#hotelid').val() == "0"){
			$('#gallery').html('<?php echo PLEASE_SELECT_A_ROOM_TYPE_ALERT; ?>');
		}
	});

	if($('#hotelid').val() != ""){
		getImage1();
		getImage2();
		getImage3();
		getImage4();
		getImage5();
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////

	function getImage1(){
		var querystr='actioncode=9';
		//alert(querystr)
		$.post("admin_ajax_processor.php", querystr, function(data){
			if(data.errorcode == 0){

				$('#gallery1').html(data.viewcontent);
			}else{
				$('#gallery1').html(data.strmsg);
			}

			$('input[name="show_in_homepage"]').bind('change', function() {
				showShowInHomePage1($(this).val(), $(this).attr('checked') ? 1 : 0);
			})
		}, "json");
	}

	function showShowInHomePage1(picid, value) {
		var loading = $('#set-image-loading');

		loading.removeClass('hide');

		$.post('admin_ajax_processor.php', {
			actioncode: 7,
			picid: picid,
			value: value
		}, function (data) {
			if (!data.errorcode) {
				loading.addClass('hide');
			}
		}, 'json');
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////

	function getImage2(){
		var querystr='actioncode=10';
		//alert(querystr)
		$.post("admin_ajax_processor.php", querystr, function(data){
			if(data.errorcode == 0){

				$('#gallery2').html(data.viewcontent);
			}else{
				$('#gallery2').html(data.strmsg);
			}

			$('input[name="show_in_homepage"]').bind('change', function() {
				showShowInHomePage2($(this).val(), $(this).attr('checked') ? 1 : 0);
			})
		}, "json");
	}

	function showShowInHomePage2(picid, value) {
		var loading = $('#set-image-loading');

		loading.removeClass('hide');

		$.post('admin_ajax_processor.php', {
			actioncode: 7,
			picid: picid,
			value: value
		}, function (data) {
			if (!data.errorcode) {
				loading.addClass('hide');
			}
		}, 'json');
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////

	function getImage3(){
		var querystr='actioncode=11';
		//alert(querystr)
		$.post("admin_ajax_processor.php", querystr, function(data){
			if(data.errorcode == 0){

				$('#gallery3').html(data.viewcontent);
			}else{
				$('#gallery3').html(data.strmsg);
			}

			$('input[name="show_in_homepage"]').bind('change', function() {
				showShowInHomePage3($(this).val(), $(this).attr('checked') ? 1 : 0);
			})
		}, "json");
	}

	function showShowInHomePage3(picid, value) {
		var loading = $('#set-image-loading');

		loading.removeClass('hide');

		$.post('admin_ajax_processor.php', {
			actioncode: 7,
			picid: picid,
			value: value
		}, function (data) {
			if (!data.errorcode) {
				loading.addClass('hide');
			}
		}, 'json');
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////

	function getImage4(){
		var querystr='actioncode=12';
		//alert(querystr)
		$.post("admin_ajax_processor.php", querystr, function(data){
			if(data.errorcode == 0){

				$('#gallery4').html(data.viewcontent);
			}else{
				$('#gallery4').html(data.strmsg);
			}

			$('input[name="show_in_homepage"]').bind('change', function() {
				showShowInHomePage4($(this).val(), $(this).attr('checked') ? 1 : 0);
			})
		}, "json");
	}

	function showShowInHomePage4(picid, value) {
		var loading = $('#set-image-loading');

		loading.removeClass('hide');

		$.post('admin_ajax_processor.php', {
			actioncode: 7,
			picid: picid,
			value: value
		}, function (data) {
			if (!data.errorcode) {
				loading.addClass('hide');
			}
		}, 'json');
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////

	function getImage5(){
		var querystr='actioncode=13';
		//alert(querystr)
		$.post("admin_ajax_processor.php", querystr, function(data){
			if(data.errorcode == 0){

				$('#gallery5').html(data.viewcontent);
			}else{
				$('#gallery5').html(data.strmsg);
			}

			$('input[name="show_in_homepage"]').bind('change', function() {
				showShowInHomePage5($(this).val(), $(this).attr('checked') ? 1 : 0);
			})
		}, "json");
	}

	function showShowInHomePage5(picid, value) {
		var loading = $('#set-image-loading');

		loading.removeClass('hide');

		$.post('admin_ajax_processor.php', {
			actioncode: 7,
			picid: picid,
			value: value
		}, function (data) {
			if (!data.errorcode) {
				loading.addClass('hide');
			}
		}, 'json');
	}
});

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////

function deleteImageCMS1(delimg){
	window.location.href='cms_room_gallery.php?delimg='+delimg;
}

function deleteImageCMS2(delimg){
	window.location.href='cms_room_gallery.php?delimg='+delimg;
}

function deleteImageCMS3(delimg){
	window.location.href='cms_room_gallery.php?delimg='+delimg;
}

function deleteImageCMS4(delimg){
	window.location.href='cms_room_gallery.php?delimg='+delimg;
}

function deleteImageCMS5(delimg){
	window.location.href='cms_room_gallery.php?delimg='+delimg;
}


</script>

<script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.js"></script>
<?php include("footer.php"); ?>
