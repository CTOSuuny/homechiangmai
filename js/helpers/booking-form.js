var BookingForm = function(selector) {
    this.selector = selector || '.booking-form';
}

BookingForm.prototype = {
    constructor: BookingForm,

    init: function() {
        this.bindEvent();
    },

    bindEvent: function() {
        $(document).on('submit', this.selector, this.handleFormSubmit);
    },

    handleFormSubmit: function(e) {
        if (!$(this).find('input[name="check_in"]').val() || !$(this).find('input[name="check_out"]').val()) {
            e.preventDefault()
        }
    }
}
