<?php

class SMTPMail
{
    protected $transport;

    protected $mailer;

    protected $message;

    protected $body;

    protected $sender = [];

    protected $receiver = [];

    protected $subject;

    protected $isSendingEnable;

    protected $host;

    public function __construct()
    {
        $env = new Env();
        $env->getEnvFile();

        $this->isSendingEnable = $env->getValue('ENABLE_EMAIL_SENDING');
        $this->host = $env->getValue('SMTPMAIL_HOST');

        $this->transport = (new Swift_SmtpTransport($env->getValue('SMTPMAIL_HOST'), $env->getValue('SMTPMAIL_PORT'), $env->getValue('SMTPMAIL_ENCRYPTION')))
            ->setUsername($env->getValue('SMTPMAIL_USERNAME'))
            ->setPassword($env->getValue('SMTPMAIL_PASSWORD'));
    }

    /**
     * Set smtp setting config
     *
     * @param array $config config
     *
     * @return STMPMail
     */
    public function setSystemConfig($config)
    {
        if (empty($config['smtp_host'])) {
            return $this;
        }

        $this->host = $config['smtp_host'];

        $this->transport = (new Swift_SmtpTransport($config['smtp_host'], $config['smtp_port'], $config['smtp_encryption']))
            ->setUsername($config['smtp_username'])
            ->setPassword($config['smtp_password']);

        return $this;
    }

    /**
     * Set email subject
     *
     * @param string $subject email subject
     *
     * @return STMPMail
     */
    public function setSubject(string $subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Set email body
     *
     * @param mixed $body email body
     *
     * @return STMPMail
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Set email sender
     *
     * @param  array  $sender senders
     *
     * @return SMTPMail
     */
    public function sender(array $sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Set receiver
     *
     * @param  array  $receiver receivers
     *
     * @return STMPMail
     */
    public function receiver(array $receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }
    /**
     * Send email
     *
     * @return mixed
     */
    public function send()
    {
        if (!$this->isSendingEnable || empty($this->host)) {
            return true;
        }

        $this->mailer = new Swift_Mailer($this->transport);

        $this->message = (new Swift_Message($this->subject))
            ->setFrom($this->sender)
            ->setTo($this->receiver)
            ->setContentType('text/html')
            ->setBody($this->body);

        return $this->mailer->send($this->message);
    }

}
