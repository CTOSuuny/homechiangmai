<nav class="navbar navbar-default my-navbar hidden-md hidden-sm">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div id="logo-wraper">
            <a href="/">
                <img src="<?php echo asset('images/content-villa-logo.png') ?>" id="img-logo">
            </a>
        </div>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/"><?php trans('HOME_TEXT') ?></a></li>
            <li><a href="/page/about_hotel"><?php trans('ABOUT_US') ?></a></li>
            <li><a href="/page/rooms"><?php trans('ROOMS') ?></a></li>
            <li><a href="/page/food-and-drink"><?php trans('FOOD_AND_DRINKS') ?></a></li>
            <li><a href="/page/gallery"><?php trans('GALLERY_TEXT') ?></a></li>
            <li><a href="/page/contact_us"><?php trans('CONTACT_US') ?></a></li>
            <li><a href="/page/our-video"><?php trans('OUR_VIDEO') ?></a></li>                    
            <li class="nav-img">
                <a href="<?php echo $disableSelectLanguage ? 'javascript: alert(\'' . trans('CANNOT_SWITCH_LANGUAGE_DURING_BOOKING', true) . '\');' : '?lang=en' ?>"><img src="<?php echo asset('images/en.png') ?>"></a>
                <a href="<?php echo $disableSelectLanguage ? 'javascript: alert(\'' . trans('CANNOT_SWITCH_LANGUAGE_DURING_BOOKING', true) . '\');' : '?lang=zh-cn' ?>"><img src="<?php echo asset('images/cn.png') ?>"></a>
                <a href="<?php echo $disableSelectLanguage ? 'javascript: alert(\'' . trans('CANNOT_SWITCH_LANGUAGE_DURING_BOOKING', true) . '\');' : '?lang=th' ?>"><img src="<?php echo asset('images/th.png') ?>"></a>
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<nav class="navbar navbar-default my-navbar visible-md visible-sm">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div id="logo-wraper">
            <a href="/">
                <img src="<?php echo asset('images/content-villa-logo.png') ?>" id="img-logo">
            </a>
        </div>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right" style="margin-top:0;">
            <li><a href="/"><?php trans('HOME_TEXT') ?></a></li>
            <li><a href="/page/about_hotel"><?php trans('ABOUT_US') ?></a></li>
            <li><a href="/page/rooms"><?php trans('ROOMS') ?></a></li>
            <li><a href="/page/food-and-drink"><?php trans('FOOD_AND_DRINKS') ?></a></li>
            <br>
            <li><a href="/page/gallery"><?php trans('GALLERY_TEXT') ?></a></li>
            <li><a href="/page/contact_us"><?php trans('CONTACT_US') ?></a></li>
            <li><a href="/page/our-video"><?php trans('OUR_VIDEO') ?></a></li>          
            <li class="nav-img">
                <a href="<?php echo $disableSelectLanguage ? 'javascript: alert(\'' . trans('CANNOT_SWITCH_LANGUAGE_DURING_BOOKING', true) . '\');' : '?lang=en' ?>"><img src="<?php echo asset('images/en.png') ?>"></a>
                <a href="<?php echo $disableSelectLanguage ? 'javascript: alert(\'' . trans('CANNOT_SWITCH_LANGUAGE_DURING_BOOKING', true) . '\');' : '?lang=zh-cn' ?>"><img src="<?php echo asset('images/cn.png') ?>"></a>
                <a href="<?php echo $disableSelectLanguage ? 'javascript: alert(\'' . trans('CANNOT_SWITCH_LANGUAGE_DURING_BOOKING', true) . '\');' : '?lang=th' ?>"><img src="<?php echo asset('images/th.png') ?>"></a>
            </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
