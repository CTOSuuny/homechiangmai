
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $bsiCore->config['conf_hotel_name']?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Playfair+Display" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo asset('css/datepicker.css" type="text/css') ?>" media="screen"/>
<link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider.css') ?>" />
<!-- <link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider-customize.css') ?>"> -->
<link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-customize.css') ?>">
</head>
<body>
    <div class="container-fluid">
    <?php
        include("modules/inner-modules.php")
    ?>

            <div class="row show-room-dialog" >
                <div id="body-div">
                <h1><?php echo $bsiCore->config['conf_hotel_name'] ?></h1>
                <?php
                    $active = "payment";
                    include("breadcrumb.php");
                ?>
                <br><br>
                <div class="wrapper">
                    <div class="htitel">
                        <h2 class="fl" style="border:0; margin:0;"><?php echo CC_DETAILS?> (Authorize.Net)</h2>
                    </div>
                    <br>
                    <!-- start of search row -->
                    <div class="container-fluid" style="margin:0; padding:0;">
                        <div class="row-fluid" style="border: 1px solid #dfc27c; padding: 1% 0">
                            <div class="span12">
                                <form class="form-horizontal" method="post" action="<?php echo $post_url?>" id="form1" style="width: 95%; margin: 0 2.5%"><?php echo $hidden_fields?>
                                    <div class="form-group">
                                        <label class="control-label col-sm-offset-2 col-sm-2" for="ea"><?php echo FIRST_NAME_TEXT; ?>:</label>
                                        <div class="controls col-sm-5">
                                           <input type="text" name="x_first_name" id="x_first_name" class="input-large required form-control">
                                           <div class="errorTxt" align="left"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-offset-2 col-sm-2" for="ea"><?php echo LAST_NAME_TEXT; ?>:</label>
                                        <div class="controls col-sm-5">
                                           <input type="text" name="x_last_name" id="x_last_name" class="input-large required form-control">
                                           <div class="errorTxt" align="left"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-offset-2 col-sm-2" for="ea"><?php echo CC_NUMBER; ?>:</label>
                                        <div class="controls col-sm-5">
                                           <input type="text" name="x_card_num" id="x_card_num" maxlength="16" class="input-large digits form-control">
                                           <div class="errorTxt" align="left"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-offset-2 col-sm-2" for="ea"><?php echo CC_EXPIRY; ?> (mm/yy):</label>
                                        <div class="controls col-sm-2">
                                           <input type="text" name="x_exp_date" id="x_exp_date" maxlength="5" class="input-mini required form-control">
                                           <div class="errorTxt" align="left"></div>
                                        </div>
                                        <label class="control-label col-sm-1" for="ea">CCV/CCV2:</label>
                                        <div class="controls col-sm-1">
                                           <input type="text" name="x_card_code" id="x_card_code" maxlength="4" class="input-mini required number form-control">
                                           <div class="errorTxt" align="left"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-offset-2 col-sm-2" for="ea"><?php echo CC_AMOUNT?> :</label>
                                        <div class="controls col-sm-5" align="left">
                                           <strong><?php echo  $bsiCore->currency_symbol().number_format($amount,2)?></strong>
                                           <label class="checkbox col-sm-12">
                                                <input type="checkbox" name="tos" id="tos" value="" class="required">
                                                <?php echo CC_TOS1 ?> <?php echo $bsiCore->currency_symbol().number_format($amount,2) ?> <?php echo CC_TOS3 ?>
                                                <div class="errorTxt" align="left"></div>
                                           </label>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of search row -->
                </div>
                <div style="margin-bottom: 80px;">
                    <div class="col-sm-6">
                        <button id="registerButton" class="btn btn-default btn-book" type="button" onClick="window.location.href =\'booking-failure.php?error_code=26\'" ><?php echo BTN_CANCEL?></button>
                    </div>
                    <div class="col-sm-6">
                        <button id="registerButton" class="btn btn-default btn-book" type="submit" class="conti" ><?php echo CC_SUBMIT?></button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <?php
            include("modules/footer.php");
        ?>
    </div>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo asset('js/modernizr-2.6.2.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('js/jquery-zoomslider.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('js/jquery.validate.js') ?>"></script>

<script type="text/javascript">
    $(window).load(function() {

    });
    $(document).ready(function() {
        $("#form1").validate({
            errorPlacement: function(error, element) {
                error.appendTo( element.nextAll( ".errorTxt" ) );
            }
        });
    });
</script>
</body>
</html>
