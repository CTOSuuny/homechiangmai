var Scroll = function(){
    this.el = {
        btn: "#btn-to-top",
    }
}

Scroll.prototype = {
    constructor: Scroll,

    init: function(){
        this.initScroll();
    },

    initScroll: function(){
        $("#btn-to-top").hide();
        var element = this.el;
        $( window ).scroll(function(){
            if( $(this).scrollTop() > 300 ){
                $(element.btn).fadeIn(500);
            }else{
                $(element.btn).fadeOut(500);
            }
        });
        $(element.btn).click(function(){
            $('html').animate({ scrollTop: 0 }, 500);
        });
    }
}

$(document).ready(function(){
    var scroll = new Scroll;
    scroll.init();
});
