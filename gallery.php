<?php
  include("language_set.php");
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Home chiang mai Hotel</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Rubik:300,400,700" rel="stylesheet">
    <link rel="icon" href="favicon.ico" sizes="16x16 32x32" type="image/png">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <link rel="stylesheet" href="css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
    <!-- Theme Style -->
    <link rel="stylesheet" href="css/style.css">

    <style>
            body::-webkit-scrollbar { 
                display: none; 
            } 
    @media (max-width: 767px) {
      .gallery{
        font-size: 70px !important;
      }
    }
      .inactive{
            color: #FFFFFF;
          text-align: center;

          -webkit-transition: opacity 0.5s ease-in-out;
          -moz-transition: opacity 0.5s ease-in-out;
          -ms-transition: opacity 0.5s ease-in-out;
          -o-transition: opacity 0.5s ease-in-out;
           opacity: 0;
      }
      /* Make the image fully responsive */
      .carousel-inner img {
          width: 100%;
          height: 100%;
      }
      .disable-links {
          pointer-events: none;
      }
      #mixedSlider {
        position: relative;
      }
      #mixedSlider .MS-content {
        white-space: nowrap;
        overflow: hidden;
        margin: 0 5%;
      }
      #mixedSlider .MS-content .item {
        display: inline-block;
        width: 50%;
        position: relative;
        vertical-align: top;
        overflow: hidden;
        height: 100%;
        white-space: normal;
        padding: 0 10px;
      }
      /*@media (max-width: 991px) {
        #mixedSlider .MS-content .item {
          width: 50%;
        }
      }*/
      @media (max-width: 767px) {
        #mixedSlider .MS-content .item {
          width: 100%;
        }
      }
      #mixedSlider .MS-controls button {
        position: absolute;
        border: none;
        background-color: transparent;
        outline: 0;
        font-size: 50px;
        top: 95px;
        color: rgba(0, 0, 0, 0.4);
        transition: 0.15s linear;
      }
      #mixedSlider .MS-controls button:hover {
        color: rgba(0, 0, 0, 0.8);
      }
      @media (max-width: 992px) {
        #mixedSlider .MS-controls button {
          font-size: 30px;
        }
      }
      @media (max-width: 767px) {
        #mixedSlider .MS-controls button {
          font-size: 20px;
        }
      }
      #mixedSlider .MS-controls .MS-left {
        left: 0px;
      }
      @media (max-width: 767px) {
        #mixedSlider .MS-controls .MS-left {
          left: -10px;
        }
      }
      #mixedSlider .MS-controls .MS-right {
        right: 0px;
      }
      @media (max-width: 767px) {
        #mixedSlider .MS-controls .MS-right {
          right: -10px;
        }
      }
      .carousel {
        background: #EEE;
      }

      .carousel-cell {
        width: 66%;
        height: 200px;
        margin-right: 10px;
        background: #8C8;
        border-radius: 5px;
        counter-increment: gallery-cell;
      }

      /* cell number */
      .carousel-cell:before {
        display: block;
        text-align: center;
        content: counter(gallery-cell);
        line-height: 200px;
        font-size: 80px;
        color: white;
      }
    </style>
  </head>
  <body>

  <!-- Sidebar -->
  <div class="w3-sidebar w3-bar-block w3-animate-left" style="display:none;z-index: 4;" id="mySidebar">
    <a href="#" class="w3-bar-item w3-button" style="margin-top: 100px;"></a>
    <a href="index.php" class="w3-bar-item w3-button"><?php echo $manu["home"]; ?></a>
    <a class="w3-bar-item w3-button" href="abouts.php"><?php echo $manu["about"]; ?></a>
    <a class="w3-bar-item w3-button" href="room.php"><?php echo $manu["room"]; ?></a>
    <a class="w3-bar-item w3-button" href="gallery.php"><?php echo $manu["gallery"]; ?></a>
    <a class="w3-bar-item w3-button" href="bar.php"><?php echo $manu["bar"]; ?></a>
    
    <a class="w3-bar-item w3-button" href="workwithus.php"><?php echo $manu["work"]; ?></a>
    <a class="w3-bar-item w3-button" href="calendar.php"><?php echo $manu["event"]; ?></a>
    <a class="w3-bar-item w3-button" href="contacts.php"><?php echo $manu["contact"]; ?></a>    
    <div class="w3-row" style="width: 30%;margin-left: 5%;padding-top: 50px;">
      <div class="w3-third">
        <div><img src="/images/thai.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=thai'"></div>
      </div>
      <div class="w3-third">
        <div><img src="/images/engv.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=eng'"></div>
      </div>
      <div class="w3-third">
        <div><img src="/images/chinav.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=china'"></div>
      </div>
    </div>


    <!-- <a class="w3-bar-item w3-button" href=""><span>Book Now</span></a> -->
  </div>
    
    <header role="banner" style="position: fixed;">
     
      <nav class="navbar navbar-expand-md navbar-dark bg-light">
        <div class="container">
          <a href="index.php"><img src="images/logo.jpg" class="logo" id="logo"></a>
          <div>
            
            <!-- <ul class="navbar-nav ml-auto pl-lg-5 pl-0 activate-nav">
              <li class="nav-item">
                <a class="nav-link active" href="index.html">Home</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="rooms.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Rooms</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="rooms.html">Room Videos</a>
                  <a class="dropdown-item" href="rooms.html">Presidential Room</a>
                  <a class="dropdown-item" href="rooms.html">Luxury Room</a>
                  <a class="dropdown-item" href="rooms.html">Deluxe Room</a>
                </div>

              </li>
              <li class="nav-item">
                <a class="nav-link" href="">Event</a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="">Contact</a>
              </li>

               <li class="nav-item cta">
                <a class="nav-link" href=""><span>Book Now</span></a>
              </li>
            </ul> -->


            <!-- <a id="trigger" ></a> -->
            
            <!-- <button class="w3-button w3-xlarge" id="trigger" onclick="w3_open()"></button> -->
            <div class="container1" onclick="myFunction(this)">
              <div class="bar1" id="bar1"></div>
              <div class="bar3" id="bar3"></div>
            </div>
          </div>
        </div>
        <div class="book">
          <a id="book" href="booking.php"><?php echo $manu["book"]; ?></a>
        </div>
      </nav>
    </header>
    <!-- END header -->

    <section class="overlay" data-stellar-background-ratio="0.5" style="overflow: hidden; height: 100vh !important">
      <div class="fullscreen-bg">
              <!-- <div class="first" id="section07" style="text-align: center;
    top: 80vh;
    position: absolute;
    width: 100%;">
                <div class="text">
                  <div class="scrolling" id="secter7">
                    <a href="#section08"><span></span><span></span><span></span><p style="font-size: 50px;
    color: white;margin-top: 30px;">Please scroll down</p></a>
                  </div>
                </div>
              </div> -->
                <!-- <video loop muted autoplay class="fullscreen-bg__video" style="  
                      top: 50%;
                      left: 50%;
                      min-width: 100%;
                      min-height: 100%;
                      width: auto;
                      height: auto;
                      z-index: -100;
                      transform: translate(-50%, -50%);">
                    <source src="video/background.mp4" type="video/mp4">
                </video> -->

                <div id="maximage" style=" position: inherit;  
                /*filter: blur(4px);
                -webkit-filter: blur(4px);*/
                ">
                  <?php
                    define("MYSQL_SERVER", "127.0.0.1");
                    define("MYSQL_USER", "root");
                    define("MYSQL_PASSWORD", "luxury@123");
                    define("MYSQL_DATABASE", "content_villa_db");
                    define('MYSQL_PORT', "3306");

                    $mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
                    if (mysqli_connect_errno()) {  printf("Connect failed: %s
                    ", mysqli_connect_error());  exit(); }
                    // echo $id;

                    $myquery = $mysqli->query("select * from bsi_cms_gallery ");
                    $today_event = array();
                    $chk = 0;
                    // $row    = $result->fetch_assoc();

                    while ($row = $myquery->fetch_assoc()) {
                  ?>

                    <img src="gallery/cms/<?php echo $row["img_path"]; ?>" alt="" />

                  <?php
                    }
                  ?>
                </div>

                <div class="arrow_left">
                    <a href="" id="arrow_left"><img src="images/arrow_left.png" alt="Slide Left" /></a>
                </div>
                <!-- <div style="    top: 50%;
    position: absolute;
    /* right: 0; */
    width: 20px;
    z-index: 3;
    /* color: white; */
    width: 100%;
    text-align: center;">
                    <p class="gallery" style="
                    color: white;
                    font-size: 150px;
                    font-family: FarnhamDisplayLightRegular;
                    ">Gallery</p>
                </div> -->
                <div class="arrow_right">
                    <a href="" id="arrow_right"><img src="images/arrow_right.png" alt="Slide Right" /></a>
                </div>

<!--                 <div class="container">
                  <div class="row align-items-center site-hero-inner justify-content-center">
                    <div class="col-md-12 text-center">

                      <div class="mb-5 element-animate">
                        <h1>Gallery</h1>
                        <p>Discover our world's #1 lifestyle hotel</p>
                        <p><a href="" class="btn btn-primary" style="background-color: #fff;color: #000;border: inherit;">Book Now</a></p>
                      </div>

                    </div>
                  </div>
                </div> -->
            </div>
    </section>
    <!-- END section -->

<!-- 
    <section class="site-hero" data-stellar-background-ratio="0.5" style="margin: 100px 0;overflow: hidden;height: inherit;min-height: inherit;">
      <div class="fullscreen-bg"> -->
                <!-- <video loop muted autoplay class="fullscreen-bg__video" style="  
                      top: 50%;
                      left: 50%;
                      min-width: 100%;
                      min-height: 100%;
                      width: auto;
                      height: auto;
                      z-index: -100;
                      transform: translate(-50%, -50%);">
                    <source src="video/background.mp4" type="video/mp4">
                </video> -->

                <!-- Flickity HTML init -->
                <!-- <div class="carousel" data-flickity='{ "autoPlay": true }'>
                  <img src="images/Hotel/IMG_3125-Edit.jpg" style="padding-left: 20px;padding-right: 20px;height: 500px;">
                  <img src="images/Hotel/IMG_3208.jpg" style="padding-left: 20px;padding-right: 20px;height: 500px;">
                  <img src="images/Hotel/IMG_3289.jpg" style="padding-left: 20px;padding-right: 20px;height: 500px;">
                  <img src="images/Hotel/IMG_3297.jpg" style="padding-left: 20px;padding-right: 20px;height: 500px;">
                  <img src="images/Hotel/IMG_3315-Edit.jpg" style="padding-left: 20px;padding-right: 20px;height: 500px;">
                  <img src="images/Hotel/IMG_3401.jpg" style="padding-left: 20px;padding-right: 20px;height: 500px;">
                  <img src="images/Hotel/IMG_3186.jpg" style="padding-left: 20px;padding-right: 20px;height: 500px;">
                  <img src="images/Hotel/IMG_3291.jpg" style="padding-left: 20px;padding-right: 20px;height: 500px;">
                  <img src="images/Hotel/IMG_3305.jpg" style="padding-left: 20px;padding-right: 20px;height: 500px;">
                  <img src="images/Hotel/IMG_3327.jpg" style="padding-left: 20px;padding-right: 20px;height: 500px;">
                  <img src="images/Hotel/IMG_3402.jpg" style="padding-left: 20px;padding-right: 20px;height: 500px;">
                  <img src="images/Hotel/IMG_map.png" style="padding-left: 20px;padding-right: 20px;height: 500px;"> -->
                  <!-- <img src="images/img_5.jpg" style="padding: 20px;">
                </div>

            </div> -->
      <!-- <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
          <div class="col-md-12 text-center">

            <div class="mb-5 element-animate">
              <h1>Welcome To Home Chiang Mai</h1>
              <p>Discover our world's #1 lifestyle hotel</p>
              <p><a href="" class="btn btn-primary" style="background-color: #fff;color: #000;border: inherit;">Book Now</a></p>
            </div>

          </div>
        </div>
      </div> -->
    <!-- </section> -->
    <!-- END section -->

   
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
                    <h3>Reservations</h3>
                    <p class="lead"><a href="tel://+ 66 53 218 856">+ 66 53 218 856</a></p>
          </div>
          <div class="col-md-4">
            <h3>Connect With Us</h3>
            <p>We are socialized. Follow us</p>
            <p>
              <a href="https://www.facebook.com/HomeChiangMaiHotel/" class="pl-0 p-3"><span class="fa fa-facebook"></span></a>
              <a href="https://www.instagram.com/homechiangmaihotel/" class="p-3"><span class="fa fa-instagram"></span></a>
            </p>
          </div>
          <div class="col-md-4">
            <h3>Connect With Us</h3>
            <p>
              <a href="https://www.google.com/maps/place/'@homechiangmai'/@18.8029473,98.9800778,18z/data=!4m5!3m4!1s0x30da3a9296803ec7:0xada6e29a86472be!8m2!3d18.8031711!4d98.9807778?hl=en">3 Rasemeechan Alle, Sermsuk Rd. <br>Chang Phueak, Chiang Mai, Thailand 50300</a><br>
              Phone:+66(0)53 218 856<br>
              Email:gsa@homechiangmaihotel.com<br>
              Fax:+66(0)53 218 856<br>
Website: www.homechiangmaihotel.com
            </p>
            <form action="#" class="subscribe">
              <div class="form-group">
                <button type="submit"><span class="ion-ios-arrow-thin-right"></span></button>
                <input type="email" class="form-control" placeholder="Enter Email">
              </div>
              
            </form>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-7 text-center">
            &copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | 
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </div>
        </div>
      </div>
    </footer>
    <!-- END footer -->
    
    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
    <!-- <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js'></script> -->
    <script src="js/jquery-migrate-3.0.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-browser/0.1.0/jquery.browser.js"></script>
    <!-- <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script> -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>

    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>

    <script src="js/main3.js"></script>
    <script src="js/multislider.js"></script>
    <script type="text/javascript" src="slick/slick.min.js"></script>
    <script src="js/jquery.cycle.all.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.maximage.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
    <script>
      $('#mixedSlider').multislider({
        duration: 750,
        interval: 3333000
      });

      $('.carousel').flickity({
        // options
        cellAlign: 'left',
        contain: true
      });

      $(function() {
          $(window).scroll(function() {    
              var scroll = $(window).scrollTop();

              if (scroll >= 100) {
                  $(".scrolling").addClass("inactive");
              } else {
                  $(".scrolling").removeClass("inactive");
              }
          });
    });

    </script>
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#maximage').maximage({
                cycleOptions: {
                    fx: 'fade',
                    speed: 1000, // Has to match the speed for CSS transitions in jQuery.maximage.css (lines 30 - 33)
                    timeout: 100,
                    prev: '#arrow_left',
                    next: '#arrow_right',
                    pause: 1,
                    before: function(last, current) {
                        if (!$.browser.msie) {
                            // Start HTML5 video when you arrive
                            if ($(current).find('video').length > 0) $(current).find('video')[0].play();
                        }
                    },
                    after: function(last, current) {
                        if (!$.browser.msie) {
                            // Pauses HTML5 video when you leave it
                            if ($(last).find('video').length > 0) $(last).find('video')[0].pause();
                        }
                    }
                },
                onFirstImageLoaded: function() {
                    jQuery('#cycle-loader').hide();
                    jQuery('#maximage').fadeIn('fast');
                }
            });

            // Helper function to Fill and Center the HTML5 Video
            jQuery('video,object').maximage('maxcover');

            // To show it is dynamic html text
            jQuery('.in-slide-content').delay(1200).fadeIn();
        });
      </script>

    
  </body>
</html>