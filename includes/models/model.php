<?php

class Model
{
    public $table;

    public $db;

    public function __construct()
    {
        $this->db = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
    }

    /**
     * Check exists row in table
     *
     * @param string $field  field
     * @param mixed $value  value
     * @param  array  $ignore ignore field
     *
     * @return bool
     */
    public function exists(string $field, $value, array $ignore = [])
    {
        $stmt = "SELECT {$field} FROM {$this->table} WHERE $field = '$value'";

        if (!empty($ignore)) {
            $stmt = $stmt . " AND {$ignore['field']} <> {$ignore['value']}";
        }

        $query = $this->db->query($stmt);

        return !empty($query->fetch_assoc());
    }

    /**
     * Filter number
     *
     * @param mixed $value value
     *
     * @return mixed
     */
    public function filterNumber($value)
    {
        return preg_replace(['/\D/'], '', $value);
    }

    public function __destruct()
    {
        $this->db->close();
    }
}
