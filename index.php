<?php
  include("language_set.php");
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Home chiang mai Hotel</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Rubik:300,400,700" rel="stylesheet">
    <link rel="icon" href="favicon.ico" sizes="16x16 32x32" type="image/png">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
    <link rel="stylesheet" href="css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
    <!-- Theme Style -->
    <link rel="stylesheet" href="css/style.css">

    <style>

      /* Make the image fully responsive */
      .carousel-inner img {
          width: 100%;
          height: 100%;
      }
      .disable-links {
          pointer-events: none;
      }
      #mixedSlider {
        position: relative;
      }
      #mixedSlider .MS-content {
        white-space: nowrap;
        overflow: hidden;
        margin: 0 5%;
      }
      #mixedSlider .MS-content .item {
        display: inline-block;
        width: 50%;
        position: relative;
        vertical-align: top;
        overflow: hidden;
        height: 100%;
        white-space: normal;
        padding: 0 10px;
      }
      /*@media (max-width: 991px) {
        #mixedSlider .MS-content .item {
          width: 50%;
        }
      }*/
      @media (max-width: 767px) {
        #mixedSlider .MS-content .item {
          width: 100%;
        }
      }
      #mixedSlider .MS-controls button {
        position: absolute;
        border: none;
        background-color: transparent;
        outline: 0;
        font-size: 50px;
        top: 95px;
        color: rgba(0, 0, 0, 0.4);
        transition: 0.15s linear;
      }
      #mixedSlider .MS-controls button:hover {
        color: rgba(0, 0, 0, 0.8);
      }
      @media (max-width: 992px) {
        #mixedSlider .MS-controls button {
          font-size: 30px;
        }
      }
      @media (max-width: 767px) {
        #mixedSlider .MS-controls button {
          font-size: 20px;
        }
      }
      #mixedSlider .MS-controls .MS-left {
        left: 0px;
      }
      @media (max-width: 767px) {
        #mixedSlider .MS-controls .MS-left {
          left: -10px;
        }
      }
      #mixedSlider .MS-controls .MS-right {
        right: 0px;
      }
      @media (max-width: 767px) {
        #mixedSlider .MS-controls .MS-right {
          right: -10px;
        }
      }
    </style>
  </head>
  <body>

  <!-- Sidebar -->
  <div class="w3-sidebar w3-bar-block w3-animate-left" style="display:none;z-index: 4;" id="mySidebar">
    <a href="#" class="w3-bar-item w3-button" style="margin-top: 100px;"></a>
    <a href="index.php" class="w3-bar-item w3-button"><?php echo $manu["home"]; ?></a>
    <a class="w3-bar-item w3-button" href="abouts.php"><?php echo $manu["about"]; ?></a>
    <a class="w3-bar-item w3-button" href="room.php"><?php echo $manu["room"]; ?></a>
    <a class="w3-bar-item w3-button" href="gallery.php"><?php echo $manu["gallery"]; ?></a>
    <a class="w3-bar-item w3-button" href="bar.php"><?php echo $manu["bar"]; ?></a>
    
    <a class="w3-bar-item w3-button" href="workwithus.php"><?php echo $manu["work"]; ?></a>
    <a class="w3-bar-item w3-button" href="calendar.php"><?php echo $manu["event"]; ?></a>
    <a class="w3-bar-item w3-button" href="contacts.php"><?php echo $manu["contact"]; ?></a>    
    <div class="w3-row" style="width: 30%;margin-left: 5%;padding-top: 50px;">
      <div class="w3-third">
        <div><img src="/images/thai.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=thai'"></div>
      </div>
      <div class="w3-third">
        <div><img src="/images/engv.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=eng'"></div>
      </div>
      <div class="w3-third">
        <div><img src="/images/chinav.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=china'"></div>
      </div>
    </div>


    <!-- <a class="w3-bar-item w3-button" href=""><span>Book Now</span></a> -->
  </div>
    
    <header role="banner" style="position: fixed;">
     
      <nav class="navbar navbar-expand-md navbar-dark bg-light">
        <div class="container">
          <a href="index.php"><img src="images/logo.jpg" class="logo" id="logo"></a>
          <div>
            
            <!-- <ul class="navbar-nav ml-auto pl-lg-5 pl-0 activate-nav">
              <li class="nav-item">
                <a class="nav-link active" href="index.html">Home</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="rooms.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Rooms</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="rooms.html">Room Videos</a>
                  <a class="dropdown-item" href="rooms.html">Presidential Room</a>
                  <a class="dropdown-item" href="rooms.html">Luxury Room</a>
                  <a class="dropdown-item" href="rooms.html">Deluxe Room</a>
                </div>

              </li>
              <li class="nav-item">
                <a class="nav-link" href="">Event</a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="">Contact</a>
              </li>

               <li class="nav-item cta">
                <a class="nav-link" href=""><span>Book Now</span></a>
              </li>
            </ul> -->


            <!-- <a id="trigger" ></a> -->
            
            <!-- <button class="w3-button w3-xlarge" id="trigger" onclick="w3_open()"></button> -->
            <div class="container1" onclick="myFunction(this)">
              <div class="bar1" id="bar1"></div>
              <div class="bar3" id="bar3"></div>
            </div>
          </div>
        </div>
        <div class="book">
          <a id="book" href="booking.php"><?php echo $manu["book"]; ?></a>
        </div>
      </nav>
    </header>
    <!-- END header -->

    <section class="site-hero overlay" data-stellar-background-ratio="0.5" style="overflow: hidden; height: 100vh !important">
      <div class="fullscreen-bg">
                <!-- <video loop muted autoplay class="fullscreen-bg__video" style="  
                      top: 50%;
                      left: 50%;
                      min-width: 100%;
                      min-height: 100%;
                      width: auto;
                      height: auto;
                      z-index: -100;
                      transform: translate(-50%, -50%);">
                    <source src="video/background.mp4" type="video/mp4">
                </video> -->

                <div id="maximage">
                    <!-- <img src="images/bg1.jpg" alt="" /> -->
                    <!-- <img src="images/bg2.jpg" alt="" /> -->
                    <!-- <img src="images/bg3.jpg" alt="" /> -->
                    
                    <img src="images/Hotel/IMG_3125-Edit.jpg" alt="" />
                    <img src="images/Hotel/IMG_3289.jpg" alt="" />
                    <img src="images/Hotel/IMG_3297.jpg" alt="" />
                    <img src="images/Hotel/IMG_3315-Edit.jpg" alt="" />
                    <img src="images/Hotel/IMG_3401.jpg" alt="" />
                    <img src="images/Hotel/IMG_3402.jpg" alt="" />
                </div>

                <!-- <div class="arrow_left">
                    <a href="" id="arrow_left"><img src="images/arrow_left.png" alt="Slide Left" /></a>
                </div>
                <div class="arrow_right">
                    <a href="" id="arrow_right"><img src="images/arrow_right.png" alt="Slide Right" /></a>
                </div> -->

            </div>
      <!-- <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
          <div class="col-md-12 text-center">

            <div class="mb-5 element-animate">
              <h1>Welcome To Home Chiang Mai</h1>
              <p>Discover our world's #1 lifestyle hotel</p>
              <p><a href="" class="btn btn-primary" style="background-color: #fff;color: #000;border: inherit;">Book Now</a></p>
            </div>

          </div>
        </div>
      </div> -->
    </section>
    <!-- END section -->

    <section class="site-section">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-4">
            <div class="heading-wrap text-center element-animate">
              <h4 class="sub-heading"><?php echo $lang["header_colume1"]; ?></h4>
              <h2 class="heading"><?php echo $lang["subheader_colume1"]; ?></h2>
              <p class="mb-5" class="content-index" ><?php echo $lang["content_colume1"]; ?></p>
              <p><a href="abouts.php" class="btn btn-primary btn-sm"><?php echo $lang["button_1"]; ?></a></p>
            </div>
          </div>
          <div class="col-md-1"></div>
          <div class="col-md-7">
            <img src="images/Homepage/IMG_Homeabout.png" alt="Image placeholder" class="img-md-fluid" style="width: 100%">
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

    <section class="site-section">
      <!-- <div class="container">
        <div class="row mb-5">
          <div class="col-md-12 heading-wrap text-center">
            <h4 class="sub-heading">Our Luxury Rooms</h4>
              <h2 class="heading">Our Bedrooms</h2>
          </div>
        </div>
        <div class="row ">
          <div class="col-md-7">
            <div class="media d-block room mb-0">
              <figure>
                <img src="images/img_1.jpg" alt="Generic placeholder image" class="img-fluid">
                <div class="overlap-text">
                  <span>
                    Our Bedrooms
                    <span class="ion-ios-star"></span>
                    <span class="ion-ios-star"></span>
                    <span class="ion-ios-star"></span>
                  </span>
                </div>
              </figure>
              <div class="media-body">
                <h3 class="mt-0">At this Hotel</h3>
                <ul class="room-specs">
                  <li><span class="ion-ios-people-outline"></span> 2 Guests</li>
                  <li><span class="ion-ios-crop"></span> 22 ft <sup>2</sup></li>
                  <h3 class="mt-123">Our Guest Room</h3>
                </ul>
                <p> Our 69 bedrooms come in 5 types – Cozy – Roomy and a Large Wabi Sabi Lifestyle Art... </p>
                <p><a href="#" class="btn btn-primary btn-sm">Find out more</a></p>
              </div>
            </div>
          </div>
          <div class="col-md-5 room-thumbnail-absolute">
            <a href="#" class="media d-block room bg first-room" style="background-image: url(images/img_2.jpg); ">
              <figure>
                <div class="overlap-text">
                  <span>
                    Hotel Room 
                    <span class="ion-ios-star"></span>
                    <span class="ion-ios-star"></span>
                    <span class="ion-ios-star"></span>
                  </span>
                  <span class="pricing-from">
                    out more
                  </span>
                </div>
              </figure>
            </a>

            <a href="#" class="media d-block room bg second-room" style="background-image: url(images/img_4.jpg); ">
              <figure>
                <div class="overlap-text">
                  <span>
                    Hotel Room 
                    <span class="ion-ios-star"></span>
                    <span class="ion-ios-star"></span>
                    <span class="ion-ios-star"></span>
                  </span>
                  <span class="pricing-from">
                    from $22
                  </span>
                </div>
              </figure>
            </a>
            
          </div>
        </div>
      </div> -->

      <div class="container">
        <div class="row">
          
          <div class="col-md-8">
            <div id="demo" class="carousel slide" data-ride="carousel">

              <!-- Indicators -->
              <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <!-- <li data-target="#demo" data-slide-to="2"></li> -->
              </ul>
              
              <!-- The slideshow -->
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src="images/Homepage/OurBedrooms/IMG_3230.jpg" alt="Los Angeles" width="1100" height="500">
                </div>
                <div class="carousel-item">
                  <img src="images/Homepage/OurBedrooms/IMG_3247(1).jpg" alt="Chicago" width="1100" height="500">
                </div>
<!--                 <div class="carousel-item">
                  <img src="images/romm3.jpg" alt="New York" width="1100" height="500">
                </div>
                <div class="carousel-item">
                  <img src="images/RoomS/IMG_3225-Pano-2.jpg" alt="New York" width="1100" height="500">
                </div>
                <div class="carousel-item">
                  <img src="images/RoomM/IMG_3351.jpg" alt="New York" width="1100" height="500">
                </div>
                <div class="carousel-item">
                  <img src="images/RoomL/IMG_3023(1).jpg" alt="New York" width="1100" height="500">
                </div> -->
              </div>
              
              <!-- Left and right controls -->
              <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <h3 style="font-family: Brown-Light;font-size: 36px;margin-top: 20px;"><?php echo $lang["header_colume2"]; ?></h3>
            <br>
            <p class="content-index" ><?php echo $lang["content_colume2"]; ?></p>
            <p><a href="room.php" class="btn btn-primary btn-sm"><?php echo $lang["button_2"]; ?></a></p>
          </div>
        </div>
      </div>
    </section>

   
    
    <!-- <section class="section-cover" data-stellar-background-ratio="0.5" style="background-image: url(images/img_51.jpg);">
      <div class="container">
        <div class="row justify-content-center align-items-center intro">
          <div class="col-md-9 text-center element-animate">
            <h2>Relax and Enjoy your Holiday</h2>
            <p class="lead mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto quidem tempore expedita facere facilis, dolores!</p>
            <div class="btn-play-wrap"><a href="https://vimeo.com/channels/staffpicks/93951774" class="btn-play popup-vimeo "><span class="ion-ios-play"></span></a></div>
          </div>
        </div>
      </div>
    </section> -->
    <section class="site-section">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <h3 style="font-family: Brown-Light;font-size: 36px;"><?php echo $lang["header_colume3"]; ?></h3>
          <br>
          <p class="content-index" ><?php echo $lang["content_colume3"]; ?></p>
          <p><a href="bar.php" class="btn btn-primary btn-sm"><?php echo $lang["button_3"]; ?></a></p>
        </div>
        <div class="col-md-8">
          <div id="demo1" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo1" data-slide-to="0" class="active"></li>
              <li data-target="#demo1" data-slide-to="1"></li>
              <!-- <li data-target="#demo1" data-slide-to="2"></li> -->
            </ul>
            
            <!-- The slideshow -->
            <div class="carousel-inner" style="    height: 500px;">
              <!-- <div class="carousel-item active">
                <img src="images/img_1.jpg" alt="Los Angeles" width="1100" height="500">
              </div>
              <div class="carousel-item">
                <img src="images/img_2.jpg" alt="Chicago" width="1100" height="500">
              </div> -->
              <div class="carousel-item active">
                <img src="images/Hotel/IMG_3402.jpg" alt="New York" width="1100" height="500">
              </div>
              <div class="carousel-item">
                <img src="images/Hotel/IMG_3297.jpg" alt="New York" width="1100" height="500">
              </div>
            </div>
            
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo1" data-slide="prev">
              <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo1" data-slide="next">
              <span class="carousel-control-next-icon"></span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
    <!-- END section -->
    
    <section class="site-section bg-light" style="padding-bottom: 0px;">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-12 heading-wrap text-center">
            <h4 class="sub-heading"><?php echo $lang["header_event"]; ?></h4>
              <h1 class="heading"><?php echo $lang["subheader_event"]; ?></h1>
          </div>
        </div>
          <div id="mixedSlider">
              <div class="MS-content">
<!--         <div class="row" >
 -->          <!-- <div class="col-md-4">
            <div class="post-entry">
				  <img src="images/img_5.jpg" alt="Image placeholder" class="img-fluid">
              <div class="body-text">
                <div class="category">Event 1</div>
                <h3 class="mb-3"><a href="#">Event 1</a></h3>
                <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum deserunt illo quis similique dolore voluptatem culpa voluptas rerum, dolor totam.</p>
                <p><a href="#" class="btn btn-primary btn-outline-primary btn-sm">Read More</a></p>
              </div>
            </div>
          </div> -->
        <?php
          define("MYSQL_SERVER", "127.0.0.1");
          define("MYSQL_USER", "root");
          define("MYSQL_PASSWORD", "luxury@123");
          define("MYSQL_DATABASE", "content_villa_db");
          define('MYSQL_PORT', "3306");

          $mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
          if (mysqli_connect_errno()) {  printf("Connect failed: %s
          ", mysqli_connect_error());  exit(); }
          // echo $id;

          $myquery = $mysqli->query("select * from bsi_event ");
          $today_event = array();
          $chk = 0;
          // $row    = $result->fetch_assoc();

            $i=0;
          while ($row = $myquery->fetch_assoc()) {
            $datestart = explode('-', $row["startdate"]);
            $dateend = explode('-', $row["enddate"]);
            if ($chk == 0) {
              if ((int)$datestart[1] >= (int)date("m") && (int)$dateend[1] <= (int)date("m")  ) {
                $today_event = $row;
                $chk = 1;
              }else{
                $today_event = $row;
              }
            }


        ?>

            <div class="item">
              <div class="post-entry">
                <img src="/images/Event/S__17137672.jpg" alt="Image placeholder" class="img-fluid">
               <!--  <div class="ribbon">
                  <p class="d1">Sat</p>
                  <p class="d2">4<span style="vertical-align: super;">th</span> Sep</p>
                  <p class="d3">2018</p>
                </div> -->
                <div class="body-text">
                  <div class="category">Event <?php echo $i+1; ?></div>
                  <h3 class="mb-3"><a href="#"><?php echo $row["title"]; ?></a></h3>
                  <p class="mb-4"><?php echo $row["detail"]; ?></p>
                </div>
              </div>
            </div>    
                  
        <?php
        $i++;
          }
        ?>

          
        </div>
        <div class="MS-controls">
            <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
            <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
        </div>
        </div>

        </div>
      </div>

    </section>
    <!-- END section -->
   
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
                    <h3><?php echo $manu["res"]; ?></h3>
                    <p class="lead"><a href="tel://<?php echo $manu["phone"]; ?>"><?php echo $manu["phone"]; ?></a></p>
          </div>
          <div class="col-md-4">
            <h3>Connect With Us</h3>
            <p>We are socialized. Follow us</p>
            <p>
              <a href="<?php echo $manu["fb_link"]; ?>" class="pl-0 p-3"><span class="fa fa-facebook"></span></a>
              <a href="<?php echo $manu["ins_link"]; ?>" class="p-3"><span class="fa fa-instagram"></span></a>
            </p>
          </div>
          <div class="col-md-4">
            <h3><?php echo $manu["connect"]; ?></h3>
            
            <p>
              <a href="<?php echo $manu["map_link"]; ?>"><?php echo $manu["addr"]; ?></p>
            <form action="#" class="subscribe">
              <div class="form-group">
                <button type="submit"><span class="ion-ios-arrow-thin-right"></span></button>
                <input type="email" class="form-control" placeholder="Enter Email">
              </div>
              
            </form>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-7 text-center">
            &copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | 
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </div>
        </div>
      </div>
    </footer>
    <!-- END footer -->
    
    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js"></script>
    <!-- <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js'></script> -->
    <script src="js/jquery-migrate-3.0.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-browser/0.1.0/jquery.browser.js"></script>
    <!-- <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script> -->
    <script src="js/popper.min.js"></script>
    <!-- <script src="js/bootstrap.min.js"></script> -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>

    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>

    <script src="js/main.js"></script>
    <script src="js/multislider.js"></script>
    <script type="text/javascript" src="slick/slick.min.js"></script>
    <script src="js/jquery.cycle.all.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.maximage.js" type="text/javascript" charset="utf-8"></script>
    <script>
      $('#mixedSlider').multislider({
        duration: 750,
        interval: 3333000
      });
    </script>


    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#maximage').maximage({
                cycleOptions: {
                    fx: 'fade',
                    speed: 1000, // Has to match the speed for CSS transitions in jQuery.maximage.css (lines 30 - 33)
                    timeout: 100,
                    prev: '#arrow_left',
                    next: '#arrow_right',
                    pause: 1,
                    before: function(last, current) {
                        if (!$.browser.msie) {
                            // Start HTML5 video when you arrive
                            if ($(current).find('video').length > 0) $(current).find('video')[0].play();
                        }
                    },
                    after: function(last, current) {
                        if (!$.browser.msie) {
                            // Pauses HTML5 video when you leave it
                            if ($(last).find('video').length > 0) $(last).find('video')[0].pause();
                        }
                    }
                },
                onFirstImageLoaded: function() {
                    jQuery('#cycle-loader').hide();
                    jQuery('#maximage').fadeIn('fast');
                }
            });

            // Helper function to Fill and Center the HTML5 Video
            jQuery('video,object').maximage('maxcover');

            // To show it is dynamic html text
            jQuery('.in-slide-content').delay(1200).fadeIn();
        });
    </script>
  </body>
</html>