<link rel="apple-touch-icon" sizes="180x180" href="<?php echo asset('/images/logo2.jpg'); ?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo asset('/images/logo2.jpg'); ?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo asset('/images/logo2.jpg'); ?>">
<link rel="manifest" href="<?php echo asset('images/favicon/manifest.json'); ?>">
<link rel="mask-icon" href="<?php echo asset('images/favicon/safari-pinned-tab.svg'); ?>" color="#5bbad5">
<meta name="apple-mobile-web-app-title" content="ContentvillaChiangMai">
<meta name="application-name" content="ContentvillaChiangMai">
<meta name="theme-color" content="#ffffff">
