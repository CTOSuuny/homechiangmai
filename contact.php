<!DOCTYPE html>
<html>
<head>

	<?php
	if (session_status() == PHP_SESSION_NONE) {
	    session_start();
	}
	?>

	<meta charset='utf-8' />
	<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible' />
	<title>Home Chiang Mai</title>
	
	<meta content='Serene & Private Retreat - Lanna Colonial Boutique Hotel in the North of Thailand' name='description' />
	
	
	<meta name="distribution" content="global" />
	<meta name="language" content="en" />
	<meta content='width=device-width, initial-scale=1.0' name='viewport' />
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="shortcut icon" href="images/favicon/favicon-16x16.png" />
	<link rel="apple-touch-icon" href="images/favicon/favicon-16x16.png" />
	
	<link rel="stylesheet" href="css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
	<link rel="stylesheet" href="css/screen.css?v=1.2" type="text/css" media="screen" charset="utf-8" />

	<link rel="stylesheet" type="text/css" href="css/new_style.css">
	<link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css?family=Prompt:200" rel="stylesheet">
	<script type="text/javascript" src="js/main.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js"></script>
	<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
	<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
     <script>
       function onSubmit(token) {
         document.getElementById("demo-form").submit();
       }
     </script>
	<style type="text/css">
		body {
		    font-family: Prompt;
		    font-size: 20px;

		}
		html{
			background-image: url("images/1.jpg");
			background-repeat: no-repeat;
			background-size: auto;
		}
		
		.cus1{
			width: 20%;
		}
		.cus2{
			width: 40%;
		}
		@media only screen and (max-width: 720px) {
            .cus1,.cus2{
            	width: 100%;
            }

        }
        .custer{
			margin: 20px; 
			width: 80%;
		}
		.res{
			display: none;
		}
	</style>
</head>
<script type="text/javascript">
<?php
      if($_GET['status'] == "sendmail"){
        echo "window.onload = function() {
                swal(\"Thank you\", \"We will get back to you soon.\", \"success\");
              };";
      }else if($_GET['status'] == "sub"){
                echo "window.onload = function() {
                swal(\"Thank you for subscribing\", \"\", \"success\");
              };";
      }
    ?>
</script>
<body>

		   <div class="logo">
        <a href="/">
            <img src="images/logo.png" class="main">
        </a>
        <a href="javascript:void(0);" class="icon" onclick="myFunction()">
            <img src="images/menu.png">
        </a>
    </div>
    <div id="top_menu" class="top_menu">
        <div class="inner-top"><a href="index.html"> หน้าหลัก </a></div>
        <div class="inner-top"><a href="location.html"> สถานที่ตั้ง </a></div>
        <div class="inner-top"><a href="contact.php"> ติดต่อเรา </a></div>
        <div class="inner-top">
            <a href="#"><img src="images/thai.jpg"></a>
        </div>
        <div class="inner-top">
            <a href="eng/contact.php "><img src="images/eng.png"></a>
        </div>
        <div class="inner-top">
            <a href="#"><img src="images/china.png"></a>
        </div>
            
    </div>
    <hr>
    <div id="under_menu" class="under_menu">
        <div class="inner-bot"><a href="accomudation.html"> ห้องพัก                
                <div class="dropdown-content">
                    <a href="premium-delux.html" > พรีเมียม ดีลักซ์ </a>
            <br>
            <a href="premium-delux-with-bath.html"> พรีเมียม ดีลักซ์ พร้อมอ่างอาบน้ำ </a>
            <br>
            <a href="premium-exclusive.html"> พรีเมียม เอ็กเซ็กคลูทีฟ </a>
            <br>
            <a href="premium-exclusive-with-bath.html"> พรีเมียม เอ็กเซ็กคลูทีฟ พร้อมอ่างอาบน้ำ </a>
        </div>
        </a>
    </div>


    <div class="inner-bot">
        <a href="dining.html"> ห้องอาหาร 
            <div class="dropdown-content">
                <a href="lemont.html" > ร้านอาหารเลอมองต์ </a>
                <br>
                <a href="contentrestuarent.html">ร้านอาหาร อิตาเลี่ยน/ไทย ซิโพลเล่</a>
             </div>
         </a>
    </div>
    <div class="inner-bot"><a href="promotion.php"> โปรโมชั่น </a></div>
    <div class="inner-bot">
        <a href="facilities.html"> สิ่งอำนวยความสะดวก 
                <div class="dropdown-content">
                    <a href="pool.html" > สระว่ายน้ำ </a>
                    <br>
                    <a href="fitness.html"> ห้องออกกำลังกาย 
                    </a>
                    <br>
                    <a href="contentgarden.html"> สวนคอนเทนท์ 
                    </a>
                    <br>

                    <a href="bus.html"> บริการรับส่งสนามบินและเข้าเมือง 
                    </a>
                    <br>
                    <a href="spa.html"> นวดไทย 
                    </a>
                    <br>
                </div>
        </a>
    </div>
    <div class="inner-bot"><a href="gallery.html"> รูปภาพ </a></div>
<div class="inner-bot"><a href="attraction.html"> สถานที่ท่องเที่ยว </a>
        <div class="dropdown-content">
            <a href="baanthawai.html">  บ้านถวาย </a><br>
            <a href="doisuthep.html">  ดอยสุเทพ </a><br>
            <a href="watintravas.html">  วัดอินทราวาส หรือวัดต้นแกว๋น </a><br>
            <a href="phathatdoikhom.html">  วัดพระธาตุดอยคํา </a><br>
            <a href="ratchaplue.html">  อุทยานหลวงราชพฤกษ์ </a><br>
            <a href="doiintanon.html">  ดอยอินทนนท์ </a><br>
            <a href="kadfarang.html">  กาดฝรั่ง </a><br>
            <a href="baanmuengging.html">  บ้านเหมืองกุง </a><br>
            <a href="nimman.html">  ถนนนิมมานเหมินทร์ </a><br>
            <a href="nightsafari.html">  เชียงใหม่ ไนท์ซาฟารี </a><br>
            <a href="nightbazaa.html">  เชียงใหม่ ไนท์พลาซ่า </a><br>
            <a href="grandcanyon.html">  สวนน้ํา แกรนด์แคนยอน </a><br>
            <a href="longpair.html">  เดินป่า ขี่ช้าง ล่องแพไม้ไผ่ อําเภอแม่วาง </a><br>
            <a href="maiaim.html">  พิพิธภัณฑ์ใหม่เอี่ยม เชียงใหม่ </a><br>
		</div>
    </div>
    <div class="inner-bot" >
<a href="booknow.php" style="color: inherit;
    text-shadow: inherit;">
<div class="bookicon" style="    transform: inherit;
    position: inherit;
    left: inherit;
    width: 90px;
    height: 35px;">
				<div class="booktext" style="line-height: 35px;">
                จองห้องพัก
            </div>
			</div>
			</a>
			</div>
        </div>
    </div>
    

    <script>
        function myFunction() {
            var top = document.getElementById("top_menu");
            var under = document.getElementById("under_menu");
            if (top.className === "top_menu") {
                top.className += " responsive";
            } else {
                top.className = "top_menu";
            }
            if (under.className === "under_menu") {
                under.className += " responsive";
            } else {
                under.className = "under_menu";
            }
        }
    </script>

		<a href="booknow.php">
			<div class="bookicon">
				<div class="booktext">
                จองห้องพัก
            </div>
			</div>
		</a>

		<div class="contact">
			<div class="inner">
				<p class="title">ติดต่อเรา</p>
				<p class="content"><strong>คอนเทนท์ วิลล่า เชียงใหม่ </strong>
					<br>Home Chiang Mai<br>
59 หมู่ 6 ตำบลหนองควาย อำเภอหางดง จังหวัดเชียงใหม่  50230<br>
โทร : 053 125 220-221 <br>
								Email : <a href="mailto:info.contentvilla@gmail.com"> info.contentvilla@gmail.com</a><br>
						 		<a href="http://www.contentvillachiangmai.com"> www.contentvillachiangmai.com</a>
				<br>
				<br>
				<form class="form" action="/sendmes.php" method="post" id='demo-form'>
					<input type="hidden" name="lang" value="0">
					<div class="w3-row">
	  					<div class="w3-half w3-container">
							<div class="w3-row">
								<div class="w3-col s3 wedder" style="font-size: 20px;">
									<p class="text">ชื่อ: *</p>
								</div>
								<div class="w3-col s9 wedder">
									<div class="w3-row">
										<div class="w3-col cus1" style=" padding: 5px;">
											<select class="w3-select" name="title" required>
											  <option value="นาย">นาย</option>
											  <option value="นาง">นาง</option>
											  <option value="นางสาว">นางสาว</option>
											</select>
										</div>
										<div class="w3-col cus2" style=" padding: 5px;">
											<input type="" name="firstname" class="w3-input w3-border" required>
										</div>
										<div class="w3-col cus2" style=" padding: 5px;">
											<input type="" name="lastname" class="w3-input w3-border" required>
										</div>
									</div>
								</div>
							</div>
							<div class="w3-row">
								<div class="w3-col s3 wedder" style="font-size: 20px">
									<p class="text">อีเมล: *</p>
								</div>
								<div class="w3-col s9 wedder" style="padding: 5px;">
									<input type="text" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" name="email" class="w3-input w3-border" required >
								</div>
							</div>
							<div class="w3-row">
								<div class="w3-col s3 wedder" style="font-size: 20px">
									<p class="text">ที่อยู่: *</p>
								</div>
								<div class="w3-col s9 wedder" style="padding: 5px; height: 150px">
									<textarea type="" name="adds" class="w3-input w3-border" style="height: 140px" required></textarea>
								</div>
							</div>
							<div class="w3-row">
								<div class="w3-col s3 wedder" style="font-size: 20px">
									<p class="text">เบอร์โทรศัพท์: *</p>
								</div>
								<div class="w3-col s9 wedder" style="padding: 5px;">
									<input type="" name="tel" class="w3-input w3-border" required>
								</div>
							</div>
							<div class="w3-row">
								<div class="w3-col s3 wedder" style="font-size: 20px">
									<p class="text">โทรสาร: *</p>
								</div>
								<div class="w3-col s9 wedder" style="padding: 5px;">
									<input type="" name="fax" class="w3-input w3-border" >
								</div>
							</div>
						</div>
						<div class="w3-half w3-container"> 				
							<div class="w3-row">
									<p class="text">ข้อความที่ต้องการสอบถาม: </p>
									<br>
									<textarea type="" name="mes" class="w3-input w3-border" style="height: 140px" required></textarea>
							</div> 	
							<button class="g-recaptcha" data-sitekey="6LesvpEUAAAAAM4eUzES2phg_q01oNkQjxDDijzL
" data-callback='onSubmit'>Submit</button>
      						<br/>			
							<div class="w3-row" style="text-align: center;">
										<button class="w3-button w3-blue custer">Send</button>
							</div>
						</div>


					</div>
				</form>
			</div>
	        <div class="follow">
	            <div class="inner-foll"><a href="https://www.facebook.com/contentvillacm/"><img src="images/facebook.png"></a></div>
	            <div class="inner-foll"><a href="https://www.instagram.com/contentvilla"><img src="images/inst.png"></a></div>
	            <div class="inner-foll"><a href="https://twitter.com/content_villa"><img src="images/twitter.png"></a></div>
	            <div class="inner-foll"><a href="https://plus.google.com/u/2/112129358189939629256"><img src="images/google.png"></a></div>
	            <div class="inner-foll"><a href="https://www.youtube.com/channel/UCSbmsuG8nmaTMU3QfRDWyJg"><img src="images/youtube.png"></a></div>
	        </div>
	        <div class="footer">
	         <h1>โรงแรม คอนเทนท์ วิลล่า เชียงใหม่</h1>
	         <p class="ft-text">59 หมู่ 6 ตำบลหนองควาย อำเภอหางดง จังหวัดเชียงใหม่ 50230 
	            <br>โทร : +66 53 125 220-221 โทรสาร : +66 53 125 227 <br>Email :  <a href="mailto:info.contentvilla@gmail.com"> info.contentvilla@gmail.com</a>
				<a href="http://www.contentvillachiangmai.com"> www.contentvillachiangmai.com</a></p>
	         <span style="color:white;font-size: 14px;">@ 2018 Home Chiang Mai</span>
	        </div>
		</div>
		


		<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js'></script>
		<script src="js/jquery.cycle.all.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/jquery.maximage.js" type="text/javascript" charset="utf-8"></script>
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<script type="text/javascript" charset="utf-8">
			$(function(){
				$('#maximage').maximage({
					cycleOptions: {
						fx: 'fade',
						speed: 1000, // Has to match the speed for CSS transitions in jQuery.maximage.css (lines 30 - 33)
						timeout: 100,
						prev: '#arrow_left',
						next: '#arrow_right',
						pause: 1,
						before: function(last,current){
							if(!$.browser.msie){
								// Start HTML5 video when you arrive
								if($(current).find('video').length > 0) $(current).find('video')[0].play();
							}
						},
						after: function(last,current){
							if(!$.browser.msie){
								// Pauses HTML5 video when you leave it
								if($(last).find('video').length > 0) $(last).find('video')[0].pause();
							}
						}
					},
					onFirstImageLoaded: function(){
						jQuery('#cycle-loader').hide();
						jQuery('#maximage').fadeIn('fast');
					}
				});
	
				// Helper function to Fill and Center the HTML5 Video
				jQuery('video,object').maximage('maxcover');
	
				// To show it is dynamic html text
				jQuery('.in-slide-content').delay(1200).fadeIn();
			});
		</script>
		
		<!-- DON'T USE THIS: Insert Google Analytics code here -->
		<script type="text/javascript">
			
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-733524-1']);
		  _gaq.push(['_trackPageview']);
			
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" >var scrolltotop={setting:{startline:100,scrollto:0,scrollduration:1e3,fadeduration:[500,100]},controlHTML:'<img src="https://i1155.photobucket.com/albums/p559/scrolltotop/arrow27.png" />',controlattrs:{offsetx:5,offsety:5},anchorkeyword:"#top",state:{isvisible:!1,shouldvisible:!1},scrollup:function(){this.cssfixedsupport||this.$control.css({opacity:0});var t=isNaN(this.setting.scrollto)?this.setting.scrollto:parseInt(this.setting.scrollto);t="string"==typeof t&&1==jQuery("#"+t).length?jQuery("#"+t).offset().top:0,this.$body.animate({scrollTop:t},this.setting.scrollduration)},keepfixed:function(){var t=jQuery(window),o=t.scrollLeft()+t.width()-this.$control.width()-this.controlattrs.offsetx,s=t.scrollTop()+t.height()-this.$control.height()-this.controlattrs.offsety;this.$control.css({left:o+"px",top:s+"px"})},togglecontrol:function(){var t=jQuery(window).scrollTop();this.cssfixedsupport||this.keepfixed(),this.state.shouldvisible=t>=this.setting.startline?!0:!1,this.state.shouldvisible&&!this.state.isvisible?(this.$control.stop().animate({opacity:1},this.setting.fadeduration[0]),this.state.isvisible=!0):0==this.state.shouldvisible&&this.state.isvisible&&(this.$control.stop().animate({opacity:0},this.setting.fadeduration[1]),this.state.isvisible=!1)},init:function(){jQuery(document).ready(function(t){var o=scrolltotop,s=document.all;o.cssfixedsupport=!s||s&&"CSS1Compat"==document.compatMode&&window.XMLHttpRequest,o.$body=t(window.opera?"CSS1Compat"==document.compatMode?"html":"body":"html,body"),o.$control=t('<div id="topcontrol">'+o.controlHTML+"</div>").css({position:o.cssfixedsupport?"fixed":"absolute",bottom:o.controlattrs.offsety,right:o.controlattrs.offsetx,opacity:0,cursor:"pointer"}).attr({title:"Scroll to Top"}).click(function(){return o.scrollup(),!1}).appendTo("body"),document.all&&!window.XMLHttpRequest&&""!=o.$control.text()&&o.$control.css({width:o.$control.width()}),o.togglecontrol(),t('a[href="'+o.anchorkeyword+'"]').click(function(){return o.scrollup(),!1}),t(window).bind("scroll resize",function(t){o.togglecontrol()})})}};scrolltotop.init();</script>
</body>
</html>