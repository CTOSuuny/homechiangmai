<?php

class CSRF
{
    private $sessionKey;

    private $sessionExpiresKey;

    public function __construct()
    {
        $this->sessionKey = '_token';
        $this->sessionExpiresKey = '_tokenx';
        $this->expires = 15; //Minutes
    }

    /**
     * Create token code
     *
     * @return string
     */
    private function createTokenCode()
    {
        return md5(uniqid(rand(), true));
    }

    /**
     * Check has session
     *
     * @return bool
     */
    private function hasSession()
    {
        return !empty($_SESSION[$this->sessionKey]);
    }

    /**
     * Set token session
     *
     * @param string $token token
     *
     * @return string
     */
    private function setSession($token)
    {
        $_SESSION[$this->sessionKey] = $token;

        return $token;
    }

    /**
     * Get token session
     *
     * @return string
     */
    private function getSession()
    {
        return $_SESSION[$this->sessionKey] ?? null;
    }

    /**
     * Get token expires time
     *
     * @return int
     */
    private function getExpires()
    {
        return time() + $this->convertMinToSec($this->expires);
    }

    /**
     * Convert minute to second
     *
     * @param int $min minute
     *
     * @return int
     */
    private function convertMinToSec($min)
    {
        return $min * 60;
    }

    /**
     * Create token
     *
     * @return string
     */
    public function createToken()
    {
        if ($this->hasSession()) {
            return $this->setSession($this->createTokenCode());
        }

        return $this->getSession();
    }

    /**
     * Check token expires exists
     *
     * @return bool
     */
    public function checkTokenExpiresExists()
    {
        if (empty($_SESSION[$this->sessionExpiresKey])) {
            return false;
        }


        $tokens = json_decode($_SESSION[$this->sessionExpiresKey], true);

        if (!empty($tokens['_tokenx']) && time() <= $tokens['expries']) {
            return true;
        }

        return false;
    }

    /**
     * Create token has expires
     *
     * @return string
     */
    public function createTokenHasExpires()
    {
        $tokenx = $this->createToken();

        $_SESSION[$this->sessionExpiresKey] = json_encode([
            'expries' => $this->getExpires(),
            '_tokenx' => $tokenx
        ]);

        return $tokenx;
    }

    /**
     * Create token has expires field
     *
     * @return string
     */
    public function createTokenHasExpiresField()
    {
        $tokenx = $this->createTokenHasExpires();

        return "<input type=\"hidden\" name=\"{$this->sessionExpiresKey}\" value=\"{$tokenx}\">";
    }

    /**
     * Create token field
     *
     * @return string
     */
    public function createTokenField()
    {
        $token = $this->getSession();

        if (!$this->hasSession()) {
            $token = $this->setSession($this->createTokenCode());
        }

        return "<input type=\"hidden\" name=\"{$this->sessionKey}\" value=\"{$token}\">";
    }

    /**
     * Check token is correct
     *
     * @return bool
     */
    public function checkToken()
    {
        return $_POST[$this->sessionKey] ?? '' == $this->getSession();
    }

    /**
     * Static function for display csrf value
     *
     * @return string
     */
    public static function csrf()
    {
        $csrf = new self();

        return $csrf->createToken();
    }

    /**
     * Static function for display csrfx value
     *
     * @return string
     */
    public static function csrfx()
    {
        $csrf = new self();

        return $csrf->createTokenHasExpires();
    }

    /**
     * Static function for display token field
     *
     * @return string
     */
    public static function tokenField()
    {
        $csrf = new self();

        return $csrf->createTokenField();
    }

    /**
     * Static function for display tokenx field
     *
     * @return string
     */
    public static function tokenxField()
    {
        $csrf = new self();

        return $csrf->createTokenHasExpiresField();
    }

    /**
     * Static function for check token is exists
     *
     * @return bool
     */
    public static function check()
    {
        $csrf = new self();

        return $csrf->checkToken();
    }

    public static function checkTokenx()
    {
        $csrf = new self();

        return $csrf->checkTokenExpiresExists();
    }
}
