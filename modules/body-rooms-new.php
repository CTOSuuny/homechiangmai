<div class="row show-room-dialog room-modules">
    <h2><?php trans('ROOMS') ?></h2>
    <br>
    <p class="text-left"><?php trans('WE_OFFER') ?></p>
    <div class="table-responsive">
        <table class="table table-room" >
            <tr>
                <th><?php trans('ROOM_TYPE') ?></th>
                <th class="text-center"><?php trans('SIZE') ?></th>
                <th class="text-center"><?php trans('HIGH_CEILING') ?></th>
                <th class="text-center"><?php trans('PANTRY') ?></th>
                <th class="text-center"><?php trans('Dining_TABLE') ?></th>
                <th class="text-center"><?php trans('BATHHUB') ?></th>
            </tr>
            <tr>
                <td><?php trans('DELUXE') ?></td>
                <td class="text-center"><?php trans('STANDARD') ?></td>
                <td class="text-center"><?php trans('NO') ?></td>
                <td class="text-center"><?php trans('NO') ?></td>
                <td class="text-center"><?php trans('NO') ?></td>
                <td class="text-center"><?php trans('NO') ?></td>
            </tr>
            <tr>
                <td><?php trans('PREMIUM_DELUXE') ?></td>
                <td class="text-center"><?php trans('STANDARD') ?></td>
                <td class="text-center"><?php trans('YES') ?></td>
                <td class="text-center"><?php trans('NO') ?></td>
                <td class="text-center"><?php trans('NO') ?></td>
                <td class="text-center"><?php trans('YES') ?></td>
            </tr>
            <tr>
                <td><?php trans('EXECUTIVE') ?></td>
                <td class="text-center"><?php trans('LARGE') ?></td>
                <td class="text-center"><?php trans('YES') ?></td>
                <td class="text-center"><?php trans('YES') ?></td>
                <td class="text-center"><?php trans('YES') ?></td>
                <td class="text-center"><?php trans('NO') ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <td><?php trans('PREMIUM_EXECUTIVE') ?></td>
                <td class="text-center"><?php trans('LARGE') ?></td>
                <td class="text-center"><?php trans('YES') ?></td>
                <td class="text-center"><?php trans('YES') ?></td>
                <td class="text-center"><?php trans('YES') ?></td>
                <td class="text-center"><?php trans('YES') ?></td>
            </tr>
        </table>
    </div>
    <p class="text-left"><?php trans('THE_ROOM_ARHITECTRE') ?></p>
    <div class="row room-new">
        <div class="col-sm-6 room-new-img">
            <img src="<?php echo asset("images/rooms/7409941254836.jpg"); ?>" class="img-room">
        </div>
        <div class="col-sm-6  room-new-img">
            <img src="<?php echo asset("images/rooms/7409941296984.jpg"); ?>" class="img-room">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6  room-new-img">
            <img src="<?php echo asset("images/rooms/7409941356797.jpg"); ?>" class="img-room">
        </div>
        <div class="col-sm-6  room-new-img">
            <img src="<?php echo asset("images/rooms/bathtub-m.jpg"); ?>" class="img-room">
        </div>
    </div>
    <br>
</div>
