<div class="row show-about-hotel-dialog ">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="color gold" style="margin-bottom: 1.5em;"><?php trans('ABOUT_US') ?></h2>
            </div>
            <div class="col-xs-12">
                <p class="color dark-grey"><?php trans('ABOUT_DESC') ?></p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <img class="img-responsive about-image" src="<?php echo asset('images/lobby.jpg') ?>" alt="">
            </div>
            <div class="col-sm-4">
                <img class="img-responsive about-image" src="<?php echo asset('images/swimming.jpg') ?>" alt="">
            </div>
            <div class="col-sm-4">
                <img class="img-responsive about-image" src="<?php echo asset('images/fitness.jpg') ?>" alt="">
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <h2 class="color gold" style="margin-bottom: 2em"><?php trans('NEARBY_ATTRACTIONS_TEXT') ?></h2>
            </div>
            
            <div class="col-xs-12 divider">
                <div class="row">
                    <div class="col-lg-8">
                        <h3 class="text-left color gold mt-0"><?php trans('WAT_TITLE') ?></h3>
                        <p class="color dark-grey"><?php trans('WAT_P1') ?></p>
                        <p class="color dark-grey"><?php trans('WAT_P2') ?></p>
                        <p class="color dark-grey"><?php trans('WAT_P3') ?></p>
                        <p class="color dark-grey"><?php trans('WAT_P4') ?></p>
                        <p class="color dark-grey"><?php trans('REF') ?>: <a class="color dark-grey" href="https://www.renown-travel.com/temples/wat-inthrawat.html">https://www.renown-travel.com/temples/wat-inthrawat.html</a></p>                       
                    </div>
                    <div class="col-lg-4">
                        <img class="img-responsive" src="<?php echo asset('images/nearby-places/wattonkewn.jpg') ?>" alt="">
                        <small class="color dark-grey"><?php trans('PHOTO_BY') ?> Pa-Kai Saralya <?php trans('REF') ?>: <a class="color dark-grey" href="https://www.thetrippacker.com/th/place/วัดอินทราวาสต้นเกว๋น/22478">https://www.thetrippacker.com/th/place/วัดอินทราวาสต้นเกว๋น/22478</a></small>
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12 divider">
                <div class="row">
                    <div class="col-lg-8">
                        <h3 class="text-left color gold mt-0"><?php trans('ROYAL_PARK_TITLE') ?></h3>
                        <p class="color dark-grey"><?php trans('ROYAL_PARK_P1') ?></p>
                        <p class="color dark-grey"><?php trans('ROYAL_PARK_P2') ?></p>
                        <p class="color dark-grey"><?php trans('REF') ?>: <a class="color dark-grey" href="www.tourismthailand.org/Attraction/Royal-Park-Rajapruek--3933 ">www.tourismthailand.org/Attraction/Royal-Park-Rajapruek--3933 </a></p>
                    </div>
                    <div class="col-lg-4">
                        <img class="img-responsive" src="<?php echo asset('images/nearby-places/royal-rajapruek.jpg') ?>" alt="">
                        <small class="color dark-grey"><?php trans('REF') ?>: <a class="color dark-grey" href="https://www.facebook.com/อุทยานหลวงราชพฤกษ์-129630133792811/">https://www.facebook.com/อุทยานหลวงราชพฤกษ์-129630133792811/</a></small>
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12 divider">
                <div class="row">
                    <div class="col-lg-8">
                        <h3 class="text-left color gold mt-0"><?php trans('BAN_TAWAI_TITLE') ?></h3>
                        <p class="color dark-grey"><?php trans('BAN_TAWAI_P1') ?></p>
                        <p class="color dark-grey"><?php trans('BAN_TAWAI_P2') ?></p>
                        <p class="color dark-grey"><?php trans('BAN_TAWAI_P3') ?></p>
                        <p class="color dark-grey"><?php trans('REF') ?>: <a class="color dark-grey" href="http://myChiangMaitour.com/BanThawai/">http://myChiangMaitour.com/BanThawai/ </a></p>
                    </div>
                    <div class="col-lg-4">
                        <img class="img-responsive" src="<?php echo asset('images/nearby-places/bantawai.jpg') ?>" alt="">
                        <small class="color dark-grey"><?php trans('PHOTO_BY') ?> Akkasid Tom Wisesklin <?php trans('REF') ?>: <a class="color dark-grey" href="https://www.touronthai.com/article/257">https://www.touronthai.com/article/257</a></small>
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12 divider">
                <div class="row">
                    <div class="col-lg-8">
                        <h3 class="text-left color gold mt-0"><?php trans('MUANG_KUNG_TITLE') ?></h3>
                        <p class="color dark-grey" ><?php trans('MUANG_KUNG_P1') ?></p>
                        <p class="color dark-grey" ><?php trans('MUANG_KUNG_P2') ?></p>
                        <p class="color dark-grey" ><?php trans('REF') ?>: <a  class="color dark-grey" href="https://www.tourismthailand.org/Attraction/Muang-Kung-Village--3953">https://www.tourismthailand.org/Attraction/Muang-Kung-Village--3953</a></p>
                    </div>
                    <div class="col-lg-4">
                        <img class="img-responsive" src="<?php echo asset('images/nearby-places/muang-kung.JPG') ?>" alt="">
                        <small class="color dark-grey"><?php trans('REF') ?>: <a class="color dark-grey" href="http://info.dla.go.th/public/travel.do?cmd=goDetail&id=307004&random=1422437829000">http://info.dla.go.th/public/travel.do?cmd=goDetail&id=307004&random=1422437829000</a></small>
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12 divider">
                <div class="row">
                    <div class="col-lg-8">
                        <h3 class="text-left color gold mt-0"><?php trans('NIGHT_SAFARI_TITLE') ?></h3>
                        <p class="color dark-grey"><?php trans('NIGHT_SAFARI_P1') ?></p>
                        <p class="color dark-grey"><?php trans('NIGHT_SAFARI_P2') ?></p>
                        <p class="color dark-grey"><?php trans('NIGHT_SAFARI_P3') ?></p>
                        <p class="color dark-grey"><?php trans('REF') ?>: <a class="color dark-grey" href="http://www.visitChiang Mai.com.au/night_safari.html">http://www.visitChiang Mai.com.au/night_safari.html</a></p>
                    </div>
                    <div class="col-lg-4">
                        <img class="img-responsive" src="<?php echo asset('images/nearby-places/night-safari.jpg') ?>" alt="">
                        <small class="color dark-grey"><?php trans('REF') ?>: <a class="color dark-grey" href="https://www.facebook.com/chiangmainightsafarifanclub/">https://www.facebook.com/chiangmainightsafarifanclub/</a></small>
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12 divider">
                <div class="row">
                    <div class="col-lg-8">
                        <h3 class="text-left color gold mt-0"><?php trans('NIGHT_BAZAAR_TITLE') ?></h3>
                        <p class="color dark-grey" ><?php trans('NIGHT_BAZAAR_P1') ?></p>
                        <p class="color dark-grey" ><?php trans('REF') ?>: <a  class="color dark-grey" href="http://www.Chiang Mai.bangkok.com/top10/top10-attractions-Chiang Mai-night-bazaar.htm ">http://www.Chiang Mai.bangkok.com/top10/top10-attractions-Chiang Mai-night-bazaar.htm </a></p>
                    </div>
                    <div class="col-lg-4">
                        <img class="img-responsive" src="<?php echo asset('images/nearby-places/night-bazaar.jpg') ?>" alt="">
                        <small class="color dark-grey"><?php trans('REF') ?>: <a class="color dark-grey" href="https://www.bangkokpost.com/travel/sightseeing/18215/kalare-night-bazaar-chiang-mai">https://www.bangkokpost.com/travel/sightseeing/18215/kalare-night-bazaar-chiang-mai </a></small>
                    </div>
                </div>
            </div>

            
            <div class="col-xs-12 divider">
                <div class="row">
                    <div class="col-lg-8">
                        <h3 class="text-left color gold mt-0"><?php trans('DOI_INTHANON_TITLE') ?></h3>
                        <p class="color dark-grey"><?php trans('DOI_INTHANON_P1') ?></p>
                        <div>
                            <p class="color dark-grey"><?php trans('DOI_INTHANON_THING_TODO_TITLE') ?></p>
                            <ul>
                                <li class="color dark-grey"><?php trans('DOI_INTHANON_THING_TODO_1') ?></li>
                                <li class="color dark-grey"><?php trans('DOI_INTHANON_THING_TODO_2') ?></li>
                                <li class="color dark-grey"><?php trans('DOI_INTHANON_THING_TODO_3') ?></li>
                                <li class="color dark-grey"><?php trans('DOI_INTHANON_THING_TODO_4') ?></li>
                                <li class="color dark-grey"><?php trans('DOI_INTHANON_THING_TODO_5') ?></li>
                                <li class="color dark-grey"><?php trans('DOI_INTHANON_THING_TODO_6') ?></li>
                                <li class="color dark-grey"><?php trans('DOI_INTHANON_THING_TODO_7') ?></li>
                            </ul>
                        </div>
                        <p class="color dark-grey"><?php trans('REF') ?>: <a class="color dark-grey" href="https://www.tourismthailand.org/Attraction/Doi-Inthanon-National-Park--153">https://www.tourismthailand.org/Attraction/Doi-Inthanon-National-Park--153</a></p>
                    </div>
                    <div class="col-lg-4">
                        <img class="img-responsive" src="<?php echo asset('images/nearby-places/doiinthanon.jpg') ?>" alt="">
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-lg-8">
                        <h3 class="text-left color gold mt-0"><?php trans('NIMMAN_TITLE') ?></h3>
                        <p class="color dark-grey"><?php trans('NIMMAN_TITLE_DETAIL') ?></p>
                        <div>
                            <ul>
                                <li class="color dark-grey"><?php trans('NIMMAN_DETAIL_1') ?></li>
                                <li class="color dark-grey"><?php trans('NIMMAN_DETAIL_2') ?></li>
                                <li class="color dark-grey"><?php trans('NIMMAN_DETAIL_3') ?></li>
                                <li class="color dark-grey"><?php trans('NIMMAN_DETAIL_4') ?></li>
                                <li class="color dark-grey"><?php trans('NIMMAN_DETAIL_5') ?></li>
                                <li class="color dark-grey"><?php trans('NIMMAN_DETAIL_6') ?></li>
                                <li class="color dark-grey"><?php trans('NIMMAN_DETAIL_7') ?></li>
                                <li class="color dark-grey"><?php trans('NIMMAN_DETAIL_8') ?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <img class="img-responsive" src="<?php echo asset('images/nearby-places/nimman.jpg') ?>" alt="">
                        <small class="color dark-grey"><?php trans('REF') ?>: <a class="color dark-grey" href="https://www.youtube.com/watch?v=l9QWd5n2xpQ">https://www.youtube.com/watch?v=l9QWd5n2xpQ</a></small>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
