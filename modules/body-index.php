<?php
    $room = new room;
?>

<div class="row show-room-dialog room-modules">
    <div class="visible-xs book-form">
        <form class="form-inline" action="booking-search.php" method="post">
            <?php
                $checkIn = "check-in-mobile";
                $checkOut = "check-out-mobile";
                include("modules/book-form.php");
            ?>
            <div align="center">
                <button type="submit" <?php echo $isDisableBookingEngine == '1' ? 'disabled' : '' ?> class="btn btn-default btn-book" ><?php echo BOOK_YOUR_ROOM_NOW; ?></button>
            </div>
        </form>
    </div>

    <h2 style="margin-bottom: 1.5em;">
        <?php echo WELCOME_MESSAGE ?>
    </h2>
    <?php
        $roomsGroups = array_chunk($room->getRoomsType(), 4);
        foreach($roomsGroups as $row){
            echo "<div class='row'>";
                foreach($row as $rowRoom){
                    echo '<script> $(document).ready(function() { ';
                        echo "var widthScreen = $( document ).width()";
                    echo '
                        $("#iframe_details_'.str_replace(" ","",$rowRoom['roomtype_ID']).'").colorbox({iframe:true, width: (widthScreen <= 767 ? 90 : 60) + \'%\', height: "60%"});
                        $(".group_'.$rowRoom['roomtype_ID'].'").colorbox({rel:\'group_'.$rowRoom['roomtype_ID'].'\', maxWidth: $(\'.room-modules\').width(), maxHeight:"80%", slideshow:true, slideshowSpeed:5000});
                            ';
                    echo '}); </script>';
        ?>
                    <div class="col-sm-3">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?php
                                    echo $room->getRoomPicture($rowRoom['roomtype_ID']);
                                ?>
                                <h3 style="text-align: center;"><?php echo $rowRoom['type_name']; ?></h3>
                                <div class="roomtype-description">
                                    <?php
                                        $language = !empty($_COOKIE['language']) ? $_COOKIE['language'] : $defaultLanguage["lang_code"];
                                        echo mb_substr(json_decode($rowRoom['description'], true)[$language], 0, 200, 'UTF-8');
                                        $countWord = count( str_split(json_decode($rowRoom['description'], true)[$language]) );
                                        if($countWord >= 200){
                                            echo " ...";
                                        }
                                    ?>
                                </div>
                                <a class="btn btn-default btn-book primary-btn room-form<?php echo $rowRoom['roomtype_ID']; ?>"><?php echo BOOK_YOUR_ROOM_NOW; ?></a>
                                <script>
                                    $(window).load(function() {
                                        var widthScreen = $( document ).width();

                                        var colorboxOption = {
                                            width: (widthScreen <= 767 ? 90 : 60) + '%',
                                            height: '90%',
                                            fixed: true,
                                            href:"modules/modal-room-form.php?room_type_id_index="+<?php echo $rowRoom['roomtype_ID']; ?>
                                        }

                                        if (widthScreen > 767) {
                                            colorboxOption.height = 'auto';
                                        }

                                        $('.room-form'+<?php echo $rowRoom['roomtype_ID']; ?>).colorbox(colorboxOption);
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
        <?php
                }
            echo "</div>";
        }
    ?>
    <br>
</div>