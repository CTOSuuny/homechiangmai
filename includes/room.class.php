<?php
    class room
    {
        protected $sql;

        function __construct() {
            $this->sql = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
        }

        public function getRoomsType()
        {
            return mysqli_query($this->sql, "select * from bsi_roomtype")->fetch_all(MYSQLI_ASSOC);
        }

        public function getRoomPicture($id)
        {
            $query = mysqli_query($this->sql, "select * from bsi_gallery where roomtype_id = $id order by pic_id DESC")->fetch_all(MYSQLI_ASSOC);
            $result = array_filter($query, function($q){
                return $q["show_in_homepage"] == 1;
            });
            $countResult = count($result);
            if(empty($countResult)){
                if( !empty($query[0]["img_path"]) ){
                    return '<a class="group_'.$id.'" href="'. asset('gallery/'.$query[0]["img_path"]). '" style="text-decoration:none; " ><img class="img-room-index" src="'. asset('gallery/thumb_'. $query[0]["img_path"]) .'"></a>';
                }else{
                    return '<a class="group_'.$id.'" href="'. asset('images/no_photo.jpg'). '" style="text-decoration:none; " ><img class="img-room-index"  src="'. asset('images/no_photo.jpg') .'"></a>';
                }
            }
            else if($countResult == 1){
                $path = array_shift($result)['img_path'];

                return '<a class="group_'.$id.'" href="'. asset('gallery/'. $path). '" style="text-decoration:none; " ><img class="img-room-index" src="'. asset('gallery/thumb_'. $path) .'"></a>';
            }else if($countResult > 1){
                $list_img = "<div class='flexslider' style='cursor:pointer; cursor:hand;' >";
                    $list_img .= "<ul class='slides'>";
                    foreach($result as $rowResult){
                        $list_img .= '<li><a class="group_'.$id.'" href="'. asset('gallery/'.$rowResult['img_path']). '" style="text-decoration:none; " ><img src="'. asset('gallery/thumb_'.$rowResult['img_path']). '"/></a></li>';
                    }
                    $list_img .= "</ul>";
                $list_img .= "</div>";

                return $list_img;
            }
        }
    }
?>
