<?php

class AddRoomMenu extends Migration
{

    public function handle()
    {
        if (!$this->valueExists('bsi_adminmenu', 'name', 'USER')) {
            $insert = $this->getConnection()->query("INSERT INTO bsi_adminmenu (name, url, parent_id, status, ord, role) VALUES ('USER', '#', 0, 'Y', 8, '[]')");

            $conn = $this->getConnection();
            $insertId = $conn->insert_id;

            if (!$this->valueExists('bsi_adminmenu', 'name', 'Manage User')) {
                $this->getConnection()->query("INSERT INTO bsi_adminmenu (name, url, parent_id, status, ord, role) VALUES ('Manage User', 'user.php', $insertId, 'Y', 1, '[]')");
            }
        }
    }

}
