<?php

class AddFiledLocalTaxServiceChargeConfig extends Migration
{

    public function handle()
    {
        foreach (['conf_price_inclu_local_tax', 'conf_local_tax', 'conf_price_inclu_service_charge', 'conf_service_charge'] as $conf) {
            if (!$this->valueExists('bsi_configure', 'conf_key', $conf)) {
                $this->getConnection()->query("INSERT INTO bsi_configure (conf_key, conf_value) VALUES ('$conf', '')");
            }
        }
    }

}
