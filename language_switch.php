<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
    $_SESSION["lang"] = "thai";
}
if (isset($_GET["lang"]) && $_GET["lang"] == "") {
    $_SESSION["lang"] = "thai";
}else{
	if ($_GET["lang"] == "thai") {
    	$_SESSION["lang"] = "thai";
	}else if ($_GET["lang"] == "eng") {
    	$_SESSION["lang"] = "eng";
	}else if ($_GET["lang"] == "china") {
    	$_SESSION["lang"] = "china";
	}
	// echo $_SESSION["lang"];
}
header('Location: ' . $_SERVER['HTTP_REFERER']);
