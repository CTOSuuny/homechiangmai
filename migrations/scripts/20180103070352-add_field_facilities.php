<?php

class AddFieldFacilities extends Migration
{

    public function handle()
    {
        if ($this->hasColumn('bsi_roomtype', 'facilities')) {
            return false;
        }

        $this->addColumn('bsi_roomtype', 'facilities', 'TEXT', 'null', true);
    }

}
