<?php
session_start();
include("includes/db.conn.php");
include("includes/conf.class.php");
include("language.php");

if (!empty($_SESSION['dvars_details2'])) {
    unset($_SESSION['dvars_details2']);
}

if (!empty($_SESSION['_token'])) {
    unset($_SESSION['_token']);
}

// session_destroy();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $bsiCore->config['conf_hotel_name']; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans|Playfair+Display" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo asset('css/datepicker.css" type="text/css" media="screen') ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider.css') ?>">
		<!-- <link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider-customize.css') ?>"> -->
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-customize.css') ?>">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

        <link rel="stylesheet" href="css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
        <link rel="stylesheet" href="css/screen.css?v=1.2" type="text/css" media="screen" charset="utf-8" />

        <link rel="stylesheet" type="text/css" href="css/new_style.css">
        <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet'>
        <link href="https://fonts.googleapis.com/css?family=Prompt:200" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet'>
        <link rel="stylesheet" href="css/style.css">
        <?php include('modules/header-favicon.php'); ?>
    </head>
    <body>

            <div class="container-fluid" style="margin-top: 120px;" >
                <?php
    				include("modules/inner-modules.php");
    			?>
                <div id="body-div">
                    <div class="row show-room-dialog" >
                        <h1><?php echo $bsiCore->config['conf_hotel_name']; ?></h1>
                        <?php
                            $active = "confirm";
                            include("modules/breadcrumb.php");
                        ?>
                        <br><br>
                        <div class="wrapper">
                            <div class="htitel">
                                <h2 class="fl" style="border:0; margin:0;"><?php echo BOOKING_COMPLETED_TEXT; ?></h2>
                            </div>
                            <br>
                            <!-- start of search row -->
                            <div class="container-fluid" style="margin:0; padding:0;">
                                <div class="row-fluid" style="border: 1px solid #dfc27c; padding: 1% 0">
                                    <div class="span12" style="text-align:center; padding:20px 0">
                                    	<p><strong><?php echo THANK_YOU_TEXT; ?>!</strong></p>
                                        <p><?php echo YOUR_BOOKING_CONFIRMED_TEXT; ?>. <?php echo INVOICE_SENT_EMAIL_ADDRESS_TEXT; ?>.</p>
                                         <div style="width:100%;">
                                            <button id="registerButton" style="margin:0 auto" class="btn btn-default btn-book" type="button" onClick="window.location.href='/'" ><?php echo BACK_TO_HOME; ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of search row -->
                        </div>
                    </div>
                </div>
                <br><br>
                <?php
                    include("modules/footer.php");
                ?>
                <div id="btn-to-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
            </div>
        <script src="js/main.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    	<script type="text/javascript" src="<?php echo asset('js/modernizr-2.6.2.min.js') ?>"></script>
    	<script type="text/javascript" src="<?php echo asset('js/jquery-zoomslider.min.js') ?>"></script>
    	<script type="text/javascript" src="<?php echo asset('js/bootstrap-datepicker.js') ?>"></script>
        <script defer src="<?php echo asset('js/helpers/btn-scroll.js') ?>"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script >
            function myFunction(x) {
              var chklogo = document.getElementById("logo");
              if (chklogo.style.display === 'none') {
                document.getElementById("logo").style.display = "block"
                document.getElementById("mySidebar").style.display = "none";
                document.getElementById("book").style.color = "white";

              } else {
                document.getElementById("logo").style.display = "none"
                document.getElementById("mySidebar").style.width = "100%";
                document.getElementById("mySidebar").style.display = "block";
                document.getElementById("book").style.color = "black";

              }
              x.classList.toggle("change");
            }
        </script>

    </body>
</html>
