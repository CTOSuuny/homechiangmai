var ContentVilla = function(){
    this.el = {
        inputCheckIn: "input[name='check_in']",
        inputCheckOut: "input[name='check_out']",
    }
}

ContentVilla.prototype = {
    constructor: ContentVilla,

    init: function(){
        this.initDatepicker();
    },

    initDatepicker: function(){
        var current = new Date();
        var element = this.el; // assign this class to this variable !!warning variable name because is reserved word
        var inputCheckIndate = $(this.el.inputCheckIn).datepicker({
            language: 'en',
            dateFormat: 'yyyy-mm-dd',
            autoClose: true,
            minDate: current, // Now can select only dates, which goes after today
            onSelect: function (dateFormatMin, dateMin, instanceMin) {
                var nextDay = new Date(dateMin.setDate(dateMin.getDate() + 1));
                if( $(element.inputCheckOut).val() == ""){
                    inputCheckOutdate.update("minDate", nextDay);
                    inputCheckOutdate.selectDate(nextDay);
                }
            }
        }).data('datepicker');
        var inputCheckOutdate = $(element.inputCheckOut).datepicker({
            language: 'en',
            dateFormat: 'yyyy-mm-dd',
            autoClose: true,
            minDate: current,
            onSelect: function (dateFormatCheckout, dateCheckout, instanceCheckout) {
                inputCheckIndate.update("maxDate", new Date(dateCheckout.setDate(dateCheckout.getDate() - 1)) );
            }
        }).data('datepicker');
    }
}

$(document).ready(function(){
    var contentVilla = new ContentVilla;
    contentVilla.init();
});
/*
$(document).ready(function(){
    $("input[name='check_in']").datepicker({
        language: 'en',
        autoClose: true,
        minDate: new Date() // Now can select only dates, which goes after today
    });
    $("input[name='check_out']").datepicker({
        language: 'en',
        autoClose: true,
        minDate: new Date() // Now can select only dates, which goes after today
    });
});
*/
