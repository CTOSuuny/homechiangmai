<?php

$successMessage = 'Success';

if ($_REQUEST['StatusMsg'] == $successMessage) {
    header('Location: ../booking-confirm.php?success_code=1');
    
    die;
} else {
    header('Location: ../booking-failure.php?error_code=26');
    
    die;
}