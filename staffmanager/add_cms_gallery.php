<?php
include_once("access.php");
if(isset($_POST['addedit'])){
	include_once("../includes/db.conn.php");
	include_once("../includes/conf.class.php");
	include_once("../includes/admin.class.php");
	$bsiAdminMain->addgallerycms();
	header("location:cms_gallery.php");
	// exit;
	// echo "Success";
}

include_once("header.php");

?>
	<link rel="stylesheet" type="text/css" href="css/jquery.validate.css" />
	<div id="container-inside"><span style="font-size:16px; font-weight:bold"><?php echo ADD_GALLERY_TEXT; ?></span>
		<hr />
		<form action="gallery_manage.php" method="post" id="form1" enctype="multipart/form-data">
			<table cellpadding="5" cellspacing="5" border="0">
				<tr>
					<td width="120px"><strong><?php echo IMAGE_TEXT;?> 1:</strong></td>
					<td valign="middle"><input type="file" name="image1" id="image1" class="required" style="width:250px;" /></td>
				</tr>
				<tr>
					<td width="120px"><strong><?php echo IMAGE_TEXT;?> 2:</strong></td>
					<td valign="middle"><input type="file" name="image2" id="image2" style="width:250px;" /></td>
				</tr>
				<tr>
					<td width="120px"><strong><?php echo IMAGE_TEXT;?> 3:</strong></td>
					<td valign="middle"><input type="file" name="image3" id="image3" style="width:250px;" /></td>
				</tr>
				<tr>
					<td width="120px"><strong><?php echo IMAGE_TEXT;?> 4:</strong></td>
					<td valign="middle"><input type="file" name="image4" id="image4" style="width:250px;" /></td>
				</tr>
				<tr>
					<td width="120px"><strong><?php echo IMAGE_TEXT;?> 5:</strong></td>
					<td valign="middle"><input type="file" name="image5" id="image5" style="width:250px;" /></td>
				</tr>
				<tr>
					<td width="120px"><strong><?php echo IMAGE_TEXT;?> 6:</strong></td>
					<td valign="middle"><input type="file" name="image6" id="image6" style="width:250px;" /></td>
				</tr>
				<tr>
					<td width="120px"><strong><?php echo IMAGE_TEXT;?> 7:</strong></td>
					<td valign="middle"><input type="file" name="image7" id="image7" style="width:250px;" /></td>
				</tr>
				<tr>
					<td width="120px"><strong><?php echo IMAGE_TEXT;?> 8:</strong></td>
					<td valign="middle"><input type="file" name="image8" id="image8" style="width:250px;" /></td>
				</tr>
				<tr>
					<td><input type="hidden" name="addedit" value="1"></td>
					<td><input type="submit" value="<?php echo PHOTO_SUBMIT; ?>" name="submitCapacity" id="submitCapacity" style="background:#e5f9bb; cursor:pointer; cursor:hand;" /></td>
				</tr>
			</table>
		</form>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#form1").validate();

		});
	</script>
	<script src="js/jquery.validate.js" type="text/javascript"></script>
	<?php include("footer.php"); ?>
