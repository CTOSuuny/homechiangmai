<?php

class AddFieldRoleToAdminmenu extends Migration
{

    public function handle()
    {
        if ($this->hasColumn('bsi_adminmenu', 'role')) {
            return false;
        }

        $this->addColumn('bsi_adminmenu', 'role', 'TEXT', null, true);
    }

}
