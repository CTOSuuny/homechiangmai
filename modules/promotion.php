<div class="row show-room-dialog room-modules">
    <h2><?php trans('PROMOTIONS') ?></h2>
    <br>
    <div class="row">
        <div class="col-lg-6" style="padding-bottom: 15px">
            <a  data-fancybox="gallery" href="<?php echo asset('images/pro1.jpg') ?>"><img src="<?php echo asset('images/pro1.jpg') ?>" style="width: 100%"></a>
        </div>
        <div class="col-lg-6" style="padding-bottom: 15px">
            <a data-fancybox="gallery" href="<?php echo asset('images/pro2.jpg') ?>"><img src="<?php echo asset('images/pro2.jpg') ?>" style="width: 100%"></a>
        </div>
    </div>
    <br>
    <br>
</div>