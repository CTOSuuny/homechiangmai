<?php
session_start();
include("includes/db.conn.php");
include("includes/conf.class.php");
include("language.php");

include("includes/details.class.php");
include('includes/csrf.class.php');

$bsibooking = new bsiBookingDetails();
$bsiCore->clearExpiredBookings();

if (!CSRF::checkTokenx()) {
	abort('500-token-expires');
}

?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $bsiCore->config['conf_hotel_name']; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans|Playfair+Display" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo asset('css/datepicker.css') ?>" type="text/css" media="screen"/>
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider.css') ?>" />
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider-customize.css') ?>"> -->
		<link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-customize.css') ?>">
                <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

        <link rel="stylesheet" href="css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
        <link rel="stylesheet" href="css/screen.css?v=1.2" type="text/css" media="screen" charset="utf-8" />

        <link rel="stylesheet" type="text/css" href="css/new_style.css">
        <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet'>
        <link href="https://fonts.googleapis.com/css?family=Prompt:200" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet'>
        <link rel="stylesheet" href="css/style.css">
		<?php include('modules/header-favicon.php'); ?>
    </head>
    <body>
        <div class="container-fluid" style="margin-top: 120px;" >
            <?php
                include("modules/inner-modules.php");
                include("modules/book-details.php");
                include("modules/footer.php");
            ?>
			<div id="btn-to-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
        </div>
    </body>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo asset('js/modernizr-2.6.2.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo asset('js/jquery-zoomslider.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset('js/jquery.validate.js') ?>"></script>
    <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
	<script defer src="<?php echo asset('js/helpers/btn-scroll.js') ?>"></script>
    <script type="text/javascript">
        $().ready(function() {

            $("#form1").validate({
				errorPlacement: function(error, element) {
				    error.appendTo( element.nextAll( ".errorTxt" ) );
				  }
			});

			$('input[name="payment_type"]').on('change', function() {
				$('.payment-label').removeClass('selected');
				$(this).parents().closest('.payment-label').addClass('selected');
			})
        });
    </script>
<script type="text/javascript">
$(document).ready(function(){
$('#btn_exisitng_cust').click(function() {
    if($('#btn_exisitng_cust').val() == '1'){
    $('#exist_wait').html("<img src='images/ajax-loader.gif' border='0'>")
    var querystr = 'actioncode=2&existing_email='+$('#email_addr_existing').val()+'&login_password='+$('#login_password').val();
    $.post("ajaxreq-processor.php", querystr, function(data){
        if(data.errorcode == 0){
            $('#title').html(data.title)
            $('#fname').val(data.first_name)
            $('#lname').val(data.surname)
            $('#str_addr').val(data.street_addr)
            $('#city').val(data.city)
            $('#state').val(data.province)
            $('#zipcode').val(data.zip)
            $('#country').val(data.country)
            $('#phone').val(data.phone)
            $('#fax').val(data.fax)
            $('#email').val(data.email)
            $('#login_c').html('leave blank for unchange password!')
            $('#id_type').val(data.id_type)
            $('#id_number').val(data.id_number)
            $('#exist_wait').html("")
            $('#btn_exisitng_cust').html('Logout')
            $('#btn_exisitng_cust').val('2')
            $('#forgot_pass').html('')
            $("#email_addr_existing").attr("disabled", "disabled");
            $("#login_password").attr("disabled", "disabled");
            $("#password").removeClass("required");
        }else {
            alert(data.strmsg);
            $('#fname').val('')
            $('#lname').val('')
            $('#str_addr').val('')
            $('#city').val('')
            $('#state').val('')
            $('#zipcode').val('')
            $('#country').val('')
            $('#phone').val('')
            $('#fax').val('')
            $('#email').val('')
            $('#login_c').html('')
            $('#id_type').val('')
            $('#id_number').val('')
            $('#exist_wait').html("")
        }
    }, "json");

    }else if($('#btn_exisitng_cust').val() == '2'){
         $('#exist_wait').html("<img src='images/ajax-loader.gif' border='0'>")
            $('#btn_exisitng_cust').html('Login')
            $('#btn_exisitng_cust').val('1')
            $("#email_addr_existing").removeAttr("disabled");
            $("#login_password").removeAttr("disabled");
            $('#email_addr_existing').val('')
            $('#login_password').val('')
            $('#fname').val('')
            $('#lname').val('')
            $('#str_addr').val('')
            $('#city').val('')
            $('#state').val('')
            $('#zipcode').val('')
            $('#country').val('')
            $('#phone').val('')
            $('#fax').val('')
            $('#email').val('')
            $('#login_c').html('')
            $('#id_type').val('')
            $('#id_number').val('')
            $('#forgot_pass').html('Forgot Password?')
            $("#password").addClass("required");
            $('#exist_wait').html("")
    }else if($('#btn_exisitng_cust').val() == '3'){
        $('#exist_wait').html("<img src='images/ajax-loader.gif' border='0'>")
        var querystr = 'actioncode=9&existing_email='+$('#email_addr_existing').val();
        $.post("ajaxreq-processor.php", querystr, function(data){
        if(data.errorcode == 0){
            alert('Your password has been reset. Please check your email and login!')
            $('#label_pass').show()
            $('#input_pass').show()
            $('#btn_exisitng_cust').val('1')
            $('#btn_exisitng_cust').html('Login')
            $('#exist_wait').html("")
        }else{
            alert(data.strmsg);
            $('#exist_wait').html("")
        }
        }, "json");
    }
});

$('#forgot_pass').toggle(function() {
            $('#label_pass').hide()
            $('#input_pass').hide()
            $('#btn_exisitng_cust').val('3')
            $('#btn_exisitng_cust').html('Submit')
            $('#forgot_pass').html('Back to Login')
    }, function() {
            $('#label_pass').show()
            $('#input_pass').show()
            $('#btn_exisitng_cust').val('1')
            $('#btn_exisitng_cust').html('Login')
            $('#exist_wait').html("")
            $('#forgot_pass').html('Forgot Password?')

});
});
function myPopup2(booking_id){
    var width = 730;
    var height = 650;
    var left = (screen.width - width)/2;
    var top = (screen.height - height)/2;
    var url='terms-and-services.php?bid='+booking_id;
    var params = 'width='+width+', height='+height;
    params += ', top='+top+', left='+left;
    params += ', directories=no';
    params += ', location=no';
    params += ', menubar=no';
    params += ', resizable=no';
    params += ', scrollbars=yes';
    params += ', status=no';
    params += ', toolbar=no';
    newwin=window.open(url,'Chat', params);
    if (window.focus) {newwin.focus()}
    return false;
}
      function myFunction(x) {
      var chklogo = document.getElementById("logo");
      if (chklogo.style.display === 'none') {
        document.getElementById("logo").style.display = "block"
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("book").style.color = "white";

      } else {
        document.getElementById("logo").style.display = "none"
        document.getElementById("mySidebar").style.width = "100%";
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("book").style.color = "black";

      }
      x.classList.toggle("change");
    }
</script>
</html>
