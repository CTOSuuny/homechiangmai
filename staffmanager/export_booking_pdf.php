<?php
ini_set('memory_limit', '-1');
include("access.php");
include("../includes/db.conn.php");
include("../includes/conf.class.php");
include("../includes/admin.class.php");
include('language.php');
include('../vendor/autoload.php');
include('../includes/pdf.class.php');
include('../includes/constant/booking.php');

if (empty($_GET['fromDate']) || empty($_GET['toDate']) || empty($_GET['shortby'])) {
    header('Location: admin-home.php');
    exit;
}

$condition = " and (DATE_FORMAT(" . $_GET['shortby'].", '%Y-%m-%d') between '" . $bsiCore->getMySqlDate($_GET['fromDate']) . "' and '" . $bsiCore->getMySqlDate($_GET['toDate'])."')";
$shortbyarr = [
    "booking_time" => VIEW_ACTIVE_BOOKING_DATE,
    "start_date" => CUSTOMER_BOOKING_CHECK_IN_DATE,
    "end_date"=>CUSTOMER_BOOKING_CHECK_OUT_DATE
];
$text_cond="( " . $_GET['fromDate']."  ".VB_TO." ". $_GET['toDate'] . "  " . VB_BY . " " . $shortbyarr[$_GET['shortby']] . " )";
$book_type = $bsiCore->ClearInput($_GET['book_type']);

$query = $bsiAdminMain->getBookingInfo($book_type, $clientid = 0, $condition);
$resut = $mysqli->query($query);

ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <style>
            body {
                font-family: "Garuda" !important;
            }

            .table .title {
                background-color: #eee;
                font-weight: bold;
            }

            .table tr td {
                border-bottom: 0.5px solid #999;
                font-size: 12px;
                padding: 5px;
            }

            .table {
                border: 1px solid #999;
            }

            .page-divider {
                page-break-after: always;
            }

        </style>
    </head>
    <body>

        <h2 style="padding: 0; margin: 0;">Booking from <?php echo $_GET['fromDate'] . ' - ' . $_GET['toDate'] ?></h2>
        <p style="padding: 0; margin: 0;">Status: <?php echo BOOKING_TYPE[$_GET['book_type']] ?></p>
        <hr/>

        <?php foreach ($resut->fetch_all(MYSQLI_ASSOC) as $booking) : ?>

            <?php
            $invoiceQuery = $mysqli->query("select * from bsi_invoice where booking_id=". $bsiCore->ClearInput($booking['booking_id']));
            $invoice = $invoiceQuery->fetch_assoc();

            $bookingQuery = $mysqli->query("select * from bsi_bookings where booking_id=".$bsiCore->ClearInput($booking['booking_id']));
            $booking = $bookingQuery->fetch_assoc();

            $customerQuery = $mysqli->query("select bc.*, bb.* from bsi_bookings as bb, bsi_clients as bc where  bb.client_id=bc.client_id and booking_id=" . $booking['booking_id']);
            $customer = $customerQuery->fetch_assoc();

            $paymentgateway = $bsiAdminMain->getPayment_Gateway($booking['payment_type']);

            ?>

            <?php
                $curdate = date('Y-m-d');
                $status = [
                    'color' => 'grey',
                    'message' => 'UNKNOW'
                ];

                if ($customer['is_deleted'] == 0 && $customer['end_date'] < $curdate ) {
                    $status = [
                        'color' => 'blue',
                        'message' => 'DEPARTED'
                    ];
                } elseif ($customer['is_deleted'] == 0 && $customer['end_date'] >= $curdate) {
                    $status = [
                        'color' => 'green',
                        'message' => 'ACTIVE'
                    ];
                } elseif ($customer['is_deleted'] == 1) {
                    $status = [
                        'color' => 'red',
                        'message' => 'CANCELLED'
                    ];
                }
            ?>
            <p style="text-align: right;">Booking Status: <span style="color:<?php echo $status['color'] ?>"><?php echo $status['message'] ?></span></p>
            <div align="center">
                <?php echo preg_replace(['/Geneva/'], ['Garuda'], $invoice['invoice']); ?>
                <br/>
                <table class="table" cellpadding="0" cellspacing="0" width="700">
                    <tr>
                        <td style="font-weight: bold" colspan="2">Customer Details</td>
                    </tr>
                    <tr>
                        <td class="title" style="width: 20%"><strong>Name:</strong></td>
                        <td><?php echo $customer['title'] . $customer['first_name'] . ' ' . $customer['surname'] ?>
                    </tr>
                    <tr>
                        <td class="title"><strong>Address:</strong></td>
                        <td><?php echo $customer['street_addr'] ?></td>
                    </tr>
                    <tr>
                        <td class="title"><strong>City:</strong></td>
                        <td><?php echo $customer['city'] ?></td>
                    </tr>
                    <tr>
                        <td class="title"><strong>State:</strong></td>
                        <td><?php echo $customer['province'] ?></td>
                    </tr>
                    <tr>
                        <td class="title"><strong>Country:</strong></td>
                        <td><?php echo $customer['country'] ?></td>
                    </tr>
                    <tr>
                        <td class="title"><strong>Zip / Post code:</strong></td>
                        <td><?php echo $customer['zip'] ?></td>
                    </tr>
                    <tr>
                        <td class="title"><strong>Phone:</strong></td>
                        <td><?php echo $customer['phone'] ?></td>
                    </tr>
                    <tr>
                        <td class="title"><strong>Fax:</strong></td>
                        <td><?php echo $customer['fax'] ?></td>
                    </tr>
                    <tr>
                        <td class="title"><strong>Email:</strong></td>
                        <td><?php echo $customer['email'] ?></td>
                    </tr>
                    <tr>
                        <td class="title"><strong>Identity Type:</strong></td>
                        <td><?php echo $customer['id_type'] ?></td>
                    </tr>
                    <tr>
                        <td class="title"><strong>Identity Number:</strong></td>
                        <td><?php echo $customer['id_number'] ?></td>
                    </tr>
                    <tr>
                        <td class="title"><strong>Additional Requests:</strong></td>
                        <td><?php echo $customer['additional_comments'] ?></td>
                    </tr>
                </table>
            </div>


        <div class="page-divider"></div>
        <?php endforeach; ?>
    </body>
</html>
<?php
$content = ob_get_contents();
ob_end_clean();

$pdfname = 'booking-' . date('Y_m_d', strtotime($_GET['fromDate'])) . '-' . date('Y_m_d', strtotime($_GET['toDate'])) . '-' . str_replace(' ', '', strtolower(BOOKING_TYPE[$_GET['book_type']])) . '.pdf';

$pdf = new PDF();
$pdf->setHTML($content)
    ->generate($pdfname);
