<?php
    include 'access.php';
    include 'header.php';
    include '../includes/models/user.php';
    $user = new User();
?>
<div id="container-inside">
    <span style="font-size:16px; font-weight:bold"><?php trans('USER_LIST') ?></span>

    <input type="button" value="<?php trans('ADD_NEW_USER') ?>" onClick="window.location.href='add_edit_user.php'" style="background: #EFEFEF; float:right"/>
    <hr />

    <table class="display datatable" border="0">
        <thead>
            <tr>
                <th width="5%">ID</th>
                <th width="20%"><?php trans('EMAIL') ?></th>
                <th width="15%"><?php trans('USERNAME') ?></th>
                <th width="20%"><?php trans('NAME') ?></th>
                <th width="20%"><?php trans('LAST_LOGIN') ?></th>
                <th width="10%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($user->list() as $user): ?>
                <tr>
                    <td><?php echo $user['id']; ?></td>
                    <td><?php echo $user['email']; ?></td>
                    <td><?php echo $user['username']; ?></td>
                    <td><?php echo $user['f_name'] . ' ' . $user['l_name']; ?></td>
                    <td><?php echo $user['last_login']; ?></td>
                    <td><a href="add_edit_user.php?id=<?php echo $user['id']; ?>"><?php trans('EDIT') ?></a></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script type="text/javascript" src="js/DataTables/jquery.dataTables.js"></script>
<script>
 $(document).ready(function() {
	 	var oTable = $('.datatable').dataTable( {
				"bJQueryUI": true,
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[0,'desc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
					"sSearch": "<?=DT_SEARCH; ?>:",
					"sInfo": "<?=DT_SINFO1; ?> _START_ <?=DT_SINFO2; ?> _END_ <?=DT_SINFO3; ?> _TOTAL_ <?=DT_SINFO4; ?>",
					"sInfoEmpty": "<?=DT_INFOEMPTY; ?>",
					"sZeroRecords": "<?=DT_ZERORECORD; ?>",
					"sInfoFiltered": "(<?=DT_FILTER1; ?> _MAX_ <?=DT_FILTER2; ?>)",
					"sEmptyTable": "<?=DT_EMPTYTABLE; ?>",
					"sLengthMenu": "<?=DT_LMENU; ?> _MENU_ <?=DT_SINFO4; ?>",
					"oPaginate": {
						"sFirst":    "<?=DT_FIRST; ?>",
						"sPrevious": "<?=DT_PREV; ?>",
						"sNext":     "<?=DT_NEXT;?>",
						"sLast":     "<?=DT_LAST;?>"
                    }
				 }
	} );
} );
</script>
<script type="text/javascript" src="js/bsi_datatables.js"></script>
<link href="css/data.table.css" rel="stylesheet" type="text/css" />
<link href="css/jqueryui.css" rel="stylesheet" type="text/css" />
<?php include("footer.php"); ?>
