<?php
/**
* @author BestSoft Inc see README.php
* @copyright BestSoft Inc.
* See COPYRIGHT.php for copyright notices and details.
*/
$bsiCore = new bsiHotelCore;
class bsiHotelCore
{
    public $config         = [];
    public $userDateFormat = '';

    public function __construct()
    {
        $this->getBSIConfig();
        $this->getUserDateFormat();
    }

    private function getBSIConfig()
    {
        global $mysqli;

        $sql = $mysqli->query('SELECT conf_id, IFNULL(conf_key, false) AS conf_key, IFNULL(conf_value,false) AS conf_value FROM bsi_configure');

        while ($currentRow = $sql->fetch_assoc()) {
            if ($currentRow['conf_key']) {
                if ($currentRow['conf_value']) {
                    $this->config[trim($currentRow['conf_key'])] = trim($currentRow['conf_value']);
                } else {
                    $this->config[trim($currentRow['conf_key'])] = false;
                }
            }
        }

        $sql->close();
    }

    private function getUserDateFormat()
    {
        $dtformatter          = ['dd' => '%d', 'mm' => '%m', 'yyyy' => '%Y', 'yy' => '%Y'];
        $dtformat             = preg_split('@[/.-]@', $this->config['conf_dateformat']);
        $dtseparator          = ('yyyy' === $dtformat[0]) ? substr($this->config['conf_dateformat'], 4, 1) : substr($this->config['conf_dateformat'], 2, 1);
        $this->userDateFormat = $dtformatter[$dtformat[0]] . $dtseparator . $dtformatter[$dtformat[1]] . $dtseparator . $dtformatter[$dtformat[2]];
    }

    public function getMySqlDate($date)
    {
        if ('' == $date) {
            return '';
        }

        $dateformatter = preg_split('@[/.-]@', $this->config['conf_dateformat']);
        $date_part     = preg_split('@[/.-]@', $date);
        $date_array    = [];

        for ($i=0; $i < 3; ++$i) {
            $date_array[$dateformatter[$i]] = $date_part[$i];
        }

        return $date_array['yy'] . '-' . $date_array['mm'] . '-' . $date_array['dd'];
    }

    public function ClearInput($dirty)
    {
        global $mysqli;

        $dirty = $mysqli->real_escape_string($dirty);

        return $dirty;
    }

    public function capacitycombo()
    {
        global $mysqli;

        $chtml = '<select id="capacity" name="capacity" class="input-medium">';
        $sql   =$mysqli->query('SELECT Max(capacity) as capa FROM bsi_capacity WHERE `id` IN (SELECT DISTINCT (capacity_id) FROM bsi_room) ORDER BY capacity');

        $capacityrow = $sql->fetch_assoc();

        for ($i=1; $i <= $capacityrow['capa']; ++$i) {
            $chtml .= '<option value="' . $i . '">' . $i . '</option>';
        }

        $chtml .= '</select>';

        return $chtml;
    }

    public function clearExpiredBookings()
    {
        global $mysqli;

        $sql = $mysqli->query('SELECT booking_id FROM bsi_bookings WHERE payment_success = false AND ((NOW() - booking_time) > ' . intval($this->config['conf_booking_exptime']) . ' )');

        while ($currentRow = $sql->fetch_assoc()) {
            $mysqli->query("DELETE FROM bsi_invoice WHERE booking_id = '" . $currentRow['booking_id'] . "'");
            $mysqli->query("DELETE FROM bsi_reservation WHERE bookings_id = '" . $currentRow['booking_id'] . "'");
            $mysqli->query("DELETE FROM bsi_bookings WHERE booking_id = '" . $currentRow['booking_id'] . "'");
        }

        $sql->close();
    }

    public function loadPaymentGateways()
    {
        global $mysqli;

        $paymentGateways = [];
        $sql             = $mysqli->query('SELECT * FROM bsi_payment_gateway where enabled=true');

        while ($currentRow = $sql->fetch_assoc()) {
            $paymentGateways[$currentRow['gateway_code']] = ['name' => $currentRow['gateway_name'], 'account' => $currentRow['account']];
        }

        $sql->close();

        return $paymentGateways;
    }

    public function encryptCard($data)
    {
        $key = $this->config['conf_encrypt_key'];
        // Remove the base64 encoding from our key
        $encryption_key = base64_decode($key);
        // Generate an initialization vector
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
        $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
        // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
        return base64_encode($encrypted . '::' . $iv);
    }

    public function decryptCard($data)
    {
        // Remove the base64 encoding from our key

        $key            = $this->config['conf_encrypt_key'];
        $encryption_key = base64_decode($key);
        // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
        list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);

        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }

    public function paymentGateway($code)
    {
        global $mysqli;

        $sql = $mysqli->query("SELECT gateway_name FROM bsi_payment_gateway where gateway_code='" . $code . "'");
        $row = $sql->fetch_assoc();

        return  $row['gateway_name'];
    }

    public function getInvoiceinfo($bid)
    {
        global $mysqli;
        $sql       	= $mysqli->query("select * from bsi_invoice where booking_id='" . $bid . "'");
        $invoiceres = $sql->fetch_assoc();

        return $invoiceres['invoice'];
    }

    public function paymentGatewayName($gcode)
    {
        global $mysqli;

        $sql = $mysqli->query("select gateway_name from bsi_payment_gateway where gateway_code='" . $gcode . "'");
        $row = $sql->fetch_assoc();

        return $row[0];
    }

    public function getChildcombo()
    {
        global $mysqli;

        $child_res=$mysqli->query('SELECT max(`no_of_child`) as mchild FROM `bsi_room`');
        $rowchild =$child_res->fetch_assoc();
        $childhtml='';

        if ($rowchild['mchild']) {
            $childhtml .= '<div class="control-group">
                            <label class="control-label" for="checkInDate">' . CHILD_PER_ROOM_TEXT . ':</label>
                            <div class="controls">
                            	<select class="input-medium" id="child_per_room" name="child_per_room"><option value="0" selected>' . NONE_TEXT . '</option>';

            for ($k=1; $k <= $rowchild['mchild']; ++$k) {
                $childhtml .= '<option value="' . $k . '">' . $k . '</option>';
            }

            $childhtml .= ' </select></div></div>';
        } else {
            $childhtml ='<input type="hidden" name="child_per_room" value="0"/>';
        }

        return $childhtml;
    }

    public function getExchangemoney_update()
    {
        global $mysqli;

        $sql      = $mysqli->query('select * from bsi_currency where default_c = 0');
        $sql1     = $mysqli->query('select * from bsi_currency where default_c = 1');
        $default2 = $sql1->fetch_assoc();

        while ($row = $sql->fetch_assoc()) {
            $amount            = 1;
            $amount            = urlencode($amount);
            $from_Currency     = urlencode($default2['currency_code']);
            $to_Currency       = urlencode($row['currency_code']);
            $converted_currency=$this->currencyConverter($from_Currency, $to_Currency, $amount);
            //return round($var,3);
            $mysqli->query("update bsi_currency set  exchange_rate ='" . $converted_currency . "' where currency_code='" . $row['currency_code'] . "'");
        }
    }

    public function currencyConverter($from_Currency, $to_Currency, $amount)
    {
        $from_Currency      = urlencode($from_Currency);
        $to_Currency        = urlencode($to_Currency);
        $get                = file_get_contents("https://finance.google.com/finance/converter?a=1&from=$from_Currency&to=$to_Currency");
        $get                = explode('<span class=bld>', $get);
        $get                = explode('</span>', $get[1]);
        $converted_currency = preg_replace("/[^0-9\.]/", null, $get[0]);

        return $converted_currency;
    }

    public function getExchangemoney($amount1, $to_Currency1)
    {
        global $mysqli;

        $sql           = $mysqli->query("select * from bsi_currency where currency_code = '" . $to_Currency1 . "'");
        $row           = $sql->fetch_assoc();
        $exchange_rate = $row['exchange_rate'];
        $amount        = $amount1 * $exchange_rate;

        return number_format($amount, 2);
    }

    public function currency_symbol()
    {
        global $mysqli;

        $sql      = $mysqli->query('select * from bsi_currency where default_c = 1');
        $default2 = $sql->fetch_assoc();

        return $default2['currency_symbl'];
    }

    public function currency_code()
    {
        global $mysqli;

        $sql      = $mysqli->query('select * from bsi_currency where default_c = 1');
        $default2 = $sql->fetch_assoc();

        return $default2['currency_code'];
    }

    public function get_currency_symbol($c_code)
    {
        global $mysqli;

        $sql      = $mysqli->query("select * from bsi_currency where currency_code = '" . $c_code . "'");
        $default2 = $sql->fetch_assoc();

        return $default2['currency_symbl'];
    }

    public function get_currency_combo3($c_code)
    {
        global $mysqli;

        $sql   = $mysqli->query('select * from bsi_currency order by currency_code');
        $combo = '<div class="text-form"> Currency </div>

                            	<select class="form-control" name="currency" id="currency">';

        while ($row = $sql->fetch_assoc()) {
            if ($row['currency_code'] == $c_code) {
                $combo .= '<option value="' . $row['currency_code'] . '"  selected="selected">' . $row['currency_code'] . '</option>';
            } else {
                $combo .= '<option value="' . $row['currency_code'] . '">' . $row['currency_code'] . '</option>';
            }
        }
        $combo .= '  </select>';
        if (1 == $sql->num_rows) {
            $combo='<input type="hidden" name="currency" value="' . $this->currency_code() . '" />';
        }

        return $combo;
    }

    public function get_currency_combo2($c_code)
    {
        global $mysqli;

        $combo = '<select name="currency"  class="input-small" onchange="currency_change(this.value)">';
        $sql   = $mysqli->query('select * from bsi_currency order by currency_code');

        while ($row=$sql->fetch_assoc()) {
            if ($row['currency_code'] == $c_code) {
                $combo .= '<option value="' . $row['currency_code'] . '"  selected="selected">' . $row['currency_code'] . '</option>';
            } else {
                $combo .= '<option value="' . $row['currency_code'] . '">' . $row['currency_code'] . '</option>';
            }
        }

        $combo .= '</select>';

        if (1 == $sql->num_rows) {
            $combo = '';
        }

        return $combo;
    }

    public function exchange_rate_update($type=1)
    {
        global $mysqli;

        if ($type) {
            if ('' != $this->config['conf_currency_update_time']) {
                if (time() > ($this->config['conf_currency_update_time'] + 12 * 3600)) {
                    $this->getExchangemoney_update();
                    $mysqli->query("update bsi_configure set conf_value='" . time() . "' where conf_key='conf_currency_update_time'");
                }
            } else {
                $this->getExchangemoney_update();
                $mysqli->query("update bsi_configure set conf_value='" . time() . "' where conf_key='conf_currency_update_time'");
            }
        } else {
            $this->getExchangemoney_update();
        }
    }

    public function roomtype_photos($rid, $cid)
    {
        global $mysqli;
         $sql=$mysqli->query("select * from bsi_gallery where roomtype_id=".$rid." and capacity_id=".$cid);
         $list_img='';
         $lbox='';
         if($sql->num_rows){
             while($row=$sql->fetch_assoc()){
              $list_img.='<li><a class="group_'.$rid.'_'.$cid.'" href="'. asset('gallery/'.$row['img_path']). '" style="text-decoration:none; " ><img src="'. asset('gallery/thumb_'.$row['img_path']). '" style="border-style: none; width:98%;" /></a></li>';

             }
         }else{
              $list_img.='<li><img src="'. asset('images/no_photo.jpg'). '"></li>';
         }

         return $list_img;
    }

    public function roomtype_photos_only($rid)
    {
        global $mysqli;
         $sql=$mysqli->query("select * from bsi_gallery where roomtype_id=".$rid);
         $list_img='';
         $lbox='';
         if($sql->num_rows){
             while($row=$sql->fetch_assoc()){
              $list_img.='<li><a class="group_'.$rid.'" href="'. asset('gallery/thumb_'.$row['img_path']). '" style="text-decoration:none; " ><img src="'. asset('gallery/thumb_'.$row['img_path']). '" style="margin-top:4px; margin-left:6.5px; height:250px; border-style: none" /></a></li>';

             }
         }else{
              $list_img.='<li><img src="'. asset('images/no_photo.jpg'). '" style="width:97.5%; margin:5px 5px 5px 5px;"></li>';
         }

         return $list_img;
    }

    public function bt_date_format()
    {
        if ('yy-mm-dd' == $this->config['conf_dateformat']) {
            $df='yy' . $this->config['conf_dateformat'];
        } else {
            $df = $this->config['conf_dateformat'] . 'yy';
        }

        return $df;
    }
}
