<div class="visible-xs" id="language-modal-wrapper">
    <div id="language-modal" class="hide">
        <div class="language-selection-container">

            <ul class="flag-container">
                <li>
                    <a onclick="selectLanguage('en')" href="#"><img class="flag" src="<?php echo asset('images/en-64.png') ?>"></a>
                </li>
                <li>
                    <a onclick="selectLanguage('zh-cn')" href="#"><img class="flag" src="<?php echo asset('images/cn-64.png') ?>"></a>
                </li>
                <li>
                    <a onclick="selectLanguage('th')" href="#"><img class="flag" src="<?php echo asset('images/th-64.png') ?>"></a>
                </li>
            </ul>
        </div>
    </div>
</div>
<script>
    function selectLanguage(language) {
        window.localStorage.setItem('mobile_language', language);
        window.location.href = '?lang=' + language;
    }

    window.addEventListener('load', function() {
        if (!window.localStorage.getItem('mobile_language')) {
            var modalWrapper = document.getElementById('language-modal');
            modalWrapper.classList.remove('hide')
        }
    })
</script>
