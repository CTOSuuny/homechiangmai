<?php
session_start();
include("../includes/db.conn.php");
include("../includes/conf.class.php");
include("../language.php");
http_response_code($code);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $h1Text; ?></title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Playfair+Display" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider.css') ?>" />
<!-- <link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider-customize.css') ?>"> -->
<link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-customize.css') ?>">

</head>
<body>
	<div class="container-fluid">
        <?php
			include("../modules/inner-error.php");
		?>
        <div class="error-page">
            <h1 align="center"><?php echo $h1Text; ?></h1>
            <br>
            <p style="font-size: 20px;" class="text-center">Please try again</p>
        </div>
		<?php
			include("../modules/footer.php");
		?>
	</div>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo asset('js/modernizr-2.6.2.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo asset('js/jquery-zoomslider.min.js') ?>"></script>
</body>
</html>
