<?php
    include("includes/db.conn.php");
    include("includes/conf.class.php");
    // include("includes/room.class.php");
    include('includes/csrf.class.php');
    // include('includes/mail.class.php');
    // include('includes/smtp-mail.class.php');

    $bsiCore->exchange_rate_update();
?>
<?php
  include("language_set.php");
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Home chiang mai Hotel</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Rubik:300,400,700" rel="stylesheet">
    <link rel="icon" href="favicon.ico" sizes="16x16 32x32" type="image/png">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    
    <link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>

  <!-- Core CSS file -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.css"> 

  <!-- Skin CSS file (styling of UI - buttons, caption, etc.)
       In the folder of skin CSS file there are also:
       - .png and .svg icons sprite, 
       - preloader.gif (for browsers that do not support CSS animations) -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/default-skin/default-skin.css"> 

  <!-- Core JS file -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.js"></script> 

  <!-- UI JS file -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe-ui-default.js"></script> 

    <!-- Theme Style -->
    <link rel="stylesheet" href="css/style.css">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
     <script>
       function onSubmit(token) {
         document.getElementById("demo-form").submit();
       }
     </script>

    <style>
      .right-side::-webkit-scrollbar { 
                display: none; 
            } 
    /* Style the Image Used to Trigger the Modal */
#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
      z-index: 999;
}

/* Modal Content (Image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image (Image Text) - Same Width as the Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation - Zoom in the Modal */
.modal-content, #caption { 
  animation-name: zoom;
  animation-duration: 0.6s;
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}

      #mixedSlider {
        position: relative;
      }
      #mixedSlider .MS-content {
        white-space: nowrap;
        overflow: hidden;
        margin: 0 5%;
      }
      #mixedSlider .MS-content .item {
        display: inline-block;
        width: 50%;
        position: relative;
        vertical-align: top;
        overflow: hidden;
        height: 100%;
        white-space: normal;
        padding: 0 10px;
      }
      /*@media (max-width: 991px) {
        #mixedSlider .MS-content .item {
          width: 50%;
        }
      }*/
      @media (max-width: 767px) {
        #mixedSlider .MS-content .item {
          width: 100%;
        }
      }
      #mixedSlider .MS-controls button {
        position: absolute;
        border: none;
        background-color: transparent;
        outline: 0;
        font-size: 50px;
        top: 95px;
        color: rgba(0, 0, 0, 0.4);
        transition: 0.15s linear;
      }
      #mixedSlider .MS-controls button:hover {
        color: rgba(0, 0, 0, 0.8);
      }
      @media (max-width: 992px) {
        #mixedSlider .MS-controls button {
          font-size: 30px;
        }
      }
      @media (max-width: 767px) {
        #mixedSlider .MS-controls button {
          font-size: 20px;
        }
      }
      #mixedSlider .MS-controls .MS-left {
        left: 0px;
      }
      @media (max-width: 767px) {
        #mixedSlider .MS-controls .MS-left {
          left: -10px;
        }
      }
      #mixedSlider .MS-controls .MS-right {
        right: 0px;
      }
      @media (max-width: 767px) {
        #mixedSlider .MS-controls .MS-right {
          right: -10px;
        }
      }

      @media (max-width: 767px) {
        
        .site-footer .container{
          margin-top: 455vh !important;
        }

        .container2{
          left: 35px;
        }
  
      }
      .font-ss{
        font-weight: bold;
        font-size: 15px;
      }
      .paddwe{
        padding: 20px 0;
      }
    </style>
  </head>
  <body>

  <!-- Sidebar -->
  <div class="w3-sidebar w3-bar-block w3-animate-left" style="display:none;z-index: 4;" id="mySidebar">
    <a href="#" class="w3-bar-item w3-button" style="margin-top: 100px;"></a>
    <a href="index.php" class="w3-bar-item w3-button"><?php echo $manu["home"]; ?></a>
    <a class="w3-bar-item w3-button" href="abouts.php"><?php echo $manu["about"]; ?></a>
    <a class="w3-bar-item w3-button" href="room.php"><?php echo $manu["room"]; ?></a>
    <a class="w3-bar-item w3-button" href="gallery.php"><?php echo $manu["gallery"]; ?></a>
    <a class="w3-bar-item w3-button" href="bar.php"><?php echo $manu["bar"]; ?></a>
    
    <a class="w3-bar-item w3-button" href="workwithus.php"><?php echo $manu["work"]; ?></a>
    <a class="w3-bar-item w3-button" href="calendar.php"><?php echo $manu["event"]; ?></a>
    <a class="w3-bar-item w3-button" href="contacts.php"><?php echo $manu["contact"]; ?></a>    
    <div class="w3-row" style="width: 30%;margin-left: 5%;padding-top: 50px;">
      <div class="w3-third">
        <div><img src="/images/thai.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=thai'"></div>
      </div>
      <div class="w3-third">
        <div><img src="/images/engv.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=eng'"></div>
      </div>
      <div class="w3-third">
        <div><img src="/images/chinav.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=china'"></div>
      </div>
    </div>


    <!-- <a class="w3-bar-item w3-button" href=""><span>Book Now</span></a> -->
  </div>
    
    <header role="banner" style="position: fixed;">
     
      <nav class="navbar2 navbar-expand-md navbar-dark bg-light" style="height: 100px;background-color: #fff !important">
        <div class="container">
          <a href="index.php"><img src="images/logo2.jpg" class="logox logo2" id="logo"></a>
          <div>
            
            <!-- <ul class="navbar-nav ml-auto pl-lg-5 pl-0 activate-nav">
              <li class="nav-item">
                <a class="nav-link active" href="index.html">Home</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="rooms.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Rooms</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="rooms.html">Room Videos</a>
                  <a class="dropdown-item" href="rooms.html">Presidential Room</a>
                  <a class="dropdown-item" href="rooms.html">Luxury Room</a>
                  <a class="dropdown-item" href="rooms.html">Deluxe Room</a>
                </div>

              </li>
              <li class="nav-item">
                <a class="nav-link" href="">Event</a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="">Contact</a>
              </li>

               <li class="nav-item cta">
                <a class="nav-link" href=""><span>Book Now</span></a>
              </li>
            </ul> -->


            <!-- <a id="trigger" ></a> -->
            
            <!-- <button class="w3-button w3-xlarge" id="trigger" onclick="w3_open()"></button> -->
            <div class="container2" onclick="myFunction(this)">
              <div class="bar1 bar1-new" id="bar1"></div>
              <div class="bar3 bar3-new" id="bar3"></div>
            </div>
          </div>
        </div>
        <div class="book">
          <a class="a2"  id="book" href="booking.php"><?php echo $manu["book"]; ?></a>
        </div>
      </nav>
    </header>
    <!-- END header -->
    
    <div class="contact">    
      <div class="left-side">
        <div class="top">
          <div class="mapouter"><div class="gmap_canvas"><iframe style="width: 100%;height: 100%;" id="gmap_canvas" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1888.483025462506!2d98.9752695581786!3d18.799666235929227!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30da3bbde8e0b157%3A0xff5cc8bb7062ff4e!2sHome+Chiang+Mai+Hotel!5e0!3m2!1sen!2sth!4v1550227689365" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.pureblack.de">internetagentur</a></div><style>.mapouter{text-align:right;height:100%;width:100%}.gmap_canvas {overflow:hidden;background:none!important;height:100%;width:100%;}</style></div>
          
        </div>
        <div class="bot">
          <h3 class="middle"><a href="index.html"> <?php echo $lang["bh"]; ?> </a></h3>
        </div>
      </div>
      <div class="right-side">
        <div class="text">
          <h3><?php echo $lang["Header_1"]; ?></h3>
          <p><?php echo $lang["content_1"]; ?></p>

<h3><?php echo $lang["Header_2"]; ?></h3>

<div class="w3-row">
  <div>
    
      <p><?php echo $lang["content_2"]; ?></p>
    
  </div>
</div>

<br>

<!-- <h3>RESERVATIONS</h3>

<p>It’s here online where you’ll always find our best rates. Our website rates are £10 cheaper than on the phone so keep it on the web for the best price.</p>

<p>Telephone: +66(0)53 218 856</p>

<p>RESERVATIONS: book.shoreditch@thehox.com</p>

<p>Restaurant Enquiries: eat.shoreditch@thehox.com</p>

<p>Meetings, Events and Parties: events.shoreditch@thehox.com</p>

<p>Groups: groups.shoreditch@thehox.com</p>

<p>Corporate Bedrooms Enquiries: sales.shoreditch@thehox.com</p>

<p>General: hello.shoreditch@thehox.com</p>

<p>Press: inthepapers@thehox.com</p> -->

<p><strong style="font-weight: 700"><?php echo $lang["contact_header"]; ?></strong></p>

<form method="post" action="/sendmail.php" id='demo-form'>
  <?php echo CSRF::tokenField() ?>
  <div class="w3-row">
    <div class="w3-col s6 pad">
        <p class="sleft"><?php echo $lang["contact_1"]; ?></p>
        <input class="fullwidth" type="text" name="firstname">
    </div>
    <div class="w3-col s6 pad">
      <p class="sleft"><?php echo $lang["contact_2"]; ?></p>
      <input class="fullwidth" type="text" name="surnale">
    </div>
  </div> 
  <div class="w3-row">
    <div class="w3-col s6 pad">
        <p class="sleft"><?php echo $lang["contact_3"]; ?></p>
        <input class="fullwidth" type="text" name="telephone">
    </div>
    <div class="w3-col s6 pad">
      <p class="sleft"><?php echo $lang["contact_4"]; ?></p>
      <input class="fullwidth" type="text" name="email">
    </div>
  </div> 
  <div class="w3-row">
    <div class="w3-col s6 pad">
        <p class="sleft"><?php echo $lang["contact_5"]; ?></p>
        <input class="fullwidth" type="text" name="type">
    </div>
    <div class="w3-col s6 pad">
      <p class="sleft"><?php echo $lang["contact_6"]; ?></p>
      <input class="fullwidth" type="text" name="abuot">
    </div>
  </div> 
  <div class="w3-row">
    <div class="w3-col s12 pad">
        <p class="sleft"><?php echo $lang["contact_7"]; ?></p>
        <textarea class="fullwidth" type="text" name="message" style="height: 90px;"></textarea>
    </div>
  </div>  
  <input type="submit" name="" class="g-recaptcha" data-sitekey="6LesvpEUAAAAAM4eUzES2phg_q01oNkQjxDDijzL" data-callback='onSubmit' style="background-color: #353535;

    color: #fff;
    width: 45%;
    height: 50px;
    border-style: none;
    text-transform: uppercase;
    font-size: 14px;
    font-family: BrownRegular;
    margin-top: 10px;
    border: 1px solid #979797;">
</form>

        </div>
      </div>
    </div>
    

    
    <!-- END section -->
   
    <footer class="site-footer">
      <div class="container" style="margin-top: 95vh">
        <div class="row">
          <div class="col-md-4">
                    <h3>Reservations</h3>
                    <p class="lead"><a href="tel://+ 66 53 218 856">+ 66 53 218 856</a></p>
          </div>
          <div class="col-md-4">
            <h3>Connect With Us</h3>
            <p>We are socialized. Follow us</p>
            <p>
              <a href="https://www.facebook.com/HomeChiangMaiHotel/" class="pl-0 p-3"><span class="fa fa-facebook"></span></a>
              <a href="https://www.instagram.com/homechiangmaihotel/" class="p-3"><span class="fa fa-instagram"></span></a>
            </p>
          </div>
          <div class="col-md-4">
            <h3>Connect With Us</h3>
            <p>
              <a href="https://www.google.com/maps/place/'@homechiangmai'/@18.8029473,98.9800778,18z/data=!4m5!3m4!1s0x30da3a9296803ec7:0xada6e29a86472be!8m2!3d18.8031711!4d98.9807778?hl=en">3 Rasemeechan Alle, Sermsuk Rd. <br>Chang Phueak, Chiang Mai, Thailand 50300</a><br>
              Phone:+66(0)53 218 856<br>
              Email:gsa@homechiangmaihotel.com<br>
              Fax:+66(0)53 218 856<br>
Website: www.homechiangmaihotel.com
            </p>
            <form action="#" class="subscribe">
              <div class="form-group">
                <button type="submit"><span class="ion-ios-arrow-thin-right"></span></button>
                <input type="email" class="form-control" placeholder="Enter Email">
              </div>
              
            </form>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-7 text-center">
            &copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | 
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </div>
        </div>
      </div>
    </footer>
    <!-- END footer -->


    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/jquery-migrate-3.0.0.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>

    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>

    <script src="js/main2.js"></script>
    <script src="js/multislider.js"></script>
    <script type="text/javascript" src="slick/slick.min.js"></script>
    <script>
      $('#mixedSlider').multislider({
      duration: 750,
      interval: 3000
    });

      function myFunction(x) {
      var chklogo = document.getElementById("logo");
      if (chklogo.style.display === 'none') {
        document.getElementById("logo").style.display = "block"
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("book").style.color = "white";

      } else {
        document.getElementById("logo").style.display = "none"
        document.getElementById("mySidebar").style.width = "100%";
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("book").style.color = "black";

      }
      x.classList.toggle("change");
    }


    // Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg1');
var img2 = document.getElementById('myImg2');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}
img2.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}

    </script>
  </body>
</html>