<?php

class Migration
{
    private $conn;

    public function __construct()
    {
        $this->conn  = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);

        if (mysqli_connect_errno()) {
            printf("Connect failed: %s", mysqli_connect_error());
            exit();
        }
    }

    /**
     * Get connection
     *
     * @return \Mysqli
     */
    public function getConnection()
    {
        return $this->conn;
    }

    /**
     * Check add column
     *
     * @param string $table table name
     * @param string  $column columns name
     *
     * @return boolean
     */
    public function hasColumn(string $table, string $column)
    {
        $query = $this->conn->query("SHOW COLUMNS FROM $table");
        $columns = array_map(function ($column) {
            return $column[0];
        }, $query->fetch_all());

        return in_array($column, $columns);
    }

    /**
     * Add column
     *
     * @param string  $table        table name
     * @param string  $column       colomn name
     * @param string  $type         column type
     * @param string  $defaultValue default value
     * @param boolean $isNullable   flag allow nullable
     *
     * @return \Mysqli
     */
    public function addColumn(string $table, string $column, string $type, $defaultValue = "null", $isNullable = true)
    {
        return $this->conn->query("ALTER TABLE $table ADD COLUMN $column $type DEFAULT $defaultValue" . ($isNullable ? ' NULL' : ''));
    }

    public function valueExists(string $table, string $column, $value)
    {
        $query = $this->conn->query("SELECT $column FROM $table WHERE $column = '$value'");

        return !empty($query->fetch_assoc());
    }
}
