<link rel="stylesheet" href="<?php echo asset('css/smoothslides.theme.css') ?>">

<div class="row show-gallery-dialog room-modules">

        <?php

            $gallery = [
                'images/8.JPG',
                'images/swimming.jpg',
                'images/gallery/1517455683411.jpg',
                'images/gallery/1517455688645.jpg',
                'images/gallery/1517455690547.jpg',
                'images/gallery/1517455697323.jpg',
                'images/gallery/1517455699423.jpg',
                'images/gallery/1517455704313.jpg',
                'images/gallery/1517455706214.jpg',
                'images/gallery/1517826646802.jpg',
                'images/gallery/1517826678742.jpg',
                'images/gallery/1517826682432.jpg',
                'images/gallery/1517826688216.jpg',
                'images/gallery/1517826694031.jpg',
                'images/gallery/1517885259129.jpg',
                'images/gallery/1517885255106.jpg',
                'images/gallery/1517885246709.jpg',
                'images/gallery/1517885880800.jpg',
                'images/gallery/1517885857685.jpg',
                'images/gallery/1517885855654.jpg',
                'images/gallery/1517885855654.jpg',
                'images/gallery/1517885851477.jpg',
                'images/gallery/1517887668156.jpg',
                'images/gallery/1517887663603.jpg',
                'images/gallery/1517887658335.jpg',
                'images/gallery/1517887648767.jpg',
            ];
        ?>

        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 1em;">
                <p></p>
                <div class="smoothslides gallery-stage" id="gallery-stage">
                <?php foreach ($gallery as $img): ?>
                    <a class="hotel-gallery" href="<?php echo asset($img) ?>">
                        <div class="gallery-item" style="background-image: url('<?php echo asset($img) ?>')" > </div>
                    </a>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
</div>
<script src="<?php echo asset('js/smoothslides-2.1.0.js') ?>"></script>
<script src="<?php echo asset('js/helpers/gallery.js') ?>"></script>
<script>
    var gallery = new Gallery('#gallery-stage');
    gallery.init();
</script>
<script>
    $(function() {
        var widthScreen = $( document ).width();

        var colorboxOption = {
            rel: 'group',
            width: (widthScreen <= 767 ? 90 : 60) + '%',
            fixed: true
        }

        if (widthScreen > 767) {
            colorboxOption.height = '90%';
        }

        $('.hotel-gallery').colorbox($.extend(colorboxOption, {rel: 'group-hotel'}));

        $(document).on('cbox_complete', function() {

            var nav = [
                '<div class="nav-gallery" id="nav-right"><i class="fa fa-chevron-right"></i></div>',
                '<div class="nav-gallery" id="nav-left"><i class="fa fa-chevron-left"></i></div>'
            ];

            $('#cboxLoadedContent').append(nav.join(''));
            $('#nav-right').on('click', function() {
                $('.hotel-gallery').colorbox.next()
            });
            $('#nav-left').on('click', function() {
                $('.hotel-gallery').colorbox.prev()
            })
        });

    });
</script>
