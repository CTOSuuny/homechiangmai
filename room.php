<?php
  include("language_set.php");
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Home chiang mai Hotel</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="favicon.ico" sizes="16x16 32x32" type="image/png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Rubik:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
    <!-- Theme Style -->
    <link rel="stylesheet" href="css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
    $(document).ready(function(){
      // Add smooth scrolling to all links
      $("#room-link1").click(function() {
          $('html,body').animate({
              scrollTop: $("#s1").offset().top-200},
              'slow');
      });
      $("#room-link2").click(function() {
          $('html,body').animate({
              scrollTop: $("#s2").offset().top-200},
              'slow');
      });
      $("#room-link3").click(function() {
          $('html,body').animate({
              scrollTop: $("#s3").offset().top-200},
              'slow');
      });
      $("#room-link4").click(function() {
          $('html,body').animate({
              scrollTop: $("#s4").offset().top-200},
              'slow');
      });
      $("#room-link5").click(function() {
          $('html,body').animate({
              scrollTop: $("#s5").offset().top-200},
              'slow');
      });
      // $("#room-link4").click(function() {
      //     $('html,body').animate({
      //         scrollTop: $("#s4").offset().top-200},
      //         'slow');
      // });
      // Add smooth scrolling to all links
      
    });

    $(document).on('scroll', function() {

        var top=$(window).scrollTop();
        const mq = window.matchMedia( "(min-width: 991.89px)" );

        if($(this).scrollTop()>=$('#s1').position().top){

          if($(this).scrollTop()>=$('#s1').position().top){
              $('#room-link1').removeClass('inactive');
              $('#room-link2').addClass('inactive');
              $('#room-link3').addClass('inactive');
              $('#room-link4').addClass('inactive');
              $('#room-link5').addClass('inactive');
              
          }else{
            $('#room-link1').addClass('inactive');
          }

          if($(this).scrollTop()>=$('#s2').position().top){
              $('#room-link2').removeClass('inactive');
              $('#room-link1').addClass('inactive');
              $('#room-link3').addClass('inactive');
              $('#room-link4').addClass('inactive');
              $('#room-link5').addClass('inactive');
              
          }else{
            $('#room-link2').addClass('inactive');
          }

          if($(this).scrollTop()>=$('#s3').position().top){
              $('#room-link3').removeClass('inactive');
              $('#room-link1').addClass('inactive');
              $('#room-link2').addClass('inactive');
              $('#room-link4').addClass('inactive');
              $('#room-link5').addClass('inactive');
              
          }else{
            $('#room-link3').addClass('inactive');
          }

          if($(this).scrollTop()>=$('#s4').position().top){
              $('#room-link4').removeClass('inactive');
              $('#room-link1').addClass('inactive');
              $('#room-link2').addClass('inactive');
              $('#room-link3').addClass('inactive');
              $('#room-link5').addClass('inactive');
              
          }else{
            $('#room-link4').addClass('inactive');
          }

          if($(this).scrollTop()>=$('#s5').position().top){
              $('#room-link5').removeClass('inactive');
              $('#room-link1').addClass('inactive');
              $('#room-link2').addClass('inactive');
              $('#room-link3').addClass('inactive');
              $('#room-link4').addClass('inactive');
              
          }else{
            $('#room-link5').addClass('inactive');
          }

          // if($(this).scrollTop()>=$('#s4').position().top){
          //     $('#room-link4').removeClass('inactive');
          //     $('#room-link1').addClass('inactive');
          //     $('#room-link2').addClass('inactive');
          //     $('#room-link3').addClass('inactive');
          // }else{
          //   $('#room-link4').addClass('inactive');
          // }

        }else{
          $('#room-link1').removeClass('inactive');
          $('#room-link2').removeClass('inactive');
          $('#room-link3').removeClass('inactive');
          $('#room-link4').removeClass('inactive');
          $('#room-link5').removeClass('inactive');
        }

      })
    </script>
    <style>
    /* Make the image fully responsive */
    ,#s1,#s2,#s3,#s4{
      margin-bottom: 50px;
    }
    .carousel-inner img {
    width: 100%;
    height: 100%;
    }
    #mixedSlider {
    position: relative;
    }
    #mixedSlider .MS-content {
    white-space: nowrap;
    overflow: hidden;
    margin: 0 5%;
    }
    #mixedSlider .MS-content .item {
    display: inline-block;
    width: 50%;
    position: relative;
    vertical-align: top;
    overflow: hidden;
    height: 100%;
    white-space: normal;
    padding: 0 10px;
    }
    /*@media (max-width: 991px) {
    #mixedSlider .MS-content .item {
    width: 50%;
    }
    }*/
    @media (max-width: 767px) {
    #mixedSlider .MS-content .item {
    width: 100%;
    }
    }
    #mixedSlider .MS-controls button {
    position: absolute;
    border: none;
    background-color: transparent;
    outline: 0;
    font-size: 50px;
    top: 95px;
    color: rgba(0, 0, 0, 0.4);
    transition: 0.15s linear;
    }
    #mixedSlider .MS-controls button:hover {
    color: rgba(0, 0, 0, 0.8);
    }
    @media (max-width: 992px) {
    #mixedSlider .MS-controls button {
    font-size: 30px;
    }
    }
    @media (max-width: 767px) {
    #mixedSlider .MS-controls button {
    font-size: 20px;
    }
    }
    #mixedSlider .MS-controls .MS-left {
    left: 0px;
    }
    @media (max-width: 767px) {
    #mixedSlider .MS-controls .MS-left {
    left: -10px;
    }
    }
    #mixedSlider .MS-controls .MS-right {
    right: 0px;
    }
    @media (max-width: 767px) {
    #mixedSlider .MS-controls .MS-right {
    right: -10px;
    }
    }
    .ava{
      text-decoration: none;
    }
    .ava:hover{
      text-decoration: underline;
    }
    .bef-footer{
      width: 80%;
      margin: 0 auto;
    }
    </style>
  </head>
  <body>
    <!-- Sidebar -->
  <div class="w3-sidebar w3-bar-block w3-animate-left" style="display:none;z-index: 4;" id="mySidebar">
    <a href="#" class="w3-bar-item w3-button" style="margin-top: 100px;"></a>
    <a href="index.php" class="w3-bar-item w3-button"><?php echo $manu["home"]; ?></a>
    <a class="w3-bar-item w3-button" href="abouts.php"><?php echo $manu["about"]; ?></a>
    <a class="w3-bar-item w3-button" href="room.php"><?php echo $manu["room"]; ?></a>
    <a class="w3-bar-item w3-button" href="gallery.php"><?php echo $manu["gallery"]; ?></a>
    <a class="w3-bar-item w3-button" href="bar.php"><?php echo $manu["bar"]; ?></a>
    
    <a class="w3-bar-item w3-button" href="workwithus.php"><?php echo $manu["work"]; ?></a>
    <a class="w3-bar-item w3-button" href="calendar.php"><?php echo $manu["event"]; ?></a>
    <a class="w3-bar-item w3-button" href="contacts.php"><?php echo $manu["contact"]; ?></a>    
    <div class="w3-row" style="width: 30%;margin-left: 5%;padding-top: 50px;">
      <div class="w3-third">
        <div><img src="/images/thai.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=thai'"></div>
      </div>
      <div class="w3-third">
        <div><img src="/images/engv.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=eng'"></div>
      </div>
      <div class="w3-third">
        <div><img src="/images/chinav.png" style="height: 100px;padding: 8px 16px;cursor: pointer;" onclick="window.location.href='/language_switch.php?lang=china'"></div>
      </div>
    </div>


    <!-- <a class="w3-bar-item w3-button" href=""><span>Book Now</span></a> -->
  </div>
    
    <header role="banner" style="position: fixed;">
      
      <nav class="navbar navbar-expand-md navbar-dark bg-light">
        <div class="container">
          <a href="index.php"><img src="images/logo.jpg" class="logo" id="logo"></a>
          <div>
            
            <!-- <ul class="navbar-nav ml-auto pl-lg-5 pl-0 activate-nav">
              <li class="nav-item">
                <a class="nav-link active" href="index.html">Home</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="rooms.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Rooms</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="rooms.html">Room Videos</a>
                  <a class="dropdown-item" href="rooms.html">Presidential Room</a>
                  <a class="dropdown-item" href="rooms.html">Luxury Room</a>
                  <a class="dropdown-item" href="rooms.html">Deluxe Room</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="">Event</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="">Contact</a>
              </li>
              <li class="nav-item cta">
                <a class="nav-link" href=""><span>Book Now</span></a>
              </li>
            </ul> -->
            <!-- <a id="trigger" ></a> -->
            
            <!-- <button class="w3-button w3-xlarge" id="trigger" onclick="w3_open()"></button> -->
            <div class="container1" onclick="myFunction(this)">
              <div class="bar1" id="bar1"></div>
              <div class="bar3" id="bar3"></div>
            </div>
          </div>
        </div>
        <div class="book">
          <a id="book" href="booking.php"><?php echo $manu["book"]; ?></a>
        </div>
      </nav>
    </header>
    <!-- END header -->
    <div class="topping">
      <div class="fullscreen-bg">
                <!-- <video loop muted autoplay class="fullscreen-bg__video" style="  
                      top: 15%;
                      left: 50%;
                      min-width: 100%;
                      min-height: 100%;
                      width: 100%;
                      height: auto;
                      z-index: -100;
                      transform: translate(-50%, -50%);">
                    <source src="video/background.mp4" type="video/mp4">
                </video> -->
                <div style="background-image: url(images/bg3.jpg);width: 100%;height: 100%;position: fixed;">
                              
                </div>
            </div>
    </div>
    <div class="content-container rooms-page">
      <div class="page-content">
        <ul class="rooms-links">
          <li class="room-link" id="room-link1">
            <a data-link="shoebox">
              <!-- <p>
                <span>cozy & comfy</span>
              </p> -->
              <p class="size"><span><?php echo $lang["rt_1"]; ?></span></p>
            </a>
          </li>
          <li class="room-link" id="room-link2">
            <a data-link="cosy">
              <!-- <p>
                <span>Roomy</span>
              </p> -->
              <p class="size"><span><?php echo $lang["rt_2"]; ?></span></p>
            </a>
          </li>
          <li class="room-link" id="room-link3">
            <a data-link="roomy">
              <!-- <p>
                <span>Wabi – Sabi</span>
              </p> -->
              <p class="size"><span><?php echo $lang["rt_3"]; ?></span></p>
            </a>
          </li>
          <li class="room-link" id="room-link4">
            <a data-link="roomy">
              <!-- <p>
                <span>Wabi – Sabi</span>
              </p> -->
              <p class="size"><span><?php echo $lang["rt_4"]; ?></span></p>
            </a>
          </li>
          <li class="room-link" id="room-link5">
            <a data-link="roomy">
              <!-- <p>
                <span>Wabi – Sabi</span>
              </p> -->
              <p class="size"><span><?php echo $lang["rt_5"]; ?></span></p>
            </a>
          </li>
<!--           <li class="room-link" id="room-link4">
            <a data-link="concept">
              <p>
                <span>Extra Large Room</span>
              </p>
              <p class="size"><span>Extra Large</span></p>
            </a>
          </li> -->
        </ul>
        <div class="container">
          <div class="row">
            
            <div class="col-md-4">
              <p class="des"><span><?php echo $lang["it_content"]; ?></span></p>
            </div>
            <div class="col-md-8">
              <img style="width: 100%" src="images/RoomL/IMG_3125-Edit.jpg">
            </div>
          </div>
        </div>
        <div class="room-benefits-section">
<!--           <div class="primary-container">
            <ul class="room-benefits clearfix">
              <li class="room-benefit" style="width: 20%;">
                <img class="svg" src="images/candy.png" height="40" alt="Smallbar with high street prices" />
                <p class="room-benefit-title">
                  REFRESHMENTS WATER
                </p>
                <p class="room-benefit-description">
                  Plus free fresh milk and water
                </p>
              </li>
              <li class="room-benefit" style="width: 20%;">
                <img class="svg" src="images/shopping-bag.png" height="40" alt="A free light breakfast bag" />
                <p class="room-benefit-title">
                  FLAT SCREEN TELEVISION - HAIR DRYER
                </p>
                <p class="room-benefit-description">
                  To keep hunger pangs at bay
                </p>
              </li>
              <li class="room-benefit" style="width: 20%;">
                <img class="svg" src="images/wifi.png" height="40" alt="Free wi-fi and international calls" />
                <p class="room-benefit-title">
                  TOWELS
                </p>
                <p class="room-benefit-description">
                  To anywhere but the moon
                </p>
              </li>
              <li class="room-benefit" style="width: 20%;">
                <img class="svg" src="images/guarantee.png" height="40" alt="Price Match Guarantee" />
                <p class="room-benefit-title">
                  CLOTHING STORAGE
                </p>
                <p class="room-benefit-description">
                  Best rates right here
                </p>
              </li>
              <li class="room-benefit" style="width: 20%;">
                <img class="svg" src="images/alarm-clock.png" height="40" alt="Snooze... Late check out" />
                <p class="room-benefit-title">
                  SANDALS
                </p>
                <p class="room-benefit-description">
                  Just $9 per hour.
                </p>
              </li>
            </ul>
            <ul class="room-benefits clearfix">
              <li class="room-benefit" style="width: 25%;">
                <img class="svg" src="images/candy.png" height="40" alt="Smallbar with high street prices" />
                <p class="room-benefit-title">
                  REFRESHMENTS WATER
                </p>
                <p class="room-benefit-description">
                  Plus free fresh milk and water
                </p>
              </li>
              <li class="room-benefit" style="width: 25%;">
                <img class="svg" src="images/shopping-bag.png" height="40" alt="A free light breakfast bag" />
                <p class="room-benefit-title">
                  FLAT SCREEN TELEVISION - HAIR DRYER
                </p>
                <p class="room-benefit-description">
                  To keep hunger pangs at bay
                </p>
              </li>
              <li class="room-benefit" style="width: 25%;">
                <img class="svg" src="images/wifi.png" height="40" alt="Free wi-fi and international calls" />
                <p class="room-benefit-title">
                  TOWELS
                </p>
                <p class="room-benefit-description">
                  To anywhere but the moon
                </p>
              </li>
              <li class="room-benefit" style="width: 25%;">
                <img class="svg" src="images/guarantee.png" height="40" alt="Price Match Guarantee" />
                <p class="room-benefit-title">
                  CLOTHING STORAGE
                </p>
                <p class="room-benefit-description">
                  Best rates right here
                </p>
              </li>
            </ul>
            <a href="#" class="read-more">Read more</a>
          </div> -->
        </div>
        <br>
        <br>
        <div class="container" id="s1">
          <div class="row" style="    width: 100%;">
            
            <!-- <div class="image-slider visible-sm">
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3466/hoxton_ams_1_2344.jpg?anchor=center&amp;mode=crop&amp;width=900&amp;height=500&amp;rnd=130822256570000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3289/hoxton_ams_1_2306.jpg?anchor=center&amp;mode=crop&amp;width=900&amp;height=500&amp;rnd=130812421680000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3294/hoxton_ams_1_2378.jpg?anchor=center&amp;mode=crop&amp;width=900&amp;height=500&amp;rnd=130812421690000000');"></div>
            </div> -->
            <div class="col-md-4 padder">
              <h3>
              <?php echo $lang["rt_1"]; ?>
              <!-- <span class="from-price">from</span>
              <span class="from-price-price">€159</span> -->
              </h3>
              <div class="room-body-text">
                <p><?php echo $lang["rt1_content"]; ?></p>
              </div>
              <div class="room-feature-content">
                <ul>
                  <li class="clearfix">
                    <img src="/images/Artboard.png" alt="room size"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["rs"]; ?></p>
                      <p class="feature-description">215.28 sq ft</p>
                    </div>
                  </li>
                  <li class="clearfix">
                    <img src="/images/Artboardx.png" alt="mattress size"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["ms"]; ?></p>
                      <p class="feature-description">Queen</p>
                    </div>
                  </li>
                  <li class="clearfix">
                    <img src="/images/Artboarda.png" alt="room capacity"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["rc"]; ?></p>
                      <p class="feature-description">2 Adult</p>
                    </div>
                  </li>
                </ul>
              </div>
              <a href="booking.php" class="ava" data-roomcodes="SHOEBX,,SHOESG">Check Availability</a>
            </div>
            <div class="col-md-8">

              <div id="demo2" class="carousel slide" data-ride="carousel">

              <!-- Indicators -->
              <ul class="carousel-indicators">
                  <?php
                    define("MYSQL_SERVER", "127.0.0.1");
                    define("MYSQL_USER", "root");
                    define("MYSQL_PASSWORD", "luxury@123");
                    define("MYSQL_DATABASE", "content_villa_db");
                    define('MYSQL_PORT', "3306");

                    $mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
                    if (mysqli_connect_errno()) {  printf("Connect failed: %s
                    ", mysqli_connect_error());  exit(); }
                    // echo $id;

                    $myquery = $mysqli->query("select * from bsi_cms_gallery_room where type = 1");
                    $today_event = array();
                    $chk = 0;
                    // $row    = $result->fetch_assoc();

                    $i=0;
                    while ($row = $myquery->fetch_assoc()) {
                  ?>

                    <!-- <img src="gallery/cms/<?php echo $row["img_path"]; ?>" alt="" /> -->
                    <li data-target="#demo2" data-slide-to="<?php echo $i; ?>"></li>

                  <?php
                    $i++; 
                    }
                  ?>

              </ul>
              
              <div class="carousel-inner" style="max-height: 450px;">
<!--                 <div class="carousel-item active">
                  <img src="images/RoomS/IMG_3225-Pano-2.jpg">
                </div> -->
                  <?php
                    define("MYSQL_SERVER", "127.0.0.1");
                    define("MYSQL_USER", "root");
                    define("MYSQL_PASSWORD", "luxury@123");
                    define("MYSQL_DATABASE", "content_villa_db");
                    define('MYSQL_PORT', "3306");

                    $mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
                    if (mysqli_connect_errno()) {  printf("Connect failed: %s
                    ", mysqli_connect_error());  exit(); }
                    // echo $id;

                    $myquery = $mysqli->query("select * from bsi_cms_gallery_room where type = 1");
                    $today_event = array();
                    $chk = 0;
                    // $row    = $result->fetch_assoc();

                    while ($row = $myquery->fetch_assoc()) {
                      $i=1;
                  ?>

                    <!-- <img src="gallery/cms/<?php echo $row["img_path"]; ?>" alt="" /> -->
                    <!-- <li data-target="#demo2" data-slide-to="<?php echo $i; ?>"></li> -->
                    <div class="carousel-item">
                      <img src="gallery/cms/<?php echo $row["img_path"]; ?>">
                    </div>

                  <?php
                    $i++; 
                    }
                  ?>
              </div>
            <!-- Left and right controls -->
              <a class="carousel-control-prev" href="#demo2" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#demo2" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>
            </div>

          </div>
          </div>
        </div>
        <br>
        <br>
        <div class="container" id="s2">
          <div class="row">
            <div class="col-md-8">
              <div id="demo3" class="carousel slide" data-ride="carousel">

              <!-- Indicators -->
              <ul class="carousel-indicators">
                  <?php
                    define("MYSQL_SERVER", "127.0.0.1");
                    define("MYSQL_USER", "root");
                    define("MYSQL_PASSWORD", "luxury@123");
                    define("MYSQL_DATABASE", "content_villa_db");
                    define('MYSQL_PORT', "3306");

                    $mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
                    if (mysqli_connect_errno()) {  printf("Connect failed: %s
                    ", mysqli_connect_error());  exit(); }
                    // echo $id;

                    $myquery = $mysqli->query("select * from bsi_cms_gallery_room where type = 2");
                    $today_event = array();
                    $chk = 0;
                    // $row    = $result->fetch_assoc();

                    $i=0;
                    while ($row = $myquery->fetch_assoc()) {
                  ?>

                    <!-- <img src="gallery/cms/<?php echo $row["img_path"]; ?>" alt="" /> -->
                    <li data-target="#demo3" data-slide-to="<?php echo $i; ?>"></li>

                  <?php
                    $i++; 
                    }
                  ?>

              </ul>
              
              <div class="carousel-inner" style="max-height: 450px;">
<!--                 <div class="carousel-item active">
                  <img src="images/RoomS/IMG_3225-Pano-2.jpg">
                </div> -->
                  <?php
                    define("MYSQL_SERVER", "127.0.0.1");
                    define("MYSQL_USER", "root");
                    define("MYSQL_PASSWORD", "luxury@123");
                    define("MYSQL_DATABASE", "content_villa_db");
                    define('MYSQL_PORT', "3306");

                    $mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
                    if (mysqli_connect_errno()) {  printf("Connect failed: %s
                    ", mysqli_connect_error());  exit(); }
                    // echo $id;

                    $myquery = $mysqli->query("select * from bsi_cms_gallery_room where type = 2");
                    $today_event = array();
                    $chk = 0;
                    // $row    = $result->fetch_assoc();

                    while ($row = $myquery->fetch_assoc()) {
                      $i=1;
                  ?>

                    <!-- <img src="gallery/cms/<?php echo $row["img_path"]; ?>" alt="" /> -->
                    <!-- <li data-target="#demo2" data-slide-to="<?php echo $i; ?>"></li> -->
                    <div class="carousel-item">
                      <img src="gallery/cms/<?php echo $row["img_path"]; ?>">
                    </div>

                  <?php
                    $i++; 
                    }
                  ?>
              </div>

              <a class="carousel-control-prev" href="#demo3" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#demo3" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>

            </div>
          </div>
            <div class="col-md-4 padder">
              <h3>
              <?php echo $lang["rt_2"]; ?>
              <!-- <span class="from-price">from</span>
              <span class="from-price-price">€199</span> -->
              </h3>
              <div class="room-body-text">
                <p><?php echo $lang["rt2_content"]; ?></p>
              </div>
              <div class="room-feature-content">
                <ul>
                  <li class="clearfix">
                    <img src="/images/Artboard.png" alt="room size"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["rs"]; ?></p>
                      <p class="feature-description">322.9 sq ft/p>
                    </div>
                  </li>
                  <li class="clearfix">
                    <img src="/images/Artboardx.png" alt="mattress size"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["ms"]; ?></p>
                      <p class="feature-description">King</p>
                    </div>
                  </li>
                  <li class="clearfix">
                    <img src="/images/Artboarda.png" alt="room capacity"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["rc"]; ?></p>
                      <p class="feature-description">2 Adults</p>
                    </div>
                  </li>
                </ul>
              </div>
              <a href="booking.php" class="ava" data-roomcodes="COSY,,COSYV">Check Availability</a>
            </div>
          </div>
        </div>
        <br>
        <br>
        <div class="container" id="s3">
          <div class="row">
            
            <div class="col-md-4 padder">
              <h3>
              <?php echo $lang["rt_3"]; ?>
              
              <!-- <span class="from-price">from</span>
              <span class="from-price-price">€259</span> -->
              </h3>
              <div class="room-body-text">
                <p><?php echo $lang["rt3_content"]; ?></p>
              </div>
              <div class="room-feature-content">
                <ul>
                  <li class="clearfix">
                    <img src="/images/Artboard.png" alt="room size"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["rs"]; ?></p>
                      <p class="feature-description">452.1 sq ft/p>
                    </div>
                  </li>
                  <li class="clearfix">
                    <img src="/images/Artboardx.png" alt="mattress size"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["ms"]; ?></p>
                      <p class="feature-description">King – Twin </p>
                    </div>
                  </li>
                  <li class="clearfix">
                    <img src="/images/Artboarda.png" alt="room capacity"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["rc"]; ?></p>
                      <p class="feature-description">Up to 2 Adults</p>
                    </div>
                  </li><!-- 
                  <li class="clearfix">
                    <img src="/images/roomview.svg" alt="room view"/>
                    <div class="feature-content">
                      <p class="feature-title">Room with a view available</p>
                      <p class="feature-description">Watch the world go by</p>
                    </div>
                  </li> -->
                </ul>
              </div>
              <a href="booking.php" class="ava" data-roomcodes="ROOMY,,ROOMYV">Check Availability</a>
            </div>
            <div class="col-md-8">
              <div id="demo4" class="carousel slide" data-ride="carousel">

              <!-- Indicators -->
              <ul class="carousel-indicators">
                  <?php
                    define("MYSQL_SERVER", "127.0.0.1");
                    define("MYSQL_USER", "root");
                    define("MYSQL_PASSWORD", "luxury@123");
                    define("MYSQL_DATABASE", "content_villa_db");
                    define('MYSQL_PORT', "3306");

                    $mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
                    if (mysqli_connect_errno()) {  printf("Connect failed: %s
                    ", mysqli_connect_error());  exit(); }
                    // echo $id;

                    $myquery = $mysqli->query("select * from bsi_cms_gallery_room where type = 3");
                    $today_event = array();
                    $chk = 0;
                    // $row    = $result->fetch_assoc();

                    $i=0;
                    while ($row = $myquery->fetch_assoc()) {
                  ?>

                    <!-- <img src="gallery/cms/<?php echo $row["img_path"]; ?>" alt="" /> -->
                    <li data-target="#demo4" data-slide-to="<?php echo $i; ?>"></li>

                  <?php
                    $i++; 
                    }
                  ?>

              </ul>
              
              <div class="carousel-inner" style="max-height: 450px;">
<!--                 <div class="carousel-item active">
                  <img src="images/RoomS/IMG_3225-Pano-2.jpg">
                </div> -->
                  <?php
                    define("MYSQL_SERVER", "127.0.0.1");
                    define("MYSQL_USER", "root");
                    define("MYSQL_PASSWORD", "luxury@123");
                    define("MYSQL_DATABASE", "content_villa_db");
                    define('MYSQL_PORT', "3306");

                    $mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
                    if (mysqli_connect_errno()) {  printf("Connect failed: %s
                    ", mysqli_connect_error());  exit(); }
                    // echo $id;

                    $myquery = $mysqli->query("select * from bsi_cms_gallery_room where type = 3");
                    $today_event = array();
                    $chk = 0;
                    // $row    = $result->fetch_assoc();

                    while ($row = $myquery->fetch_assoc()) {
                      $i=1;
                  ?>

                    <!-- <img src="gallery/cms/<?php echo $row["img_path"]; ?>" alt="" /> -->
                    <!-- <li data-target="#demo2" data-slide-to="<?php echo $i; ?>"></li> -->
                    <div class="carousel-item">
                      <img src="gallery/cms/<?php echo $row["img_path"]; ?>">
                    </div>

                  <?php
                    $i++; 
                    }
                  ?>
              </div>
              <a class="carousel-control-prev" href="#demo4" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#demo4" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
        <br>
        <br>
        <div class="container" id="s4">
          <div class="row">
            <div class="col-md-8">
              <div id="demo5" class="carousel slide" data-ride="carousel">

              <!-- Indicators -->
              <ul class="carousel-indicators">
                  <?php
                    define("MYSQL_SERVER", "127.0.0.1");
                    define("MYSQL_USER", "root");
                    define("MYSQL_PASSWORD", "luxury@123");
                    define("MYSQL_DATABASE", "content_villa_db");
                    define('MYSQL_PORT', "3306");

                    $mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
                    if (mysqli_connect_errno()) {  printf("Connect failed: %s
                    ", mysqli_connect_error());  exit(); }
                    // echo $id;

                    $myquery = $mysqli->query("select * from bsi_cms_gallery_room where type = 4");
                    $today_event = array();
                    $chk = 0;
                    // $row    = $result->fetch_assoc();

                    $i=0;
                    while ($row = $myquery->fetch_assoc()) {
                  ?>

                    <!-- <img src="gallery/cms/<?php echo $row["img_path"]; ?>" alt="" /> -->
                    <li data-target="#demo5" data-slide-to="<?php echo $i; ?>"></li>

                  <?php
                    $i++; 
                    }
                  ?>

              </ul>
              
              <div class="carousel-inner" style="max-height: 450px;">
<!--                 <div class="carousel-item active">
                  <img src="images/RoomS/IMG_3225-Pano-2.jpg">
                </div> -->
                  <?php
                    define("MYSQL_SERVER", "127.0.0.1");
                    define("MYSQL_USER", "root");
                    define("MYSQL_PASSWORD", "luxury@123");
                    define("MYSQL_DATABASE", "content_villa_db");
                    define('MYSQL_PORT', "3306");

                    $mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
                    if (mysqli_connect_errno()) {  printf("Connect failed: %s
                    ", mysqli_connect_error());  exit(); }
                    // echo $id;

                    $myquery = $mysqli->query("select * from bsi_cms_gallery_room where type = 4");
                    $today_event = array();
                    $chk = 0;
                    // $row    = $result->fetch_assoc();

                    while ($row = $myquery->fetch_assoc()) {
                      $i=1;
                  ?>

                    <!-- <img src="gallery/cms/<?php echo $row["img_path"]; ?>" alt="" /> -->
                    <!-- <li data-target="#demo2" data-slide-to="<?php echo $i; ?>"></li> -->
                    <div class="carousel-item">
                      <img src="gallery/cms/<?php echo $row["img_path"]; ?>">
                    </div>

                  <?php
                    $i++; 
                    }
                  ?>
              </div>
              <a class="carousel-control-prev" href="#demo5" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#demo5" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>
            </div>
          </div>
            
            <div class="col-md-4 padder">
              <h3>
              <?php echo $lang["rt_4"]; ?>
              
              <!-- <span class="from-price">from</span>
              <span class="from-price-price">€259</span> -->
              </h3>
              <div class="room-body-text">
                <p><?php echo $lang["rt4_content"]; ?></p>
              </div>
              <div class="room-feature-content">
                <ul>
                  <li class="clearfix">
                    <img src="/images/Artboard.png" alt="room size"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["rs"]; ?></p>
                      <p class="feature-description">452.1 sq ft/p>
                    </div>
                  </li>
                  <li class="clearfix">
                    <img src="/images/Artboardx.png" alt="mattress size"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["ms"]; ?></p>
                      <p class="feature-description">King – Twin </p>
                    </div>
                  </li>
                  <li class="clearfix">
                    <img src="/images/Artboarda.png" alt="room capacity"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["rc"]; ?></p>
                      <p class="feature-description">Up to 2 Adults</p>
                    </div>
                  </li><!-- 
                  <li class="clearfix">
                    <img src="/images/roomview.svg" alt="room view"/>
                    <div class="feature-content">
                      <p class="feature-title">Room with a view available</p>
                      <p class="feature-description">Watch the world go by</p>
                    </div>
                  </li> -->
                </ul>
              </div>
              <a href="booking.php" class="ava" data-roomcodes="ROOMY,,ROOMYV">Check Availability</a>
            </div>


        </div>
      </div>
        <br>
        <br>
        <div class="container" id="s5">
          <div class="row">
            
            <div class="col-md-4 padder">
              <h3>
              <?php echo $lang["rt_5"]; ?>
              
              <!-- <span class="from-price">from</span>
              <span class="from-price-price">€259</span> -->
              </h3>
              <div class="room-body-text">
                <p><?php echo $lang["rt5_content"]; ?></p>
              </div>
              <div class="room-feature-content">
                <ul>
                  <li class="clearfix">
                    <img src="/images/Artboard.png" alt="room size"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["rs"]; ?></p>
                      <p class="feature-description">452.1 sq ft/p>
                    </div>
                  </li>
                  <li class="clearfix">
                    <img src="/images/Artboardx.png" alt="mattress size"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["ms"]; ?></p>
                      <p class="feature-description">King – Twin </p>
                    </div>
                  </li>
                  <li class="clearfix">
                    <img src="/images/Artboarda.png" alt="room capacity"/>
                    <div class="feature-content">
                      <p class="feature-title"><?php echo $lang["rc"]; ?></p>
                      <p class="feature-description">Up to 2 Adults</p>
                    </div>
                  </li><!-- 
                  <li class="clearfix">
                    <img src="/images/roomview.svg" alt="room view"/>
                    <div class="feature-content">
                      <p class="feature-title">Room with a view available</p>
                      <p class="feature-description">Watch the world go by</p>
                    </div>
                  </li> -->
                </ul>
              </div>
              <a href="booking.php" class="ava" data-roomcodes="ROOMY,,ROOMYV">Check Availability</a>
            </div>
            <div class="col-md-8">
              <div id="demo6" class="carousel slide" data-ride="carousel">

              <!-- Indicators -->
              <ul class="carousel-indicators">
                  <?php
                    define("MYSQL_SERVER", "127.0.0.1");
                    define("MYSQL_USER", "root");
                    define("MYSQL_PASSWORD", "luxury@123");
                    define("MYSQL_DATABASE", "content_villa_db");
                    define('MYSQL_PORT', "3306");

                    $mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
                    if (mysqli_connect_errno()) {  printf("Connect failed: %s
                    ", mysqli_connect_error());  exit(); }
                    // echo $id;

                    $myquery = $mysqli->query("select * from bsi_cms_gallery_room where type = 5");
                    $today_event = array();
                    $chk = 0;
                    // $row    = $result->fetch_assoc();

                    $i=0;
                    while ($row = $myquery->fetch_assoc()) {
                  ?>

                    <!-- <img src="gallery/cms/<?php echo $row["img_path"]; ?>" alt="" /> -->
                    <li data-target="#demo6" data-slide-to="<?php echo $i; ?>"></li>

                  <?php
                    $i++; 
                    }
                  ?>

              </ul>
              
              <div class="carousel-inner" style="max-height: 450px;">
<!--                 <div class="carousel-item active">
                  <img src="images/RoomS/IMG_3225-Pano-2.jpg">
                </div> -->
                  <?php
                    define("MYSQL_SERVER", "127.0.0.1");
                    define("MYSQL_USER", "root");
                    define("MYSQL_PASSWORD", "luxury@123");
                    define("MYSQL_DATABASE", "content_villa_db");
                    define('MYSQL_PORT', "3306");

                    $mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
                    if (mysqli_connect_errno()) {  printf("Connect failed: %s
                    ", mysqli_connect_error());  exit(); }
                    // echo $id;

                    $myquery = $mysqli->query("select * from bsi_cms_gallery_room where type = 5");
                    $today_event = array();
                    $chk = 0;
                    // $row    = $result->fetch_assoc();

                    while ($row = $myquery->fetch_assoc()) {
                      $i=1;
                  ?>

                    <!-- <img src="gallery/cms/<?php echo $row["img_path"]; ?>" alt="" /> -->
                    <!-- <li data-target="#demo2" data-slide-to="<?php echo $i; ?>"></li> -->
                    <div class="carousel-item">
                      <img src="gallery/cms/<?php echo $row["img_path"]; ?>">
                    </div>

                  <?php
                    $i++; 
                    }
                  ?>
              </div>
              <a class="carousel-control-prev" href="#demo6" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#demo6" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
        <br>
        <br>
        <br>
        <br>
        <br>
<!--         <div class="bef-footer">
          <h1></h1>
          <form action="/action_page.php">
            <div class="form-group">
              <label for="email">What is your favorite drink?</label>
              <div class="col-sm-10">
                <input type="email" class="form-control" id="email" >
              </div>
            </div>
            <div class="form-group">
              <label for="pwd">What is your favorite singer?</label>
              <div class="col-sm-10"> 
                <input type="password" class="form-control" id="pwd" >
              </div>
            </div>
            <div class="form-group">
              <label for="pwd">What is your favorite scent?</label>
                <div class="checkbox">
                  <label><input type="checkbox" value="">Jasmine</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" value="">Vanilla</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" value="">Lemongrass</label>
                </div>
            </div>
            
            <div style="margin: 0 auto;">
              <button type="submit" class="btn btn-default">Submit</button>
            </div>

          </form>
        </div> -->
        <br>
        <br>
        <br>
        <!-- <div class="room-detail concept clearfix reversed" id="concept">
          <div class="">
            <div class="text-content concept-titles" data-index="0">
              <h4 class="title"><span>Concept Room – Tubby</span></h4>
              <h4 class="sub-title"><span>The Hoxton, Amsterdam</span></h4>
              <div class="arrows">
                <img class="prev" src="/img/slider-prev.png" alt="previous arrow"/>
                <img class="next" src="/img/slider-next.png" alt="next arrow"/>
              </div>
            </div>
            <div class="image-slider" data-index="0">
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3296/hoxton_ams_1_2388.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130812421690000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3467/hoxton_ams_1_2391.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130822257620000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3469/hoxton_ams_1_3194.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130822258010000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3294/hoxton_ams_1_2378.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130812421690000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3291/hoxton_ams_1_2352.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130812421680000000');"></div>
            </div>
            <div class="text-content concept-titles" data-index="1">
              <h4 class="title"><span>Concept Room – Floral</span></h4>
              <h4 class="sub-title"><span>The Hoxton, Amsterdam</span></h4>
              <div class="arrows">
                <img class="prev" src="/img/slider-prev.png" alt="previous arrow"/>
                <img class="next" src="/img/slider-next.png" alt="next arrow"/>
              </div>
            </div>
            <div class="image-slider" data-index="1">
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3314/hoxton_ams_4_3126.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130812421780000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3480/hoxton_ams_4_3799.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130822258840000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3482/hoxton_ams_4_3811.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130822258840000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3478/hoxton_ams_4_3807.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130822258810000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3470/hoxton_ams_4_3118.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130822258790000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3484/hoxton_ams_4_3826.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130822258850000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3471/hoxton_ams_4_3140.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130822258790000000');"></div>
            </div>
            <div class="text-content concept-titles" data-index="2">
              <h4 class="title"><span>Concept Room – Fruity</span></h4>
              <h4 class="sub-title"><span>The Hoxton, Amsterdam</span></h4>
              <div class="arrows">
                <img class="prev" src="/img/slider-prev.png" alt="previous arrow"/>
                <img class="next" src="/img/slider-next.png" alt="next arrow"/>
              </div>
            </div>
            <div class="image-slider" data-index="2">
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3316/hoxton_ams_4_3155.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130812421780000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3477/hoxton_ams_4_3179.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130822258810000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3475/hoxton_ams_4_3168.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130822258800000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3479/hoxton_ams_4_3813.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130822258830000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3294/hoxton_ams_1_2378.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130812421690000000');"></div>
              <div class="main-image" style="background-image: url('https://thehoxton.azureedge.net/media/3289/hoxton_ams_1_2306.jpg?anchor=center&amp;mode=crop&amp;width=690&amp;height=550&amp;rnd=130812421680000000');"></div>
            </div>
            <div class="room-text-content text-content clearfix">
              <h3>Concept Rooms</h3>
              <div class="room-body-text room-concept-text">
                <p>Our three unique, Concept Rooms can’t be booked, only requested. Fruity and Floral are the grandest rooms in the hotel, with dramatic ceilings, inspired by the grandeur of once being the Mayor’s rooms. Tubby, up in the attic, boasts the only roll-top bathtub.</p>
              </div>
            </div>
          </div>
        </div> -->
      </div>
    </div>
  </div>
  <script>
    $(function() {
      $( "#demo2 ul.carousel-indicators li:first-child" ).addClass( "active" );
      $( "#demo2 .carousel-inner .carousel-item:first-child" ).addClass( "active" );
      $( "#demo3 ul.carousel-indicators li:first-child" ).addClass( "active" );
      $( "#demo3 .carousel-inner .carousel-item:first-child" ).addClass( "active" );
      $( "#demo4 ul.carousel-indicators li:first-child" ).addClass( "active" );
      $( "#demo4 .carousel-inner .carousel-item:first-child" ).addClass( "active" );
      $( "#demo5 ul.carousel-indicators li:first-child" ).addClass( "active" );
      $( "#demo5 .carousel-inner .carousel-item:first-child" ).addClass( "active" );
      $( "#demo6 ul.carousel-indicators li:first-child" ).addClass( "active" );
      $( "#demo6 .carousel-inner .carousel-item:first-child" ).addClass( "active" );
    });
</script>
  
  <footer class="site-footer">
    <div class="container">
      <div class="row mb-5">
        <div class="col-md-4">
          <h3>Phone Support</h3>
          <p>24/7 Call us now.</p>
          <p class="lead"><a href="tel://06653218856">+ 66 53 218 856</a></p>
        </div>
        <div class="col-md-4">
          <h3>Connect With Us</h3>
          <p>We are socialized. Follow us</p>
          <p>
            <a href="https://www.facebook.com/HomeChiangMaiHotel/" class="pl-0 p-3"><span class="fa fa-facebook"></span></a>
            <a href="#" class="p-3"><span class="fa fa-twitter"></span></a>
            <a href="https://www.instagram.com/homechiangmaihotel/" class="p-3"><span class="fa fa-instagram"></span></a>
            <a href="#" class="p-3"><span class="fa fa-vimeo"></span></a>
            <a href="#" class="p-3"><span class="fa fa-youtube-play"></span></a>
          </p>
        </div>
        <div class="col-md-4">
          <h3>Connect With Us</h3>
          <p>“Home Chiang Mai Hotel
          Address:</p><p>
              <a href="https://www.google.com/maps/place/'@homechiangmai'/@18.8029473,98.9800778,18z/data=!4m5!3m4!1s0x30da3a9296803ec7:0xada6e29a86472be!8m2!3d18.8031711!4d98.9807778?hl=en">3 Rasemeechan Alle, Sermsuk Rd. <br>Chang Phueak, Chiang Mai, Thailand 50300</a><br>
              Phone:+66(0)53 218 856<br>
              Email:gsa@homechiangmaihotel.com<br>
              Fax:+66(0)53 218 856<br>
Website: www.homechiangmaihotel.com
            </p>
          <form action="#" class="subscribe">
            <div class="form-group">
              <button type="submit"><span class="ion-ios-arrow-thin-right"></span></button>
              <input type="email" class="form-control" placeholder="Enter Email">
            </div>
            
          </form>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-md-7 text-center">
          &copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved |
          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        </div>
      </div>
    </div>
  </footer>
  <!-- END footer -->
  
  <!-- loader -->
  <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>
  <!-- <script src="js/jquery-3.2.1.min.js"></script> -->
  <script src="js/jquery-migrate-3.0.0.js"></script>
  <script src="js/popper.min.js"></script>
  <!-- <script src="js/bootstrap.min.js"></script> -->
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/magnific-popup-options.js"></script>
  <script src="js/main.js"></script>
  <script src="js/multislider.js"></script>
  <script type="text/javascript" src="slick/slick.min.js"></script>
  <script>
  $('#mixedSlider').multislider({
  duration: 750,
  interval: 3333000
  });
  </script>
</body>
</html>