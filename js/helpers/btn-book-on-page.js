
var BookOnPage = function(){
    this.el = {
        btn: "#btn-book-on-page"
    }
}

BookOnPage.prototype = {
    constructor: BookOnPage,

    init: function(){
        this.initModal();
    },

    initModal: function(){
        $("#btn-book-on-page").hide();
        var element = this.el;
        $( window ).scroll(function(){
            if( $(this).scrollTop() > 450 ){
                $(element.btn).fadeIn(500);
            }else{
                $(element.btn).fadeOut(500);
            }
        });
        $(element.btn).click(function(){
            var widthScreen = $( document ).width();
            var colorboxOption = {
                width: (widthScreen <= 767 ? 90 : 60) + '%',
                height: '90%',
                fixed: true,
                href: "/modules/modal-room-form-on-page.php"
            }
            if (widthScreen > 767) {
                colorboxOption.height = 'auto';
            }
            $('.room-form-on-page').colorbox(colorboxOption);
        });
    }
}

$(document).ready(function(){
    var bookOnPage = new BookOnPage;
    bookOnPage.init();
});
