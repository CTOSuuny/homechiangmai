<?php

return [
    'room_list.php' => [
        'add_edit_room.php'
    ],
    'roomtype.php' => [
        'add_edit_roomtype.php',
    ],
    'admin_capacity.php' => [
        'add_edit_capacity.php'
    ],
    'gallery_list.php' => [
        'add_gallery.php'
    ],
    'priceplan.php' => [
        'add_edit_priceplan.php',
    ],
    'view_special_offer.php' => [
        'entry_edit_special_offer.php'
    ],
    'view_bookings.php' => [
        'view_active_or_archieve_bookings.php',
        'viewdetails.php',
        'print_invoice.php'
    ],
    'customerlookup.php' => [
        'customerbooking.php',
        'customerlookupEdit.php'
    ],
    'admin_block_room.php' => [
        'block_room.php',
    ],
    'manage_langauge.php' => [
        'add_edit_language.php',
    ],
    'currency_list.php' => [
        'add_edit_currency.php'
    ],
    'user.php' => [
        'add_edit_user.php'
    ]
];
