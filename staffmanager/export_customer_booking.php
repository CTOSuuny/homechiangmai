<?php
ini_set('memory_limit', '-1');
include("access.php");
include("../includes/db.conn.php");
include("../includes/conf.class.php");
include("../includes/admin.class.php");
include('language.php');
include('../vendor/autoload.php');
include('../includes/pdf.class.php');
include('../includes/constant/booking.php');

if (empty($_GET['client'])) {
    header('Location: admin-home.php');
    exit;
}

$clientid = $bsiCore->ClearInput($_GET['client']);

$sql = $mysqli->query('select * from bsi_clients where client_id=' . $clientid);
$customer = $sql->fetch_assoc();

$bookingQuery = $bsiAdminMain->getBookingInfo(3, $clientid);
$bookingQuery = $mysqli->query($bookingQuery);
$bookings = $bookingQuery->fetch_all(MYSQLI_ASSOC);
ob_start();
?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <title></title>
            <style>
                body {
                    font-family: "Garuda" !important;
                }

                .table .title {
                    background-color: #eee;
                    font-weight: bold;
                }

                .table tr td {
                    border-bottom: 0.5px solid #999;
                    font-size: 12px;
                    padding: 5px;
                }

                .table {
                    border: 1px solid #999;
                }

                .page-divider {
                    page-break-after: always;
                }

            </style>
        </head>
        <body>
            <h2>Customer Details</h2>
            <table class="table" cellpadding="0" cellspacing="0" width="700">
                <tr>
                    <td class="title" style="width: 20%"><strong>Name:</strong></td>
                    <td><?php echo $customer['title'] . $customer['first_name'] . ' ' . $customer['surname'] ?>
                </tr>
                <tr>
                    <td class="title"><strong>Address:</strong></td>
                    <td><?php echo $customer['street_addr'] ?></td>
                </tr>
                <tr>
                    <td class="title"><strong>City:</strong></td>
                    <td><?php echo $customer['city'] ?></td>
                </tr>
                <tr>
                    <td class="title"><strong>State:</strong></td>
                    <td><?php echo $customer['province'] ?></td>
                </tr>
                <tr>
                    <td class="title"><strong>Country:</strong></td>
                    <td><?php echo $customer['country'] ?></td>
                </tr>
                <tr>
                    <td class="title"><strong>Zip / Post code:</strong></td>
                    <td><?php echo $customer['zip'] ?></td>
                </tr>
                <tr>
                    <td class="title"><strong>Phone:</strong></td>
                    <td><?php echo $customer['phone'] ?></td>
                </tr>
                <tr>
                    <td class="title"><strong>Fax:</strong></td>
                    <td><?php echo $customer['fax'] ?></td>
                </tr>
                <tr>
                    <td class="title"><strong>Email:</strong></td>
                    <td><?php echo $customer['email'] ?></td>
                </tr>
                <tr>
                    <td class="title"><strong>Identity Type:</strong></td>
                    <td><?php echo $customer['id_type'] ?></td>
                </tr>
                <tr>
                    <td class="title"><strong>Identity Number:</strong></td>
                    <td><?php echo $customer['id_number'] ?></td>
                </tr>
                <tr>
                    <td class="title"><strong>Additional Requests:</strong></td>
                    <td><?php echo $customer['additional_comments'] ?></td>
                </tr>
            </table>

            <h2>Bookings</h2>

            <?php foreach ($bookings as $booking): ?>

                <?php
                    $curdate = date('Y-m-d');
                    $status = [
                        'color' => 'grey',
                        'message' => 'UNKNOW'
                    ];

                    $booking['end_date'] = date('Y-m-d', strtotime($booking['end_date']));

                    if ($booking['is_deleted'] == 0 && $booking['end_date'] < $curdate ) {
                        $status = [
                            'color' => 'blue',
                            'message' => 'COMPLETED'
                        ];
                    } elseif ($booking['is_deleted'] == 0 && $booking['end_date'] >= $curdate) {
                        $status = [
                            'color' => 'green',
                            'message' => 'ACTIVE'
                        ];
                    } elseif ($booking['is_deleted'] == 1) {
                        $status = [
                            'color' => 'red',
                            'message' => 'CANCELLED'
                        ];
                    }
                ?>
                <p style="text-align: right;">Booking Status: <span style="color:<?php echo $status['color'] ?>"><?php echo $status['message'] ?></span></p>

                <?php
                    $invoiceQuery = $mysqli->query("select * from bsi_invoice where booking_id=". $bsiCore->ClearInput($booking['booking_id']));
                    $invoice = $invoiceQuery->fetch_assoc();

                    echo preg_replace(['/Geneva/'], ['Garuda'], $invoice['invoice']);
                ?>

                <div class="page-divider"></div>
            <?php endforeach; ?>
        </body>
    </html>
<?php
$content = ob_get_contents();
ob_end_clean();

$pdfname = 'booking-customer.pdf';

$pdf = new PDF();
$pdf->setHTML($content)
    ->generate($pdfname);
