<div id="body-div">
    <div class="row show-room-dialog" >
            <!-- <h1><?php echo $bsiCore->config['conf_hotel_name']; ?> </h1> -->
            <?php $bookingDetails = $bsibooking->generateBookingDetails(); ?>
            <?php
                $active = "detail";
                include("breadcrumb.php");
            ?>
            <div class="container-fluid">
                <div class="row hidden-xs">
                    <div class="table-responsive">
                        <h2><?php echo BOOKING_DETAILS_TEXT; ?></h2>

                        <table cellpadding="4" cellspacing="1" class="table table-bordered table-book-details">
                            <tr>
                                <td bgcolor="#AD7D09" align="center"><strong><?php echo CHECKIN_DATE_TEXT; ?></strong></td>
                                <td bgcolor="#AD7D09" align="center"><strong><?php echo CHECKOUT_DATE_TEXT; ?></strong></td>
                                <td bgcolor="#AD7D09" align="center"><strong><?php echo TOTAL_NIGHT_TEXT; ?></strong></td>
                                <td bgcolor="#AD7D09" align="center"><strong><?php echo TOTAL_ROOMS_TEXT; ?></strong></td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#fcb66c"><?php echo $bsibooking->checkInDate; ?></td>
                                <td align="center" bgcolor="#fcb66c"><?php echo $bsibooking->checkOutDate; ?></td>
                                <td align="center" bgcolor="#fcb66c"><?php echo $bsibooking->nightCount; ?></td>
                                <td align="center" bgcolor="#fcb66c"><?php echo $bsibooking->totalRoomCount; ?></td>
                            </tr>
                            <tr>
                                <td bgcolor="#faa448" align="center"><strong><?php echo NUMBER_OF_ROOM_TEXT; ?></strong></td>
                                <td bgcolor="#faa448" align="center"><strong><?php echo ROOM_TYPE_TEXT; ?></strong></td>
                                <td bgcolor="#faa448" align="center"><strong><?php echo MAXI_OCCUPENCY_TEXT; ?></strong></td>
                                <td bgcolor="#faa448" class="al-r" style="padding-right:5px;"><strong><?php echo GROSS_TOTAL_TEXT; ?></strong></td>
                            </tr>
                             <?php
								foreach($bookingDetails as $bookings){
								$child_title2=($bookings['child_flag2'])? " + ".$bookings['childcount3']." ".CHILD_TEXT." ":"";
									echo '<tr>';
									echo '<td align="center" bgcolor="#fcb66c">'.$bookings['roomno'].'</td>';
									echo '<td align="center" bgcolor="#fcb66c">'.$bookings['roomtype'].' ('.$bookings['capacitytitle'].')</td>';
									echo '<td align="center" bgcolor="#fcb66c">'.$bookings['capacity'].' '.INV_ADULT.' '.$child_title2.'</td>';

									echo '<td class="al-r" bgcolor="#fcb66c" style="padding-right:5px;">'.$bsiCore->get_currency_symbol($_SESSION['sv_currency']).$bsiCore->getExchangemoney($bookings['grosstotal'],$_SESSION['sv_currency']).'</td>';
									echo '</tr>';
								}
							 ?>

                                <td  class="al-r" colspan="3" bgcolor="#faa448"><strong><?php echo SUB_TOTAL_TEXT; ?></strong></td>
                                <td  class="al-r" bgcolor="#faa448" style="padding-right:5px;"><strong> <?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']).$bsiCore->getExchangemoney($bsibooking->roomPrices['subtotal'],$_SESSION['sv_currency']); ?></strong></td>
                            </tr>

                            <?php $taxtext = '<br/> ' . trans('INCLUDED', true) ?>

                            <!-- Begin: Service charge -->
                            <?php if ($bsiCore->config['conf_service_charge'] > 0 && $bsiCore->config['conf_price_inclu_service_charge'] == 0) : ?>

                            <tr>
                                <td class="al-r" colspan="3" bgcolor="#fcb66c"><?php trans('SERVICE_CHARGE') ?> (<?php echo $bsiCore->config['conf_service_charge']; ?> %) </td>
                                <td class="al-r" bgcolor="#fcb66c" style="padding-right:5px;">
                                    <span>
                                        <?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']) . $bsiCore->getExchangemoney($bsibooking->roomPrices['total_service_charge'], $_SESSION['sv_currency']); ?>
                                    </span>
                                </td>
                            </tr>

                            <?php endif; ?>

                            <?php
                                if ($bsiCore->config['conf_service_charge'] > 0 && $bsiCore->config['conf_price_inclu_service_charge'] == 1) {
                                    $taxtext .= " " . trans('SERVICE_CHARGE', true) .  " ({$bsiCore->config['conf_service_charge']}%), ";
                                }
                                if (empty($bsiCore->config['conf_service_charge']) && $bsiCore->config['conf_price_inclu_service_charge'] == 1) {
                                    $taxtext .= " " . trans('SERVICE_CHARGE', true) . ', ';
                                }
                            ?>
                            <!-- End: Service charge -->

                            <!-- Begin: Local Tax -->
                            <?php if ($bsiCore->config['conf_local_tax'] > 0 && $bsiCore->config['conf_price_inclu_local_tax'] == 0): ?>

                                <tr>
                                    <td class="al-r" colspan="3" bgcolor="#fcb66c"><?php trans('LOCAL_TAX') ?> (<?php echo $bsiCore->config['conf_local_tax']; ?> %) </td>
                                    <td class="al-r" bgcolor="#fcb66c" style="padding-right:5px;">
                                        <span>
                                            <?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']) . $bsiCore->getExchangemoney($bsibooking->roomPrices['total_local_tax'], $_SESSION['sv_currency']); ?>
                                        </span>
                                    </td>
                                </tr>

                            <?php endif; ?>

                            <?php
                                if ($bsiCore->config['conf_local_tax'] > 0 && $bsiCore->config['conf_price_inclu_local_tax'] == 1) {
                                    $taxtext .= " " . trans('LOCAL_TAX', true) . " ({$bsiCore->config['conf_price_inclu_local_tax']}%), ";
                                }

                                if (empty($bsiCore->config['conf_local_tax']) && $bsiCore->config['conf_price_inclu_local_tax'] == 1) {
                                    $taxtext .= " " . trans('LOCAL_TAX', true) . ', ';
                                }
                            ?>
                            <!-- End: Local Tax -->

                            <?php
							if ($bsiCore->config['conf_tax_amount'] > 0 && $bsiCore->config['conf_price_with_tax'] == 0) {
								$taxtext = "";
							?>
                            <tr>
                                <td class="al-r" colspan="3" bgcolor="#fcb66c"><?php echo TAX_TEXT; ?> (<?php echo $bsiCore->config['conf_tax_amount']; ?> %)</td>
                                <td class="al-r" bgcolor="#fcb66c" style="padding-right:5px;">
                                    <span id="taxamountdisplay">
                                        <?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']) . $bsiCore->getExchangemoney($bsibooking->roomPrices['totaltax'],$_SESSION['sv_currency']); ?>
                                    </span>
                                </td>
                            </tr>
                            <?php
                                } else {
                                    $taxtext .= TAX_TEXT . (empty($bsiCore->config['conf_tax_amount']) ? '' : " ({$bsiCore->config['conf_tax_amount']} %)");
								}
							?>

                            <tr>
                                <td  class="al-r" colspan="3" bgcolor="#faa448"><strong><?php echo GRAND_TOTAL_TEXT; ?></strong> <?php echo $taxtext; ?></td>
                                <td class="al-r" bgcolor="#faa448" style="padding-right:5px;"><strong> <span id="grandtotaldisplay"><?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']).$bsiCore->getExchangemoney($bsibooking->roomPrices['grandtotal'],$_SESSION['sv_currency']); ?></span></strong></td>
                            </tr>
                             <?php
									if($bsiCore->config['conf_enabled_deposit'] && ($bsibooking->depositPlans['deposit_percent'] > 0 && $bsibooking->depositPlans['deposit_percent'] < 100)){
									?>

                            <tr id="advancepaymentdisplay">
                                <td class="al-r" colspan="3" bgcolor="#faa448"><strong></strong> <?php echo ADVANCE_PAYMENT_TEXT; ?>(<span style="font-size:11px;">
                                    <?php echo $bsibooking->depositPlans['deposit_percent']; ?>%<?php echo OF_GRAND_TOTAL_TEXT; ?></span>)</td>
                                <td class="al-r" bgcolor="#faa448" style="padding-right:5px;"><span id="advancepaymentamount"><?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']).$bsiCore->getExchangemoney($bsibooking->roomPrices['advanceamount'],$_SESSION['sv_currency']); ?> </span></td>
                            </tr>
                            <?php
                            }
						?>
                        </table>
                    </div>
                </div>

                <div class="row visible-xs">
                    <div class="col-xs-12">
                        <h2><?php echo BOOKING_DETAILS_TEXT; ?></h2>
                    </div>
                </div>
                <div class="row visible-xs mobile-summary-detail">
                    <div class="col-xs-12">
                        <div class="row summary-row">
                            <div class="col-xs-12">
                                <strong>
                                    <?php trans('CHECKIN_DATE_TEXT') ?> - <?php echo trans('CHECKOUT_DATE_TEXT') ?>
                                </strong>
                            </div>
                            <div class="col-xs-12">
                                <span class="summary-detail"><?php echo $bsibooking->checkInDate . ' - ' . $bsibooking->checkOutDate; ?></span>
                            </div>
                        </div>
                        <div class="row summary-row">
                            <div class="col-xs-8">
                                <strong><?php trans('TOTAL_NIGHT_TEXT') ?></strong>
                            </div>
                            <div class="col-xs-4">
                                <span class="summary-detail"><?php echo $bsibooking->nightCount; ?></span>
                            </div>
                        </div>
                        <div class="row summary-row">
                            <div class="col-xs-8">
                                <strong>
                                    <?php trans('TOTAL_ROOMS_TEXT'); ?>
                                </strong>
                            </div>
                            <div class="col-xs-4">
                                <span class="summary-detail"><?php echo $bsibooking->totalRoomCount; ?></span>
                            </div>
                        </div>

                        <?php foreach($bookingDetails as $bookings) : ?>
                            <?php
                            $child_title2=($bookings['child_flag2'])? " + ".$bookings['childcount3']." ".CHILD_TEXT." ":"";
                            ?>
                            <div class="row summary-row summary-room-type">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <strong>
                                                <?php trans('ROOM_TYPE_TEXT') ?>
                                                <?php echo $bookings['roomtype'].' ('.$bookings['capacitytitle'].')' ?>
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <strong class="room-type-title"><?php trans('NUMBER_OF_ROOM_TEXT') ?></strong>
                                        </div>
                                        <div class="col-xs-4">
                                            <span class="summary-detail"><?php echo $bookings['roomno'] ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <strong class="room-type-title"><?php trans('MAXI_OCCUPENCY_TEXT') ?></strong>
                                        </div>
                                        <div class="col-xs-4">
                                            <span class="summary-detail"><?php echo $bookings['capacity'].' '.INV_ADULT.' '.$child_title2 ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <strong class="room-type-title"><?php trans('GROSS_TOTAL_TEXT') ?></strong>
                                        </div>
                                        <div class="col-xs-4">
                                            <span class="summary-detail">
                                                <?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']).$bsiCore->getExchangemoney($bookings['grosstotal'],$_SESSION['sv_currency']) ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>

                        <div class="row summary-row">
                            <div class="col-xs-8">
                                <strong><?php echo trans('SUB_TOTAL_TEXT') ?></strong>
                            </div>
                            <div class="col-xs-4">
                                <span class="summary-detail">
                                    <?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']).$bsiCore->getExchangemoney($bsibooking->roomPrices['subtotal'],$_SESSION['sv_currency']); ?>
                                </span>
                            </div>
                        </div>

                        <?php $taxtext = '' ?>

                        <!-- Begin: Service charge -->
                        <?php if ($bsiCore->config['conf_service_charge'] > 0 && $bsiCore->config['conf_price_inclu_service_charge'] == 0) : ?>

                            <div class="row summary-row">
                                <div class="col-xs-8">
                                    <strong><?php trans('SERVICE_CHARGE') ?> (<?php echo $bsiCore->config['conf_service_charge']; ?> %) </strong>
                                </div>
                                <div class="col-xs-4">
                                    <span class="summary-detail">
                                        <?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']) . $bsiCore->getExchangemoney($bsibooking->roomPrices['total_service_charge'], $_SESSION['sv_currency']); ?>
                                    </span>
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php
                        if ($bsiCore->config['conf_service_charge'] > 0 && $bsiCore->config['conf_price_inclu_service_charge'] == 1) {
                            $taxtext .= " " . trans('SERVICE_CHARGE', true) .  " ({$bsiCore->config['conf_service_charge']}%), ";
                        }

                        if (empty($bsiCore->config['conf_service_charge']) && $bsiCore->config['conf_price_inclu_service_charge'] == 1) {
                            $taxtext .= " " . trans('SERVICE_CHARGE', true);
                        }
                        ?>
                        <!-- End: Service charge -->

                        <!-- Begin: Local Tax -->
                        <?php if ($bsiCore->config['conf_local_tax'] > 0 && $bsiCore->config['conf_price_inclu_local_tax'] == 0): ?>

                            <div class="row summary-row">
                                <div class="col-xs-8">
                                    <strong><?php trans('LOCAL_TAX') ?> (<?php echo $bsiCore->config['conf_local_tax']; ?> %)</strong>
                                </div>
                                <div class="col-xs-4">
                                    <span class="summary-detail">
                                        <?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']) . $bsiCore->getExchangemoney($bsibooking->roomPrices['total_local_tax'], $_SESSION['sv_currency']); ?>
                                    </span>
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php
                        if ($bsiCore->config['conf_local_tax'] > 0 && $bsiCore->config['conf_price_inclu_local_tax'] == 1) {
                            $taxtext .= " " . trans('LOCAL_TAX', true) . " ({$bsiCore->config['conf_price_inclu_local_tax']}%), ";
                        }

                        if (empty($bsiCore->config['conf_local_tax']) && $bsiCore->config['conf_price_inclu_local_tax'] == 1) {
                            $taxtext .= " " . trans('LOCAL_TAX', true) . ", ";
                        }
                        ?>
                        <!-- End: Local Tax -->

                        <!-- Begin: Tax -->
                        <?php if ($bsiCore->config['conf_tax_amount'] > 0 && $bsiCore->config['conf_price_with_tax'] == 0): ?>

                            <div class="row summary-row">
                                <div class="col-xs-8">
                                    <strong><?php echo TAX_TEXT; ?> (<?php echo $bsiCore->config['conf_tax_amount']; ?> %)</strong>
                                </div>
                                <div class="col-xs-4">
                                    <span class="summary-detail taxamountdisplay">
                                        <?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']) . $bsiCore->getExchangemoney($bsibooking->roomPrices['totaltax'],$_SESSION['sv_currency']); ?>
                                    </span>
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php
                        if ($bsiCore->config['conf_tax_amount'] > 0 && $bsiCore->config['conf_price_with_tax'] == 1) {
                            $taxtext .= " " . trans('TAX_TEXT', true) . " ({$bsiCore->config['conf_tax_amount']}%)";
                        }

                        if ($bsiCore->config['conf_tax_amount'] == 0 && $bsiCore->config['conf_price_with_tax'] == 1) {
                            $taxtext .= " " . trans('TAX_TEXT', true);
                        }

                        ?>
                        <!-- End: Tax -->

                        <div class="row summary-row">
                            <div class="col-xs-8">
                                <strong><?php echo GRAND_TOTAL_TEXT; ?></strong>
                                <?php echo empty($taxtext) ? '' : '<br/> ' . trans('INCLUDED', true) . $taxtext; ?></strong>
                            </div>
                            <div class="col-xs-4">
                                <strong>
                                    <span id="grandtotaldisplay"><?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']).$bsiCore->getExchangemoney($bsibooking->roomPrices['grandtotal'],$_SESSION['sv_currency']); ?></span>
                                </strong>
                            </div>
                        </div>

                        <?php if($bsiCore->config['conf_enabled_deposit'] && ($bsibooking->depositPlans['deposit_percent'] > 0 && $bsibooking->depositPlans['deposit_percent'] < 100)) : ?>

                            <div class="row summary-row" id="advancepaymentdisplay">
                                <div class="col-xs-8">
                                    <strong><?php echo ADVANCE_PAYMENT_TEXT; ?></strong>
                                    <br/>
                                    (<?php echo $bsibooking->depositPlans['deposit_percent']; ?>% <?php echo OF_GRAND_TOTAL_TEXT; ?>)
                                </div>
                                <div class="col-xs-4">
                                    <strong id="advancepaymentamount">
                                        <?php echo $bsiCore->get_currency_symbol($_SESSION['sv_currency']).$bsiCore->getExchangemoney($bsibooking->roomPrices['advanceamount'],$_SESSION['sv_currency']); ?>
                                    </strong>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>

                </div>

            </div>

            <div class="wrapper">
                <div class="htitel">
                    <h2 class="fl"><?php echo CUSTOMER_DETAILS_TEXT; ?></h2>
                </div>
                <!-- start of search row -->
                <div class="container-fluid" style="margin:0; padding:0;">
                    <div class="row-fluid" style="border: 1px solid #e1e1e1; padding: 1% 0">
                        <form class="form-horizontal" action="booking-process.php" method="post" id="form1" style="width: 95%; margin: 0 2.5%">
                            <?php echo CSRF::tokenxField(); ?>
                            <?php /*
                                <h3 align="left" style="color:#999;"><?php echo EXISTING_CUSTOMER_TEXT; ?>?</h3>
                                <div class="form-group">
                                    <label class="control-label col-sm-offset-2 col-sm-2" for="ea"><?php echo EMAIL_ADDRESS_TEXT; ?>:</label>
                                    <div class="controls col-sm-5">
                                        <input type="text" name="email_addr_existing" id="email_addr_existing"  class="input-large form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-offset-2  col-sm-2" for="pa">Password:</label>
                                    <div class="controls col-sm-5">
                                        <input type="password" name="login_password" id="login_password"  class="input-large form-control" />
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <div class="controls col-sm-5" id="exist_wait" >

                                    </div>
                                  </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-offset-2  col-sm-2"></label>
                                    <div class="controls col-sm-5">
                                        <button id="btn_exisitng_cust" type="button" style="margin-top: 0px;" class="btn btn-default btn-book pull-right" ><?php echo FETCH_DETAILS_TEXT; ?></button>
                                        <a href="javascript:;" id="forgot_pass" style="width:150px; display:inline-block; padding-left:10px; cursor:pointer;">Forgot Password?</a>
                                    </div>
                                </div>
                               <div class="text-center"><h1><?php echo OR_TEXT; ?></h1></div>
                              */ ?>
                      <h3 align="left" style="color:#999; display:none"><?php echo NEW_CUSTOMER_TEXT; ?>?</h3>
                        <input type="hidden" name="allowlang" id="allowlang" value="no" />
                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" name="title1" for="title"><?php trans('NAME_TITLE') ?>: </label>
                                <div class="controls col-sm-1">
                                    <select id="title" name="title" class="input-small form-control">
                                        <option value="Mr."><?php echo MR_TEXT; ; ?>.</option>
                                        <option value="Ms."><?php echo MS_TEXT; ?>.</option>
                                        <option value="Mrs."><?php echo MRS_TEXT; ?>.</option>
                                        <option value="Miss."><?php echo MISS_TEXT; ?>.</option>
                                        <option value="Dr."><?php echo DR_TEXT; ?>.</option>
                                        <option value="Prof."><?php echo PROF_TEXT; ?>.</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" for="fn"><?php echo FIRST_NAME_TEXT; ?>:</label>
                                <div class="controls col-sm-6">
                                    <input type="text" name="fname" id="fname" class="input-large form-control required">
                                    <div class="errorTxt" align="left"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" for="ln"><?php echo LAST_NAME_TEXT; ?>:</label>
                                <div class="controls col-sm-6">
                                    <input type="text" name="lname" id="lname" class="input-large form-control required">
                                    <div class="errorTxt" align="left"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" for="ln"><?php echo ADDRESS_TEXT; ?>:</label>
                                <div class="controls col-sm-6">
                                    <input type="text" name="str_addr" id="str_addr" class="input-large form-control required">
                                    <div class="errorTxt" align="left"></div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" for="ct"><?php echo CITY_TEXT; ?>:</label>
                                <div class="controls col-sm-6">
                                    <input type="text" name="city"  id="city" class="input-large form-control required">
                                    <div class="errorTxt" align="left"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" for="st"><?php echo STATE_TEXT; ?>:</label>
                                <div class="controls col-sm-6">
                                    <input name="state"  id="state" type="text" class="input-large form-control required">
                                    <div class="errorTxt" align="left"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" for="pc"><?php echo POSTAL_CODE_TEXT; ?>:</label>
                                <div class="controls col-sm-6">
                                    <input name="zipcode"  id="zipcode" type="text" class="input-large form-control required number">
                                    <div class="errorTxt" align="left"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" for="co"><?php echo COUNTRY_TEXT; ?>:</label>
                                <div class="controls col-sm-6">
                                    <input name="country"  id="country" type="text" class="input-large form-control required">
                                    <div class="errorTxt" align="left"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" for="ph"><?php echo PHONE_TEXT; ?>:</label>
                                <div class="controls col-sm-6">
                                    <input name="phone"  id="phone" type="text" class="input-large form-control required number">
                                    <div class="errorTxt" align="left"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" for="fx"><?php echo FAX_TEXT; ?>:</label>
                                <div class="controls col-sm-6">
                                    <input name="fax"  id="fax" type="text" class="input-large form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" for="it"><?php echo ID_TYPE; ?>:</label>
                                <div class="controls col-sm-2">
                                    <select name="id_type" id="id_type" class="input-large form-control">
                                        <option value="id_card"><?php echo IDCARD; ?></option>
                                        <option value="passport"><?php echo PASSPORT; ?></option>
                                        <option value="other"><?php echo OTHER; ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" for="in"><?php echo ID_NUMBER; ?>:</label>
                                <div class="controls col-sm-6">
                                    <input name="id_number"  id="id_number" type="text" class="input-large form-control required">
                                    <span class="pull-left  help-inline">(<?php trans('EG_PASSPORT_NUMBER') ?>.)</span>
                                    <div class="errorTxt" align="left"></div>
                                </div>
                            </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-offset-2  col-sm-2" for="em"><?php echo EMAIL_TEXT; ?>:</label>
                                    <div class="controls col-sm-6">
                                        <input name="email"  id="email" type="text" class="input-large form-control required email">
                                        <div class="errorTxt" align="left"></div>
                                    </div>

                                </div>
                            <?php /*
                                <div class="form-group">
                                    <label class="control-label col-sm-offset-2  col-sm-2" for="ps">Password:</label>
                                    <div class="controls col-sm-5">
                                        <input name="password"  id="password" type="password" class="input-large form-control required">
                                    </div>
                                </div>
                            */ ?>
                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" for="pb"><?php echo PAYMENT_BY_TEXT; ?>:</label>
                                <div class="controls col-sm-6">

                                    <?php
                                        $isDisable = false;
                                        $isHide2c2p = true;
                                    ?>

                                    <?php if (ENV::get('ENABLE_PAYPAID')) : ?>
                                    <!-- <div class="payment-section">
                                        <p class="payment-title">
                                            <?php echo "Internet Banking" ?>
                                            <?php //echo $isDisable ? '<span style="color: red;">(' . trans('SERVICE_UNVAILABLE', true) . ')</span>' : '' ?>
                                        </p>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-lg-3 payment-label">
                                                <label class="">
                                                    <input class="required" type="radio" name="payment_type" value="internet_bank_scb">&nbsp;<img src="<?php echo asset('images/payments/SCB.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-3 payment-label">
                                                <label class="">
                                                    <input class="required" type="radio" name="payment_type" value="internet_bank_ktb">&nbsp;<img src="<?php echo asset('images/payments/KTB.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-3 payment-label">
                                                <label class="">
                                                    <input class="required" type="radio" name="payment_type" value="internet_bank_bay">&nbsp;<img src="<?php echo asset('images/payments/BAY.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-3 payment-label">
                                                <label class="">
                                                    <input class="required" type="radio" name="payment_type" value="internet_bank_bbl">&nbsp;<img src="<?php echo asset('images/payments/BBL.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="payment-section">
                                        <p class="payment-title">
                                            <?php echo "Credit Card" ?>
                                        </p>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-lg-3 payment-label">
                                                <label class="">
                                                    <input class="required" type="radio" name="payment_type" value="credit_card_paypaid">&nbsp;
                                                    <img style="width: 120px;" src="<?php echo asset('images/payments/visa-master-card.png') ?>" alt="">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <?php if(!$isHide2c2p): ?>
                                    
                                    <div class="payment-section">
                                        <p class="payment-title">
                                            <?php trans('DIRECT_DEBIT') ?>
                                            <?php //echo $isDisable ? '<span style="color: red;">(' . trans('SERVICE_UNVAILABLE', true) . ')</span>' : '' ?>
                                        </p>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-lg-4 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="direct_scb">&nbsp;<img src="<?php echo asset('images/payments/SCB.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-4 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="direct_ktb">&nbsp;<img src="<?php echo asset('images/payments/KTB.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-4 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="direct_uob">&nbsp;<img src="<?php echo asset('images/payments/UOB.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-4 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="direct_tmb">&nbsp;<img src="<?php echo asset('images/payments/TMB.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-4 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="direct_bbl">&nbsp;<img src="<?php echo asset('images/payments/BBL.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-4 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="direct_bay">&nbsp;<img src="<?php echo asset('images/payments/BAY.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-4 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="direct_tbank">&nbsp;<img src="<?php echo asset('images/payments/TBANK.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-4 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="direct_kbank">&nbsp;<img src="<?php echo asset('images/payments/KBANK.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="payment-section">
                                        <p class="payment-title">
                                            <?php trans('COUNTER_PAYMENT') ?>
                                            <?php //echo $isDisable ? '<span style="color: red;">(' . trans('SERVICE_UNVAILABLE', true) . ')</span>' : '' ?>
                                        </p>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-lg-3 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="counter_tesco">&nbsp;<img src="<?php echo asset('images/payments/TESCO.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-3 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="counter_truemoney">&nbsp;<img src="<?php echo asset('images/payments/TRUEMONEY.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-3 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="counter_payatpost">&nbsp;<img src="<?php echo asset('images/payments/PAYATPOST.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-3 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="counter_mpay">&nbsp;<img src="<?php echo asset('images/payments/MPAY.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-3 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="counter_bigc">&nbsp;<img src="<?php echo asset('images/payments/BIGC.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-3 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="counter_seveneleven">&nbsp;<img src="<?php echo asset('images/payments/SEVENELEVEN.jpg') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-3 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="counter_familymart">&nbsp;<img src="<?php echo asset('images/payments/FAMILYMART.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="payment-section">
                                        <p class="payment-title">
                                            <?php trans('ATM_PAYMENT') ?>
                                            <?php //echo $isDisable ? '<span style="color: red;">(' . trans('SERVICE_UNVAILABLE', true) . ')</span>' : '' ?>
                                        </p>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-lg-4 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="atm_scb">&nbsp;<img src="<?php echo asset('images/payments/SCB.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-4 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="atm_ktb">&nbsp;<img src="<?php echo asset('images/payments/KTB.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-4 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="atm_kbank">&nbsp;<img src="<?php echo asset('images/payments/KBANK.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-4 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="atm_bay">&nbsp;<img src="<?php echo asset('images/payments/BAY.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-lg-4 payment-label">
                                                <label class="<?php echo $isDisable ? 'disabled' : '' ?>">
                                                    <input <?php echo $isDisable ? 'disabled' : '' ?> class="required" type="radio" name="payment_type" value="atm_bbl">&nbsp;<img src="<?php echo asset('images/payments/BBL.PNG') ?>" alt="">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?> -->

                                    <div class="">
                                        <p class="hidden payment-title"><?php trans('CREDIT_CARD_PAYMENT') ?></p>
                                        <?php
                                        $paymentGatewayDetails = $bsiCore->loadPaymentGateways();
                                        foreach($paymentGatewayDetails as $key => $value){

                                            if (strtolower($value['name']) === 'paypal') {
                                                $value['name'] = '<img style="width: 200px;" src="' . asset('images/paypal-credit.jpg') . '">';
                                            }

                                            echo '<p style="color:black;" class="text-left"><label style="cursor:pointer;"><input type="radio" name="payment_type" id="payment_type_'.$key.'" value="'.$key.'" class="required" /> '.$value['name']. '</label></p>';
                                        }
                                        ?>
                                        <div class="errorTxt" align="left">
                                            <label class="error" generated="true" for="payment_type" style="display:none;"><?php echo FIELD_REQUIRED_ALERT; ?>.</label>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-offset-2  col-sm-2" for="ar"><?php echo ADDITIONAL_REQUESTS_TEXT; ?> :</label>
                                <div class="controls col-sm-6" align="left">
                                    <textarea rows="3" id='ar' name="message" class="input-large form-control"></textarea>
                                    <label class="checkbox col-sm-12">
                                        <input type="checkbox" name="tos" id="tos" class="required" checked>
                                        <?php echo I_AGREE_WITH_THE_TEXT; ?> <a style="color:black;" href="javascript: ;" onClick="javascript:myPopup2();"><?php echo TERMS_AND_CONDITIONS_TEXT; ?>.</a>
                                        <div class="errorTxt"  align="left"></div>
                                    </label>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <!-- end of search row -->
            <div style="margin-bottom: 80px;">
                <div class="col-xs-6 col-sm-4 " >
                    <button type="button" class="btn btn-default btn-book" onClick="window.location.href='/'" ><?php trans('BTN_CANCEL'); ?></button>
                </div>
                <div class="hidden-xs col-sm-4" >
                    <button  type="button"  class='home-btn btn btn-default btn-book' onClick="window.location.href='index.php'" ><?php trans('HOME_TEXT'); ?></button>
                </div>
                <div class="col-xs-6 col-sm-4" >
                    <button type="submit" class="conti btn btn-default btn-book" ><?php echo CONTINUE_TEXT; ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
