<?php

class AddConfHotelUrl extends Migration
{

    public function handle()
    {
        if (
            !$this->valueExists('bsi_configure', 'conf_key', 'conf_hotel_url_facebook') and
            !$this->valueExists('bsi_configure', 'conf_key', 'conf_hotel_url_twitter') and
            !$this->valueExists('bsi_configure', 'conf_key', 'conf_hotel_url_instagram') and
            !$this->valueExists('bsi_configure', 'conf_key', 'conf_hotel_url_googleplus')
        ) {
            $this->getConnection()->query("INSERT INTO bsi_configure (conf_key, conf_value) VALUES ('conf_hotel_url_facebook', '')");
            $this->getConnection()->query("INSERT INTO bsi_configure (conf_key, conf_value) VALUES ('conf_hotel_url_twitter', '')");
            $this->getConnection()->query("INSERT INTO bsi_configure (conf_key, conf_value) VALUES ('conf_hotel_url_instagram', '')");
            $this->getConnection()->query("INSERT INTO bsi_configure (conf_key, conf_value) VALUES ('conf_hotel_url_googleplus', '')");
        }
    }

}
