<div class="row show-gallery-dialog room-modules">
    <h2 style="margin-bottom: 1em;"><?php trans('OUR_VIDEO') ?></h2>
    <div class="row">
        <div class="col-xs-12">
            <iframe width="100%" class="video-player" src="https://www.youtube.com/embed/dSOKAYsqrKA?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
    </div>
</div>