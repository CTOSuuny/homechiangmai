<?php
    include 'access.php';
    include 'header.php';
    include '../includes/models/user.php';

    $isPasswordRequired = true;
    $submitButton = trans('CREATE_NEW_USER', true);
    $userModel = new User();
    $route = 'actions/user_action.php';

    if (!empty($_GET['id'])) {
        $user = $userModel->findUserById($_GET['id'] ?? 0);
        $isPasswordRequired = false;
        $route = route('actions/user_action.php', ['id' => $_GET['id']]);
        $submitButton = trans('EDIT_USER', true);
    }

    $old = old();

    if (!empty($old)) {
        $user = $old;
    }
?>

<link rel="stylesheet" type="text/css" href="css/jquery.validate.css" />
<link rel="stylesheet" href="<?php echo asset('css/admin-style.css'); ?>">

<div id="container-inside">
    <span style="font-size:16px; font-weight:bold"><?php echo trans('EDIT_USER'); ?></span>
    <hr />

    <?php $isValideFail = get_flash('errors'); ?>

    <?php if ($isValideFail): ?>
        <p class="panel-error"><?php echo $isValideFail['error']; ?></p>
    <?php endif; ?>

    <?php if (!empty($_GET['id']) && $_SESSION['cpuidBSI'] !== $_GET['id']): ?>
    <div style="float: right">
        <form action="<?php echo $route ?>" method="post">
            <input type="hidden" name="method" value="delete">
            <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
            <table>
                <tr>
                    <td><strong><?php echo trans('DELETE_USER'); ?></strong></td>
                    <td style="width: 30%;">
                        <input type="submit" onclick="return confirm('<?php echo trans('DO_YOU_WANT_TO_DELETE') ?>')" class="btn danger" value="<?php echo trans('DELETE') ?>">
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <?php endif; ?>

    <div class="">
        <form id="user-form" action="<?php echo $route ?>" method="post">

            <?php echo method_field(!empty($_GET['id']) ? 'put' : 'post'); ?>

            <table cellpadding="5" cellspacing="2" border="0">
                <tr>
                    <td><strong><?php echo trans('USERNAME'); ?></strong></td>
                    <td valign="middle">
                        <input type="text" name="username" class="required" value="<?php echo $user['username'] ?? ''; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong><?php echo trans('FIRSTNAME'); ?></strong></td>
                    <td valign="middle">
                        <input type="text" name="f_name" value="<?php echo $user['f_name'] ?? ''; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong><?php echo trans('LASTNAME'); ?></strong></td>
                    <td valign="middle">
                        <input type="text" name="l_name" value="<?php echo $user['l_name'] ?? ''; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong><?php echo trans('EMAIL'); ?></strong></td>
                    <td valign="middle">
                        <input type="email" name="email" class="required" value="<?php echo $user['email'] ?? ''; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong><?php echo trans('PASSWORD'); ?></strong></td>
                    <td valign="middle">
                        <input type="password" name="pass" class="<?php echo !empty($isPasswordRequired) ? 'required' : ''; ?>" value="">
                    </td>
                </tr>
                <tr>
                    <td><strong><?php echo trans('ROLE'); ?></strong></td>
                    <td valign="middle">
                        <select class="" name="designation">
                            <?php foreach (USER_TYPE as $type): ?>
                                <option <?php echo (!empty($user['designation']) && $user['designation'] == $type) ? 'selected' : '' ?> value="<?php echo $type ?>"><?php echo $type ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" value="<?php echo $submitButton ?? ''; ?>" class="btn success">
                    </td>
                </tr>
            </table>
        </form>
    </div>

</div>
<script type="text/javascript">
	$().ready(function() {

		$("#user-form").validate();
	});
</script>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<?php include 'footer.php'; ?>
