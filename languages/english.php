<?php

define('WELCOME_MESSAGE', 'Home Chiang Mai');
define('WELCOME_TO', 'Welcome to');
define('WELCOME_QOUTE', 'A Tranquility Villa Mingles with Lanna Culture and British Colonial Characters.');
define('SUBTITLE_MESSAGE', 'Enjoy Your Stay In');
define('CHILDREN_TEXT','Children');
define('BOOK_YOUR_ROOM_NOW', 'จองห้องพัก');
define('HOTEL_DETAIL', 'Romantic Lanna Colonial Style The Combination of Lanna art and colonial perfectly. The surrounding area has a beautiful garden and mountain of Lanna Applied Design. Within decorate of the western art colonial Style Interior design. Accommodation The villa have 18 guestrooms divided 6 Deluxe, 4 Deluxe Premium 4 Executive and 4 Executive Suite. The luxury amenities comfortable spaces are offered in every room. Special Atmosphere you will find only in Home Chiangmai. Staying at the Home Chiangmai, talking off the journey fatigue and relaxing into the beautiful dream, a perfect place for your choice to escape from the city life to relax with a warm service, unforgettable experience and classic food tour. Design to offer a warm welcome and friendly services minded to short stay guests, while we\'re offering espercially care our long stay guests to feel comfortable and safety in capable hands. In the moment of your arrival at the resort, you are treated to the highest standard of services, tasteful hospitality and attractive recreation.');
define('GALLERY_TEXT', 'Galleries');
define('ABOUT_US', 'About Us');
define('CONTACT_US', 'Contact');
define('QUICK_LINKS', 'Quick links');
define('ADDRESS', 'Address');
define('ADDRESS_NAME', '59 Moo 6 Nong Kwai, Hangdong, Chiangmai');
define('CONNECT_WITH_US', 'Connect with us');
define('CHOOCE_CHECK_IN_DATE', 'Check in');
define('CHOOCE_CHECK_OUT_DATE', 'Check out');
define('SELECT_ROOM', 'Select Room');
define('CANNOT_SWITCH_LANGUAGE_DURING_BOOKING', 'Can not switch language during booking.');
define('NAME_TITLE', 'Title');
define('TO_OFFER_THAI_FOOD', 'To offer Thai food, we have recently introduced one of the top restaurants in the UK, EAT THAI, which serves Thai fusion food more than 16 years and gained many awards such as Michellin Guide in 2006-2013, TripAdvisor, etc. EAT THAI restaurant expects to open in early April 2018.');
define('THERE_ARE_THREE', 'There are three separate menus for different occasions, namely; Le Mont Coffee House, EAT THAI England, and Room Service');
define('OPEN_DAILY', 'Open daily from 7am to 4pm');

define('WE_OFFER', 'We offer four different types of room depending on your choices; and those are in Table');
define('ROOM_TYPE', 'Room Type');
define('SIZE', 'Size');
define('HIGH_CEILING', 'High Ceiling');
define('PANTRY', 'Pantry');
define('Dining_TABLE', 'Dining Table');
define('BATHHUB', 'Bathtub');
define('DELUXE', 'Deluxe');
define('STANDARD', 'Standard');
define('NO', 'No');
define('PREMIUM_DELUXE', 'Premium Deluxe');
define('YES', 'Yes');
define('EXECUTIVE', 'Executive');
define('LARGE', 'Large');
define('PREMIUM_EXECUTIVE', 'Premium Executive');
define('THE_ROOM_ARHITECTRE', 'The room arhitecture is mingling with Lanna and British Colonial characters whilst the walls and ceilings are docorated with teak wood. Over the facility wise, some rooms furnised with bathtubs. In each room, there is wide screen Smart TV, wifi, cable channels such as CNN, BBC and CCTV.
In The Executive and Premium Executive  are equiped with a Dining set and pantry, at which our guests can invite their friends for private meeting and Dining.');


// index.php
define('ONLINE_BOOKING_TEXT', 'Online Booking');
define('CHECK_IN_DATE_TEXT','Check-in Date');
define('CHECK_OUT_DATE_TEXT','Check-out Date');
define('ADULT_OR_ROOM_TEXT','Adult/Room');
define('SEARCH_TEXT','Search');
define('ENTER_CHECK_IN_DATE_ALERT','Please Enter Check-In Date');
define('ENTER_CHECK_OUT_DATE_ALERT','Please Enter Check-Out Date');
define('CURRENCY_TEXT','Currency');

//booking-search.php
define('SEARCH_INPUT_TEXT','Search Input');
define('CHECK_IN_D_TEXT','Check-in');
define('CHECK_OUT_D_TEXT','Check-out');
define('TOTAL_NIGHTS_TEXT','Total Nights');
define('ADULT_ROOM_TEXT','Adult/Room');
define('SEARCH_RESULT_TEXT','Search Result');
define('SELECT_ONE_ROOM_ALERT','Please Select atleast one room');
define('MAX_OCCUPENCY_TEXT','Max Occupancy');
define('SELECT_NUMBER_OF_ROOM_TEXT','Select Number of Room');
define('ADULT_TEXT','Adults');
define('TOTAL_PRICE_OR_ROOM_TEXT','Total Price / Room');
define('VIEW_PRICE_DETAILS_TEXT','View Price Details');
define('HIDE_PRICE_DETAILS_TEXT','Hide Price Details');
define('CONTINUE_TEXT','Continue');
define('MON','Mon');
define('TUE','Tue');
define('WED','Wed');
define('THU','Thu');
define('FRI','Fri');
define('SAT','Sat');
define('SUN','Sun');
define('MONTH', 'Month');
define('CHILD_PER_ROOM_TEXT','Child / Room');
define('WITH_CHILD','with child');
define('CHILD_TEXT','Child');
define('NONE_TEXT','None');
define('MODIFY_SEARCH_TEXT','Modify Search');
define('VIEW_ROOM_FACILITIES_TEXT','View Room Facilities');
define('VIEW_NIGHTLY_PRICE_TEXT','View Nightly Price');
define('CALENDAR_AVAILABILITY_TEXT','Calendar Availability');
define('CHECK_AVILABILITY','Check Availability');
define('BACK_TEXT','Back');
define('SORRY_ONLINE_BOOKING_CURRENTLY_NOT_AVAILABLE_TEXT','Sorry online booking currently not available. Please try later.');
define('SORRY_YOU_HAVE_ENTERED_A_INVALID_SEARCHING_CRITERIA_TEXT','Sorry you have entered a invalid searching criteria. Please try with invalid searching criteria.');
define('MINIMUM_NUMBER_OF_NIGHT_SHOULD_NOT_BE_LESS_THAN_TEXT','Minimum number of night should not be less than.');
define('PLEASE_MODIFY_YOUR_SEARCHING_CRITERIA_TEXT','Please modify your searching criteria.');
define('BOOKING_NOT_POSSIBLE_FOR_CHECK_IN_DATE_TEXT','Booking not possible for check in date:');
define('PLEASE_MODIFY_YOUR_SEARCHING_CRITERIA_TO_HOTELS_DATE_TIME_TEXT','Please modify your search  criteria according to hotels date time.');
define('HOTELS_CURRENT_DATE_TIME_TEXT','Hotels Current Date Time:');
define('SORRY_NO_ROOM_AVAILABLE_AS_YOUR_SEARCHING_CRITERIA_TRY_DIFFERENT_DATE_SLOT','Sorry no room available as your searching criteria. Please try with different date slot.');
define('PER_ROOM_TEXT','per Room');
define('AND_TEXT','and');
define('FOR_TEXT','for');
//booking_details.php
define('BOOKING_DETAILS_TEXT','Booking Details');
define('CHECKIN_DATE_TEXT','Check-In Date');
define('CHECKOUT_DATE_TEXT','Check-Out Date');
define('TOTAL_NIGHT_TEXT','Total Nights');
define('TOTAL_ROOMS_TEXT','Total Rooms');
define('NUMBER_OF_ROOM_TEXT','Number of Room');
define('ROOM_TYPE_TEXT','Room Type');
define('MAXI_OCCUPENCY_TEXT','Max Occupancy');
define('GROSS_TOTAL_TEXT','Gross Total');
define('SUB_TOTAL_TEXT','Sub Total');
define('TAX_TEXT','Tax');
define('GRAND_TOTAL_TEXT','Grand Total');
define('ADVANCE_PAYMENT_TEXT','Advance Payment');
define('OF_GRAND_TOTAL_TEXT','of Grand Total');
define('CUSTOMER_DETAILS_TEXT','Customer Details');
define('EXISTING_CUSTOMER_TEXT','Existing Customer');
define('EMAIL_ADDRESS_TEXT','Email Address');
define('FETCH_DETAILS_TEXT','Login');
define('OR_TEXT','OR');
define('NEW_CUSTOMER_TEXT','New Customer');
define('TITLE_TEXT','Title');
define('MR_TEXT','Mr');
define('MS_TEXT','Ms');
define('MRS_TEXT','Mrs');
define('MISS_TEXT','Miss');
define('DR_TEXT','Dr');
define('PROF_TEXT','Prof');
define('FIRST_NAME_TEXT','First Name');
define('LAST_NAME_TEXT','Last Name');
define('ADDRESS_TEXT','Address');
define('CITY_TEXT','City');
define('STATE_TEXT','State/Province');
define('POSTAL_CODE_TEXT','Postal Code');
define('COUNTRY_TEXT','Country');
define('PHONE_TEXT','Phone');
define('FAX_TEXT','Fax');
define('EMAIL_TEXT','Email');
define('PAYMENT_BY_TEXT','Payment by');
define('FIELD_REQUIRED_ALERT','This field is required');
define('ADDITIONAL_REQUESTS_TEXT','Any additional requests');
define('I_AGREE_WITH_THE_TEXT',' I agree with the');
define('TERMS_AND_CONDITIONS_TEXT','Terms & Conditions');
define('CONFIRM_TEXT','Confirm');
define('CHECKOUT_TEXT','Checkout');
define('BD_INC_TAX','Including Tax');
define('HOME_TEXT','Home');
define('NEARBY_ATTRACTIONS_TEXT','Nearby Attractions');
//process.class.php
define('INV_BOOKING_NUMBER','Booking Number');
define('INV_CUSTOMER_NAME','Customer Name');
define('INV_ADULT','Adult');
define('INV_PAY_DETAILS','Payment Details');
define('INV_PAY_OPTION','Payment Option');
define('INV_TXN_ID','Transaction ID');
define('PP_REGARDS','Regards');
define('PP_CARRY','You will need to carry a print out of this e-mail and present it to the hotel on arrival and check-in. This e-mail is the confirmation voucher for your booking.');
//offlinecc-payment.php
define('CC_DETAILS','Credit Card Details');
define('CC_HOLDER','Card Holder Name');
define('CC_TYPE','Credit Card Type');
define('CC_NUMBER','Credit Card Number');
define('CC_EXPIRY','Expiry Date');
define('CC_AMOUNT','Total Amount');
define('CC_TOS1','I agree to allow');
define('CC_TOS2','to deduct');
define('CC_TOS3','from my credit card');
define('CC_SUBMIT','Submit');
//booking-confirm.php
define('BOOKING_COMPLETED_TEXT','Booking Completed');
define('THANK_YOU_TEXT','Thank You');
define('YOUR_BOOKING_CONFIRMED_TEXT','Your Booking confirmed');
define('INVOICE_SENT_EMAIL_ADDRESS_TEXT','Invoice sent in your email address');
define('BACK_TO_HOME','Back to Home');
//booking-failure.php
define('BOOKING_FAILURE_TEXT','Booking Failure');
define('BOOKING_FAILURE_ERROR_9', 'Direct access to this page is restricted.');

define('BOOKING_FAILURE_ERROR_13', 'Somebody else already acquire  the reservation lock on rooms specified by you. Reservation lock will be automatically released after few minutes on booking completion or failure by the other person. Please modify your search criteria and try again.');

define('BOOKING_FAILURE_ERROR_22', 'Undefined payment method selected. Please contact administrator.');

define('BOOKING_FAILURE_ERROR_25', 'Failed to send email notification. Please contact technical support.');

define('BOOKING_FAILURE_ERROR_26', 'Booking process Cancelled! if any query please contact us!');

//wizard
define('SELECT_DATES_TEXT','Select Dates');
define('ROOMS_TEXT','Rooms');
define('RATES_TEXT','Rates');
define('YOUR_DETAILS_TEXT','Details');
define('PAYMENT_TEXT','Payment');
define('LOADING_TEXT','loading');
define('BTN_CANCEL','Cancel');

define('ID_TYPE',' Identity Type');
define('PASSPORT','Passport');
define('IDCARD','ID Card');
define('OTHER','Other');
define('ID_NUMBER',' Identity Number');

define('NOT_AVAILABLE', 'Not Available');
define('INCLUDED', 'Included');
define('SERVICE_CHARGE', 'Service Charge');
define('LOCAL_TAX', 'Local Tax');

define('FACILITIES_TEXT', 'Facilities');


define('ROMANTIC_LANNA_COLONIAL_STYLE', 'Romantic Lanna Colonial Style');
define('ROMANTIC_LANNA_COLONIAL_STYLE_DESC', 'The combination of Lanna art and colonial perfectly. The surrounding area has a beautiful garden and mountain of Lanna Applied Design.Within decorate of the western art colonial Style Interior design.');
define('ACCOMMODATION', 'Accommodation');
define('ACCOMMODATION_DESC', 'The villa have 18 guestrooms divided 6 Deluxe, 4 Deluxe Premium 4 Executive and 4 Executive Suite. The luxury amenities, comfortable spaces are offered in every room. Special Atmosphere you will find only in Home Chiang Mai.');

define('RESTAURANT', 'Restaurant');
define('RESTAURANT_OPENNING', 'Breakfast main restaurant open 07.00 am. - 10.00 am.');
define('RESTAURANT_ALA', 'A’LA carte restaurant offer whole day dining service until 24:00 pm.');
define('RESTAURANT_ROOM_SERVICE', 'Room service open 11.00 am. - 24.00 pm.');
define('RESTAURANT_ROOM_BUFFET', 'Buffet: Diet theme buffet, children and vegetarian buffet are available in addition to the presentation of the food and beverage at our open buffet at the main restaurant under the concept.');

define('ESCAPE_THE_CITY_LIFE', 'Escape the city life.');
define('ESCAPE_THE_CITY_LIFE_DESC', 'Staying at the Home Chiang Mai, taking off the journey fatigue and relaxing into the beautiful dream, a perfect place for your choice to escape from the city life to relax with a warm service, unforgettable experience and classic food tour.');

define('EVENTS', 'Events');
define('EVENTS_DESC', 'Hotel provide the service for wedding arrangements or the nice place for pre-wedding photos');

define('LE_MONT_COFFEE', 'Le Mont Coffee');
define('COFFEE_SHOP', 'Coffee Shop');
define('ROOM_SERVICE', 'Room service');
define('FITNESS_ROOM', 'Fitness room');
define('NICE_PLACE_FOR_WEDDING', 'Nice place for pre-wedding');
define('FREE_SPORT_ACTIVE', 'Free sport actives open 08.00 - 20.00 pm. Gymnastics center.');
define('OUTDOOR_POOL', 'Outdoor pool 48 Sqm. Depth 200 cm Swimming pool open 08.00 am. - 20.00 pm.');
define('FACILITY_REMARK', 'OPERATION HOURS AND SERVICES MAY CHANGE DURING SEASON. THESE WILL BE SCHEDULED RELATING TO COAST AND WEATHER CONDITIONS.');

define('GUESTROOM_FEATURES', 'Guestroom Features');
define('PRIVATE_BATHROOM', 'Private bathroom');
define('HOT_SHOWER', 'Hot shower / bathtub');
define('INDIVIDUALLY_AIR', 'Individually controlled air conditioner');
define('WORKING_DESK', 'Working desk');
define('MINIBAR_FREE', 'Mini Bar and refrigerator Free');
define('HAIR_DRYER', 'Hair dryer and full set of toiletries');
define('INDIVIDUALLY_TEL', 'Individual telephone');
define('SAT_LCD', 'Satellite LCD');
define('BATHROBE_SLIPPER', 'Bathrobe and slippers');
define('DIRECT_DEL_PHONE', 'Direct dial phone');
define('TEA_AND_COFFEE', 'Tea and Coffee making facilities');
define('WIFI_CONNECTION', 'Wifi Connection');

define('ROOMS', 'Rooms');
define('ROOMS_DESC', 'The villa have 18 guestrooms divided 6 Deluxe, 4 Deluxe Premium 4 Executive and 4 Executive Suite. The luxury amenities, comfortable spaces are offered in every room. Special Atmosphere you will find only in Home Chiang Mai.');
define('HOTEL', 'Hotel');
define('PICTURES', 'Pictures');
define('FOOD_AND_DRINKS', 'Food And Drinks');
define('LEMONT_COFFEE_HOUSE', 'Le Mont Coffee House');
define('EAT_THAI_ENGLAND', 'EAT THAI England');
define('DIRECT_DEBIT', 'Direct Debit');
define('COUNTER_PAYMENT', 'Counter Payment');
define('ATM_PAYMENT', 'ATM Payment');
define('CREDIT_CARD_PAYMENT', 'Credit Card Payment');

define('CONTACT_INFO', 'Contact Info');
define('ZIP', 'Post Code');
define('TEL', 'Tel');
define('SEND', 'Send');
define('YOUR_NAME', 'Your Name');
define('EMAIL_OR_PHONE', 'Email or Phone Number');
define('SUBJECT', 'Subject');
define('CONTENT', 'Content');

define('SERVICE_UNVAILABLE', 'Service is unavailable');

define('ABOUT_DESC', 'Over two hundred years, one of the traditions of the Lanna Kingdom have celebrated during the New Year festival is to water the right coronal of the Lord Buddha, at which the relic has presided more than 1,000 years  at Wat Pharthat Sri Jom Tong where located 56 km away from the center of Chiang Mai. Every year, the ceremony started by mobilising the relic to center of Chiang Mai at Wat Suan Dorg. The Home Chiang Mai is situated in the suburban of Chiang Mai; whereby the hotel area has been an important passing way for the ceremonial journey. Home believes in serenity and wholehearted service providing to our guests in every square inch of the hotel at all time. A lieu of extra features we offer Thai fusion cuisine, in which are designed and cooked by a top chef who trained at Le Cordon Bleu in the UK. Of course, it is second to none.');
define('CONTENT_UNDER_CONSTRUCTION', 'Under construction');
define('EG_PASSPORT_NUMBER', 'e.g.: passport number');

define('PRICE_UNAVAILABLE', 'Sorry, Price unavailable for your date range.');


define('REF', 'Ref');

define('WAT_TITLE', 'The Wat Inthrawat (Wat Ton Kwen) (5 minutes from the Hotel)');
define('WAT_P1', 'The Wat Inthrawat in the countryside of Chiang Mai province is one of the finest examples of classic Lanna style architecture in Northern Thailand. The complex consists of a viharn, an open pavilion and a large open mondop.');
define('WAT_P2', 'The temple, locally also known as the Wat Ton Kwen or Wat Ton Khwen, is one of the very few remaining wooden temples in its original state in Chiang Mai province. The temple is under the care of the Thai Fine Arts Department. Renovations have been carried out aiming to leave the original structures in tact as much as possible.');
define('WAT_P3', 'The simple and small, yet very elegant viharn was built in 1858 in Lanna style with Thai Lü influence. It is surrounded on three sides by an open pavilion. The stairs of the viharn contain large mythological Naga snakes on either side guarding the entrance of the structure.');
define('WAT_P4', 'The viharn is partially surrounded by an open pavilion named Sala Bat. The original wooden floor of the Sala Bat has been replaced with a concrete one. The mondop was built as a temporary shelter for the Phra Boromathat Chom Thong Buddha relic, which is now enshrined in the Wat Phra That Si Chom Thong temple further South West of Chiang Mai. ');

define('ROYAL_PARK_TITLE', 'The Royal Park Rajapruek (10 minutes from the Hotel)');
define('ROYAL_PARK_P1', 'The Royal Park Rajapruek was built in the purpose of commemorating the 60th anniversary of the late King Bhumibol Adulyadej’s Accession to the throne on June 9, 2006 as well as celebrating his 80th Birthday Anniversary on December 5, 2007. It is one of the most popular agro-tourism places of Chiang Mai and is counted as the center of Agricultural research. You will discover countless number of plants and varied kinds of agricultural exposition which would be advantageous to visitors at any age. Also, it is reckoned as a meeting point for agriculturist and those who are into agriculture and cultivation.');
define('ROYAL_PARK_P2', 'The landscape architecture of the park is gorgeous and very delicate. Thus, Ho Kham Luang Royal Pavilion and other architectural style buildings here can prove that the saying is totally true. The exposition is divided distinctly into zones which make it easier for visitors to wander around the park and learn from everything you see since the very first step of yours here as there is a 250 million years old pine tree maintained at the entrance.');

define('BAN_TAWAI_TITLE', 'Ban Thawai (10 minutes from the Hotel)');
define('BAN_TAWAI_P1', 'Ban Thawai, a woodcrafts village, is considered one of the most famous cultural tourist attractions in Chiang Mai. It has been known as the major cultural attraction of Chiang Mai for Thai and foreign tourists.');
define('BAN_TAWAI_P2', 'The best quality and bargains of wood carving items can be found in Ban Thawai. If you’re just here for a little souvenir shopping you won’t regret squeezing a half day tour in to your Northern Thailand schedule. Ban Thawai is the cheapest place in Thailand to find silk, antique reproductions, incense, candles, oil and soap gift packs, ornaments, lamps, bamboo products, wall hangings, frames, vases and much more. Most the items on display at Ban Thawai are uniquely Thai, many characteristically Northern and hand made from natural materials.');
define('BAN_TAWAI_P3', 'Ban Thawai is also famous for Asian antiques, some of which rank among the world’s best. ');

define('MUANG_KUNG_TITLE', 'Muang Kung Pottery Village');
define('MUANG_KUNG_P1', 'Muang Kung Village has been told its history by the ancestors from generations to generations that their ancestors who settled down in this village were Tai people who had been herded from Mong Phu and Mong Hsat in Kengtung, currently is a town in Shan State, Burma. It was an escape from an invasion of Burmese soldiers and there were only 6 families moved. ');
define('MUANG_KUNG_P2', 'Almost every family in this village does pottery which is what they do as their main job. If you walk by little concrete streets in the village, you will find cruses in various type and different sizes including vases, pots, etc. lined drying in the courtyards. Moreover, on basements, there are many elders actively help painting earthenware. The most popular product is the boiler that has narrow top, bulge middle, and tapering bottom with a lid.');

define('NIGHT_SAFARI_TITLE', 'Night Safari');
define('NIGHT_SAFARI_P1', 'It is considered by many to be the most beautiful Night Safari in the world. Twice the size of the Singapore Night Safari it stands on over 300');
define('NIGHT_SAFARI_P2', 'There is a day time area called the Jaguar Trail Zone and consists of a 1.2km walking trail around a lovely lake. There are over 400 animals to see here including White Tigers, Jaguars, Leopards, Tapirs, Monkeys, Camels, miniature Horses, Crowned and they even have wallabies and koalas.');
define('NIGHT_SAFARI_P3', 'The night time area is split into two zones, the Savanna Safari Zone and the Predator Prowl Zone. These sections are open from 6pm till midnight daily. Visitors travel through the different zones in the Night Safari in an open sided tram. The tram ride is around 5km and takes about 1hr to get around it all. Trams run from 7pm till 10.30pm, with a tram every 15mins.');

define('NIGHT_BAZAAR_TITLE', 'Night Bazaar');
define('NIGHT_BAZAAR_P1', 'Chiang Mai Night Bazaar is a commercial wonderland of shops, hotels, restaurants and bars, and most visitors to Chiang Mai will find themselves here at least once – especially after 18:00 when all of the market stalls open for business. Although only a small area there is plenty to experience, and it’s fair to say the Night Bazaar is as hectic as Chiang Mai can become. Not surprisingly with a name like Night Bazaar, markets feature heavily in our list of attractions, but we have also sourced some interesting museums and galleries for a little culture. We have covered the whole area of Chiang Mai Night Bazaar and have compiled the best of what to see and do to make your stay a memorable one.');

define('DOI_INTHANON_TITLE', 'Doi Inthanon');
define('DOI_INTHANON_P1', 'Doi Inthanon is the highest mountain in Thailand, rising 2,565 meters above sea level. Doi Inthanon National Park has lots of amazing variation in its lush geography and forest. It is home to rainforest, mixed forest, and pine forest. The weather is cool throughout the year, especially in the winter season when there will be almost constant fog coverage all day and a stunning phenomenon of Mae Kha Ning or frost flower in the early morning.');
define('DOI_INTHANON_THING_TODO_TITLE', 'Things to do in Doi Inthanon');
define('DOI_INTHANON_THING_TODO_1', 'Pay homage to Phramahathat Napamathanidol and Phramahathat Napaphol Bhumisiri (the Great Holy Relics Pagodas). ');
define('DOI_INTHANON_THING_TODO_2', 'Savor the beauty of mystical morning winter mist.');
define('DOI_INTHANON_THING_TODO_3', 'Explore the Mae Klang, Wachirathan, Siriphum, Mae Ya Waterfalls and Kew Mae Pan and Ang Ka nature trails.');
define('DOI_INTHANON_THING_TODO_4', 'See Mae Klang Luang Rice Terraces');
define('DOI_INTHANON_THING_TODO_5', 'Participate in bird watching ');
define('DOI_INTHANON_THING_TODO_6', 'Participate in sky observing at the Thai National Observatory, one of the most advanced optical astronomy facilities in ASEAN.');
define('DOI_INTHANON_THING_TODO_7', 'Explore the Royal Agricultural Station Inthanon ');

define('NIMMAN_TITLE', 'Nimman Road');
define('NIMMAN_TITLE_DETAIL', 'A collection of local and international cuisines with exotic night.');
define('NIMMAN_DETAIL_1', 'The best Northern Noodles, Khao Soi Mae Sai, at Ratchaphuek Alley, the opposite side of Huaykaew Road,');
define('NIMMAN_DETAIL_2', 'The best Lanna Food at Tong Tem Toh, Nimman Soi 13');
define('NIMMAN_DETAIL_3', 'The best Coffee Shop at Ristr8to Lab, Nimman Soi 3');
define('NIMMAN_DETAIL_4', 'The best Dessert Café at iBerry Gardens, Nimman 17');
define('NIMMAN_DETAIL_5', 'The best Japanese Curry at Mu’s Katsu, Nimman Soi 8');
define('NIMMAN_DETAIL_6', 'The best beer Best Health Food: The Salad Concept, Soi 13');
define('NIMMAN_DETAIL_7', 'The best Bar at Beer Lab, Nimman, Soi 12');
define('NIMMAN_DETAIL_8', 'The best night club at Club Café, Nimmam Soi 7');

define('PHOTO_BY', 'Photo By');
define('OUR_VIDEO', 'Our Video');
define('PROMOTIONS', 'Promotions');

?>
