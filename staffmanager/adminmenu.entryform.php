<?php
include 'access.php';
include 'header.php';
$msg='';

if (isset($_POST['Update'])) {

    $role = !empty($_POST['role']) ? json_encode($_POST['role']) : '[]';

    if (!$_POST['id']):
        $r = $mysqli->query("insert into bsi_adminmenu(name,url,ord, parent_id, role) values ('" . $_POST['name'] . "','" . $_POST['url'] . "'," . $_POST['ord'] . ',' . $_POST['parent_id'] . ',' . "'{$role}'" . ')');
        // echo "insert into bsi_adminmenu(name,url,ord, parent_id) values ('". $_POST['name'] ."','".$_POST['url'] ."',". $_POST['ord'].",". $_POST['parent_id'].")";
        $id = $mysqli->insert_id;
    else:
        $r =$mysqli->query("update bsi_adminmenu set name='" . $_POST['name'] . "',url='" . $_POST['url'] . "',ord=" . $_POST['ord'] . ",role='{$role}'" . ' where id=' . $_POST['id']);
        $id=$_POST['id'];
    // echo "update bsi_adminmenu set name='".$_POST['name']."',url='".$_POST['url']."',ord=".$_POST['ord']." where id=". $_POST['id'];

    endif;

    if ($r):
       $msg='<font face=verdana size=1 color=#111111><b>Update Successful.</b></font>';
    echo"<script>window.location.href='adminmenu.list.php';</script>"; else:
       $msg='<font face=verdana size=1 color=red><b>Update Failure.</b></font>';
    endif;
}

if (isset($_REQUEST['id'])) {
    $r = $mysqli->query('select * from  bsi_adminmenu where id=' . $_REQUEST['id']);
    //$d=mysql_fetch_array($r);
    $d   =$r->fetch_assoc();
    $name=$d['name'];
    $url =$d['url'];
    $ord =$d['ord'];
    $roles = empty($d['role']) ? [] : json_decode($d['role'], true);
    $id  =$_REQUEST['id'];
} else {
    $name='';
    $url ='';
    $ord ='';
    $id  ='';
    $roles = [];
}
if (isset($_REQUEST['parent_id'])) {
    $r=$mysqli->query('select * from  bsi_adminmenu where id=' . $_REQUEST['parent_id']);
    //$dd=mysql_fetch_array($r);
    $dd  =$r->fetch_assoc();
    $pid=$_REQUEST['parent_id'];
} else {
    $pid=0;
}

if ($msg) {
    echo'<div align=center class=lnk>' . $msg . '</div>';
}
?>
<link rel="stylesheet" type="text/css" href="css/jquery.validate.css" />
<div id="container-inside">
    <span style="font-size:16px; font-weight:bold"><?=ADMN_TITLE; ?>  <?php if (isset($_REQUEST['parent_id'])) {
    echo'(Under : ' . $dd['name'] . ')';
}?>  : <?=$name; ?></span>
    <hr />
    <form method="post" action="<?=$_SERVER['PHP_SELF']; ?>" id="form1">
        <input type="hidden" name="parent_id" value="<?=$pid; ?>">
        <input type="hidden" name="id" value="<?=$id; ?>">

        <table width="60%" border="0" cellpadding="3">


            <tr>
                <td valign=top>
                    <?=ADMN_TITLE1; ?>
                </td>
                <td><input type="text" size=25 name="name" value="<?=$name; ?>" class="required" /></td>
            </tr>
            <tr>
                <td valign=top>
                    <?=ADMN_FNAME; ?>
                </td>
                <td><input type="text" size=25 name="url" value="<?=$url; ?>" class="required" /> [Example. test.php]</td>
            </tr>
            <tr>
                <td valign=top nowrap="nowrap">
                    <?=ADMN_ORDER1; ?>
                </td>
                <td><input type="text" size=5 name="ord" value="<?=$ord; ?>" class="required" /> [Example. 2]</td>
            </tr>
            <tr>
                <td>
                    Role
                </td>
                <td>
                    <?php foreach (USER_TYPE as $type): ?>
                        <p style="padding:0; margin:0;">
                            <label>
                                <input <?php echo in_array($type, $roles) ? 'checked' : '' ?> <?php echo $type == 'Administrator' ? 'checked disabled' : '' ?> type="checkbox" name="role[]" value="<?php echo $type; ?>"> <?php echo $type; ?>
                            </label>
                        </p>
                    <?php endforeach; ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="<?php echo ADD_EDIT_CAPACITY_SUBMIT;?>" name="Update" style="background:#e5f9bb; cursor:pointer; cursor:hand;" /></td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
    $().ready(function() {
        $("#form1").validate();

    });
</script>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<?php include 'footer.php'; ?>
