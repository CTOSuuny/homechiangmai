<?php

define('SUBTITLE_MESSAGE', 'เพลิดเพลินกับการเข้าพัก');
define('WELCOME_MESSAGE', 'Home Chiang Mai');
define('WELCOME_TO', 'ยินดีต้อนรับเข้าสู่');
define('WELCOME_QOUTE', 'ที่พักแสนสงบ ผสมผสานด้วยวัฒนธรรมล้านนาและเอกลักษณ์รูปแบบโคโลเนียล');

define('CHILDREN_TEXT','เด็ก');
define('BOOK_YOUR_ROOM_NOW', 'จองตอนนี้');
define('HOTEL_DETAIL', 'Romantic Lanna Colonial Style The Combination of Lanna art and colonial perfectly. The surrounding area has a beautiful garden and mountain of Lanna Applied Design. Within decorate of the western art colonial Style Interior design. Accommodation The villa have 18 guestrooms divided 6 Deluxe, 4 Deluxe Premium 4 Executive and 4 Executive Suite. The luxury amenities comfortable spaces are offered in every room. Special Atmosphere you will find only in Home Chiangmai. Staying at the Home Chiangmai, talking off the journey fatigue and relaxing into the beautiful dream, a perfect place for your choice to escape from the city life to relax with a warm service, unforgettable experience and classic food tour. Design to offer a warm welcome and friendly services minded to short stay guests, while we\'re offering espercially care our long stay guests to feel comfortable and safety in capable hands. In the moment of your arrival at the resort, you are treated to the highest standard of services, tasteful hospitality and attractive recreation.');
define('GALLERY_TEXT', 'รูปภาพ');
define('ABOUT_US', 'เกี่ยวกับเรา');
define('CONTACT_US', 'ติดต่อเรา');
define('QUICK_LINKS', 'ลิงก์');
define('ADDRESS', 'ที่อยู่');
define('ADDRESS_NAME', '59 หมู่ 6 ตำบล หนองแก๋ว, อำเภอ หางดง, จังหวัดเชียงใหม่');
define('CONNECT_WITH_US', 'เชื่อมต่อกับเรา');
define('CHOOCE_CHECK_IN_DATE', 'เลือกวันเช็คอิน');
define('CHOOCE_CHECK_OUT_DATE', 'เลือกวันเช็คเอาท์');
define('SELECT_ROOM', 'เลือกห้อง');
define('CANNOT_SWITCH_LANGUAGE_DURING_BOOKING', 'ขออภัยไม่สามารถเปลี่ยนภาษาได้ขณะจองห้องพัก');
define('NAME_TITLE', 'คำนำหน้า');
define('TO_OFFER_THAI_FOOD', 'หนึ่งในบริการที่โรงแรมต้องการนำเสนอคือร้านอาหาร EAT THAI ซึ่งเป็นสุดยอดร้านอาหารในประเทศสหราชอาณาจักร ให้บริการอาหารไทยแบบฟิวชั่นมามากกว่า 16 ปี พร้อมรางวัลการันตีมากมาย อาทิ Michellin Guide in 2006-2013 และ TripAdvisor โดยร้านอาหาร EAT THAI จะเปิดให้บริการในเดือนเมษายนนี้');
define('THERE_ARE_THREE', 'เมนูอาหารและเครื่องดื่มที่ทางโรงแรมให้บริการแบ่งออกเป็น 3 ส่วนคือ เมนูร้านอาหาร ร้านกาแฟ และเมนูที่บริการถึงห้องพัก');
define('OPEN_DAILY', 'เปิดให้บริการตั้งแต่ 7.00 น. จนถึง 19.00 น.');

define('WE_OFFER', 'เรามีห้องพักให้บริการเพื่อรองรับต่อความต้องการของผู้เข้าพัก โดยรายละเอียดตามตารางดังนี้');
define('ROOM_TYPE', 'ประเภทของห้องพัก');
define('SIZE', 'ขนาด');
define('HIGH_CEILING', 'เพดานสูง');
define('PANTRY', 'ตู้กับข้าว');
define('Dining_TABLE', 'โต๊ะอาหาร');
define('BATHHUB', 'อ่างอาบน้ำ');
define('DELUXE', 'Deluxe');
define('STANDARD', 'มาตรฐาน');
define('NO', 'ไม่');
define('PREMIUM_DELUXE', 'Premium Deluxe');
define('YES', 'มี');
define('EXECUTIVE', 'Executive');
define('LARGE', 'ใหญ่');
define('PREMIUM_EXECUTIVE', 'Premium Executive');
define('THE_ROOM_ARHITECTRE', 'สถาปัตยกรรมและรูปแบบของห้องพักมีการออกแบบโดยผสมผสานระหว่างความเป็นล้านนาและรูปแบบโคโลเนียล พื้นและฝาผนังถูกตกแต่งด้วยไม้สัก ภายในห้องมีเครื่องอำนวยสะดวกได้แก่ อ่างอาบน้ำ, Wifi, Smart TV ที่รองรับสัญญาจากสถานีโทรศัพท์ชื่อดังอย่าง CNN, BBC และ CCTV นอกจากนี้ในห้องพักแบบ Executive และ Premium Executive ยังประกอบไปด้วย โต๊ะอาหารและตู้กับข้าว เพื่อให้ผู้เข้าพักทุกท่านสามารถชักชวนเพื่อนๆมาพูดคุยและรับประทานอาหารร่วมกันได้');


define('ID_TYPE',' ประเภทประจำตัว');
define('PASSPORT','พาสปอร์ต');
define('IDCARD','หมายเลขบัตรประจำตัวประชาชน');
define('OTHER','อื่นๆ');
define('ID_NUMBER',' หมายเลขบัตรประจำตัว');
define('ONLINE_BOOKING_TEXT','จองออนไลน์');
define('CHECK_IN_DATE_TEXT','วันเช็คอิน');
define('CHECK_OUT_DATE_TEXT','วันเช็คเอาท์');
define('ADULT_OR_ROOM_TEXT','ห้องผู้ใหญ่');
define('SEARCH_TEXT','ค้นหา');
define('ENTER_CHECK_IN_DATE_ALERT','กรุณาระบุวันที่เช็คอิน');
define('ENTER_CHECK_OUT_DATE_ALERT','กรุณาระบุวันเช็คเอาท์');
define('CURRENCY_TEXT','สกุลเงิน');
define('SEARCH_INPUT_TEXT','ป้อนข้อมูลการค้นหา');
define('CHECK_IN_D_TEXT','วันเช็คอิน');
define('CHECK_OUT_D_TEXT','วันเช็คเอาท์');
define('TOTAL_NIGHTS_TEXT','รวมคืนเข้าพัก');
define('ADULT_ROOM_TEXT','จำนวนผู้ใหญ่/ห้อง');
define('SEARCH_RESULT_TEXT','ผลการค้นหา');
define('SELECT_ONE_ROOM_ALERT','โปรดเลือกอย่างน้อยหนึ่งห้อง');
define('MAX_OCCUPENCY_TEXT','ผู้เข้าพักสูงสุด');
define('SELECT_NUMBER_OF_ROOM_TEXT','เลือกจำนวนห้อง');
define('ADULT_TEXT','ผู้ใหญ่');
define('TOTAL_PRICE_OR_ROOM_TEXT','ราคารวม / ห้อง');
define('VIEW_PRICE_DETAILS_TEXT','ดูรายละเอียดราคา');
define('HIDE_PRICE_DETAILS_TEXT','ซ่อนรายละเอียดราคา');
define('CONTINUE_TEXT','ดำเนินการต่อไป');
define('MON','จันทร์');
define('TUE','อังคาร');
define('WED','พ.');
define('THU','ทู');
define('FRI','วันศุกร์');
define('SAT','วันเสาร์');
define('SUN','ซัน');
define('MONTH','เดือน');
define('CHILD_PER_ROOM_TEXT','จำนวนเด็ก/ห้อง');
define('WITH_CHILD','กับเด็ก');
define('CHILD_TEXT','เด็ก');
define('NONE_TEXT','ไม่มี');
define('MODIFY_SEARCH_TEXT','แก้ไขการค้นหา');
define('VIEW_ROOM_FACILITIES_TEXT','ดูสิ่งอำนวยความสะดวก');
define('VIEW_NIGHTLY_PRICE_TEXT','ดูราคาคืน');
define('CALENDAR_AVAILABILITY_TEXT','ปฏิทินวันว่างพร้อมเข้าพัก');
define('CHECK_AVILABILITY','ตรวจสอบห้องว่าง');
define('BACK_TEXT','ย้อนกลับ');
define('SORRY_ONLINE_BOOKING_CURRENTLY_NOT_AVAILABLE_TEXT','ขอจองออนไลน์ในขณะนี้ไม่พร้อมใช้งาน กรุณาลองใหม่ในภายหลัง');
define('SORRY_YOU_HAVE_ENTERED_A_INVALID_SEARCHING_CRITERIA_TEXT','ขออภัยคุณป้อนเงื่อนไขการค้นหาไม่ถูกต้อง กรุณาลองกับเงื่อนไขการค้นหาไม่ถูกต้อง');
define('MINIMUM_NUMBER_OF_NIGHT_SHOULD_NOT_BE_LESS_THAN_TEXT','จำนวนคืนที่ต่ำสุดไม่ควรน้อยกว่า');
define('PLEASE_MODIFY_YOUR_SEARCHING_CRITERIA_TEXT','กรุณาปรับเปลี่ยนเงื่อนไขการค้นหา');
define('BOOKING_NOT_POSSIBLE_FOR_CHECK_IN_DATE_TEXT','จองไม่ได้สำหรับการตรวจสอบในวัน:');
define('PLEASE_MODIFY_YOUR_SEARCHING_CRITERIA_TO_HOTELS_DATE_TIME_TEXT','โปรดแก้ไขเกณฑ์การค้นหาตามโรงแรมเวลาวัน');
define('HOTELS_CURRENT_DATE_TIME_TEXT','โรงแรมเวลาวันปัจจุบัน:');
define('SORRY_NO_ROOM_AVAILABLE_AS_YOUR_SEARCHING_CRITERIA_TRY_DIFFERENT_DATE_SLOT','ขออภัยไม่มีห้องว่างเป็นเกณฑ์การค้นหา กรุณาลองกับช่องวันแตกต่างกัน');
define('PER_ROOM_TEXT','ต่อห้อง');
define('AND_TEXT','และ');
define('FOR_TEXT','สำหรับ');
define('BOOKING_DETAILS_TEXT','รายละเอียดการจอง');
define('CHECKIN_DATE_TEXT','วันเช็คอิน');
define('CHECKOUT_DATE_TEXT','วันเช็คเอาท์');
define('TOTAL_NIGHT_TEXT','รวมคืนเข้าพัก');
define('TOTAL_ROOMS_TEXT','ห้องรวม');
define('NUMBER_OF_ROOM_TEXT','จำนวนห้องพัก');
define('ROOM_TYPE_TEXT','ประเภทห้องพัก');
define('MAXI_OCCUPENCY_TEXT','ผู้เข้าพักสูงสุด');
define('GROSS_TOTAL_TEXT','รวมทั้งหมด');
define('SUB_TOTAL_TEXT','ผลรวมย่อย');
define('TAX_TEXT','ภาษี');
define('GRAND_TOTAL_TEXT','ผลรวมทั้งหมด');
define('ADVANCE_PAYMENT_TEXT','ชำระเงินล่วงหน้า');
define('OF_GRAND_TOTAL_TEXT','ของผลรวมทั้งหมด');
define('CUSTOMER_DETAILS_TEXT','รายละเอียดของลูกค้า');
define('EXISTING_CUSTOMER_TEXT','ลูกค้าที่มีอยู่');
define('EMAIL_ADDRESS_TEXT','ที่อยู่อีเมล์');
define('FETCH_DETAILS_TEXT','นำรายละเอียด');
define('OR_TEXT','หรือ');
define('NEW_CUSTOMER_TEXT','ลูกค้าใหม่');
define('TITLE_TEXT','ชื่อเรื่อง');
define('MR_TEXT','นาย');
define('MS_TEXT','Ms');
define('MRS_TEXT','นาง');
define('MISS_TEXT','นางสาว');
define('DR_TEXT','Dr');
define('PROF_TEXT','ศาสตราจารย์');
define('FIRST_NAME_TEXT','ชื่อ');
define('LAST_NAME_TEXT','นามสกุล');
define('ADDRESS_TEXT','ที่อยู่');
define('CITY_TEXT','อำเภอ');
define('STATE_TEXT','จังหวัด');
define('POSTAL_CODE_TEXT','รหัสไปรษณีย์');
define('COUNTRY_TEXT','ประเทศ');
define('PHONE_TEXT','โทรศัพท์');
define('FAX_TEXT','โทรสาร');
define('EMAIL_TEXT','อีเมล');
define('PAYMENT_BY_TEXT','การชำระเงิน');
define('FIELD_REQUIRED_ALERT','ฟิลด์นี้จะต้อง');
define('ADDITIONAL_REQUESTS_TEXT','คำขอใด ๆ เพิ่มเติม');
define('I_AGREE_WITH_THE_TEXT',' ฉันเห็นด้วยกับการ');
define('TERMS_AND_CONDITIONS_TEXT','เงื่อนไขการให้บริการ');
define('CONFIRM_TEXT','ยืนยัน');
define('CHECKOUT_TEXT','เช็คเอาท์');
define('BD_INC_TAX','รวมภาษี');
define('HOME_TEXT','หน้าหลัก');
define('NEARBY_ATTRACTIONS_TEXT','สถานที่ใกล้เคียง');
define('INV_BOOKING_NUMBER','หมายเลขการจอง');
define('INV_CUSTOMER_NAME','ชื่อลูกค้า');
define('INV_ADULT','ผู้ใหญ่');
define('INV_PAY_DETAILS','รายละเอียดการชำระเงิน');
define('INV_PAY_OPTION','ตัวเลือกการชำระเงิน');
define('INV_TXN_ID','ID ธุรกรรม');
define('PP_REGARDS','ขอแสดงความนับถือ');
define('PP_CARRY','คุณสามารถพิมพ์หรือใช้อีเมลฉบับนี้เพื่อยืนยันการเข้าพักของคุณกับโรงแรม');
define('CC_DETAILS','รายละเอียดบัตรเครดิต');
define('CC_HOLDER','ชื่อเจ้าของบัตร');
define('CC_TYPE','ชนิดของบัตรเครดิต');
define('CC_NUMBER','หมายเลขบัตรเครดิต');
define('CC_EXPIRY','วันหมดอายุ');
define('CC_AMOUNT','ยอดเงินรวม');
define('CC_TOS1','ฉันยอมรับให้');
define('CC_TOS2','การหัก');
define('CC_TOS3','จากบัตรเครดิตของฉัน');
define('CC_SUBMIT','ส่ง');
define('BOOKING_COMPLETED_TEXT','จองเสร็จสมบูรณ์');
define('THANK_YOU_TEXT','ขอบคุณ');
define('YOUR_BOOKING_CONFIRMED_TEXT','ยืนยันการจองห้องพัก');
define('INVOICE_SENT_EMAIL_ADDRESS_TEXT','ใบแจ้งหนี้ที่ส่งไปในอีเมลของคุณ');
define('BACK_TO_HOME','กลับหน้าหลัก');
define('BOOKING_FAILURE_TEXT','ความล้มเหลวในการจอง');
define('BOOKING_FAILURE_ERROR_9','มีการจำกัดการเข้าถึงเพจนี้');
define('BOOKING_FAILURE_ERROR_13','คนอื่นแล้วได้รับล็อคจองห้องตามคุณ จองล็อคจะโดยอัตโนมัติออกหลังจากจองสำเร็จหรือล้มเหลวเพียงไม่กี่นาทีที่ผู้อื่น กรุณาปรับเปลี่ยนเงื่อนไขการค้นหาของคุณ และลองอีกครั้ง');
define('BOOKING_FAILURE_ERROR_22','เลือกวิธีการชำระเงินไม่ได้กำหนด กรุณาติดต่อผู้ดูแลระบบ');
define('BOOKING_FAILURE_ERROR_25','ล้มเหลวในการส่งอีเมล์แจ้งเตือน โปรดติดต่อฝ่ายสนับสนุนทางเทคนิค');
define('BOOKING_FAILURE_ERROR_26','กระบวนการยกเลิกการจอง ถ้าแบบสอบถามใดๆ กรุณาติดต่อเรา');
define('SELECT_DATES_TEXT','เลือกวันที่');
define('ROOMS_TEXT','ห้อง');
define('RATES_TEXT','ราคาพิเศษ');
define('YOUR_DETAILS_TEXT','ข้อมูลผู้เข้าพัก');
define('PAYMENT_TEXT','การชำระเงิน');
define('LOADING_TEXT','โหลด');
define('BTN_CANCEL','ยกเลิก');

define('NOT_AVAILABLE', 'ไม่มีห้องว่าง');
define('INCLUDED', 'ราคานี้รวม');
define('SERVICE_CHARGE', 'ค่าบริการ');
define('LOCAL_TAX', 'ภาษีท้องถิ่น');
define('FACILITIES_TEXT', 'สิ่งอำนวยความสะดวก');


define('ROMANTIC_LANNA_COLONIAL_STYLE', 'Romantic Lanna Colonial Style');
define('ROMANTIC_LANNA_COLONIAL_STYLE_DESC', 'The combination of Lanna art and colonial perfectly. The surrounding area has a beautiful garden and mountain of Lanna Applied Design.Within decorate of the western art colonial Style Interior design.');
define('ACCOMMODATION', 'Accommodation');
define('ACCOMMODATION_DESC', 'The villa have 18 guestrooms divided 6 Deluxe, 4 Deluxe Premium 4 Executive and 4 Executive Suite. The luxury amenities, comfortable spaces are offered in every room. Special Atmosphere you will find only in Home Chiang Mai.');

define('RESTAURANT', 'Restaurant');
define('RESTAURANT_OPENNING', 'Breakfast main restaurant open 07.00 am. - 10.00 am.');
define('RESTAURANT_ALA', 'A’LA carte restaurant offer whole day dining service until 24:00 pm.');
define('RESTAURANT_ROOM_SERVICE', 'Room service open 11.00 am. - 24.00 pm.');
define('RESTAURANT_ROOM_BUFFET', 'Buffet: Diet theme buffet, children and vegetarian buffet are available in addition to the presentation of the food and beverage at our open buffet at the main restaurant under the concept.');

define('ESCAPE_THE_CITY_LIFE', 'Escape the city life.');
define('ESCAPE_THE_CITY_LIFE_DESC', 'Staying at the Home Chiang Mai, taking off the journey fatigue and relaxing into the beautiful dream, a perfect place for your choice to escape from the city life to relax with a warm service, unforgettable experience and classic food tour.');

define('EVENTS', 'Events');
define('EVENTS_DESC', 'Hotel provide the service for wedding arrangements or the nice place for pre-wedding photos');

define('COFFEE_SHOP', 'Coffee Shop');
define('ROOM_SERVICE', 'Room service');
define('FITNESS_ROOM', 'Fitness room');
define('NICE_PLACE_FOR_WEDDING', 'Nice place for pre-wedding');
define('FREE_SPORT_ACTIVE', 'Free sport actives open 08.00 - 20.00 pm. Gymnastics center.');
define('OUTDOOR_POOL', 'Outdoor pool 48 Sqm. Depth 200 cm Swimming pool open 08.00 am. - 20.00 pm.');
define('FACILITY_REMARK', 'OPERATION HOURS AND SERVICES MAY CHANGE DURING SEASON. THESE WILL BE SCHEDULED RELATING TO COAST AND WEATHER CONDITIONS.');

define('GUESTROOM_FEATURES', 'Guestroom Features');
define('PRIVATE_BATHROOM', 'Private bathroom');
define('HOT_SHOWER', 'Hot shower / bathtub');
define('INDIVIDUALLY_AIR', 'Individually controlled air conditioner');
define('WORKING_DESK', 'Working desk');
define('MINIBAR_FREE', 'Mini Bar and refrigerator Free');
define('HAIR_DRYER', 'Hair dryer and full set of toiletries');
define('INDIVIDUALLY_TEL', 'Individual telephone');
define('SAT_LCD', 'Satellite LCD');
define('BATHROBE_SLIPPER', 'Bathrobe and slippers');
define('DIRECT_DEL_PHONE', 'Direct dial phone');
define('TEA_AND_COFFEE', 'Tea and Coffee making facilities');
define('WIFI_CONNECTION', 'Wifi Connection');

define('ROOMS', 'ห้องพัก');
define('ROOMS_DESC', 'The villa have 18 guestrooms divided 6 Deluxe, 4 Deluxe Premium 4 Executive and 4 Executive Suite. The luxury amenities, comfortable spaces are offered in every room. Special Atmosphere you will find only in Home Chiang Mai.');
define('HOTEL', 'Hotel');
define('PICTURES', 'รูปภาพ');
define('FOOD_AND_DRINKS', 'อาหารและเครื่องดื่ม');
define('LEMONT_COFFEE_HOUSE', 'Le Mont Coffee House');
define('EAT_THAI_ENGLAND', 'EAT THAI England');

define('DIRECT_DEBIT', 'ชำระผ่านการโอน');
define('COUNTER_PAYMENT', 'ชำระผ่าน Counter');
define('ATM_PAYMENT', 'ชำระผ่าน ATM');
define('CREDIT_CARD_PAYMENT', 'ชำระผ่านบัตรเครดิต');

define('CONTACT_INFO', 'ข้อมูลการติดต่อ');
define('ZIP', 'รหัสไปรษณีย์');
define('TEL', 'โทร');
define('SEND', 'ส่ง');
define('YOUR_NAME', 'ชื่อ');
define('EMAIL_OR_PHONE', 'อีเมลหรือเบอร์โทรศัพท์');
define('SUBJECT', 'หัวข้อ');
define('CONTENT', 'เนื้อหา');

define('LE_MONT_COFFEE', 'Le Mont Coffee');
define('SERVICE_UNVAILABLE', 'บริการนี้ยังไม่พร้อมใช้งาน');


define('ABOUT_DESC', 'ประเพณีสำคัญของอาณาจักรลานนาที่มีมากว่า 200 ปี คือการเฉลิมฉลองช่วงเทศกาลปีใหม่ด้วยการรดน้ำพระพุทธรูปซึ่งเป็นที่ประดิษฐานพระธาตุอายุมากกว่า 1,000 ปี ณ วัดพระธาตุศรีจอมทองซึ่งตั้งอยู่ห่างจากเมืองเชียงใหม่ 56 กิโลเมตร พิธีจะเริ่มต้นด้วยการเคลื่อนย้ายพระธาตุไปยังใจกลางเมืองเชียงใหม่ที่วัดสวนดอก โรงแรม Home Chiang Mai ตั้งอยู่ในเขตชานเมืองเชียงใหม่ อยู่ในเส้นทางที่สำคัญสำหรับพิธีนี้ด้วย Home ให้บรรยากาศของความเงียบสงบและบริการที่ดีเยี่ยมแก่ผู้พักของเรา บนทุกๆตารางนิ้วของโรงแรมตลอดเวลา เรายังมีอาหารฟิวชั่นแบบไทยซึ่งได้รับการออกแบบและปรุงโดยพ่อครัวชั้นนำที่ได้รับการฝึกฝนจาก Le Cordon Bleu ประเทศอังกฤษ เราเชื่อว่าจะไม่เป็นรองใครในเรื่องนี้');
define('CONTENT_UNDER_CONSTRUCTION', 'กำลังรวบรวมเนื้อหา');
define('EG_PASSPORT_NUMBER', 'เช่น เลขประจำตัวหนังสือเดินทาง');

define('PRICE_UNAVAILABLE', 'ขออภัย ในช่วงเวลาที่คุณเลือกยังไม่ได้กำหนดราคา');

define('REF', 'อ้างอิง');

define('WAT_TITLE', 'วัดอินทราวาส หรือวัดต้นเกว๋น (5 นาทีจากโรงแรม)');
define('WAT_P1', 'วัดอินทราวาสตั้งอยู่ชานเมืองจังหวัดเชียงใหม่เป็นวัดหนึ่งที่มีสถาปัตยกรรมแบบล้านนาคลาสสิค ประกอบด้วยวิหาร ระเบียง และมณฑป');
define('WAT_P2', 'วัดต้นเกว๋นเป็นหนึ่งในวัดที่ทำด้วยไม้ที่เหลืออยู่เพียงไม่กี่วัด เป็นวัดที่มีค่าสำหรับคนเชียงใหม่ ได้รับการบูรณะเพื่อให้คงสภาพเดิมและอยู่ภายใต้การดูแลของกรมศิลปากร ในการปรับปรุงใหม่มีจุดมุ่งหมายเพื่อรักษาโครงสร้างเดิมไว้ให้มากที่สุด');
define('WAT_P3', 'วัดต้นเกว๋นสร้างขึ้นในปี พ. ศ. 1858 ในรูปแบบล้านนาและได้รับอิทธิพลมากจากชาวไทยลื้อ บันไดของพระวิหารมีจิตรกรรมปูนปั้นของพญานาค ');
define('WAT_P4', 'พระวิหารเป็นวิหารแบบเปิดโล่ง เดิมเป็นไม้แต่บางส่วนทรุดโทรมจึงเสริมด้วยคอนกรีต มณฑปถูกสร้างเป็นที่พักพิงชั่วคราวสำหรับพระบรมสารีริกธาตุ พระบรมธาตุจอมทองที่ประดิษฐานไว้ในวัดพระธาตุศรีจอมทองซึ่งทางตอนใต้ของจังหวัดเชียงใหม่');

define('ROYAL_PARK_TITLE', 'อุทยานหลวงราชพฤกษ์ (10 นาทีจากโรงแรม)');
define('ROYAL_PARK_P1', 'อุทยานหลวงราชพฤกษ์ หรือชื่อเดิม สวนเฉลิมพระเกียรติ ราชพฤกษ์ เป็นสวนพฤกษศาสตร์ขนาดใหญ่ พื้นที่กว่า 468 ไร่ ตั้งอยู่ ณ ศูนย์วิจัยเกษตรหลวงเชียงใหม่ ตำบลแม่เหียะ อำเภอเมือง จังหวัดเชียงใหม่ เคยใช้เป็นสถานที่ที่ใช้จัดงาน มหกรรมพืชสวนโลกเฉลิมพระเกียรติฯ ราชพฤกษ์ 2549 และ มหกรรมพืชสวนโลกเฉลิมพระเกียรติฯ ราชพฤกษ์ 2554 ที่มีดอกไม้งามหลากหลายสายพันธุ์ ชูช่อ เบ่งบานนับแสนนับล้านดอก');
define('ROYAL_PARK_P2', 'ภายในบริเวณอุทยานหลวงราชพฤกษ์มี “หอคำหลวง” ซึ่งเป็นสถาปัตยกรรมแบบล้านนา ทางเข้าหอคำหลวงนั้นมีทั้งหมด 30 ประตู ระหว่างทางเข้าด้านหน้ามีภาพฉายาลักษณ์ของพระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช รัชกาลที่ 9 ระหว่างทางเดินคู่ขนานมีต้นราชพฤกษ์ นอกจากนี้ยังมีห้องจัดแสดงนิทรรศการสวนนานาชาติและสวนที่มีกิจกรรมมากมายและเปิดให้ประชาชนทั่วไปเข้าชมได้ตลอดปี');

define('BAN_TAWAI_TITLE', 'บ้านถวาย (10 นาทีจากโรงแรม)');
define('BAN_TAWAI_P1', 'บ้านถวายเป็นหมู่บ้านหัตถกรรมไม้แกะสลัก เป็นแหล่งท่องเที่ยวทางวัฒนธรรมที่ขึ้นชื่อที่สุดแห่งหนึ่งของเชียงใหม่เป็นที่รู้จักสำหรับนักท่องเที่ยวชาวไทยและชาวต่างประเทศ ');
define('BAN_TAWAI_P2', 'เป็นแหล่งสินค้าที่ทำจากไม้แกะสลักที่มีคุณภาพดีที่สุดและราคาไม่แพง ท่านสามารถซื้อเป็นของที่ระลึกเล็กๆน้อยๆ ท่านจะไม่เสียใจกับการท่องเที่ยวครึ่งวันในการท่องเที่ยวหมู่บ้านนี้เลย บ้านถวายยังมีผ้าไหมโบราณ มีของใช้เครื่องใช้โบราณที่ทำขึ้นมาใหม่ ธูป เทียน น้ำมันหอมชุดของขวัญ สบู่ โคมไฟ ผลิตภัณฑ์ไม้ไผ่แขวนผนัง กรอบรูป แจกันและอื่น ๆ อีกมากมาย สินค้าส่วนใหญ่ที่จัดแสดงที่บ้านถวายเป็นสินค้าไทยที่มีเอกลักษณ์เฉพาะตัวมีหลายแบบที่ทำจากวัสดุธรรมชาติ');
define('BAN_TAWAI_P3', 'บ้านถวายยังมีชื่อเสียงในเรื่องเป็นแหล่งของโบราณวัตถุในเอเชียติดอันดับที่ดีที่สุดในโลกอีกด้วย');

define('MUANG_KUNG_TITLE', 'หมู่บ้านเครื่องปั้นดินเผาเหมืองกุง');
define('MUANG_KUNG_P1', 'หมู่บ้านเหมืองกุง มีประวัติศาสตร์การบอกเล่ามาจากบรรพบุรุษรุ่นสู่รุ่นว่าบรรพบุรุษของพวกเขาที่ตั้งรกรากอยู่ในหมู่บ้านแห่งนี้เป็นชาวไทที่ถูกกวาดต้อนมาจากเมืองปุ และเมืองสาดในรัฐเชียงตุงปัจจุบันอยู่ที่รัฐฉาน ประเทศพม่า เป็นชนเผ่าที่อพยพมาเพราะถูกทหารพม่าขับไล่และย้ายมาเพียง 6 ครอบครัวเท่านั้น');
define('MUANG_KUNG_P2', ' เกือบทุกครอบครัวในหมู่บ้านนี้ทำเครื่องปั้นดินเผาเป็นอาชีพ ถ้าท่านได้เดินทางไปในถนนของหมู่บ้านท่านจะพบเครื่องปั้นดินเผาหลากหลายรูปแบบ หลายขนาด ไม่ว่าจะเป็นแจกัน โถสูง และอื่นๆเรียงกระจายอยู่ตามพื้นหญ้า ขณะเดียวกันก็จะมีเครื่องปั้นดินเผา ที่ทำจากดินเผาและทาเคลือบ สินค้าที่เป็นที่นิยมที่สุดก็คือหม้อที่มีปากหม้อแคบ ลำตัวนูน ส่วนล่างเรียวเล็ก และมีฝาปิด นอกจากนั้น ยังมีการปรับปรุงรูปแบบผลิตภัณฑ์เพื่อให้สอดคล้องกับความต้องการของผู้บริโภค เช่น การสร้างลวดลายที่แปลกใหม่ และการแต่งสีสันให้ทันสมัย อีกทั้งยังมีการผลิตตามคำสั่งซื้อของลูกค้าอีกด้วย เช่น โคมไฟ กระถาง อ่างบัว หรือแจกัน เป็นต้น');

define('NIGHT_SAFARI_TITLE', 'ไนท์ซาฟารี');
define('NIGHT_SAFARI_P1', 'ถือว่าเป็นสวนสัตว์กลางคืนที่สวยงามที่สุดของโลก และมีขนาดใหญ่กว่าไนท์ซาฟารีสิงค์โปรถึงสองเท่าและมากกว่า 300 เอเคอร์ ไนท์ซาฟารีเป็นสวนสัตว์กลางคืน สำหรับการดูสัตว์แยกออกเป็น 3 พื้นที่ ');
define('NIGHT_SAFARI_P2', 'ตอนกลางวันจะมีโซนเสือจากัวร์ ซึ่งเส้นทางนี้มีทางเดินยาวถึง 1.2 กิโลเมตร โดยรอบๆ มีทะเลสาบ โดยที่นี้มีสัตว์มากกว่า 400 ชนิด มีทั้ง เสือโคร่งขาว, เสือดาว, สมเสร็จ, ลิง, อิฐ, ม้าแคระ, นกกระสาสีขาว, วอลลาบี และ โคอาล่า ');
define('NIGHT_SAFARI_P3', 'ตอนกลางคืนมีอีก 2 โซน คือ ซาวาน่าโซน์ และพรีดาเตอร์ พราว โซน ซึ่งจะเปิดให้บริการ 6 โมงเย็นถึงเที่ยงคืนของทุกวัน แขกและนักท่องเที่ยวสามารถนั่งรถรางเพื่อผ่านอีกโซนระยะทางประมาณ 5 กิโลเมตร ใช้เวลา 1 ชั่วโมงต่อรอบ รถรางเริ่มตั้งแต่ 1 ทุ่มถึง 4 ทุ่มครึ่ง รถรางจะมีทุกๆ 15 นาที');

define('NIGHT_BAZAAR_TITLE', 'ไนท์บาซาร์ ');
define('NIGHT_BAZAAR_P1', 'ไนท์บาซาร์เชียงใหม่ เป็นสถานที่ท่องเที่ยวย่านธุรกิจของร้านค้าโรงแรม ภัตตาคาร และบาร์ อย่างน้อยที่สุด นักท่องเที่ยวที่มาจังหวัดเชียงใหม่ต้องมาที่นี่สักครั้ง หลังจาก 6 โมงเย็น ตลาดจะเปิดทำการ แม้ว่าจะเป็นพื้นที่เล็กๆ แต่ก็ได้สัมผัสกับสิ่งต่างๆมากมาย ไนท์บาซาร์เป็นสัญลักษณ์ของเชียงใหม่ ใครที่พูดถึงจังหวัดเชียงใหม่ก็จะคิดถึงไนท์บาซาร์ ซึ่งเป็นสถานที่ที่มีเสน่ห์ดึงดูดนักท่องเที่ยว และมีผลิตภัณฑ์ และสินค้าหัตถกรรมให้เลือกซื้อมากมาย ');

define('DOI_INTHANON_TITLE', 'ดอยอินทนนท์');
define('DOI_INTHANON_P1', 'ดอยอินทนนท์ คือ ยอดเขาที่สูงที่สุดในประเทศไทยที่สูงถึง 2,565 เมตร จากระดับน้ำทะเล อุทยานแห่งชาติดอยอินทนนท์เต็มไปด้วยป่าที่เขียวชอุ่มและภูมมิศาสตร์ที่ดี เป็นเขตป่าฝนป่าผสมป่าสน จะมีภูมิอาการเย็นตลอดปี โดยเฉพาะอย่างยิ่งในฤดูหนาวจะมีหมอกปกคลุมตลอดทั้งวัน และปรากฏการณ์ที่น่าตื่นใจคือ "เหมยขาบ" หรือปรากฏการณ์น้ำค้างแข็งที่เกิดขึ้นในยามที่อากาศหนาวจัด');
define('DOI_INTHANON_THING_TODO_TITLE', 'สิ่งที่น่าสนใจ ณ ดอยอินทนนท์');
define('DOI_INTHANON_THING_TODO_1', 'นมัสการพระมหาธาตุนภเมทนีดลและพระมหาธาตุนภพลภูริสิริ (พระเจดีย์ศักดิ์สิทธิ์อันยิ่งใหญ่)');
define('DOI_INTHANON_THING_TODO_2', 'ลิ้มลองความงามของหมอกในตอนเช้าในฤดูหนาวที่ลึกลับ ');
define('DOI_INTHANON_THING_TODO_3', 'สำรวจน้ำตกแม่กลาง น้ำตกวชิรธาร น้ำตกสิริภูมิ น้ำตกแม่ยะ กิ่วแม่ปาน และเส้นทางศึกษาธรรมชาติอ่างกา');
define('DOI_INTHANON_THING_TODO_4', 'ชมนาขั้นบันไดที่บ้านแม่กลางหลวง');
define('DOI_INTHANON_THING_TODO_5', 'กิจกรรมเฝ้าดูนก');
define('DOI_INTHANON_THING_TODO_6', 'ชมท้องฟ้าในหอสังเกตการณ์แห่งชาติไทยซึ่งเป็นหนึ่งในสถานที่ดาราศาสตร์ทางแสงที่ทันสมัยที่สุดในอาเซียน');
define('DOI_INTHANON_THING_TODO_7', 'แวะชมสถานีเกษตรหลวงอินทนนท์');

define('NIMMAN_TITLE', 'ถนนนิมมานเหมินทร์');
define('NIMMAN_TITLE_DETAIL', 'เป็นแหล่งรวมร้านอาหารพื้นเมืองและอาหารนานาชาติ');
define('NIMMAN_DETAIL_1', 'ร้านก๋วยเตี๋ยวสไตล์ทางภาคเหนือที่ดีที่สุดคือ ร้านข้าวซอยแม่สาย ที่ถนนราชพฤกษ์ ฝั่งตรงข้ามถนนห้วยแก้ว');
define('NIMMAN_DETAIL_2', 'สุดยอดอาหารล้านนา ต้องที่ ต๋องเต็มโต๊ะ นิมมานซอย 13 ');
define('NIMMAN_DETAIL_3', 'ร้านกาแฟที่ดีที่สุดที่ Ristr8to Lab, นิมมานซอย 3 ');
define('NIMMAN_DETAIL_4', 'ร้านขนมหวานที่ดีที่สุดที่ iBerry Gardens, นิมมาน 17 ');
define('NIMMAN_DETAIL_5', 'แกงกะหรี่ญี่ปุ่นต้องที่ร้าน Katsu, Nimman Soi 8 ');
define('NIMMAN_DETAIL_6', 'เบียร์ที่ดีที่สุดและอาหารสำหรับคนรักสุขภาพต้อง: The Salad Concept, Soi 13 ');
define('NIMMAN_DETAIL_7', 'บาร์ดีที่สุดที่ต้อง Beer Lab นิมมาน, ซอย 12');
define('NIMMAN_DETAIL_8', 'ไนท์คลับที่ดีที่สุดที่ Club Café, Nimmam ซอย 7 ');

define('PHOTO_BY', 'ภาถ่ายโดย');
define('OUR_VIDEO', 'วิดีโอของเรา');
define('PROMOTIONS', 'โปรโมชั่น');

?>
