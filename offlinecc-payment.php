<?php
$pos2 = strpos($_SERVER['HTTP_REFERER'],$_SERVER['SERVER_NAME']);
if(!$pos2){
	header('Location: booking-failure.php?error_code=9');
}
session_start();
include("includes/db.conn.php");
include("includes/conf.class.php");
include("language.php");
include('includes/csrf.class.php');

if (!CSRF::checkTokenx()) {
	abort('500-token-expires');
}

?>
	<!doctype html>
	<html>

	<head>
		<meta charset="utf-8">
		<title>
			<?php echo $bsiCore->config['conf_hotel_name']; ?>
		</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans|Playfair+Display" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo asset('css/datepicker.css" type="text/css" media="screen') ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider.css') ?>" />
		<!-- <link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider-customize.css') ?>"> -->
		<link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-customize.css') ?>">
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

	    <link rel="stylesheet" href="css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
	    <link rel="stylesheet" href="css/screen.css?v=1.2" type="text/css" media="screen" charset="utf-8" />

	    <link rel="stylesheet" type="text/css" href="css/new_style.css">
	    <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet'>
	    <link href="https://fonts.googleapis.com/css?family=Prompt:200" rel="stylesheet">
	    <link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet'>
		<?php include('modules/header-favicon.php'); ?>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<link rel="stylesheet" href="css/style.css">
	</head>

	<body>
		<div class="container-fluid" style="margin-top: 120px;" >
			<?php
					include("modules/inner-modules.php")
				?>
			<div id="body-div">
				<div class="row show-room-dialog">
					<!-- <h1><?php echo $bsiCore->config['conf_hotel_name']; ?></h1> -->
					<?php
			                $active = "payment";
			                include("modules/breadcrumb.php");
			            ?>
						<br><br>
						<div class="wrapper">
							<div class="htitel">
								<h2 class="fl" style="border:0; margin:0;"><?php echo CC_DETAILS; ?></h2>
							</div>
							<br>
							<!-- start of search row -->
							<div class="container-fluid" style="margin:0; padding:0;">
								<div class="row-fluid" style="border: 1px solid #dfc27c; padding: 1% 0">
									<div class="span12">
										<form name="signupform" id="form1" action="cc_process.php" method="post" onSubmit="return testCreditCard();" style="width: 95%; margin: 0 2.5%" class="form-horizontal">
											<?php echo CSRF::tokenxField(); ?>
											<input type="hidden" name="bookingid" value="<?php echo $_POST['x_invoice_num']; ?>" />
											<div class="form-group">
												<label class="control-label col-sm-offset-2 col-sm-2" for="ea"><?php echo CC_HOLDER; ?>:</label>
												<div class="controls col-sm-5">
													<input type="text" name="cc_holder_name" id="cc_holder_name" class="input-large form-control required" />
													<div class="errorTxt" align="left"></div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-offset-2 col-sm-2" for="ea"><?php echo CC_TYPE; ?>:</label>
												<div class="controls col-sm-2">
													<select name="CardType" id="CardType" class="input-large form-control">
												   <option value="AmEx">AmEx</option>
												   <option value="DinersClub">DinersClub</option>
												   <option value="Discover">Discover</option>
												   <option value="JCB">JCB</option>
												   <option value="Maestro">Maestro</option>
												   <option value="MasterCard">MasterCard</option>
												   <option value="Solo">Solo</option>
												   <option value="Switch">Switch</option>
												   <option value="Visa">Visa</option>
												   <option value="VisaElectron">VisaElectron</option>
												 </select>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-offset-2 col-sm-2" for="ea"><?php echo CC_NUMBER; ?>:</label>
												<div class="controls col-sm-5">
													<input type="text" name="CardNumber" id="CardNumber" maxlength="16" class="input-large form-control required" />
													<div class="errorTxt" align="left"></div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-offset-2 col-sm-2" for="ea"><?php echo CC_EXPIRY; ?>:</label>
												<div class="controls col-sm-2">
													<input placeholder="01/20" type="text" name="cc_exp_dt" id="cc_exp_dt" maxlength="5" class="input-mini form-control required" />
													<div class="errorTxt" align="left"></div>
												</div>
												<label class="control-label col-sm-1" for="ea">CCV/CCV2:</label>
												<div class="controls col-sm-1">
													<input placeholder="xxx" type="text" name="cc_ccv" id="cc_ccv" maxlength="4" class="input-mini form-control required" />
													<div class="errorTxt" align="left"></div>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-offset-2 col-sm-2" for="ea"><?php echo CC_AMOUNT; ?></label>
												<div class="controls col-sm-5" align="left">
													<strong><?php echo (($bsiCore->config['conf_payment_currency']=='1')? $bsiCore->get_currency_symbol($_SESSION['sv_currency']) : $bsiCore->currency_symbol())?><?php echo $_POST['total']; ?></strong>
													<label class="checkbox col-sm-12">
													<input type="checkbox" name="tos" id="tos" value="" class="required"/>
												 	<?php echo CC_TOS1; ?> <?php echo $bsiCore->config['conf_hotel_name']?>
												 	<?php echo CC_TOS2; ?> <?php echo $bsiCore->currency_symbol(); ?><?php echo $_POST['total']; ?> <?php echo CC_TOS3; ?>.
													<div class="errorTxt" align="left"></div>
											    </label>
												</div>
											</div>
									</div>
								</div>
							</div>
							<!-- end of search row -->
						</div>
						<div style="margin-bottom: 80px;">
							<div class="col-sm-6">
								<button id="registerButton" type="button" class="btn btn-default btn-book" onClick="window.location.href='booking-failure.php?error_code=26'"><?php echo BTN_CANCEL; ?></button>
							</div>
							<div class="col-sm-6">
								<button id="registerButton" type="submit" class="conti btn btn-default btn-book"><?php echo CC_SUBMIT; ?></button>
							</div>
						</div>
						</form>
				</div>
			</div>
			<?php
	                include("modules/footer.php");
	            ?>
		</div>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo asset('js/bootstrap-datepicker.js') ?>"></script>
		<script type="text/javascript" src="<?php echo asset('js/jquery.validate.js') ?>"></script>

		<script type="text/javascript" src="<?php echo asset('js/modernizr-2.6.2.min.js') ?>"></script>
		<script type="text/javascript" src="<?php echo asset('js/jquery-zoomslider.min.js') ?>"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript">
			$(window).load(function() {

			});
			$(document).ready(function() {
				$("#form1").validate({
					errorPlacement: function(error, element) {
						error.appendTo(element.nextAll(".errorTxt"));
					}
				});
			});
		</script>
	</body>

	</html>
