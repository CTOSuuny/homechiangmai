<?php
session_start();
include("includes/db.conn.php");
include("includes/conf.class.php");
include("language.php");
if(isset($_REQUEST["error_code"]))
$errorCode = $bsiCore->ClearInput($_REQUEST["error_code"]);
else
$errorCode=9;

$erroMessage = array();
$erroMsg[9] = BOOKING_FAILURE_ERROR_9;
$erroMsg[13] = BOOKING_FAILURE_ERROR_13;
$erroMsg[22] = BOOKING_FAILURE_ERROR_22;
$erroMsg[25] = BOOKING_FAILURE_ERROR_25;
$erroMsg[26] = BOOKING_FAILURE_ERROR_26;
if(isset($_GET['rescode'])){ $erroMsg[$errorCode]=$_GET['rescode']; }
?>




<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $bsiCore->config['conf_hotel_name']; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans|Playfair+Display" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo asset('css/datepicker.css" type="text/css') ?>" media="screen"/>

        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/jquery.lightbox-0.5.css') ?>" media="screen" />
        <link rel="stylesheet" href="<?php echo asset('css/colorbox.css') ?>" />
        <link rel="stylesheet" href="<?php echo asset('css/flexslider.css') ?>" type="text/css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider.css') ?>" />
		<!-- <link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider-customize.css') ?>"> -->
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-customize.css') ?>">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

        <link rel="stylesheet" href="css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
        <link rel="stylesheet" href="css/screen.css?v=1.2" type="text/css" media="screen" charset="utf-8" />

        <link rel="stylesheet" type="text/css" href="css/new_style.css">
        <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet'>
        <link href="https://fonts.googleapis.com/css?family=Prompt:200" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet'>
        <link rel="stylesheet" href="css/style.css">

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    </head>
    <body>
        <div class="container-fluid" style="margin-top: 120px;" >
            <?php
                include("modules/inner-modules.php");
            ?>
            <div id="body-div">
                <div class="row show-room-dialog" >
                    <div class="error-page">
                        <h1 align="center"><?php echo $bsiCore->config['conf_hotel_name']; ?></h1>
                        <br>
                        <p class="text-center"><?php echo BOOKING_FAILURE_TEXT; ?></p>
                        <p style="color:#C00"><?php echo $erroMsg[$errorCode]; ?> </p>
                         <div style="width:100%;">
                            <button class="btn btn-book" id="registerButton" style="margin:0 auto" type="button" onClick="window.location.href='index.html'" ><?php echo BACK_TO_HOME; ?></button>
                        </div>
                    </div>
                    <!-- start of search row -->

                    <!-- end of search row -->
                </div>
            </div>
            <?php
                include("modules/footer.php");
            ?>
        </div>
    </body>
    <script src="js/main.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo asset('js/modernizr-2.6.2.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo asset('js/jquery-zoomslider.min.js') ?>"></script>
    <script defer src="<?php echo asset('js/helpers/btn-scroll.js') ?>"></script>
    <script >
              function myFunction(x) {
      var chklogo = document.getElementById("logo");
      if (chklogo.style.display === 'none') {
        document.getElementById("logo").style.display = "block"
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("book").style.color = "white";

      } else {
        document.getElementById("logo").style.display = "none"
        document.getElementById("mySidebar").style.width = "100%";
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("book").style.color = "black";

      }
      x.classList.toggle("change");
    }
    </script>
</html>
