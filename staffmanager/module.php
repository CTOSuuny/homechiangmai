<?php
session_start();
$module = $_GET['module'] ?? '';
$page = $_GET['page'] ?? '';
$action = $_GET['action'] ?? '';
$modulePath = 'modules/';

include '../includes/module_helpers.php';

if (empty($module) || !file_exists($modulePath . $module . '/pages/index.php') || strpos($page, '_') !== false ) {
    import('includes/helpers.php');
    abort(404);
}

$languageFile = $modulePath . $module . '/languages/' . $_SESSION['language1'] . '.php';

if (file_exists($languageFile)) {
    include $languageFile;
}

$pagePath = $modulePath . $module . '/pages/';

if (empty($page) && empty($action)) {
    include $pagePath . 'index.php';
    die;
}

$pageFile = $pagePath . $page . '.php';

if (!empty($page) && file_exists($pageFile)) {
    include $pageFile;
    die;
}

$actionFile = $modulePath . $module . '/actions/' . $action . '.php';

if (!empty($action) && file_exists($actionFile)) {
    include $actionFile;
    die;
}

import('includes/helpers.php');
abort(404);
