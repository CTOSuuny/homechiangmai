<div class="row show-about-hotel-dialog room-modules">
    <div class="row">
        <div class="col-sm-4">
            <h1 class="color gold"><?php trans('ROMANTIC_LANNA_COLONIAL_STYLE') ?></h1>
            <p><?php trans('ROMANTIC_LANNA_COLONIAL_STYLE_DESC') ?><p>
        </div>
        <div class="col-sm-8">
            <img src="<?php echo asset('images/gallery/126285221.jpg') ?>" class="img-thumbnail">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <h1 class="color gold"><?php echo trans('ACCOMMODATION'); ?></h1>
            <p><?php trans('ACCOMMODATION_DESC') ?> </p>
        </div>
        <div class="col-sm-8">
            <img src="<?php echo asset('images/gallery/126290939.jpg') ?>" class="img-thumbnail">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <h1 class="color gold"><?php trans('RESTAURANT') ?></h1>
            <p><?php trans('LE_MONT_COFFEE') ?></p>
            <p><?php trans('RESTAURANT_OPENNING') ?></p>
            <p><?php trans('RESTAURANT_ALA'); ?> </p>
            <p><?php trans('RESTAURANT_ROOM_SERVICE') ?></p>
            <p><?php trans('RESTAURANT_ROOM_BUFFET') ?></p>
        </div>
        <div class="col-sm-8">
            <img src="<?php echo asset('images/gallery/125435512.jpg') ?>" class="img-thumbnail">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <h1 class="color gold"><?php trans('ESCAPE_THE_CITY_LIFE'); ?></h1>
            <p><?php trans('ESCAPE_THE_CITY_LIFE_DESC') ?></p>
        </div>
        <div class="col-sm-8">
            <img src="<?php echo asset('images/gallery/124853532.jpg') ?>" class="img-thumbnail">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <h1 class="color gold"><?php trans('FACILITIES_TEXT') ?></h1>
            <p>
                <?php trans('RESTAURANT') ?><br/>
                <?php trans('COFFEE_SHOP') ?><br/>
                <?php trans('ROOM_SERVICE') ?><br/>
                <?php trans('FITNESS_ROOM') ?><br/>
                <?php trans('NICE_PLACE_FOR_WEDDING') ?><br/>
                <?php trans('FREE_SPORT_ACTIVE') ?><br/>
                <?php trans('FREE_SPORT_ACTIVE') ?><br/>
                <?php trans('FACILITY_REMARK') ?>
            </p>
        </div>
        <div class="col-sm-8">
            <img src="<?php echo asset('images/gallery/125849122.jpg') ?>" class="img-thumbnail">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <h1 class="color gold"><?php trans('GUESTROOM_FEATURES') ?></h1>
            <p>
                <?php trans('PRIVATE_BATHROOM') ?><br/>
                <?php trans('HOT_SHOWER') ?> <br/>
                <?php trans('INDIVIDUALLY_AIR') ?> <br/>
                <?php trans('WORKING_DESK') ?> <br/>
                <?php trans('MINIBAR_FREE') ?><br/>
                <?php trans('HAIR_DRYER') ?><br/>
                <?php trans('INDIVIDUALLY_TEL') ?><br/>
                <?php trans('SAT_LCD') ?><br/>
                <?php trans('BATHROBE_SLIPPER') ?><br/>
                <?php trans('DIRECT_DEL_PHONE') ?><br/>
                <?php trans('TEA_AND_COFFEE') ?><br/>
                <?php trans('WIFI_CONNECTION') ?><br/>
            </p>
        </div>
        <div class="col-sm-8">
            <img src="<?php echo asset('images/gallery/126294107.jpg') ?>" class="img-thumbnail">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <h1 class="color gold"><?php trans('EVENTS'); ?></h1>
            <p><?php trans('EVENTS_DESC') ?></p>
        </div>
        <div class="col-sm-8">
            <img src="<?php echo asset('images/gallery/124853526.jpg') ?>" class="img-thumbnail">
        </div>
    </div>
</div>
