<div id="body-div">
    <div class="row show-room-dialog">
        <?php
            $active = "room_rate";
            include("breadcrumb.php");
            $isDisableBookingEngine = $bsiCore->config['conf_booking_turn_off'];
        ?>
            <div class="show-room-dialog-rooms">
                <h2><?php echo SEARCH_INPUT_TEXT; ?>
                    (<a data-toggle="collapse" href="#collapseExample"><?php echo MODIFY_SEARCH_TEXT; ?></a>)
                </h2>
                <div class="collapse" id="collapseExample">
                    <div class="book-form-search">
                        <form class="form-inline booking-form" action="booking-search.php" method="post">
                            <?php
                                $checkIn = "check-in";
                                $checkOut = "check-out";
                                include("book-form.php");
                            ?>
                        <button type="submit" <?php echo $isDisableBookingEngine == '1' ? 'disabled' : '' ?> class="btn btn-default btn-book" ><?php echo BOOK_YOUR_ROOM_NOW; ?></button>
                        </form>
                    </div>
                </div>
                <div class="row hidden-xs data-input-book">
                    <div class="col-sm-3 text-left">
                        <strong><?php echo CHECK_IN_D_TEXT; ?>:</strong>
                        <?php echo $bsisearch->checkInDate; ?>
                    </div>
                    <div class="col-sm-3 text-left">
                        <strong><?php echo CHECK_OUT_D_TEXT; ?>:</strong>
                        <?php echo $bsisearch->checkOutDate; ?>
                    </div>
                    <div class="col-sm-2 text-left">
                        <strong><?php echo TOTAL_NIGHTS_TEXT; ?>:</strong>
                        <?php echo $bsisearch->nightCount; ?>
                    </div>
                    <div class="col-sm-2 text-left">
                        <strong><?php echo ADULT_ROOM_TEXT; ?>:</strong>
                        <?php echo $bsisearch->guestsPerRoom; ?>
                    </div>
                    <div class="col-sm-2 text-left">
                        <?php if($bsisearch->childPerRoom){ ?>
                        <strong><?php echo CHILD_PER_ROOM_TEXT; ?>:</strong>
                        <?php echo $bsisearch->childPerRoom; ?>
                        <?php } ?>
                    </div>
                </div>
                <table class="table visible-xs col-xs-12">
                    <tbody>
                        <tr>
                            <td><strong>
                            <?php echo CHECK_IN_D_TEXT; ?>
                            :</strong></td>
                            <td>
                                <?php echo $bsisearch->checkInDate; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>
                            <?php echo CHECK_OUT_D_TEXT; ?>
                            :</strong></td>
                            <td>
                                <?php echo $bsisearch->checkOutDate; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>
                            <?php echo TOTAL_NIGHTS_TEXT; ?>
                            :</strong></td>
                            <td>
                                <?php echo $bsisearch->nightCount; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>
                            <?php echo ADULT_ROOM_TEXT; ?>
                            :</strong></td>
                            <td>
                                <?php echo $bsisearch->guestsPerRoom?>
                            </td>
                        </tr>

                        <?php if($bsisearch->childPerRoom){ ?>
                        <tr>
                            <td><strong><?php echo CHILD_PER_ROOM_TEXT; ?>:</strong></td>
                            <td>
                                <?php echo $bsisearch->childPerRoom; ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <br>
                <hr class="hr-line">
                <br><br>
                <?php //echo $_POST['number_room_index']; ?>
                <form name="searchresult" id="searchresult" method="post" action="booking_details.php" onSubmit="return validateSearchResultForm('<?php echo SELECT_ONE_ROOM_ALERT; ?>');">
                    <?php echo CSRF::tokenxField() ?>
                    <?php
                            $gotSearchResult = false;
                            $idgenrator = 0;
                            $ik=1;

                            foreach($bsisearch->roomType as $room_type){

                               foreach($bsisearch->multiCapacity as $capid => $capvalues){

                                    if(!empty($_POST['room_type_id_index'])){
                                        $rommTypeIdIndex = $_POST['room_type_id_index'];
                                    } else {
                                        $rommTypeIdIndex = "";
                                    }

                                    $room_result = $bsisearch->getAvailableRooms($room_type['rtid'], $room_type['rtname'], $capid, $_POST['number_room_index'] ?? 0, $room_type['rtid'] == $rommTypeIdIndex ? $rommTypeIdIndex : 0);
                                    $sqlroomcheck=$mysqli->query("select * from bsi_room where roomtype_id=".$room_type['rtid']." and capacity_id=".$capid);

                                    if($sqlroomcheck->num_rows) {

                                    echo '<script> $(document).ready(function() { ';
                                    echo "var widthScreen = $( document ).width()";
                                    echo '
                                        $("#iframe_'.str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik.'").colorbox({iframe:true, width: 50 + \'%\', height: "90%"});
                                        $("#iframe_details_'.str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik.'").colorbox({iframe:true, width: (widthScreen <= 767 ? 90 : 60) + \'%\', height: "60%"});
                                        $(".group_'.$room_type['rtid'].'_'.$capid.'").colorbox({rel:\'group_'.$room_type['rtid'].'_'.$capid.'\', maxWidth: $(\'#body-div\').width(), maxHeight:"80%", slideshow:true, slideshowSpeed:5000});';
                                    echo '}); </script>';
                                    echo '<script type="text/javascript">
                                            $(document).ready(function() {
                                                $("#mySlides_'.$capid.'_'.$room_type['rtid'].' a").lightBox();
                                            });
                                        </script>';
                        ?>
                        <!-- start of search row -->
                        <div class="row" id="scroll-to-typeId-<?php echo $room_type['rtid']; ?>">
                            <div class="col-sm-4">
                                <div class="search-gallery">
                                    <div class="flexslider" style="cursor:pointer; cursor:hand;">
                                        <ul class="slides">
                                            <?php echo $bsiCore->roomtype_photos($room_type['rtid'],$capid); ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <table class="table">
                                    <?php
                                        if ($room_result['specail_price_flag']) {
                                            $offertag='style="background:url(images/offer.png) no-repeat left top; padding-left:23px; height:35px;"';
                                        } else {
                                            $offertag='';
                                        }
                                    ?>
                                    <tr>
                                        <td width="100%" <?php echo $offertag; ?>>&nbsp;
                                            <span style="font-size:18px;">
                                                   <strong><?php echo $room_type['rtname']; ?></strong> (<?php echo $capvalues['captitle']; ?>) <?php if($room_result['child_flag']){ ?> <?php echo WITH_CHILD; ?> <?php } ?></strong> </span>                                            <span style="float:right;">
                                                   <a style="color:black" href="roomtype-details.php?tid=<?php echo $room_type['rtid']; ?>" id='iframe_details_<?php echo str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik; ?>' style="font-weight:bold; color:#FFF;" ><?php echo VIEW_ROOM_FACILITIES_TEXT; ?></a></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" valign="top" style="font-size:13px">
                                            <table width="100%" class="table table-bordered table-book-search">
                                                <tr>
                                                    <td colspan="2" bgcolor="#e1e1e1">
                                                        <span style=" font-size:14px; font-weight:bold">
                                                               <a  id='iframe_<?php echo str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik; ?>' href="calendar.php?rtype=<?php echo $room_type['rtid']; ?>&cid=<?php echo $capid; ?>" title='<span  style="font-size:16px;"><strong><?php echo $room_type['rtname']; ?></strong> ( <?php echo $capvalues['captitle']; ?> ) </span>'
                                                        >
                                                        <?php echo VIEW_NIGHTLY_PRICE_TEXT; ?> &amp;
                                                        <?php echo CALENDAR_AVAILABILITY_TEXT; ?>
                                                        </a>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 30%;" bgcolor="#e1e1e1"><strong><?php echo MAX_OCCUPENCY_TEXT; ?></strong></td>
                                                    <td bgcolor="#e1e1e1">
                                                        <?php echo $capvalues['capval']; ?>
                                                        <?php echo ADULT_TEXT; ?>
                                                        <?php if($room_result['child_flag']){ ?>
                                                        <?php echo AND_TEXT; ?>
                                                        <?php echo $bsisearch->childPerRoom;?>
                                                        <?php echo CHILD_TEXT; ?>
                                                        <?php } ?>
                                                        <?php echo PER_ROOM_TEXT; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#e1e1e1">
                                                        <strong> <?php echo TOTAL_PRICE_OR_ROOM_TEXT; ?> </strong>
                                                    </td>

                                                    <?php if ($room_result['specail_price_flag']) { ?>
                                                    <td bgcolor="#e1e1e1">

                                                        <span style="font-weight:bold; color:#cc0000;">
                                                            <del><?php echo $bsiCore->get_currency_symbol($bsisearch->currency).$bsiCore->getExchangemoney($room_result['totalprice'],$bsisearch->currency); ?></del>
                                                        </span>

                                                        <strong><?php echo $bsiCore->get_currency_symbol($bsisearch->currency).$bsiCore->getExchangemoney($room_result['total_specail_price'],$bsisearch->currency); ?></strong>

                                                        <?php if($room_result['child_flag']){ ?>
                                                            (included <span style="color:#cc0000; text-decoration:line-through;">
                                                                <?php echo $bsiCore->get_currency_symbol($bsisearch->currency).$bsiCore->getExchangemoney($room_result['total_child_price'],$bsisearch->currency); ?>
                                                            </span>

                                                        <?php echo $bsiCore->get_currency_symbol($bsisearch->currency).$bsiCore->getExchangemoney($room_result['total_child_price2'],$bsisearch->currency); ?>
                                                        <?php echo FOR_TEXT; ?>
                                                        <?php echo $bsisearch->childPerRoom;?>
                                                        <?php echo CHILD_TEXT; ?>)
                                                        <?php } ?>
                                                    </td>

                                                    <?php } else { ?>
                                                    <td bgcolor="#e1e1e1"><span style="font-weight:bold;">
                                                        <strong>
                                                            <?php echo $bsiCore->get_currency_symbol($bsisearch->currency) . $bsiCore->getExchangemoney($room_result['totalprice'], $bsisearch->currency); ?>
                                                        </strong>

                                                        <?php if($room_result['child_flag']) { ?>
                                                        <br/>
                                                        <strong>
                                                            (<?php trans('INCLUDED') ?> <?php echo $bsiCore->get_currency_symbol($bsisearch->currency) . $bsiCore->getExchangemoney($room_result['total_child_price'], $bsisearch->currency); ?>
                                                        </strong>
                                                        <?php echo FOR_TEXT; ?>
                                                        <?php echo $bsisearch->childPerRoom;?>
                                                        <?php echo CHILD_TEXT; ?>)
                                                        <?php } ?>
                                                    </td>
                                                    <?php } ?>
                                                </tr>

                                                <?php ?>
                                                <tr style="background:red">
                                                    <?php if (intval($room_result['roomcnt']) > 0 && !empty(intval($bsiCore->getExchangemoney($room_result['totalprice'], $bsisearch->currency)))) :
                                                            $gotSearchResult = true;
                                                    ?>
                                                        <!-- Begin Room count > 0 -->
                                                        <td bgcolor="#e1e1e1"><strong><?php echo SELECT_NUMBER_OF_ROOM_TEXT; ?></strong></td>
                                                        <td bgcolor="#e1e1e1">
                                                            <div class="col-sm-4">
                                                                <select name="svars_selectedrooms[]" class="form-control">
                                                                    <?php echo $room_result['roomdropdown']; ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <!-- End Room count > 0 -->
                                                    <?php endif ?>

                                                    <?php if (empty(intval($bsiCore->getExchangemoney($room_result['totalprice'], $bsisearch->currency)))) : ?>
                                                        <!-- Begin Price Unavailable -->
                                                        <td bgcolor="#e1e1e1" colspan="2">
                                                            <strong style="color: red;"><?php trans('PRICE_UNAVAILABLE') ?></strong>
                                                        </td>
                                                        <!-- End Price Unavailable -->
                                                    <?php endif ?>

                                                    <?php if (empty(intval($room_result['roomcnt'])) && !empty(intval($bsiCore->getExchangemoney($room_result['totalprice'], $bsisearch->currency)))) :
                                                        echo '<script> $(document).ready(function() { ';
                                                        echo '$("#iframe2_'.str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik.'").colorbox({iframe:true, width: 50 + \'%\', height: "80%"});';
                                                        echo '}); </script>';
                                                    ?>
                                                        <!-- Begin Room Count == 0 -->
                                                        <td bgcolor="#e1e1e1" colspan="2">
                                                            <strong><?php trans('NOT_AVAILABLE') ?></strong>
                                                            </span>(<a
                                                                style="color:#ad7d09; font-size:13px"
                                                                id='iframe2_<?php echo str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik; ?>'
                                                                href="calendar.php?rtype=<?php echo $room_type['rtid']; ?>&cid=<?php echo $capid; ?>"
                                                                title='<?php echo $room_type['rtname']; ?> ( <?php echo $capvalues['captitle']; ?> )'>
                                                                    <strong><?php echo CHECK_AVILABILITY; ?> </strong>
                                                                </a>)
                                                        </td>
                                                        <!-- End Room Count == 0 -->
                                                    <?php endif; ?>
                                                </tr>
                                                <?php ?>

                                            </table>

                                            <?php
                                                $includeText = [];

                                                if ($bsiCore->config['conf_price_inclu_service_charge']) {
                                                    $includeText[] = trans('SERVICE_CHARGE', true) . ' ' . $bsiCore->config['conf_service_charge'] . '%';
                                                }

                                                if ($bsiCore->config['conf_price_inclu_local_tax']) {
                                                    $includeText[] = trans('LOCAL_TAX', true) . ' ' . $bsiCore->config['conf_local_tax'] . '%';
                                                }

                                                if ($bsiCore->config['conf_price_with_tax']) {
                                                    $includeText[] = trans('TAX_TEXT', true) . ' ' . $bsiCore->config['conf_tax_amount'] . '%';
                                                }
                                            ?>

                                            <?php if (!empty($includeText)) : ?>
                                                <div style="text-align: right; color:#9a9a97; margin: 0; font-size: 12px;">*<?php trans('INCLUDED') ?> - <?php echo implode(', ', $includeText) ?></div>
                                            <?php endif; ?>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <hr class="hr-line">

                        <!-- end of search row -->
                        <?php } } } ?>
                        <button type="submit" class="btn btn-default btn-book pull-right"><?php trans('CONTINUE_TEXT') ?></button>

                        <?php
                            $flag88=0;

                            if ($gotSearchResult) {
                                echo '<div id="" style="width:100% !important;"><table cellpadding="5" cellspacing="0" border="0" width="100%" >';
                                echo '<tr><td align="right" style="padding-right:30px;"></td></tr>';
                                echo '</table></div>';
                                $flag88=1;
                            } else {
                                echo '<table cellpadding="4" cellspacing="0" width="100%"><tbody><tr><td style="font-size:13px; color:#F00;" align="center"><br /><br />';

                                if($bsisearch->searchCode == "SEARCH_ENGINE_TURN_OFF"){
                                    echo "<div class='alert-booked'>". SORRY_ONLINE_BOOKING_CURRENTLY_NOT_AVAILABLE_TEXT. "</div>";
                                } else if ($bsisearch->searchCode == "OUT_BEFORE_IN"){
                                    echo "<div class='alert-booked'>". SORRY_YOU_HAVE_ENTERED_A_INVALID_SEARCHING_CRITERIA_TEXT. "</div>";
                                } else if ($bsisearch->searchCode == "NOT_MINNIMUM_NIGHT"){
                                    echo "<div class='alert-booked'>". MINIMUM_NUMBER_OF_NIGHT_SHOULD_NOT_BE_LESS_THAN_TEXT.' '.$bsiCore->config['conf_min_night_booking'].' '. PLEASE_MODIFY_YOUR_SEARCHING_CRITERIA_TEXT. "</div>";
                                } else if ($bsisearch->searchCode == "TIME_ZONE_MISMATCH"){
                                    $tempdate = date("l F j, Y G:i:s T");
                                    echo "<div class='alert-booked'>". BOOKING_NOT_POSSIBLE_FOR_CHECK_IN_DATE_TEXT.' '.$bsisearch->checkInDate.' '. PLEASE_MODIFY_YOUR_SEARCHING_CRITERIA_TO_HOTELS_DATE_TIME_TEXT.'<br>'. HOTELS_CURRENT_DATE_TIME_TEXT.' '.$tempdate. "</div>";
                                } else {
                                    echo "<div class='alert-booked'>". SORRY_NO_ROOM_AVAILABLE_AS_YOUR_SEARCHING_CRITERIA_TRY_DIFFERENT_DATE_SLOT. " <a href='index.php' style='text-decoration: underline;'>". MODIFY_SEARCH_TEXT. "</a></div>";
                                }

                                echo '<br /><br /><br /></td></tr></tbody></table>';
                            }
                        ?>
                </form>
            </div>
    </div>
</div>
<br>
