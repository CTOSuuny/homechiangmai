<?php

include '../action.php';
include APP_PATH . 'includes/models/user.php';

allow_http_method();

$userModel = new User();
$form = '../add_edit_user.php';
$userList = '../user.php';

if (is_request('put')) {

    if ($userModel->exists('username', $_POST['username'], ['field' => 'id', 'value' => $_GET['id']])) {
        set_flash('errors', ['error' => trans('USER_ALREADY_EXISTS', true)]);
        redirect(route($form, ['id' => $_GET['id']]));
        die;
    }

    if ($userModel->exists('email', $_POST['email'], ['field' => 'id', 'value' => $_GET['id']])) {
        set_flash('errors', ['error' => trans('EMAIL_ALREADY_EXISTS', true)]);
        redirect(route($form, ['id' => $_GET['id']]));
        die;
    }

    $_POST['id'] = $_GET['id'];
    $userModel->updateUser($_POST);

    redirect($userList);
}

if (is_request('post')) {

    if ($userModel->exists('username', $_POST['username'])) {
        set_flash('errors', ['error' => trans('USER_ALREADY_EXISTS', true)]);
        set_old_session($_POST);
        redirect($form);
        die;
    }

    if ($userModel->exists('email', $_POST['email'])) {
        set_flash('errors', ['error' => trans('EMAIL_ALREADY_EXISTS', true)]);
        set_old_session($_POST);
        redirect($form);
        die;
    }

    $userModel->createUser($_POST);

    redirect($userList);
}

if (is_request('delete')) {

    $userModel->destroy($_POST['id']);

    redirect($userList);
}
