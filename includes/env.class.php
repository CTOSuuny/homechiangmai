<?php

class ENV
{
    public $envValue;

    public function __construct()
    {
        $this->envFilePath = __DIR__ . DIRECTORY_SEPARATOR . 'env' . DIRECTORY_SEPARATOR . '.env';
    }

    /**
     * Get data from env file
     *
     * @return array
     */
    public function getEnvFile()
    {
        $env = file_get_contents($this->envFilePath);

        $envAsArray = explode("\n", $env);
        $envAsArray = array_filter($envAsArray, function ($env) {
            return !empty($env);
        });

        $envAsArray = $this->sanitizeValue($envAsArray);

        $this->envValue = $envAsArray;

        return $envAsArray;
    }

    /**
     * Sainitize env value
     *
     * @param mixed $envValue env value
     *
     * @return array
     */
    private function sanitizeValue($envValue)
    {
        $envResult = [];

        foreach ($envValue as $value) {
            if (false === strpos($value, '=')) {
                continue;
            }

            list($key, $value) = explode('=', $value);
            $envResult[$key]   = trim($value);
        }

        return $envResult;
    }

    /**
     * Get env value
     *
     * @param mixed $key env key
     *
     * @return mixed
     */
    public function getValue($key = null)
    {
        $envValue = empty($this->envValue) ? $this->getEnvFile() : $this->envValue;

        if (empty($envValue[$key])) {
            return null;
        }

        if (in_array($envValue[$key], ['true', 'false'])) {
            $envValue[$key] = $envValue[$key] == 'true' ? true : false;

            return $envValue[$key];
        }

        return empty($envValue[$key]) ? null : $envValue[$key];
    }

    /**
     * Static function for get env value
     *
     * @param mixed $key env key
     *
     * @return mixed
     */
    public static function get($key = null)
    {
        $env = new self();

        return $env->getValue($key);
    }
}
