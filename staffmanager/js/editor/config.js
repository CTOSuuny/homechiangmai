/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.toolbar = [
		{ name: 'document', items: [ 'Source'] },
		{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline' ] },
		{ name: 'insert', items: [ 'Table' ] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] }
	];

	config.extraPlugins = 'colorbutton';

	config.allowedContent = true;
};
