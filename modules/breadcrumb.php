
    <div class="row">
        <div class="my-breadcrumb">
            <div class="hidden-xs <?php echo (is_lang('th') ? 'col-sm-2' : 'col-sm-3') ?>">
                <a ><h3>1.<?php echo SELECT_DATES_TEXT; ?></h3></a>
            </div>
            <div class="hidden-xs <?php echo (is_lang('th') ? 'col-sm-2' : 'col-sm-3') ?>">
                <?php if (is_lang('th')): ?>
                    <h3 class="<?php echo $active == "room_rate" ? "active" : "" ?>">2.<?php echo SELECT_ROOM; ?></h3>
                <?php else: ?>
                    <h3 class="<?php echo $active == "room_rate" ? "active" : "" ?>">2.<?php echo ROOMS_TEXT; ?> &amp; <?php echo RATES_TEXT; ?></h3>
                <?php endif; ?>
            </div>
            <div class="hidden-xs <?php echo (is_lang('th') ? 'col-sm-3' : 'col-sm-2') ?>">
                <h3 class="<?php echo $active == "detail" ? "active" : "" ?>">3.<?php echo YOUR_DETAILS_TEXT; ?></h3>
            </div>
            <div class="hidden-xs <?php echo (is_lang('th') ? 'col-sm-3' : 'col-sm-2') ?>">
                <h3 class="<?php echo $active == "payment" ? "active" : "" ?>">4.<?php echo PAYMENT_TEXT; ?></h3>
            </div>
            <div class="hidden-xs col-sm-2">
                <h3 class="<?php echo $active == "confirm" ? "active" : "" ?>">5.<?php echo CONFIRM_TEXT; ?></h3>
            </div>
        </div>
        <div class="my-breadcrumb visible-xs ">
            <div class="breadcrumb-style">
                <a ><h3><i class="fa fa-home"></i></h3></a>
            </div>
            <div class="breadcrumb-style">
                <h3 class="<?php echo $active == "room_rate" ? "active" : "" ?>"><i class="fa fa-th-list"></i></h3>
            </div>
            <div class="breadcrumb-style">
                <h3 class="<?php echo $active == "detail" ? "active" : "" ?>"><i class="fa fa-info"></i></h3>
            </div>
            <div class="breadcrumb-style">
                <h3 class="<?php echo $active == "payment" ? "active" : "" ?>"><i class="fa fa-credit-card"></i></h3>
            </div>
            <div class="breadcrumb-style">
                <h3 class="<?php echo $active == "confirm" ? "active" : "" ?>"><i class="fa fa-check-square-o"></i></h3>
            </div>
        </div>
    </div>
