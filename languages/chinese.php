<?php

define('WELCOME_MESSAGE', 'Home Chiang Mai');
define('WELCOME_TO', '欢迎来到');
define('WELCOME_QOUTE', '明清兰纳文化与英国殖民特色的宁静别墅');
define('SUBTITLE_MESSAGE', '享受你的逗留');
define('CHILDREN_TEXT','孩子');
define('BOOK_YOUR_ROOM_NOW', '现在预订');
define('HOTEL_DETAIL', '浪漫兰纳殖民风格兰纳艺术与殖民完美结合。周边地区拥有美丽的花园和兰纳应用设计山。在西方艺术殖民风格室内设计装饰。住宿别墅有18间客房，分为6间豪华间，4间豪华顶级间4间行政间和4间行政套房。每间客房都提供豪华舒适的空间。特别的氛围，您只能在Home Chiangmai中找到。入住清迈内容别墅，谈论疲惫的旅程，并放松到美丽的梦境中，这是一个完美的地方，您可以选择逃离城市生活，享受温馨的服务，难忘的体验和经典的美食之旅。设计提供热情的欢迎和友好的服务，为短期入住的客人提供服务，同时我们提供espercially护理我们的长期入住的客人感到舒适和安全的能力的手中。在您抵达度假村的那一刻，您将享受到最高标准的服务，高雅的款待和迷人的休闲娱乐。');
define('GALLERY_TEXT', '画廊');
define('ABOUT_US', '关于我们');
define('CONTACT_US', '联系');
define('QUICK_LINKS', '快速链接');
define('ADDRESS', '地址');
define('ADDRESS_NAME', '59 Moo 6 Nong Kwai, Hangdong, Chiangmai');
define('CONNECT_WITH_US', '联系我们');
define('CHOOCE_CHECK_IN_DATE', '报到');
define('CHOOCE_CHECK_OUT_DATE', '查看');
define('SELECT_ROOM', '选择房间');
define('CANNOT_SWITCH_LANGUAGE_DURING_BOOKING', '在预订过程中无法切换语言。');
define('NAME_TITLE', '标题');
define('TO_OFFER_THAI_FOOD', '为了提供泰式美食，我们最近推出了英国顶级餐厅之一EAT THAI，该餐厅供应超过16年的泰式融合食品，并获得了诸多殊荣，如2006-2013年的Michellin Guide，TripAdvisor等。EAT THAI餐厅预计将于2018年4月初开放。');
define('THERE_ARE_THREE', '对于不同的场合有三种不同的菜单，即： Le Mont咖啡屋，EAT THAI英格兰和客房服务');
define('OPEN_DAILY', '每日上午7点至下午4点开放');

define('WE_OFFER', '我们根据您的选择提供四种不同的房间类型;那些在表中');
define('ROOM_TYPE', '房型');
define('SIZE', '尺寸');
define('HIGH_CEILING', '高高的天花板');
define('PANTRY', '茶水');
define('Dining_TABLE', '餐桌');
define('BATHHUB', '浴缸');
define('DELUXE', 'Deluxe');
define('STANDARD', '标准');
define('NO', '没有');
define('PREMIUM_DELUXE', 'Premium Deluxe');
define('YES', '是');
define('EXECUTIVE', 'Executive');
define('LARGE', '大');
define('PREMIUM_EXECUTIVE', 'Premium Executive');
define('THE_ROOM_ARHITECTRE', '房间的设计与兰纳和英国殖民地特色交织在一起，而墙壁和天花板则用柚木木材修饰。在设施方面，部分客房配有浴缸。在每个房间里，都有宽屏智能电视，wifi，有线电视频道，如CNN，BBC和CCTV。
在行政和高级行政人员配备了一个餐厅和厨房，我们的客人可以邀请他们的朋友进行私人会议和用餐。');


// index.php
define('ONLINE_BOOKING_TEXT', '网上预订');
define('CHECK_IN_DATE_TEXT','入住日期');
define('CHECK_OUT_DATE_TEXT','离开日期');
define('ADULT_OR_ROOM_TEXT','成人/间');
define('SEARCH_TEXT','搜索');
define('ENTER_CHECK_IN_DATE_ALERT','请输入入住日期');
define('ENTER_CHECK_OUT_DATE_ALERT','请输入退房日期');
define('CURRENCY_TEXT','货币');

//booking-search.php
define('SEARCH_INPUT_TEXT','搜索输入');
define('CHECK_IN_D_TEXT','报到');
define('CHECK_OUT_D_TEXT','查看');
define('TOTAL_NIGHTS_TEXT','全夜');
define('ADULT_ROOM_TEXT','成人/间');
define('SEARCH_RESULT_TEXT','搜索结果');
define('SELECT_ONE_ROOM_ALERT','请选择至少一个房间');
define('MAX_OCCUPENCY_TEXT','最大入住率');
define('SELECT_NUMBER_OF_ROOM_TEXT','选择房间数量');
define('ADULT_TEXT','成人');
define('TOTAL_PRICE_OR_ROOM_TEXT','总价格/房间');
define('VIEW_PRICE_DETAILS_TEXT','查看价格详情');
define('HIDE_PRICE_DETAILS_TEXT','隐藏价格详情');
define('CONTINUE_TEXT','继续');
define('MON','星期一');
define('TUE','星期二');
define('WED','星期三');
define('THU','星期四');
define('FRI','星期五');
define('SAT','星期六');
define('SUN','星期日');
define('MONTH', '月');
define('CHILD_PER_ROOM_TEXT','儿童/房间');
define('WITH_CHILD','带着孩子');
define('CHILD_TEXT','儿童');
define('NONE_TEXT','没有');
define('MODIFY_SEARCH_TEXT','修改搜索');
define('VIEW_ROOM_FACILITIES_TEXT','查看客房设施');
define('VIEW_NIGHTLY_PRICE_TEXT','查看每晚价格');
define('CALENDAR_AVAILABILITY_TEXT','日历可用性');
define('CHECK_AVILABILITY','检查可用性');
define('BACK_TEXT','背部');
define('SORRY_ONLINE_BOOKING_CURRENTLY_NOT_AVAILABLE_TEXT','对不起，网上预订目前不可用。请稍后再试。');
define('SORRY_YOU_HAVE_ENTERED_A_INVALID_SEARCHING_CRITERIA_TEXT','抱歉，您输入的搜索条件无效。请尝试使用无效的搜索条件。');
define('MINIMUM_NUMBER_OF_NIGHT_SHOULD_NOT_BE_LESS_THAN_TEXT','夜晚的最低数量不应少于。');
define('PLEASE_MODIFY_YOUR_SEARCHING_CRITERIA_TEXT','请修改您的搜索条件。');
define('BOOKING_NOT_POSSIBLE_FOR_CHECK_IN_DATE_TEXT','无法预订登记入住日期：');
define('PLEASE_MODIFY_YOUR_SEARCHING_CRITERIA_TO_HOTELS_DATE_TIME_TEXT','请根据酒店日期时间修改搜索条件。');
define('HOTELS_CURRENT_DATE_TIME_TEXT','酒店当前日期时间：');
define('SORRY_NO_ROOM_AVAILABLE_AS_YOUR_SEARCHING_CRITERIA_TRY_DIFFERENT_DATE_SLOT','对不起，没有空房作为您的搜索条件。请尝试不同的日期插槽。');
define('PER_ROOM_TEXT','每间客房');
define('AND_TEXT','和');
define('FOR_TEXT','对于');
//booking_details.php
define('BOOKING_DETAILS_TEXT','预订详情');
define('CHECKIN_DATE_TEXT','入住日期');
define('CHECKOUT_DATE_TEXT','离开日期');
define('TOTAL_NIGHT_TEXT','全夜');
define('TOTAL_ROOMS_TEXT','客房总数');
define('NUMBER_OF_ROOM_TEXT','房间数量');
define('ROOM_TYPE_TEXT','房型');
define('MAXI_OCCUPENCY_TEXT','最大入住率');
define('GROSS_TOTAL_TEXT','总计');
define('SUB_TOTAL_TEXT','小计');
define('TAX_TEXT','税');
define('GRAND_TOTAL_TEXT','累计');
define('ADVANCE_PAYMENT_TEXT','预付款');
define('OF_GRAND_TOTAL_TEXT','总计');
define('CUSTOMER_DETAILS_TEXT','顾客信息');
define('EXISTING_CUSTOMER_TEXT','现有客户');
define('EMAIL_ADDRESS_TEXT','电子邮件地址');
define('FETCH_DETAILS_TEXT','登录');
define('OR_TEXT','要么');
define('NEW_CUSTOMER_TEXT','新客户');
define('TITLE_TEXT','标题');
define('MR_TEXT','先生');
define('MS_TEXT','女士');
define('MRS_TEXT','太太');
define('MISS_TEXT','小姐');
define('DR_TEXT','博士');
define('PROF_TEXT','教授');
define('FIRST_NAME_TEXT','名字');
define('LAST_NAME_TEXT','姓');
define('ADDRESS_TEXT','地址');
define('CITY_TEXT','市');
define('STATE_TEXT','州/省');
define('POSTAL_CODE_TEXT','邮政编码');
define('COUNTRY_TEXT','国家');
define('PHONE_TEXT','电话');
define('FAX_TEXT','传真');
define('EMAIL_TEXT','电子邮件');
define('PAYMENT_BY_TEXT','通过付款');
define('FIELD_REQUIRED_ALERT','这是必填栏');
define('ADDITIONAL_REQUESTS_TEXT','任何其他请求');
define('I_AGREE_WITH_THE_TEXT',' 我同意');
define('TERMS_AND_CONDITIONS_TEXT','条款和条件');
define('CONFIRM_TEXT','确认');
define('CHECKOUT_TEXT','查看');
define('BD_INC_TAX','含税');
define('HOME_TEXT','家');
define('NEARBY_ATTRACTIONS_TEXT','附近的景点');
//process.class.php
define('INV_BOOKING_NUMBER','订单号');
define('INV_CUSTOMER_NAME','顾客姓名');
define('INV_ADULT','成人');
define('INV_PAY_DETAILS','付款详情');
define('INV_PAY_OPTION','付款选项');
define('INV_TXN_ID','交易ID');
define('PP_REGARDS','问候');
define('PP_CARRY','您需要将此电子邮件打印出来，并在抵达时和入住时出示给酒店。这封电子邮件是您预订的确认凭证。');
//offlinecc-payment.php
define('CC_DETAILS','信用卡细节');
define('CC_HOLDER','持卡人姓名');
define('CC_TYPE','信用卡类型');
define('CC_NUMBER','信用卡号');
define('CC_EXPIRY','到期日');
define('CC_AMOUNT','总金额');
define('CC_TOS1','我同意允许');
define('CC_TOS2','扣除');
define('CC_TOS3','从我的信用卡');
define('CC_SUBMIT','提交');
//booking-confirm.php
define('BOOKING_COMPLETED_TEXT','预订完成');
define('THANK_YOU_TEXT','谢谢');
define('YOUR_BOOKING_CONFIRMED_TEXT','您的预订已确认');
define('INVOICE_SENT_EMAIL_ADDRESS_TEXT','用您的电子邮件地址发送发票');
define('BACK_TO_HOME','回到首页');
//booking-failure.php
define('BOOKING_FAILURE_TEXT','预订失败');
define('BOOKING_FAILURE_ERROR_9', '直接访问此页面受到限制。');

define('BOOKING_FAILURE_ERROR_13', '其他人已经获得了您指定的房间的预订锁定。预订锁定将在预订完成后几分钟后自动释放，或由对方失败。请修改您的搜索条件，然后重试。');

define('BOOKING_FAILURE_ERROR_22', '未选择未定义的付款方式。请联系管理员。');

define('BOOKING_FAILURE_ERROR_25', '无法发送电子邮件通知。请联系技术支持。');

define('BOOKING_FAILURE_ERROR_26', '预订程序已取消！如有任何疑问请联系我们！');

//wizard
define('SELECT_DATES_TEXT','选择日期');
define('ROOMS_TEXT','客房');
define('RATES_TEXT','价格');
define('YOUR_DETAILS_TEXT','细节');
define('PAYMENT_TEXT','付款');
define('LOADING_TEXT','装载');
define('BTN_CANCEL','取消');

define('ID_TYPE',' 身份类型');
define('PASSPORT','护照');
define('IDCARD','身份证');
define('OTHER','其他');
define('ID_NUMBER',' 身份号码');

define('NOT_AVAILABLE', '无法使用');
define('INCLUDED', '包括');
define('SERVICE_CHARGE', '服务费');
define('LOCAL_TAX', '地方税');

define('FACILITIES_TEXT', '设备');


define('ROMANTIC_LANNA_COLONIAL_STYLE', '浪漫兰纳殖民风格');
define('ROMANTIC_LANNA_COLONIAL_STYLE_DESC', '兰纳艺术与殖民完美结合。周边地区有一个美丽的花园和兰纳应用设计山。内部装饰有西方艺术殖民风格的室内设计。');
define('ACCOMMODATION', '住所');
define('ACCOMMODATION_DESC', '别墅共有18间客房，分为6间豪华间，4间豪华顶级间4间行政间和4间行政套房。每间客房都提供豪华的设施和舒适的空间。您只能在Home Chiang Mai中找到特殊的氛围。');

define('RESTAURANT', '餐厅');
define('RESTAURANT_OPENNING', '早餐主餐厅07:00开放。 - 上午10点');
define('RESTAURANT_ALA', 'A\'LA carte餐厅提供全天餐饮服务，直至下午24:00。');
define('RESTAURANT_ROOM_SERVICE', '客房服务于上午11点开放。 - 下午24点。');
define('RESTAURANT_ROOM_BUFFET', '自助餐：饮食主题自助餐，儿童和素食自助餐除了在概念餐厅的主餐厅的开放式自助餐厅提供食品和饮料外，还可提供。');

define('ESCAPE_THE_CITY_LIFE', '逃离城市生活。');
define('ESCAPE_THE_CITY_LIFE_DESC', '入住清迈内容别墅，将旅途中的疲惫和放松带入美丽的梦境中，这是一个完美的地方，您可以选择离开城市生活，享受温馨的服务，难忘的体验和经典的美食之旅。');

define('EVENTS', '活动');
define('EVENTS_DESC', '酒店提供婚礼安排服务或婚前照片的好地方');

define('LE_MONT_COFFEE', 'Le Mont Coffee');
define('COFFEE_SHOP', '咖啡店');
define('ROOM_SERVICE', '客房服务');
define('FITNESS_ROOM', '健身房');
define('NICE_PLACE_FOR_WEDDING', '婚礼前的好地方');
define('FREE_SPORT_ACTIVE', '免费体育活动开放时间为08.00  -  20.00。体操中心。');
define('OUTDOOR_POOL', '室外游泳池48平方米深度200厘米游泳池开放08.00上午。 - 下午20点。');
define('FACILITY_REMARK', '运营时间和服务可能在季节变化。这些将与海岸和天气状况有关。');

define('GUESTROOM_FEATURES', '客房特色');
define('PRIVATE_BATHROOM', '私人浴室');
define('HOT_SHOWER', '热水淋浴/浴缸');
define('INDIVIDUALLY_AIR', '独立控制的空调');
define('WORKING_DESK', '办公桌');
define('MINIBAR_FREE', '迷你吧和冰箱免费');
define('HAIR_DRYER', '吹风机和全套洗浴用品');
define('INDIVIDUALLY_TEL', '个人电话');
define('SAT_LCD', '卫星液晶');
define('BATHROBE_SLIPPER', '浴袍和拖鞋');
define('DIRECT_DEL_PHONE', '直拨电话');
define('TEA_AND_COFFEE', '茶及咖啡设施');
define('WIFI_CONNECTION', 'Wifi');

define('ROOMS', '客房');
define('ROOMS_DESC', '别墅共有18间客房，分为6间豪华间，4间豪华顶级间4间行政间和4间行政套房。每间客房都提供豪华的设施和舒适的空间。您只能在Home Chiang Mai中找到特殊的氛围。');
define('HOTEL', '旅馆');
define('PICTURES', '图片');
define('FOOD_AND_DRINKS', '食品和饮料');
define('LEMONT_COFFEE_HOUSE', 'Le Mont Coffee House');
define('EAT_THAI_ENGLAND', 'EAT THAI England');
define('DIRECT_DEBIT', '直接借记');
define('COUNTER_PAYMENT', '计数器支付');
define('ATM_PAYMENT', 'ATM付款');
define('CREDIT_CARD_PAYMENT', '信用卡支付');

define('CONTACT_INFO', '联系信息');
define('ZIP', '邮政编码');
define('TEL', '联系电话');
define('SEND', '发送');
define('YOUR_NAME', '你的名字');
define('EMAIL_OR_PHONE', '电子邮件或电话号码');
define('SUBJECT', '学科');
define('CONTENT', '内容');

define('SERVICE_UNVAILABLE', '服务不可用');

define('ABOUT_DESC', '两百多年来，兰纳王国在新年节期间庆祝的传统之一是为主佛的右冠提供水，在那里该遗迹已在位于56号的Wat Pharthat Sri Jom Tong主持了超过1,000年距离清迈市中心有10公里。每年，仪式都将清真寺的动物遗物送到Wat Suan Dorg的清迈中心。内容别墅清迈位于清迈郊区，因此酒店区域一直是礼仪之旅的重要途径。内容别墅一直以来都在每平方英寸的酒店里为客人提供宁静和全心全意的服务。我们提供泰式融合菜肴，其中包括由在英国Le Cardon Blue培训的顶级厨师设计和烹制的特色菜肴。当然，这是首屈一指的。');
define('CONTENT_UNDER_CONSTRUCTION', '正在施工');
define('EG_PASSPORT_NUMBER', '护照号');

define('PRICE_UNAVAILABLE', '对不起，您的日期范围内价格不可用。');


define('REF', '参考');

define('WAT_TITLE', 'Wat Inthrawat（Wat Ton Kwen）（距酒店5分钟路程）');
define('WAT_P1', '泰国北部的经典兰纳风格建筑最好的例子之一是清迈省农村的Wat Inthrawat。这个建筑群由一个viharn，一个开放的亭子和一个大型开放的mondop组成。');
define('WAT_P2', '这座寺庙在当地也被称为Wat Ton Kwen或Wat Ton Khwen，它是清迈省原有的少数木制寺庙之一。这座寺庙由泰国美术部负责管理。已经进行了翻新，旨在尽可能地保留原有结构。');
define('WAT_P3', '简单而小巧，但非常优雅的viharn建于1858年，拥有泰国Lü影响力的Lanna风格。它由一个开放的亭子三面包围。 Viharn的楼梯上有大型神话般的纳迦蛇，两侧守着建筑物的入口。');
define('WAT_P4', 'Viharn部分被一个名为Sala Bat的开放式凉亭包围。萨拉蝙蝠原来的木地板已被混凝土取代。这个mondop被建造成Phra Boromathat Chom Thong佛舍利的临时避难所，现在被放置在清迈西南部的Wat Phra Si Chom Thong寺。 ');

define('ROYAL_PARK_TITLE', 'The Royal Park Rajapruek（距离酒店10分钟）');
define('ROYAL_PARK_P1', '拉贾普鲁克皇家公园的建立是为了纪念已故国王普密蓬阿杜德于2006年6月9日加入王位60周年，并于2007年12月5日庆祝其80周年生日的纪念日。这是最受欢迎的清迈的农业旅游场所，并被视为农业研究的中心。你会发现无数的植物和各种农业博览会，这对任何年龄的游客都是有利的。此外，它被视为农业和农业以及耕种者的交汇点。');
define('ROYAL_PARK_P2', '公园的景观建筑非常华丽，非常细腻。因此，何康琅皇家馆和其他建筑风格的建筑可以证明这句话是完全正确的。博览会明显地划分成区域，使游客更容易在公园四处游走，并从您在这里的第一步开始学习所见的一切，因为在入口处保留了一棵拥有2.5亿年历史的松树。');

define('BAN_TAWAI_TITLE', 'Ban Thawai (10分钟从酒店)');
define('BAN_TAWAI_P1', 'Ban Thawai是一个木制村庄，被认为是清迈最着名的文化旅游景点之一。它被称为泰国和外国游客清迈的主要文化景点。');
define('BAN_TAWAI_P2', '在Ban Thawai中可以找到最好的木雕品质和价格。如果您只是在这里购买小型纪念品，您将不会后悔将泰国北部的时间安排在半天时间。 Ban Thawai是泰国最便宜的地方，可以找到丝绸，仿古复制品，香炉，蜡烛，油和肥皂礼品包，装饰品，灯具，竹制品，墙壁挂件，相框，花瓶等等。 Ban Thawai展出的大多数产品都是独一无二的泰式产品，其中许多都是北方特色，并由天然材料制成。');
define('BAN_TAWAI_P3', 'Ban Thawai也以亚洲古董闻名，其中一些排名世界最好。');

define('MUANG_KUNG_TITLE', 'Muang Kung Pottery Village');
define('MUANG_KUNG_P1', '孟贡村已被世代祖先告诉了祖先，他们的祖先在这个村落定居下来，他们是从曾在旺山的旺富和旺萨迁出的泰人，现在是缅甸掸邦的一个城镇。这是缅甸士兵入侵的逃生，只有6户搬家。 ');
define('MUANG_KUNG_P2', '这个村庄几乎每个家庭都会做陶器，这是他们的主要工作。如果你在村里的小混凝土街道上行走，你会发现各种类型和不同尺寸的花瓶，包括花瓶，花盆等，在院子里排成一列。而且，在地下室，有许多长者积极帮助画陶器。最受欢迎的产品是具有窄顶部，中部凸起和带盖的锥形底部的锅炉。');

define('NIGHT_SAFARI_TITLE', 'Night Safari');
define('NIGHT_SAFARI_P1', '许多人认为这是世界上最美丽的夜间野生动物园。新加坡夜间野生动物园的面积有两倍，超过300人');
define('NIGHT_SAFARI_P2', '有一个叫做捷豹步行区的一天时间区域，由一条长达1.2公里的步行道绕着一个美丽的湖泊组成。这里有超过400种动物可以看到，包括白虎，美洲虎，豹子，Tap猴，猴子，骆驼，小马，冠冕，甚至还有小袋鼠和考拉。');
define('NIGHT_SAFARI_P3', '夜间区域分为两个区域，稀树草原野生动物园区和捕食者徘徊区。这些部分每天从下午6点开放至午夜。游客通过夜间野生动物园中的不同区域在一个开放的双面电车中旅行。乘坐电车大约5公里，需要大约1小时才能完成。电车从下午7点运行至晚上10点30分，电车每15分钟一班。');

define('NIGHT_BAZAAR_TITLE', 'Night Bazaar');
define('NIGHT_BAZAAR_P1', '清迈夜市是一个商店，酒店，餐馆和酒吧的商业仙境，大多数来清迈的游客至少会在这里找到一次 - 尤其是在18:00之后，所有的市场摊位都开始营业。尽管只有很小一部分区域可以体验，但可以说夜市与清迈一样繁忙。毫不奇怪，像夜市这样的名字，市场在我们的景点名单中占有很大的份额，但我们也从一些有趣的博物馆和画廊获得了一点文化。我们已经覆盖了整个清迈夜市的面积，并编制了最好的看到和做的，使您的逗留一个难忘的。');

define('DOI_INTHANON_TITLE', 'Doi Inthanon');
define('DOI_INTHANON_P1', '茵他侬山是泰国最高的山峰，海拔2565米。茵他侬国家公园在郁郁葱葱的地理和森林中拥有许多惊人的变化。它是热带雨林，混合森林和松树林的家园。全年天气凉爽，尤其是在冬季，全天几乎持续有雾覆盖，而清晨梅克宁或霜花惊人的现象。');
define('DOI_INTHANON_THING_TODO_TITLE', '在茵他侬的活动');
define('DOI_INTHANON_THING_TODO_1', '向Phramahathat Napamathanidol和Phramahathat Napaphol Bhumisiri（伟大的圣物宝塔）致敬。');
define('DOI_INTHANON_THING_TODO_2', '品尝神秘的早晨冬季薄雾之美。');
define('DOI_INTHANON_THING_TODO_3', '探索Mae Klang，Wachirathan，Siriphum，Mae Ya Waterfalls和Kew Mae Pan和Ang Ka自然小径。');
define('DOI_INTHANON_THING_TODO_4', '参见Mae Klang銮稻梯田');
define('DOI_INTHANON_THING_TODO_5', '参加观鸟 ');
define('DOI_INTHANON_THING_TODO_6', '参加在东盟最先进的光学天文设施之一的泰国国家天文台的天空观测。');
define('DOI_INTHANON_THING_TODO_7', '探索茵他侬皇家农业站');

define('NIMMAN_TITLE', 'Nimman Road');
define('NIMMAN_TITLE_DETAIL', '一系列当地和国际美食与异国情调的夜晚。');
define('NIMMAN_DETAIL_1', 'Khao Soi Mae Sai最好的北方面，位于Huaykaew路对面的Ratchaphuek Alley，');
define('NIMMAN_DETAIL_2', 'Tong Tem Toh，Nimman Soi 13最好的兰纳食物');
define('NIMMAN_DETAIL_3', 'Ristr8to Lab最好的咖啡店，Nimman Soi 3');
define('NIMMAN_DETAIL_4', '位于尼曼17号iBerry Gardens的最佳甜品咖啡厅');
define('NIMMAN_DETAIL_5', 'Mu\'s Katsu，Nimman Soi 8最好的日本咖喱');
define('NIMMAN_DETAIL_6', '最好的啤酒最佳健康食品：The Salad Concept，Soi 13');
define('NIMMAN_DETAIL_7', '位于Nimman，Soi 12的Beer Lab最好的酒吧');
define('NIMMAN_DETAIL_8', '俱乐部咖啡厅的最佳夜总会，Nimmam Soi 7');

define('PHOTO_BY', '拍摄者');
define('OUR_VIDEO', '我们的视频');
define('PROMOTIONS', '提升');

?>
