<?php

session_start();
include '../includes/db.conn.php';
include '../includes/conf.class.php';
include 'language.php';
include '../includes/admin.ajaxprocess.class.php';
$adminAjaxProc = new adminAjaxProcessor();
$actionCode    = isset($_POST['actioncode']) ? $_POST['actioncode'] : 0;
switch ($actionCode) {
    case '1': $adminAjaxProc->getbsiEmailcontent(); break;

    case '2': $adminAjaxProc->generatePriceplanList(); break;

    case '3': $adminAjaxProc->getdefaultcapacity(); break;

    case '4': $adminAjaxProc->getDeposit(); break;

    case '5': $adminAjaxProc->genearateCapacityCombo(); break;

    case '6': $adminAjaxProc->getbsiGallery(); break;

    case '7': $adminAjaxProc->setPicToHomepage(); break;

    case '8': $adminAjaxProc->getbsiCMSGallery(); break;

    case '9': $adminAjaxProc->getbsiCMSGallerRoom1(); break;

    case '10': $adminAjaxProc->getbsiCMSGallerRoom2(); break;

    case '11': $adminAjaxProc->getbsiCMSGallerRoom3(); break;

    case '12': $adminAjaxProc->getbsiCMSGallerRoom4(); break;

    case '13': $adminAjaxProc->getbsiCMSGallerRoom5(); break;

    default:  $adminAjaxProc->sendErrorMsg();
   }
