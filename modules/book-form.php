<?php echo CSRF::tokenField() ?>
<div class="form-group">
    <div class="text-form"><?php echo CHECK_IN_D_TEXT ?></div>
    <div class="input-group">
        <input readonly <?php echo $isDisableBookingEngine == '1' ? 'disabled' : '' ?> type="text" autocomplete="off" class="readonly form-control" required  id="<?php echo $checkIn; ?>"  name="check_in"  data-date-format="<?php echo $bsiCore->bt_date_format(); ?>" placeholder="<?php trans('CHOOCE_CHECK_IN_DATE') ?>">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
    </div>
</div>
<div class="form-group">
    <div class="text-form"><?php echo CHECK_OUT_D_TEXT ?></div>
    <div class="input-group">
        <input readonly <?php echo $isDisableBookingEngine == '1' ? 'disabled' : '' ?> type="text" autocomplete="off" class="readonly form-control" required id="<?php echo $checkOut; ?>"  name="check_out" data-date-format="<?php echo $bsiCore->bt_date_format(); ?>" placeholder="<?php trans('CHOOCE_CHECK_OUT_DATE') ?>">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
    </div>
</div>
<div class="form-group">
    <div class="text-form"><?php echo ADULT_TEXT ?></div>
    <div class="input-group">
        <select <?php echo $isDisableBookingEngine == '1' ? 'disabled' : '' ?> class="form-control" id="capacity" name="capacity">
            <option>2</option>
        </select>
        <div class="input-group-addon"><i class="fa fa-users"></i></div>
    </div>
</div>
<div class="hidden form-group">
    <div class="text-form"><?php echo CHILDREN_TEXT ?></div>
    <div class="input-group">
        <select <?php echo $isDisableBookingEngine == '1' ? 'disabled' : '' ?> class="form-control" id="child_per_room" name="child_per_room">
            <option>0</option>
        </select>
        <div class="input-group-addon"><i class="fa fa-users"></i></div>
    </div>
</div>

<div class="form-group">
    <?php if ($isDisableBookingEngine == '1') : ?>
        <div class="text-form"><?php trans('CURRENCY_TEXT') ?></div>
        <select disabled class="form-control" name="">
            <option value="">USD</option>
        </select>
    <?php else: ?>

        <?php echo $bsiCore->get_currency_combo3($bsiCore->currency_code()); ?>

    <?php endif; ?>
</div>
