<?php

session_start();
include 'includes/db.conn.php';
include 'includes/conf.class.php';
include 'includes/mail.class.php';
$sql              = $mysqli->query('select * from bsi_language where `lang_default`=true');
$row_default_lang = $sql->fetch_assoc();

if (!empty($_COOKIE['language'])) {
    $row_default_lang['lang_file'] = map_lang($_COOKIE['language']) . '.php';
}

include 'languages/' . $row_default_lang['lang_file'];
include 'vendor/autoload.php';
include 'includes/smtp-mail.class.php';

$paymentGatewayDetails = $bsiCore->loadPaymentGateways();
$bsiMail               = new bsiMail();
$emailContent          = $bsiMail->loadEmailContent(); require_once 'paypal.class.php';  // include the class file

$invoice = time();

$p = new paypal_class;             // initiate an instance of the class

$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url

if ('development' == Env::get('ENV')) {
    $p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
}

// setup a variable for this script (ie: 'http://www.micahcarrick.com/paypal.php')

$requestScheme = Env::get('SCHEME');
$this_script   = $requestScheme . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];

// if there is not action variable, set the default action of 'process'

if (empty($_GET['action'])) {
    $_GET['action'] = 'process';
}

switch ($_GET['action']) {
    case 'process':      // Process and order...

        $p->add_field('business', $paymentGatewayDetails['pp']['account']);
        $p->add_field('return', $this_script . '?action=success');
        $p->add_field('cancel_return', $this_script . '?action=cancel');
        $p->add_field('notify_url', $this_script . '?action=ipn');
        $p->add_field('item_name', $bsiCore->config['conf_hotel_name']);
        $p->add_field('invoice', $_POST['invoice']);
        $p->add_field('currency_code', $bsiCore->currency_code());
        $p->add_field('amount', parse_money_to_int($_POST['amount']));
        $p->submit_paypal_post(); // submit the fields to paypal

        if (Env::get('DEBUG')) {
            $p->dump_fields();      // for debugging, output a table of all the fields
        }

      break;

    case 'success':      // Order was successful...

        header('location:booking-confirm.php?success_code=1');
        break;

    case 'cancel':       // Order was canceled...

        header('location:booking-failure.php?error_code=26');
        break;

    case 'ipn':          // Paypal is calling page for IPN validation...
        if ($p->validate_ipn()) {
            if ('Completed' == $p->ipn_data['payment_status'] || 'Pending' == $p->ipn_data['payment_status']) {
                //*****************************************************************************************
                $mysqli->query("UPDATE bsi_bookings SET payment_success=true, payment_txnid='" . $p->ipn_data['txn_id'] . "', paypal_email='" . $p->ipn_data['payer_email'] . "' WHERE booking_id='" . $p->ipn_data['invoice'] . "'");

                $ppsql       =$mysqli->query("SELECT client_name, client_email, invoice FROM bsi_invoice WHERE booking_id='" . $p->ipn_data['invoice'] . "'");
                $invoiceROWS = $ppsql->fetch_assoc();
                $mysqli->query("UPDATE bsi_clients SET existing_client = 1 WHERE email='" . $invoiceROWS['client_email'] . "'");

                $invoiceHTML = $invoiceROWS['invoice'];
                $invoiceHTML .= '<br><br><table  style="font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;" cellpadding="4" cellspacing="1"><tr><td align="left" colspan="2" style="font-weight:bold; font-variant:small-caps; background:#eeeeee">' . $mysqli->real_escape_string(INV_PAY_DETAILS) . '</td></tr><tr><td align="left" width="30%" style="font-weight:bold; font-variant:small-caps; background:#ffffff">' . $mysqli->real_escape_string(INV_PAY_OPTION) . '</td><td align="left" style="background:#ffffff">PayPal</td></tr><tr><td align="left" width="30%" style="font-weight:bold; font-variant:small-caps; background:#ffffff">Payer E-Mail</td><td align="left" style="background:#ffffff">' . $p->ipn_data['payer_email'] . '</td></tr><tr><td align="left" style="font-weight:bold; font-variant:small-caps; background:#ffffff">' . $mysqli->real_escape_string(INV_TXN_ID) . '</td><td align="left" style="background:#ffffff">' . $p->ipn_data['txn_id'] . '</td></tr></table>';

                $mysqli->query("UPDATE bsi_invoice SET invoice = '$invoiceHTML' WHERE booking_id='" . $p->ipn_data['invoice'] . "'");

                $emailBody = 'Dear ' . $invoiceROWS['client_name'] . ',<br><br>';
                $emailBody .= html_entity_decode($emailContent['body']) . '<br><br>';
                $emailBody .= $invoiceHTML;
                $emailBody .= '<br><br>' . $mysqli->real_escape_string(PP_REGARDS) . ',<br>' . $bsiCore->config['conf_hotel_name'] . '<br>' . $bsiCore->config['conf_hotel_phone'];
                $emailBody .= '<br><br><font style="color:#F00; font-size:10px;">[ ' . $mysqli->real_escape_string(PP_CARRY) . ' ]</font>';
                $flag = 1;
                // $bsiMail->sendEMail($invoiceROWS['client_email'], $emailContent['subject'], $emailBody, $p->ipn_data['invoice'], $flag);

                $smtpMail = new SMTPMail();

                $smtpConfig = [
                    'smtp_host' => $bsiCore->config['conf_smtp_host'],
                    'smtp_username' => $bsiCore->config['conf_smtp_username'],
                    'smtp_password' => $bsiCore->config['conf_smtp_password'],
                    'smtp_port' => $bsiCore->config['conf_smtp_port'],
                    'smtp_encryption' => $bsiCore->config['conf_smtp_encryption']
                ];

                $returnMsg = $smtpMail->setSubject($emailContent['subject'])
                    ->setSystemConfig($smtpConfig)
                    ->sender([$bsiCore->config['conf_hotel_email']])
                    ->receiver([$invoiceROWS['client_email']])
                    ->setBody($emailBody)
                    ->send();

                /* Notify Email for Hotel about Booking */
                $notifyEmailSubject = 'Booking no.' . $p->ipn_data['invoice'] . ' - Notification of Room Booking by ' . $invoiceROWS['client_name'];

                $returnMsg = $smtpMail->setSubject($notifyEmailSubject)
                    ->setSystemConfig($smtpConfig)
                    ->sender([$bsiCore->config['conf_hotel_email']])
                    ->receiver([$bsiCore->config['conf_notification_email']])
                    ->setBody($invoiceHTML)
                    ->send();

                // $bsiMail->sendEMail($bsiCore->config['conf_notification_email'], $notifyEmailSubject, $invoiceHTML);
            //*****************************************************************************************
            } elseif ('Refunded' == $p->ipn_data['payment_status']) {
                $mysqli->query("update paypal_payment set payment_success='0' where invoice=" . $p->ipn_data['invoice']);
            } elseif ('Reversed' == $p->ipn_data['payment_status']) {
                $mysqli->query("update paypal_payment set payment_success='0' where invoice=" . $p->ipn_data['invoice']);
            }
        }
    break;
  }
