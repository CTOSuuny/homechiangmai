<?php
session_start();
if(!isset($_SESSION['bookingId'])){
	header('Location: booking-failure.php?error_code=9');
}


include("includes/db.conn.php");
include("includes/conf.class.php");
include("includes/mail.class.php");

if (isset($_POST['stripeToken'])){

$sql=$mysqli->query("select * from bsi_language where `lang_default`=true");
$row_default_lang=$sql->fetch_assoc();
	include("languages/".$row_default_lang['lang_file']);
}else{
	include("language.php");
}

$bsiMail = new bsiMail();
$paymentGatewayDetails = $bsiCore->loadPaymentGateways();
$emailContent=$bsiMail->loadEmailContent();
$st_account=explode("#",$paymentGatewayDetails['st']['account']);

require_once('includes/stripe/Stripe.php');
$stripe = array(
'secret_key'      => $st_account[0],
'publishable_key' => $st_account[1]
);
Stripe::setApiKey($stripe['secret_key']);

if ($_POST) {
    $error = NULL;
    try {
      if (!isset($_POST['stripeToken']))
        throw new Exception("The Stripe Token was not generated correctly");

		  if($st_account[2]){
		  $customer = Stripe_Customer::create(array(
			  "card" => $_POST['stripeToken'],
			  "email" => $_SESSION['clientEmail'],
			  "description"=>"Booking#".$_SESSION['bookingId']
			  ));

			Stripe_Charge::create(array(
			  "amount" => round(($_SESSION['paymentAmount']*100)), # amount in cents, again
			  "currency" => $bsiCore->currency_code(),
			  "customer" => $customer->id)
			);

		  }else{

			   $customer = Stripe_Customer::create(array(
			  "card" => $_POST['stripeToken'],
			  "email" => $_SESSION['clientEmail'],
			  "description"=>"Booking#".$_SESSION['bookingId']
			  ));


		  }

    }
    catch (Exception $e) {
      $error = $e->getMessage();
    }

    if ($error == NULL) {
	//*****************************************************************************************
			   	$mysqli->query("UPDATE bsi_bookings SET payment_success=true WHERE booking_id='".$_SESSION['bookingId']."'");
				$sqlsp=$mysqli->query("SELECT client_name, client_email, invoice FROM bsi_invoice WHERE booking_id='".$_SESSION['bookingId']."'");
				$invoiceROWS = $sqlsp->fetch_assoc();
				$mysqli->query("UPDATE bsi_clients SET existing_client = 1 WHERE email='".$invoiceROWS['client_email']."'");

				$invoiceHTML = $invoiceROWS['invoice'];
				$invoiceHTML.= '<br><br><table  style="font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;" cellpadding="4" cellspacing="1"><tr><td align="left" colspan="2" style="font-weight:bold; font-variant:small-caps; background:#eeeeee">'.mysql_real_escape_string(INV_PAY_DETAILS).'</td></tr><tr><td align="left" width="30%" style="font-weight:bold; font-variant:small-caps; background:#ffffff">'.mysql_real_escape_string(INV_PAY_OPTION).'</td><td align="left" style="background:#ffffff">'.$paymentGatewayDetails['st']['name'].'</td></tr></table>';

				$mysqli->query("UPDATE bsi_invoice SET invoice = '$invoiceHTML' WHERE booking_id='".$_SESSION['bookingId']."'");



				$emailBody = "Dear ".$invoiceROWS['client_name'].",<br><br>";
				$emailBody .= html_entity_decode($emailContent['body'])."<br><br>";
				$emailBody .= $invoiceHTML;
				$emailBody .= "<br><br>".mysql_real_escape_string(PP_REGARDS).",<br>".$bsiCore->config['conf_hotel_name'].'<br>'.$bsiCore->config['conf_hotel_phone'];
				$emailBody .= "<br><br><font style=\"color:#F00; font-size:10px;\">[ ".mysql_real_escape_string(PP_CARRY)." ]</font>";
				$flag = 1;
				$bsiMail->sendEMail($invoiceROWS['client_email'], $emailContent['subject'], $emailBody, $_SESSION['bookingId'], $flag);

				/* Notify Email for Hotel about Booking */
				$notifyEmailSubject = "Booking no.".$_SESSION['bookingId']." - Notification of Room Booking by ".$invoiceROWS['client_name'];

				$bsiMail->sendEMail($bsiCore->config['conf_notification_email'], $notifyEmailSubject, $invoiceHTML);
			//*****************************************************************************************

     header('Location: booking-confirm.php?success_code=1');
	 die;
    }
    else {
		header('Location: booking-failure.php?error_code=99&rescode='.htmlentities($error));
		die;
    }
  }
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $bsiCore->config['conf_hotel_name']; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans|Playfair+Display" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo asset('css/datepicker.css') ?>" type="text/css" media="screen"/>


		<link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider.css') ?>" />
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider-customize.css') ?>"> -->
		<link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-customize.css') ?>">

    </head>
    <body>
		<div class="container-fluid">
			<?php
				include("modules/inner-modules.php")
			?>
			<div id="body-div">
				<div class="row show-room-dialog" >
					<h1><?php echo $bsiCore->config['conf_hotel_name']; ?></h1>
					<?php
						$active = "payment";
						include("modules/breadcrumb.php");
					?>
					<br><br>
			                <div class="row-fluid">
			                    <div class="span12">
									<div id="body-div">

			                        <div class="wrapper">
			                            <div class="htitel">
			                                <h2 class="fl" style="border:0; margin:0;"><?php echo CC_DETAILS; ?> (Stripe)</h2>
			                            </div>
										<br>
			                            <!-- start of search row -->
										<div class="container-fluid" style="margin:0; padding:0;">
	 	                                   	<div class="row-fluid" style="border: 1px solid #dfc27c; padding: 1% 0">

			                                        <form name="signupform" id="payment-form"   action="stripe-processor.php" method="post" class="form-horizontal" style="width: 95%; margin: 0 2.5%">
														<div class="form-group">
				                                            <label class="control-label col-sm-offset-2 col-sm-2" for="ea"><?php echo CC_NUMBER; ?>:</label>
				                                            <div class="controls col-sm-5">
				                                               <input type="text" class="form-control" autocomplete="off" name="card-number" id="card-number" maxlength="16" class="input-large required digits">
															   <div class="errorTxt" align="left"></div>
				                                            </div>
				                                        </div>
			                                            <div class="form-group">
			                                                <label class="control-label col-sm-offset-2 col-sm-2" for="ed"><?php echo CC_EXPIRY; ?> (mm/yyyy):</label>
																<div class="col-sm-1">
																	<input type="text" class="form-control" name="card-expiry-month" id="card-expiry-month" maxlength="2" class="input-mini required digits">
																	<div class="errorTxt" align="left"></div>
																</div>
																<div class="col-sm-1">
																	<input type="text" class="form-control" name="card-expiry-year" id="card-expiry-year" maxlength="4" class="input-mini required digits" />
																	<div class="errorTxt" align="left"></div>
																</div>
														</div>
			                                            <div class="form-group">
			                                                <label class="control-label col-sm-offset-2 col-sm-2" for="cc">CCV/CCV2:</label>
			                                                <div class="controls col-sm-1">
			                                                    <input type="text" class="form-control" autocomplete="off" name="card-cvc"  id="card-cvc" maxlength="4" class="input-mini required  number">
																<div class="errorTxt" align="left"></div>
															</div>
			                                            </div>
			                                            <div class="form-group">
			                                                <label class="control-label col-sm-offset-2 col-sm-2" for="ad"><?php echo CC_AMOUNT; ?></label>
			                                                <div class="controls col-sm-5" align="left">
																<strong><?php echo $bsiCore->currency_symbol().$_SESSION['paymentAmount']; ?></strong><br />
																<label class="checkbox col-sm-12">
	   																<input type="checkbox" name="tos" id="tos" value="" class="required" >
	   																<?php echo CC_TOS1; ?> <?php echo $bsiCore->config['conf_hotel_name']; ?>
	   					   											<?php echo CC_TOS2; ?> <?php echo $bsiCore->currency_symbol().$_SESSION['paymentAmount']?> <?php echo CC_TOS3; ?>.</a>
																	<div class="errorTxt" align="left"></div>
																</label>
			                                                </div>
			                                            </div>

			                                </div>
			                            </div>
			                            <!-- end of search row -->
			                        </div>
									<div style="margin-bottom: 150px;">
										<div class="col-sm-6">
											 <button id="registerButton" class="btn btn-default btn-book" type="button" onClick="window.location.href='booking-failure.php?error_code=26'" ><?php echo BTN_CANCEL; ?></button>
										</div>
										<div class="col-sm-6">
											<button id="registerButton" class="btn btn-default btn-book" type="submit" class="conti" ><?php echo CC_SUBMIT; ?></button>
										</div>
									</div>
			                       </form>
			                    </div>
			                </div>
			            </div>

				</div>
			</div>
			<?php
		        include("modules/footer.php");
		    ?>
		</div>

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo asset('js/modernizr-2.6.2.min.js') ?>"></script>
		<script type="text/javascript" src="<?php echo asset('js/jquery-zoomslider.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo asset('js/bootstrap-datepicker.js') ?>"></script>
        <script type="text/javascript" src="<?php echo asset('js/jquery.validate.js') ?>"></script>

        <script type="text/javascript">
            $(window).load(function() {

            });
        </script>
        <script id="demo" type="text/javascript">
			$(document).ready(function() {
				$("#payment-form").validate({
		            errorPlacement: function(error, element) {
		                error.appendTo( element.nextAll( ".errorTxt" ) );
		            }
		        });
			});
			</script>
			<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
			  <script type="text/javascript">
				// this identifies your website in the createToken call below
				Stripe.setPublishableKey("<?php echo $stripe['publishable_key']; ?>");

				function stripeResponseHandler(status, response) {
					if (response.error) {
						// re-enable the submit button
						$('#registerButton1').removeAttr("disabled");
						// show the errors on the form
						//$(".payment-errors").html(response.error.message);
						alert(response.error.message);
					} else {
						var form$ = $("#payment-form");
						// token contains id, last4, and card type
						var token = response['id'];
						// insert the token into the form so it gets submitted to the server
						form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
								// and submit
						form$.get(0).submit();
					}
				}

				$(document).ready(function() {
					$("#payment-form").submit(function(event) {
						// disable the submit button to prevent repeated clicks
						$('#registerButton1').attr("disabled", "disabled");
						// createToken returns immediately - the supplied callback submits the form if there are no errors
						Stripe.createToken({
							number: $('#card-number').val(),
							cvc: $('#card-cvc').val(),
							exp_month: $('#card-expiry-month').val(),
							exp_year: $('#card-expiry-year').val()
						}, stripeResponseHandler);
						return false; // submit from callback
					});
				});

    </script>
    </body>
</html>
