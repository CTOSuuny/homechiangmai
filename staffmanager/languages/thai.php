<?php
define('ID_TYPE',' ประเภทบัตรประจำตัว');
define('ID_NUMBER',' หมายเลขบัตรประจำตัว');
define('LAST_10_BOOKING',' จอง 10 รายการ ล่าสุด');
define('TODAY_CHECK_IN',' เช็คอินวันนี้');
define('TODAY_CHECK_OUT',' เช็คเอ้าวันนี้');
define('HOTEL_DETAILS','รายละเอียดโรงแรม');
define('HOTEL_NAME','ชื่อโรงแรม');
define('STREET_ADDRESS','ที่อยู่');
define('CITY','อำเภอ');
define('STATE','จังหวัด');
define('COUNTRY','ประเทศ');
define('ZIP_AND_POST_CODE','ไปรษณีย์/รหัสไปรษณ๊ย์');
define('PHONE_NUMBER','หมายเลขโทรศัพท์');
define('FAX','โทรสาร');
define('EMAIL_ID','รหัสอีเมล');
define('URL_FACBOOK','Url Facebook');
define('URL_TWITTER','Url Twitter');
define('URL_INSTAGRAM','Url Instagram');
define('URL_GOOGLEPLUS','Url Google Plus');
define('SUBMIT','ส่ง');
define('ROOM_LIST','รายชื่อห้องพัก');
define('ADD_NEW_ROOM','เพิ่มห้องใหม่');
define('ROOM_TYPE','ประเภทห้องพัก');
define('ADULT_ROOM','ผู้ใหญ่/ห้อง');
define('TOTAL_ROOM','ห้องทั้งหมด');
define('EDIT_ROOM_LIST','แก้ไข');
define('DELETE_ROOM_LIST','ลบ');
define('MAX_CHILD_PER_ROOM','เด็กสูงสุด/ห้อง');
define('LEAVE_BLANK_IF_NONE_TEXT','ปล่อยว่างไว้ถ้าไม่มี');
define('ROOM_ADD_AND_EDIT','เพิ่ม/แก้ไขห้อง');
define('NUMBER_OF_ROOM','จำนวนห้องพัก');
define('ROOM_TYPE_ADD_EDIT','ประเภทห้องพัก');
define('NO_OF_ADULT','จำนวนผู้ใหญ่');
define('ADD_EDIT_SUBMIT','ส่ง');
define('EXAMPLE','ตัวอย่าง');
define('ADD_EDIT_ROOM_SELECT','---เลือก---');
define('ADD_EDIT_CAPACITY_SELECT','---เลือก---');
define('SAME_COMBINATION_OF_ROOMTYPE_ALREADY_EXISTS_TEXT','ประเภทห้องพักและจำนวนผู้เข้าพักที่เลือกมีอยู่แล้ว');
define('ROOM_TYPE_LIST','ชนิดของห้องพัก');
define('ROOM_TYPE_NAME','ชื่อชนิดห้อง');
define('ADD_NEW_ROOMTYPE','เพิ่มชนิดห้องใหม่');
define('ROOM_TYPE_EDIT','แก้ไข');
define('ROOM_TYPE_DELETE','ลบ');
define('DO_YOU_WANT_TO_DELETE_THE_SELECTED_ROOM_TYPE','คุณต้องการลบชนิดของห้องพักที่เลือกหรือไม่ และอย่าลืมว่าการลบประเภทของห้องพักจะทำให้ห้องพักและราคาถูกลบด้วย');
define('ROOM_TYPE_ADD_AND_EDIT','เพิ่ม/แก้ไข ชนิดห้องพัก');
define('ROOM_TYPE_TITLE','ชื่อชนิดห้อง');
define('ROOM_TYPE_IMG','รูปภาพ');
define('ROOM_TYPE_SUBMIT','ส่ง');
define('EXAMPLE_DELUXE_AND_STANDARD','ตัวอย่าง: มาตรฐาน, หรูรา');
define('DESCRIPTION_FACILITIES_TEXT','คำอธิบาย/สิ่งอำนวยความสะดวก');
define('ROOM_FACILITIES', 'สิ่งอำนวยความสะดวก');
define('CAPACITY_LIST','รายการจำนวนคน');
define('CAPACITY_TITLE','ชื่อจำนวนคน');
define('NO_OF_ADULT_CAPACITY','จำนวนผู้ใหญ่');
define('ADD_NEW_CAPACITY','เพิ่มจำนวนคนใหม่');
define('CAPACITY_LIST_EDIT','แก้ไข');
define('CAPACITY_LIST_DELETE','ลบ');
define('DO_YOU_WANT_TO_DELETE_THE_SELECTED_CAPACITY_ALERT','คุณต้องการลบจำนวนคนที่เลือกหรือไม่ และอย่าลืมว่า ประเภทของห้องพักและห้องพักจะถูกลบด้วย');
define('CAPACITY_ADD_AND_EDIT','เพิ่ม/แก้ไข จำนวนคน');
define('CAPACITY_TITLE_ADD_EDIT','ชื่อจำนวนคน');
define('EXAMPLE_SINGLE_DOUBLE','ตัวอย่าง: เดี่ยว, คู่');
define('ADD_EDIT_CAPACITY_NUMBER_OF_ADULT','จำนวนผู้ใหญ่');
define('ADD_EDIT_CAPACITY_SUBMIT','ส่ง');
define('PRICE_PLAN_PRICE_LIST','แผนราคา');
define('ADD_NEW_PRICEPLAN','เพิ่มแผนราคาใหม่');
define('PRICE_PLAN_SELECT_ROOM_TYPE','เลือกประเภทห้องพัก');
define('PLEASE_SELECT_ROOMTYPE_FIRST','กรุณาเลือกประเภทห้องพักก่อน');
define('NO_AVAILABLE_DATA_FOUND','ไม่พบข้อมูลที่พร้อมใช้งาน');
define('PRICEPLAN_PLEASE_SELECT_ROOMTYPE_FIRST','กรุณาเลือกประเภทห้องพักก่อน');
define('DO_YOU_WANT_TO_DELETE_SELECTED_PRICEPLAN','คุณต้องลบราคาวางแผนเลือก');
define('PRICEPLAN_SELECT','เลือก');
define('MON','จันทร์');
define('TUE','อังคาร');
define('WED','พุธ');
define('THU','พฤหัสบดี');
define('FRI','ศุกร์');
define('SAT','เสาร์');
define('SUN','อาทิตย์');
define('PP_CAPACITY','จำนวนผู้เข้าพัก');
define('PP_REGULAR','ราคาปกติ');
define('PP_REQUIRED','ต้องระบุ');
define('PP_ONLY_NUM','หมายเลขเท่านั้น');
define('PRICE_PLAN_ADD_AND_EDIT','เพิ่ม/แก้ไข ราคา');
define('PRICE_PLAN_BACK','ย้อนกลับ');
define('ADD_EDIT_PRICEPLAN_ROOMTYPE','ประเภทของห้องพัก');
define('ADD_EDIT_START_DATE','วันเริ่มต้น');
define('ADD_EDIT_PRICEPLAN_END_DATE','วันสิ้นสุด');
define('PLEASE_SELECT_ROOMTYPE_FROM_DROPDOWN','กรุณาเลือกประเภทห้องจากช่อง Dropdown');
define('NOT_FOUND','ไม่พบ');
define('PLEASE_SELECT_ROOMTYPE_FROM_DROPDOWN_ALERT','กรุณาเลือกประเภทห้องจากช่อง Dropdown');
define('ADVANCE_PAYMENT_SETTING','การตั้งค่าการชำระขั้นสูง');
define('ENABLED_MONTHLY_DEPOSIT_SCHEME','เปิดใช้งาน แผนฝากรายเดือน');
define('JANUARY','มกราคม');
define('FEBRUARY','กุมภาพันธ์');
define('MARCH','มีนาคม');
define('APRIL','เมษายน');
define('MAY','พฤษภาคม');
define('JUNE','มิถุนายน');
define('JULY','กรกฏาคม');
define('AUGUST','สิงหาคม');
define('SEPTEMBER','กันยายน');
define('OCTOBER','ตุลาคม');
define('NOVEMBER','พฤศจิกายน');
define('DECEMBER','ธันวาคม');
define('OF_TOTAL_AMT','จำนวนรวม');
define('ADV_PP_DESABLED','ปิดใช้งานคุณลักษณะการชำระเงินล่วงหน้า');
define('VIEW_BOOKING_LIST','รายการจอง');
define('VIEW_BOOKINGS_SELECT_TYPE','เลือกชนิด');
define('SELECT_BOOKING_TYPE','เลือกชนิดของการจอง');
define('ACTIVE_BOOKING','จองที่ใช้งานอยู่');
define('BOOKING_HISTORY','ประวัติจอง');
define('VIEW_BOOKINGS_SUBMIT','ส่ง');
define('VB_DATE_RANGE','ช่วงวัน');
define('VB_OPTIONAL','ไม่จำเป็น');
define('VB_TO','ถึง');
define('VB_BY','โดย');
define('VIEW_ACTIVE_BOOKING_ID','ID รายการจอง');
define('VIEW_ACTIVE_NAME','ชื่อ');
define('VIEW_ACTIVE_CHECK_IN','เช็คอิน');
define('VIEW_ACTIVE_CHECK_OUT','เช็คเอาท์');
define('VIEW_ACTIVE_AMOUNT','ยอดเงิน');
define('VIEW_ACTIVE_BOOKING_DATE','วันจอง');
define('VIEW_ACTIVE_DETAILS','ดูรายละเอียด');
define('VIEW_PRINT_VOUCHER','พิมพ์ใบสำคัญ');
define('VIEW_ACTIVE_CANCEL','ยกเลิก');
define('VIEW_ACTIVE_ARE_YOU_SURE_WANT_TO_CANCEL_BOOKING','คุณแน่ใจหรือไม่ที่จะยกเลิกการจอง');
define('VIEW_ACTIVE_ARE_YOU_SURE_WANT_TO_DELETE','คุณแน่ใจหรือที่ต้องการลบการจอง');
define('VB_ACTIVE','รายการจองที่ใช้งานอยู่');
define('VB_HISTORY','จองรายการประวัติ');
define('VIEW_BOOKING_DETAILS','รายละเอียดการจอง');
define('VIEW_CUSTOMER_DETAILS','รายละเอียดของลูกค้า');
define('VIEWDETAILS_NAME','ชื่อ');
define('VIEWDETAILS_ADDRESS','ที่อยู่');
define('VIEWDETAILS_CITY','อำเภอ');
define('VIEWDETAILS_STATE','จังหวัด');
define('VIEWDETAILS_COUNTRY','ประเทศ');
define('VIEWDETAILS_ZIP_AND_POST_CODE','ไปรษณีย์/รหัสไปรษณ๊ย์');
define('VIEWDETAILS_PHONE','โทรศัพท์');
define('VIEWDETAILS_FAX','โทรสาร');
define('VIEWDETAILS_EMAIL','อีเมล');
define('VIEWDETAILS_BOOKING_STATUS','สถานะการจอง');
define('DEPARTED','ออกเดินทาง');
define('ACTIVE','ใช้งานอยู่');
define('CANCELLED','ยกเลิก');
define('VIEWDETAILS_BACK','ย้อนกลับ');
define('CUSTOMERLOOKUP_CUSTOMER_LIST','รายชื่อลูกค้า');
define('CUSTOMERLOOKUP_GUEST_NAME','ชื่อลูกค้า');
define('CUSTOMER_STREET_ADDRESS','ที่อยู่');
define('CUSTOMERLOOKUP_PHONE_NUMBER','หมายเลขโทรศัพท์');
define('CUSTOMERLOOKUP_EMAIL_ID','รหัสอีเมล');
define('CUSTOMERLOOKUP_VIEW_BOOKING','ดูการจอง');
define('CUSTOMERLOOKUP_EDIT','แก้ไข');
define('CUSTOMER_BOOKING_LIST_OF','รายการจอง ');
define('CUSTOMER_BOOKING_ID','รหัสจอง');
define('CUSTOMER_BOOKING_NAME','ชื่อลูกค้า');
define('CUSTOMER_BOOKING_CHECK_IN_DATE','วันเช็คอิน');
define('CUSTOMER_BOOKING_CHECK_OUT_DATE','วันเช็คเอ้าท์');
define('CUSTOMER_BOOKING_AMOUNT','ยอดเงิน');
define('CUSTOMER_BOOKING_DATE','วันจอง');
define('CUSTOMER_BOOKING_STATUS','สถานะ');
define('CUSTOMER_BOOKING_ARE_YOU_SURE_WANT_TO_CANCEL_BOOKING','คุณแน่ใจหรือไม่ที่จะยกเลิกการจอง');
define('ARE_YOU_SURE_WANT_TO_DELETE_BOOKING_ALERT','คุณแน่ใจหรือไม่ที่จะลบการจอง ซึ่งการลบการจองจะเป็นการโดยถาวรจากฐานข้อมูล');
define('CUSTOMER_BOOKING_ACTIVE','ใช้งานอยู่');
define('CUSTOMER_CANCEL_BOOKING','ยกเลิกการจอง');
define('CUSTOMER_BOOKING_COMPLETED','เสร็จสมบูรณ์');
define('CUSTOMER_DELETE_FOREVER','ลบถาวร');
define('CUSTOMER_BOOKING_CANCELLED','ยกเลิก');
define('CUSTOMER_BOOKING_VIEW_DETAILS','ดูรายละเอียด');
define('CUSTOMER_BOOKING_PRINT_VOUCHER','พิมพ์ใบสำคัญ');
define('CALENDER_VIEW_OF_AVAILABILITY','มุมมองปฏิทินห้องที่พร้อมสำหรับการจอง');
define('CALENDER_VIEW_SUBMIT','ส่ง');
define('LEGEND','คำอธิบายแผนภูมิ');
define('CURRENT_DATE','วันปัจจุบัน');
define('PAST_DATE','วันที่ผ่านมา');
define('ALL_AVAILABLE','ทั้งหมด');
define('NOT_AVAILABLE','ไม่พร้อมใช้งาน');
define('SU','อา');
define('MO','จ');
define('TU','อ');
define('WE','พ');
define('TH','พฤ');
define('FR','ศ');
define('SA','ส');
define('CV_MONTH','เดือน');
define('MONTH','เดือน');
define('ROOM_BLOCK_LIST','รายการบล็อกห้อง');
define('CLICK_HERE_TO_BLOCK_ROOM','คลิกที่นี่เพื่อบล็อกห้อง');
define('DESCRIPTION_AND_NAME','รายละเอียด/ชื่อ');
define('DATE_RANGE','ช่วงวัน');
define('ROOMTYPE_CAPACITY','ประเภทห้อง (จำนวนผู้เข้าพัก)');
define('ADMIN_BLOCK_TOTAL_ROOM','ห้องทั้งหมด');
define('UN_BLOCK','ยกเลิกการบล็อกห้อง');
define('ROOM_BLOCKING_SEARCH','ค้นหาการบล็อกห้องพัก');
define('ROOM_BLOCK_CHECK_IN_DATE','วันเช็คอิน');
define('ROOM_BLOCK_CHECK_OUT_DATE','วันเช็คเอาท์');
define('ROOM_BLOCK_SEARCH','ค้นหา');
define('SEARCH_RESULT','ผลการค้นหา');
define('DESCRIPTION_NAME','คำอธิบาย');
define('BLOCK_ROOMTYPE','ประเภทของห้อง');
define('BLOCK_AVAILABLITY','การบล็อกที่กำลังทำงาน');
define('BLOCK','บล็อก');
define('GLOBAL_SETTING','การตั้งค่าหลักของระบบ');
define('EMAIL_NOTIFICATION','อีเมลแจ้งเตือน');
define('CURRENCY_CODE','รหัสสกุลเงิน');
define('CURRENCY_SYMBOL','สัญลักษณ์สกุลเงิน');
define('BOOKING_ENGINE','ระบบการจอง');
define('HOTEL_TIMEZONE','เขตเวลาโรงแรม');
define('MINIMUM_BOOKING','จองขั้นต่ำ');
define('DATE_FORMAT','รูปแบบวัน');
define('ROOM_LOCK_TIME','เวลาล็อคห้อง');
define('GLOBAL_SETTING_TAX','ภาษี');
define('PRICE_INCLUDING_TAX','ราคารวมภาษีหรือไม่');
define('GLOBAL_SETTING_SUBMIT','ส่ง');
define('TURN_ON','เปิดใช้งาน');
define('TURN_OFF','ปิดใช้งาน');
define('GLOBAL_SETTING_NIGHTS','คืน');
define('DURATION_FOR_CUSTOMER_SELECTED_ROOM','หมายเหตุ: ระยะเวลาของการล็อกห้องระหว่างเซ็คเอ้าท์และเข้าสู่ช่องการชำระเงิน');
define('SMTP_HOST', 'SMTP Host');
define('SMTP_USERNAME', 'SMTP Username');
define('SMTP_PASSWORD', 'SMTP Password');
define('SMTP_ENCRYPTION', 'SMTP Encryption');
define('SMTP_PORT', 'SMTP Port');
define('PAYMENT_GATEWAY_SETTING','การตั้งค่า Payment');
define('ENABLED','เปิดใช้งาน');
define('GATEWAY','เกตเวย์');
define('PAYMENT_GATEWAY_TITLE','ชื่อ Payment gateway');
define('ACCOUNT_INFO','ข้อมูลบัญชี');
define('PAYPAL','PayPal');
define('ENTER_YOUR_PAYPAL_EMAIL','อีเมล Paypal');
define('MANUAL','ชำระด้วยตนเอง');
define('CREDIT_CARD','บัตรเครดิต');
define('UPDATE','การปรับปรุง');
define('EMAIL_CONTENT_SETTING','การตั้งค่าเนื้อหาของอีเมล์');
define('SELECT_EMAIL_TYPE','เลือกชนิดของอีเมล');
define('EMAIL_SUBJECT','เรื่องอีเมล');
define('EMAIL_CONTENT','เนื้อหาอีเมล');
define('EMAIL_UPDATE','การปรับปรุง');
define('SELECT_EMAIL_TYPE_CONTENT','เลือกชนิดของอีเมล');
define('LANGUAGE', 'ภาษา');
define('LANGAUGE_LIST','รายการภาษา');
define('LANGAUGE_TITLE','ชื่อภาษา');
define('ADD_NEW_LANGAUGE','เพิ่มภาษาใหม่');
define('LANGAUGE_LIST_EDIT','แก้ไข');
define('LANGAUGE_LIST_DELETE','ลบ');
define('LANGAUGE_CODE','รหัสภาษา');
define('LANGAUGE_FNAME','ชื่อไฟล์');
define('LANGAUGE_DEFAULT','ค่าเริ่มต้น');
define('LANGAUGE_ADD_EDIT','เพิ่ม/แก้ไข ภาษา');
define('HEADER_HOME','หน้าแรก');
define('HEADER_CHANGE_PASS','เปลี่ยนรหัสผ่าน');
define('HEADER_LOGOUT','ออกจากระบบ');
define('CP_CHANGE_PASS','เปลี่ยนรหัสผ่าน');
define('CP_OLD_PASS','รหัสผ่านเดิม');
define('CP_NEW_PASS','รหัสผ่านใหม่');
define('CP_CONF_PASS','ยืนยันรหัสผ่าน');
define('CP_BTN_CHANGE','การเปลี่ยนแปลง');
define('ADMN_MENU_LST','รายการเมนู Admin');
define('ADMN_1ST_LEVEL','เมนูระดับที่ 1');
define('ADMN_2ND_LEVEL','เมนูระดับที่ 2');
define('ADMN_ORDER','ใบสั่ง');
define('ADMN_BTN_1ST','เพิ่มเมนูระดับที่ 1');
define('ADMN_BTN_SUB','เพิ่มเมนูย่อย');
define('ADMN_TITLE','เพิ่ม/ปรับเปลี่ยนเมนู Admin');
define('ADMN_TITLE1','ชื่อเมนู');
define('ADMN_FNAME','ชื่อแฟ้ม');
define('ADMN_ORDER1','การจัดเรียงเมนู');
define('DT_SEARCH','ค้นหา');
define('DT_SINFO1','แสดง');
define('DT_SINFO2','ถึง');
define('DT_SINFO3','ของ');
define('DT_SINFO4','รายการ');
define('DT_INFOEMPTY','แสดง 0 ถึง 0 0 รายการ');
define('DT_EMPTYTABLE','ไม่มีในตารางข้อมูล');
define('DT_LMENU','แสดง');
define('DT_FIRST','หน้าแรก');
define('DT_PREV','ก่อนหน้านี้');
define('DT_NEXT','ถัดไป');
define('DT_LAST','ล่าสุด');
define('DT_ZERORECORD','ไม่พบระเบียนที่ตรงกัน');
define('DT_FILTER1','กรองจาก');
define('DT_FILTER2','รายการทั้งหมด');
define('VIEW_DYNAMIC_LIST','ดูรายการข้อเสนอพิเศษ');
define('DO_YOU_WANT_TO_DELETE_SELECTED_SPECIAL_OFFER','ลบข้อเสนอพิเศษ');
define('ADD_SPECIAL_OFFER','เพิ่มข้อเสนอพิเศษ');
define('START_DATE','วันเริ่มต้น');
define('END_DATE','วันสิ้นสุด');
define('PRICE_DEDUCTED','ราคาที่ถูกหัก');
define('MINIMUM_STAY','พักอย่างน้อย');
define('DELETE','ลบ');
define('EDIT','แก้ไข');
define('OFFER_NAME_TEXT','ชื่อข้อเสนอพิเศษ');
define('ROOM_TYPE_ALLREADY_ASSIGN_WITHIN_THIS_RANGE','ประเภทของห้องที่เลือกถูกกำหนดไว้แล้วในช่วงเวลาที่เลือก');
define('ADD_EDIT_SPECIAL_OFFER','เพิ่ม/แก้ไข ข้อเสนอพิเศษ');
define('MINIMUM_STAY_OPTIONAL','พักอย่างน้อย');
define('NIGHTS','คืน');
define('OF_ROOM_PRICE_TEXT','ราคาห้องพัก');
define('ALL_ROOM_TYPE','ห้องพักทุกประเภท');
define('NA_TEXT','ไม่พบ');
define('NIGHTS_TEXT_SPECIAL','คืน');
define('LEAVE_BLANK_IF_NO_MINIMUM_NIGHT_RESTRICTION','ปล่อยว่างไว้หากไม่มีข้อจำกัดขั้นต่ำคืน');
define('SPECIAL_SUBMIT','ส่ง');
define('HOTEL_PHOTO_GALLERY_TEXT','รูปภาพโรงแรม');
define('SELECT_A_CATEGORY_LIST','เลือกประเภท');
define('ADD_PHOTO_TEXT','เพิ่มรูปภาพ');
define('PLEASE_SELECT_A_ROOM_TYPE_ALERT','กรุณาเลือกประเภทห้องพัก');
define('SORRY_NO_RESULT_FOUND_ALERT','ขออภัย ไม่พบผลลัพธ์ ');
define('ADD_GALLERY_TEXT','เพิ่มแกลเลอรี');
define('IMAGE_TEXT','รูปภาพ');
define('PHOTO_SUBMIT','ส่ง');
define('CURRENCY_LIST','รายการสกุลเงิน');
define('CURRENCY_ADD_EDIT','เพิ่ม/แก้ไข สกุลเงิน');
define('CURRENCY_CODE_LIST','รหัสสกุลเงิน');
define('CURRENCY_SYMBOL_LIST','สัญลักษณ์สกุลเงิน');
define('EXCHANGE_RATE','อัตราแลกเปลี่ยน');
define('DEFAULT_CURRENCY','ค่าเริ่มต้น');
define('ADD_NEW_CURRENCY',' เพิ่มสกุลเงินใหม่');
define('EDIT_CURRENCY','แก้ไข');
define('DELETE_CURRENCY','ลบ');
define('NOTE_EXCHANGE_RATE_WILL_BE_AUTO_GENERATED','หมายเหตุ: อัตราแลกเปลี่ยนจะถูกสร้างขึ้นโดยอัตโนมัติ');
define('WEBSITE', 'หน้าเว็บไซต์');

define('USERNAME', 'ชื่อผู้ใช้งาน');
define('FIRSTNAME', 'ชื่อ');
define('LASTNAME', 'นามสกุล');
define('NAME', 'ชื่อ');
define('EMAIL', 'อีเมล');
define('PASSWORD', 'รหัสผ่าน');
define('ROLE', 'บทบาท');
define('CREATE_USER', 'สร้างผู้ใช้งาน');
define('CREATE_NEW_USER', 'สร้างผู้ใช้งานใหม่');
define('EDIT_USER', 'แก้ไขผู้ใช้งาน');
define('DELETE_USER', 'ลบผู้ใช้งาน');
define('DO_YOU_WANT_TO_DELETE', 'คุณต้องการลบผู้ใช้งานคนนี้หรือไม่');
define('LAST_LOGIN', 'เข้าสู่ระบบครั้งล่าสุด');
define('USER_LIST', 'รายชื่อผู้ใช้งาน');
define('ADD_NEW_USER', 'เพิ่มผู้ใช้งานใหม่');

define('USER_ALREADY_EXISTS', 'ชื่อผู้ใช้งานนี้มีอยู่แล้ว');
define('EMAIL_ALREADY_EXISTS', 'อีเมลนี้มีอยู่แล้ว');

define('INCLUDE_SERVICE_CHARGE', 'รวมค่าบริการในราคา ?');
define('SERVICE_CHARGE', 'ค่าบริการ');
define('INCLUDE_LOCAL_TAX', 'รวมค่าภาษีท้องถิ่นในราคา ?');
define('LOCAL_TAX', 'ภาษีท้องถิ่น');

define('SHOW_IN_HOMEPAGE', 'แสดงในหน้าแรก');
define('SETTING_LOADING', 'กำลังกำหนดรูปให้แสดงในหน้าแรก');

define('EXPORT_AS_PDF', 'ส่งออกเป็น PDF');
define('CUSTOMERLOOKUP_BOOKING_COUNT','จำนวนการจอง');

define('EXPORT_AS_CSV', 'ส่งออกเป็นไฟล์ CSV');
?>
