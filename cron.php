<?php
include("includes/db.conn.php");
include("includes/conf.class.php");
$bsiCore->exchange_rate_update(0);

$log = '[' . date('Y-m-d H:i:s') . '] - update exchange rate' . "\n";

file_put_contents(__DIR__ . '/logs/update-exchange-rate.log', $log, FILE_APPEND);
?>
