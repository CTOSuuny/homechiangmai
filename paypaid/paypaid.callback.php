<?php
session_start();

if (empty($_REQUEST)) {
    die;
}

include '../includes/db.conn.php';
include '../includes/conf.class.php';
include '../includes/mail.class.php';
include '../vendor/autoload.php';
include '../includes/smtp-mail.class.php';

file_put_contents('paypaid.log', json_encode($_REQUEST, JSON_PRETTY_PRINT) . "\n", FILE_APPEND);

$bsiMail = new bsiMail();
$emailContent = $bsiMail->loadEmailContent();

$bank = [
    1 => 'Siam Commercial Bank (SCB)',
    2 => 'Krung Thai Bank (KTB)',
    3 => 'Bank of Ayudhya (BAY)',
    4 => 'Bangkok Bank (BBL)',
    12 => 'Thanachart Bank (TBank)'
];

//success = 1
//cancel = 3
$successCode = 1;

if ($_REQUEST['Status'] == $successCode) {

    $mysqli->query("UPDATE bsi_bookings SET payment_success = true WHERE booking_id='" . $_REQUEST['RefTransactionID'] . "'");

    $ppsql = $mysqli->query("SELECT client_name, client_email, invoice FROM bsi_invoice WHERE booking_id='" . $_REQUEST['RefTransactionID'] . "'");
    $invoiceROWS = $ppsql->fetch_assoc();
    $mysqli->query("UPDATE bsi_clients SET existing_client = 1 WHERE email='" . $invoiceROWS['client_email'] . "'");

    $invoiceHTML = $invoiceROWS['invoice'];
    $invoiceHTML .= '<p style="background-color: #fff; display:inline-block; color: green; padding: .5rem;">Paid with PayPaid via ' . $bank[$_REQUEST['BankID']] . '</p>';

    $mysqli->query("UPDATE bsi_invoice SET invoice = '$invoiceHTML' WHERE booking_id='" . $_REQUEST['RefTransactionID'] . "'");

    $emailBody = 'Dear ' . $invoiceROWS['client_name'] . ',<br><br>';
    $emailBody .= html_entity_decode($emailContent['body']) . '<br><br>';
    $emailBody .= $invoiceHTML;
    $emailBody .= ',<br>' . $bsiCore->config['conf_hotel_name'] . '<br>' . $bsiCore->config['conf_hotel_phone'];

    $smtpMail = new SMTPMail();

    $smtpConfig = [
        'smtp_host' => $bsiCore->config['conf_smtp_host'],
        'smtp_username' => $bsiCore->config['conf_smtp_username'],
        'smtp_password' => $bsiCore->config['conf_smtp_password'],
        'smtp_port' => $bsiCore->config['conf_smtp_port'],
        'smtp_encryption' => $bsiCore->config['conf_smtp_encryption']
    ];

    $returnMsg = $smtpMail->setSubject($emailContent['subject'])
        ->setSystemConfig($smtpConfig)
        ->sender([$bsiCore->config['conf_hotel_email']])
        ->receiver([$invoiceROWS['client_email']])
        ->setBody($emailBody)
        ->send();

    $notifyEmailSubject = 'Booking no.' . $p->ipn_data['invoice'] . ' - Notification of Room Booking by ' . $invoiceROWS['client_name'];

    $returnMsg = $smtpMail->setSubject($notifyEmailSubject)
        ->setSystemConfig($smtpConfig)
        ->sender([$bsiCore->config['conf_hotel_email']])
        ->receiver([$bsiCore->config['conf_notification_email']])
        ->setBody($invoiceHTML)
        ->send();
}