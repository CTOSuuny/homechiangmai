<?php
session_start();
include("includes/db.conn.php");
include("includes/conf.class.php");
include("includes/search.class.php");
include('includes/csrf.class.php');
include("language.php");
$bsisearch = new bsiSearch();
$bsiCore->clearExpiredBookings();
$pos2 = strpos($_SERVER['HTTP_REFERER'],$_SERVER['SERVER_NAME']);

//if($bsisearch->nightCount==0 and !$pos2){
//	header('Location: booking-failure.php?error_code=9');
//}

//if (!CSRF::check()) {
//	abort(500);
//}

?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $bsiCore->config['conf_hotel_name']; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans|Playfair+Display" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo asset("css/datepicker.css") ?>" type="text/css" media="screen"/>
		<link rel="stylesheet" href="<?php echo asset('css/datepicker1.css') ?>" type="text/css" media="screen"/>
        <link rel="stylesheet" type="text/css" href="<?php echo asset("css/jquery.lightbox-0.5.css") ?>" media="screen" />
        <link rel="stylesheet" href="<?php echo asset('css/colorbox.css') ?>" />
        <link rel="stylesheet" href="<?php echo asset('css/flexslider.css') ?>" type="text/css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider.css') ?>" />
		<!-- <link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-zoom-slider-customize.css') ?>"> -->
		<link rel="stylesheet" type="text/css" href="<?php echo asset('css/css-customize.css') ?>">

	    <link rel="stylesheet" href="css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
	    <link rel="stylesheet" href="css/screen.css?v=1.2" type="text/css" media="screen" charset="utf-8" />

	    <link rel="stylesheet" type="text/css" href="css/new_style.css">
	    <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet'>
	    <link href="https://fonts.googleapis.com/css?family=Prompt:200" rel="stylesheet">
	    <link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet'>
    	<link rel="stylesheet" href="css/style.css">
		<?php include('modules/header-favicon.php'); ?>
	    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    </head>
    <body>
		<div class="container-fluid" style="margin-top: 120px;" >
			<?php
				include("modules/inner-modules.php");
				include("modules/book-search.php");
				include("modules/footer.php");
			?>
			<div id="btn-to-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
		</div>
    </body>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo asset('js/modernizr-2.6.2.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo asset('js/jquery-zoomslider.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo asset('js/bootstrap-datepicker.js') ?>"></script>
	<script type="text/javascript" src="<?php echo asset('js/hotelvalidation.js') ?>"></script>
	<script type="text/javascript" src="<?php echo asset('js/jquery.lightbox-0.5.min.js') ?>"></script>
	<script src="<?php echo asset('js/jquery.colorbox.js') ?>"></script>
	<script defer src="<?php echo asset('js/jquery.flexslider.js') ?>"></script>
	<script defer src="<?php echo asset('js/helpers/btn-scroll.js') ?>"></script>
	<script defer src="<?php echo asset('js/helpers/booking-form.js') ?>"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript">
		$(window).load(function() {

			$('.flexslider').flexslider({
				animation: "fade",
				controlNav: true,
				directionNav: true
			});
			var nowTemp = new Date();
			var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

			//Checkin and Checkout date
			var maxBookingDate = new Date();
			maxBookingDate = maxBookingDate.setDate(maxBookingDate.getDate() + <?php echo $bsiCore->config['conf_maximum_global_years'] ?>)
			maxBookingDate = new Date(maxBookingDate);

			var checkin = $('#check-in').datepicker({
				onRender: function(date) {
					return date.valueOf() < now.valueOf() || date.valueOf() > maxBookingDate ? 'disabled' : '';
				}
			}).on('changeDate', function(ev) {

				if (ev.date.valueOf() > checkout.date.valueOf()) {
					var newDate = new Date(ev.date)
					newDate.setDate(newDate.getDate() + <?php echo  $bsiCore->config['conf_min_night_booking']; ?>);
					checkout.setValue(newDate);
				}

				checkin.hide();
				$('#check-out')[0].focus();
			}).data('datepicker');

			var checkout = $('#check-out').datepicker({
				onRender: function(date) {
					var checkoutdt= parseInt(checkin.date.valueOf())+(60*60*24*1000*<?php echo  ($bsiCore->config['conf_min_night_booking']-1); ?>);

					return date.valueOf() <= checkoutdt || date.valueOf() > maxBookingDate ? 'disabled' : '';

				}
			}).on('changeDate', function(ev) {
				checkout.hide();
			}).data('datepicker');


			<?php
				if(!empty($_POST['room_type_id_index'])){
			?>
					$('html, body').animate({
					   scrollTop: $("#scroll-to-typeId-"+<?php echo $_POST['room_type_id_index']; ?>).offset().top
				   }, 1000);
			<?php
				}
			?>

			var bookingForm = new BookingForm();
			bookingForm.init();
		});
	</script>
	<script>
			function currency_change(ccode)
			{
				window.location.href = '<?php echo $_SERVER['PHP_SELF']; ?>?currency=' + ccode;
			}

	</script>
</html>
