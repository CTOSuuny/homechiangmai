-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 09, 2019 at 02:03 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `content_villa_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `bsi_cms`
--

CREATE TABLE `bsi_cms` (
  `id` int(11) NOT NULL,
  `type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `key_index` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bsi_cms`
--

INSERT INTO `bsi_cms` (`id`, `type`, `key_index`, `detail`, `language`) VALUES
(1, '1', 'Key_test', 'xxxxxx', '1'),
(2, '1', 'header_colume1', 'STAY AND ENJOY WITH', '1'),
(4, '1', 'subheader_colume1', 'The First Lifestyle Hotel In Chiang Mai', '1'),
(5, '1', 'content_colume1', 'Home Chiang Mai is the ideal of open-house hotel inspired by the variance magnificence of Chiang Mai culture and Lifestyle of surrounding young generation.', '1'),
(6, '1', 'header_colume2', 'Our Bedrooms', '1'),
(7, '1', 'content_colume2', 'Home Chiang Mai features 68 of the capitalâ€™s most spacious suites a uniquely Lifestyle minimal, Wabi Sabi. All room are outfitted with plus furnishings, Comfy beds and lofty design.Each suite is individually designed with the Famous artwork selected by our owner. Complimentary WiFi is available property-wide.', '1'),
(8, '1', 'header_colume3', 'Our Bars & Restaurants', '1'),
(9, '1', 'content_colume3', 'At Home Chiang Mai, guests are treated to a cultural experience like no other, with sublime service in a serene glass house sitting. Our diverse all day dining restaurant where guests may savor and create authentic Thai and international cuisine which serves homestyle meals as well as small plates set to live music in a jazz atmosphere.', '1'),
(10, '1', 'header_event', 'SEE ALL EVENTS', '1'),
(11, '1', 'subheader_event', 'Our events this month', '1'),
(12, '2', 'Header_colume1', 'About Us', '1'),
(13, '2', 'content_1', 'The first lifestyle hotel in Chiang Mai inspired by millennials along with the charm of Northern Thai Culture. We are not just offering accommodation for the night. We want to be more than that. It is about experiences. Itâ€™s about feeling a sense of place. Itâ€™s what travelers want more and more, and itâ€™s experiences that they canâ€™t get.', '1'),
(14, '2', 'content_2', 'We provide a place where guests can hang out alongside the locals and moisten themselves in the neighborhood with energetic, welcoming public spaces, and proving style doesnâ€™t need to be sacrificed for good value.', '1'),
(15, '2', 'content_3', 'We have a strong commitment to our community, to support and to prolong the fading away of the beauty of Thai culture.', '1'),
(16, '2', 'content_4', 'Home Chiang Mai Hotel is where you can feel, not like a tourist, but a traveler in that local, or feel like you are at home.', '1'),
(17, '2', 'content2_1', 'à¹‚à¸£à¸‡à¹à¸£à¸¡à¹„à¸¥à¸Ÿà¹Œà¸ªà¹„à¸•à¸¥à¹Œà¹à¸«à¹ˆà¸‡à¹à¸£à¸à¹ƒà¸™à¹€à¸Šà¸µà¸¢à¸‡à¹ƒà¸«à¸¡à¹ˆà¸—à¸µà¹ˆà¹„à¸”à¹‰à¸£à¸±à¸šà¹à¸£à¸‡à¸šà¸±à¸™à¸”à¸²à¸¥à¹ƒà¸ˆà¸ˆà¸²à¸à¸à¸¥à¸¸à¹ˆà¸¡à¸„à¸™Millennials à¸«à¸£à¸·à¸­ Generation Yà¸›à¸£à¸°à¸ªà¸¡à¸›à¸£à¸°à¸ªà¸²à¸™à¸à¸±à¸šà¸¡à¸™à¸•à¹Œà¹€à¸ªà¸™à¹ˆà¸«à¹Œà¸‚à¸­à¸‡à¸§à¸±à¸’à¸™à¸˜à¸£à¸£à¸¡à¹„à¸—à¸¢à¸ à¸²à¸„à¹€à¸«à¸™à¸·à¸­ à¹€à¸£à¸²à¹„à¸¡à¹ˆà¹€à¸žà¸µà¸¢à¸‡à¹à¸•à¹ˆà¹€à¸ªà¸™à¸­à¸—à¸µà¹ˆà¸žà¸±à¸à¸ªà¸³à¸«à¸£à¸±à¸šà¸™à¸­à¸™à¹€à¸—à¹ˆà¸²à¸™à¸±à¹‰à¸™ à¹€à¸£à¸²à¸•à¹‰à¸­à¸‡à¸à¸²à¸£à¹€à¸›à¹‡à¸™à¸¡à¸²à¸à¸à¸§à¹ˆà¸²à¸™à¸±à¹‰à¸™ à¸¡à¸±à¸™à¸„à¸·à¸­à¹€à¸£à¸·à¹ˆà¸­à¸‡à¸‚à¸­à¸‡à¸à¸²à¸£à¸ªà¸£à¹‰à¸²à¸‡à¸›à¸£à¸°à¸ªà¸šà¸à¸²à¸£à¸“à¹Œ à¸¡à¸±à¸™à¸„à¸·à¸­à¸à¸²à¸£à¸ªà¸±à¸¡à¸œà¸±à¸ªà¹€à¸‚à¹‰à¸²à¸–à¸¶à¸‡à¸­à¸²à¸£à¸¡à¸“à¹Œà¸‚à¸­à¸‡à¸ªà¸–à¸²à¸™à¸—à¸µà¹ˆ à¸¡à¸±à¸™à¸„à¸·à¸­à¸ªà¸´à¹ˆà¸‡à¸—à¸µà¹ˆà¸™à¸±à¸à¸—à¹ˆà¸­à¸‡à¹€à¸—à¸µà¹ˆà¸¢à¸§à¸•à¹‰à¸­à¸‡à¸à¸²à¸£à¸¡à¸²à¸à¸‚à¸¶à¹‰à¸™ à¸¡à¸²à¸à¸à¸§à¹ˆà¸²à¸à¸²à¸£à¹ƒà¸Šà¹‰à¸«à¹‰à¸­à¸‡à¸žà¸±à¸à¹€à¸—à¹ˆà¸²à¸™à¸±à¹‰à¸™ à¹à¸¥à¸°à¸¡à¸±à¸™à¸„à¸·à¸­à¸›à¸£à¸°à¸ªà¸šà¸à¸²à¸£à¸“à¹Œà¸—à¸µà¹ˆà¹„à¸¡à¹ˆà¸ªà¸²à¸¡à¸²à¸£à¸–à¸«à¸²à¹„à¸”à¹‰à¹ƒà¸™à¹‚à¸£à¸‡à¹à¸£à¸¡à¸—à¸±à¹ˆà¸§à¹„à¸›', '1'),
(18, '2', 'content2_2', 'à¹€à¸£à¸²à¹€à¸›à¹‡à¸™à¸—à¸µà¹ˆà¸—à¸µà¹ˆà¸œà¸¹à¹‰à¹€à¸‚à¹‰à¸²à¸žà¸±à¸à¸ªà¸²à¸¡à¸²à¸£à¸–à¸—à¸³à¸à¸´à¸ˆà¸à¸£à¸£à¸¡à¸£à¹ˆà¸§à¸¡à¸à¸±à¸šà¸„à¸™à¹ƒà¸™à¸—à¹‰à¸­à¸‡à¸–à¸´à¹ˆà¸™à¹à¸¥à¸°à¸«à¸¥à¹ˆà¸­à¸«à¸¥à¸­à¸¡à¸•à¸±à¸§à¹€à¸­à¸‡à¹€à¸‚à¹‰à¸²à¹„à¸›à¸à¸±à¸šà¸žà¸·à¹‰à¸™à¸—à¸µà¹ˆà¹ƒà¸à¸¥à¹‰à¹€à¸„à¸µà¸¢à¸‡ à¸”à¹‰à¸§à¸¢à¸žà¸·à¹‰à¸™à¸—à¸µà¹ˆà¸ªà¸²à¸˜à¸²à¸£à¸“à¸°à¸‚à¸­à¸‡à¹‚à¸£à¸‡à¹à¸£à¸¡à¸—à¸µà¹ˆà¸¡à¸µà¸žà¸¥à¸±à¸‡à¹€à¸„à¸¥à¸·à¹ˆà¸­à¸™à¹„à¸«à¸§à¹à¸¥à¸°à¸¢à¸´à¸™à¸”à¸µà¸•à¹‰à¸­à¸™à¸£à¸±à¸šà¸­à¸¢à¸¹à¹ˆà¸•à¸¥à¸­à¸”à¹€à¸§à¸¥à¸² à¹à¸¥à¸°à¸”à¹‰à¸§à¸¢à¸§à¸´à¸–à¸µà¹€à¸žà¸·à¹ˆà¸­à¸à¸²à¸£à¹ƒà¸Šà¹‰à¸Šà¸µà¸§à¸´à¸•à¸—à¸µà¹ˆà¸”à¸µ à¸¡à¸µà¸„à¸¸à¸“à¸„à¹ˆà¸² à¹à¸¥à¸°à¸¡à¸µà¸„à¸§à¸²à¸¡à¸ªà¸¸à¸‚à¸¡à¸²à¸à¸‚à¸¶à¹‰à¸™', '1'),
(19, '2', 'content2_3', 'à¹€à¸£à¸²à¸¡à¸µà¸„à¸§à¸²à¸¡à¸¡à¸¸à¹ˆà¸‡à¸¡à¸±à¹ˆà¸™à¸­à¸¢à¹ˆà¸²à¸‡à¹à¸£à¸‡à¸à¸¥à¹‰à¸²à¸•à¹ˆà¸­à¸Šà¸¸à¸¡à¸Šà¸™à¸‚à¸­à¸‡à¹€à¸£à¸² à¹‚à¸”à¸¢à¸à¸²à¸£à¸ªà¸™à¸±à¸šà¸ªà¸™à¸¸à¸™à¸§à¸±à¸’à¸™à¸˜à¸£à¸£à¸¡à¸—à¹‰à¸­à¸‡à¸–à¸´à¹ˆà¸™à¹à¸¥à¸°à¸£à¸±à¸à¸©à¸²à¸„à¸§à¸²à¸¡à¸‡à¸”à¸‡à¸²à¸¡à¸‚à¸­à¸‡à¸§à¸±à¸’à¸™à¸˜à¸£à¸£à¸¡à¹„à¸—à¸¢à¸šà¸²à¸‡à¸ªà¸´à¹ˆà¸‡à¸—à¸µà¹ˆà¸à¸³à¸¥à¸±à¸‡à¹€à¸¥à¸·à¸­à¸™à¸«à¸²à¸¢à¹„à¸›à¹ƒà¸«à¹‰à¸­à¸¢à¸¹à¹ˆà¸™à¸²à¸™à¹€à¸—à¹ˆà¸²à¸™à¸²à¸™à¹€à¸—à¹ˆà¸²à¸—à¸µà¹ˆà¹€à¸£à¸²à¸ˆà¸°à¸—à¸³à¹„à¸”à¹‰', '1'),
(20, '2', 'content2_4', 'à¹‚à¸£à¸‡à¹à¸£à¸¡à¹‚à¸®à¸¡à¹€à¸Šà¸µà¸¢à¸‡à¹ƒà¸«à¸¡à¹ˆà¹€à¸›à¹‡à¸™à¸—à¸µà¹ˆà¸—à¸µà¹ˆà¸„à¸¸à¸“à¸£à¸¹à¹‰à¸ªà¸¶à¸à¹„à¸”à¹‰à¸§à¹ˆà¸² à¸„à¸¸à¸“à¹„à¸¡à¹ˆà¹€à¸«à¸¡à¸·à¸­à¸™à¸™à¸±à¸à¸—à¹ˆà¸­à¸‡à¹€à¸—à¸µà¹ˆà¸¢à¸§ à¹à¸•à¹ˆà¸à¸¥à¸±à¸šà¹€à¸›à¹‡à¸™à¸œà¸¹à¹‰à¹€à¸”à¸´à¸™à¸—à¸²à¸‡à¸«à¸²à¸›à¸£à¸°à¸ªà¸šà¸à¸²à¸£à¸“à¹Œà¹ƒà¸™à¸—à¹‰à¸­à¸‡à¸–à¸´à¹ˆà¸™à¸™à¸±à¹‰à¸™à¹†à¹€à¸›à¹‡à¸™à¸—à¸µà¹ˆà¸—à¸µà¹ˆà¹ƒà¸«à¹‰à¸„à¸¸à¸“à¸£à¸¹à¹‰à¸ªà¸¶à¸à¹€à¸«à¸¡à¸·à¸­à¸™à¸§à¹ˆà¸²à¸„à¸¸à¸“à¸­à¸¢à¸¹à¹ˆà¸—à¸µà¹ˆà¸šà¹‰à¸²à¸™à¸«à¸¥à¸±à¸‡à¸—à¸µà¹ˆ2 à¸‚à¸­à¸‡à¸„à¸¸à¸“', '1'),
(21, '2', 'be_footer', 'At Home Chiang Mai , our doors are always open.', '1'),
(22, '3', 'rt_1', 'Small', '1'),
(23, '3', 'rt_2', 'Medium', '1'),
(24, '3', 'rt_3', 'Large', '1'),
(25, '3', 'rt_4', 'ALMOST Large', '1'),
(26, '3', 'rt_5', 'EXTRA Large', '1'),
(27, '3', 'it_content', 'Home Chiang Mai features 68 of the capital\'s most spacious suites a uniquely Lifestyle Smallmal, Wabi-Sabi. All rooms are outfitted with plush furnishings, Comfy beds and lofty design. Each suite is individually designed with the Famous artwork selected by our owner. Complimentary WiFi is available property-wide.', '1'),
(28, '3', 'rt1_content', 'Comfort and cozy, with a queen bed. Cleverly designed to make the most of the space.', '1'),
(29, '3', 'rt2_content', 'A classic and a favorite. Fit two people without stepping on toes. Comes with twin beds or a king bed.', '1'),
(30, '3', 'rt3_content', 'A generous room with a king bed for all your living needs. Enjoy the agoda view along with the sunrise. A big round shaped bathtub for two. Perfect for a couple.', '1'),
(31, '3', 'rt4_content', 'Twin beds with window seats and plenty of space.', '1'),
(32, '3', 'rt5_content', 'Feel like home. Private and spacious with corner large outdoor balcony. A big round shaped bathtub for two. Perfect for a couple.', '1'),
(33, '5', 'Header_colume1', 'Our Bar & Restaurant', '1'),
(34, '5', 'content_colume1', 'At Home Chiang Mai, guests are treated to a cultural experience like no other, with sublime service in a serene glass house setting. Our diverse all day dining restaurant where guests may savour and create authentic Thai and international cuisine which serves homestyle meals as well as small plates set to live music in a jazzy atmosphere. The Home Lobby Bar & CafÃ© offers inhouse and surrounding guests refreshing, light snacks and beverages. Sit back in your spacious homie table and sip on sunset cocktails while taking in the unencumbered, private views over the majestic Home Chiang Mai Fish pond. Mealtimes at Home Chiang Mai are relaxed and informal, with a creative significance on our â€˜Home Memorableâ€™ and \r\nâ€˜Li festyleâ€™ concepts.', '1'),
(35, '6', 'Header_colume1', 'Work With Us', '1'),
(36, '6', 'content_colume1', 'We\'re looking for kind, hard-working people to join our team. Email us your resume and cover letter. F & B Manager', '1'),
(37, '6', 'content_1', 'Coming soon', '1'),
(38, '7', 'Header_menu1', 'CALENDAR', '1'),
(39, '7', 'Header_menu2', 'EVENTS', '1'),
(40, '8', 'Header_1', 'Address...', '1'),
(41, '8', 'content_1', '3 Rasameechan Alle, Sermsuk Rd, Chang Phueak, 50300 Chiang Mai, Thailand', '1'),
(42, '8', 'Header_2', 'Home Location', '1'),
(43, '8', 'content_2', 'Home Chiang Mai Hotel 3\r\nRasemeechan Alle, Sermsuk Rd.\r\nChang Phueak, Chiang Mai, Thailand 50300\r\nRESERVATIONS Phone:+66(0)53-218-856\r\ngsa@homechiangmaihotel.com\r\nFax:+66(0)53 218 856\r\nGENERAL INFORMATION :\r\ngm@homechiangmaihotel.com\r\nwww.homechiangmaihotal.com', '1'),
(44, '8', 'contact_header', 'MAIL ENQUIRIES', '1'),
(45, '8', 'contact_1', 'First name *', '1'),
(46, '8', 'contact_2', 'Surnname *', '1'),
(47, '8', 'contact_3', 'Telephone *', '1'),
(48, '8', 'contact_4', 'Email *', '1'),
(49, '8', 'contact_5', 'Enquiry Type *', '1'),
(50, '8', 'contact_6', 'How did you hear about us *', '1'),
(51, '8', 'contact_7', 'Message *', '1'),
(52, '1', 'button_1', 'More About Us', '1'),
(53, '1', 'button_2', 'See the rooms', '1'),
(54, '1', 'button_3', 'See all', '1'),
(55, '2', 'scroller', 'Please scroll down', '1'),
(56, '3', 'rs', 'Room size', '1'),
(57, '3', 'ms', 'Mattress size', '1'),
(58, '3', 'rc', 'Room capacity', '1'),
(59, '6', 'scroller', 'Please scroll down', '1'),
(60, '8', 'bh', 'Back to Homepage ', '1'),
(61, '0', 'home', 'Home', '1'),
(62, '0', 'about', 'About Us', '1'),
(63, '0', 'room', 'Rooms', '1'),
(64, '0', 'gallery', 'Gallery', '1'),
(65, '0', 'bar', 'Bars & Restaurants', '1'),
(66, '0', 'work', 'Work With Us', '1'),
(67, '0', 'event', 'Event', '1'),
(68, '0', 'contact', 'Contact Us', '1'),
(69, '0', 'book', 'BOOK A ROOM', '1'),
(70, '0', 'res', 'Reservations', '1'),
(71, '0', 'phone', '+ 66 53 218 856', '1'),
(72, '0', 'connect', 'Connect With Us', '1'),
(73, '0', 'follow', 'We are socialized. Follow us', '1'),
(74, '0', 'fb_link', 'https://www.facebook.com/HomeChiangMaiHotel/', '1'),
(75, '0', 'ins_link', 'https://www.instagram.com/homechiangmaihotel/', '1'),
(76, '0', 'connect', 'Connect With Us', '1'),
(77, '0', 'map_link', 'https://www.google.com/maps/place/\'@homechiangmai\'/@18.8029473,98.9800778,18z/data=!4m5!3m4!1s0x30da3a9296803ec7:0xada6e29a86472be!8m2!3d18.8031711!4d98.9807778?hl=en', '1'),
(78, '0', 'addr', '3 Rasemeechan Alle, Sermsuk Rd. <br>Chang Phueak, Chiang Mai, Thailand 50300</a><br>\r\n              Phone:+66(0)53 218 856<br>\r\n              Email:gsa@homechiangmaihotel.com<br>\r\n              Fax:+66(0)53 218 856<br>\r\nWebsite: www.homechiangmaihotel.com', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bsi_cms`
--
ALTER TABLE `bsi_cms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bsi_cms`
--
ALTER TABLE `bsi_cms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
