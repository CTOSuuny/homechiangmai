<?php

require __DIR__ . '/model.php';

class Access extends Model
{
    public $table = 'bsi_adminmenu';

    public $menu;

    /**
     * Get menu
     *
     * @return array
     */
    public function getMenu()
    {
        if (!empty($this->menu)) {
            return $this->menu;
        }

        $query = $this->db->query("SELECT * FROM {$this->table}");
        $this->menu = $query->fetch_all(MYSQLI_ASSOC);

        return $this->menu;
    }

    /**
     * Get allowed page
     * @param  string $role role
     *
     * @return array
     */
    public function getAllowedPage($role)
    {
        $pages = [];

        foreach ($this->getmenu() as $menu) {
            $roles = json_decode($menu['role'], true);

            if (empty($roles)) {
                continue;
            }

            if (in_array($role,$roles)) {
                $pages[] = $menu['url'];

                if ($menu['parent_id'] == 0) {

                    foreach ($this->getSubmenu($menu['id']) as $submenu) {
                        $pages[] = $submenu['url'];
                    }

                }
            }
        }

        return $pages;
    }

    /**
     * Get sub menu
     *
     * @param mixed $parentId parent id
     *
     * @return array
     */
    public function getSubmenu($parentId)
    {
        $submenu = [];

        foreach ($this->getMenu() as $menu) {
            if ($menu['parent_id'] == $parentId) {
                $submenu[] = $menu;
            }
        }

        return $submenu;
    }
}
