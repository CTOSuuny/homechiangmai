<?php

$_POST = array_map(function($post) {
    return strip_tags($post);
}, $_POST);

session_start();
include 'includes/db.conn.php';
include 'includes/csrf.class.php';

if (!CSRF::checkTokenx()) {
    abort('500-token-expires');
}
include 'includes/conf.class.php';

$sql = $mysqli->query('select * from bsi_language where `lang_default`=true');
$row_default_lang = $sql->fetch_assoc();

if (!empty($_COOKIE['language'])) {
    $row_default_lang['lang_file'] = map_lang($_COOKIE['language']) . '.php';
}

include 'languages/' . $row_default_lang['lang_file'];

$pos2 = strpos($_SERVER['HTTP_REFERER'], $_SERVER['SERVER_NAME']);
if (!$pos2) {
    header('Location: booking-failure.php?error_code=9');
}
include 'includes/mail.class.php';
include 'includes/process.class.php';
include 'vendor/autoload.php';
include 'includes/smtp-mail.class.php';
include 'paypaid/paypaid.class.php';
$bookprs = new BookingProcess();

if (strpos($bookprs->paymentGatewayCode, 'internet_bank') === 0) {
    processPayPaid();
}

if (strpos($bookprs->paymentGatewayCode, 'credit_card_paypaid') === 0) {
    processPayPaidCreditCard();
}

switch ($bookprs->paymentGatewayCode) {
    case 'poa':
        processPayOnArrival();
        break;

    case 'pp':
        processPayPal();
        break;

    case 'cc':
        processCreditCard();
        break;

    case 'an':
        processAuthorizeNet();
        break;

    case '2co':
        process2Checkout();
        break;

    case 'st':
        processStripe();
        break;

    default:
        processOther();
}
/* PAY ON ARIVAL: MANUAL PAYMENT */
function processPayOnArrival()
{
    global $bookprs;
    global $bsiCore;
    global $mysqli;
    $bsiMail     = new bsiMail();
    $emailContent=$bsiMail->loadEmailContent();
    $subject     = $emailContent['subject'];
    $mysqli->query('UPDATE bsi_bookings SET payment_success=true WHERE booking_id = ' . $bookprs->bookingId);
    $mysqli->query("UPDATE bsi_clients SET existing_client = 1 WHERE email = '" . $bookprs->clientEmail . "'");
    $emailBody  = 'Dear ' . $bookprs->clientName . ',<br><br>';
    $emailBody .= $emailContent['body'] . '<br><br>';
    $emailBody .= $bookprs->invoiceHtml;
    $emailBody .= '<br><br>' . $mysqli->real_escape_string(PP_REGARDS) . ',<br>' . $bsiCore->config['conf_hotel_name'] . '<br>' . $bsiCore->config['conf_hotel_phone'];
    $emailBody .= '<br><br><font style=\"color:#F00; font-size:10px;\">[ ' . $mysqli->real_escape_string(PP_CARRY) . ' ]</font>';

    $smtpMail = new SMTPMail();
    $smtpConfig = [
        'smtp_host' => $bsiCore->config['conf_smtp_host'],
        'smtp_username' => $bsiCore->config['conf_smtp_username'],
        'smtp_password' => $bsiCore->config['conf_smtp_password'],
        'smtp_port' => $bsiCore->config['conf_smtp_port'],
        'smtp_encryption' => $bsiCore->config['conf_smtp_encryption']
    ];

    $returnMsg = $smtpMail->setSubject($subject)
        ->setSystemConfig($smtpConfig)
        ->sender([$bsiCore->config['conf_hotel_email']])
        ->receiver([$bookprs->clientEmail])
        ->setBody($emailBody)
        ->send();

    if ($returnMsg) {
        $notifyEmailSubject = 'Booking no.' . $bookprs->bookingId . ' - Notification of Room Booking by ' . $bookprs->clientName;

        $smtpMail->setSubject($notifyEmailSubject)
            ->setSystemConfig($smtpConfig)
            ->sender([$bsiCore->config['conf_hotel_email']])
            ->receiver([$bsiCore->config['conf_notification_email'], ENV::get('WEBMASTER_EMAIL')])
            ->setBody($bookprs->invoiceHtml)
            ->send();

        header('Location: booking-confirm.php?success_code=1');
        die;
    } else {
        header('Location: booking-failure.php?error_code=25');
        die;
    }
    //header('Location: booking-confirm.php?success_code=1');
}

function processPayPaid()
{
    global $bookprs;
    global $bsiCore;
    
    $merchantId = ENV::get('PAYPAID_MERCHANT_ID');
    $websiteCode = ENV::get('PAYPAID_WEBSITE_CODE');
    $secretKey = ENV::get('PAYPAID_SECRET_KEY');
    $refTransactionId = $bookprs->bookingId;
    $price = $bookprs->totalPaymentAmount;

    $description = 'Order ' . $bookprs->bookingId ;
    $address = implode(' ', [$_POST['str_addr'], $_POST['city'], $_POST['state'], $_POST['zipcode'], $_POST['country']]);

    $paypaid = new PayPaid();
    $paypaid->setCredential([
        'merchant_id' => $merchantId,
        'website_code' => $websiteCode,
        'secret_key' => $secretKey
    ])
    ->setHost(ENV::get('PAYPAID_HOST'))
    ->setBank(str_replace('internet_bank_', '', $_POST['payment_type']))
    ->buildForm([
        'id' => $refTransactionId,
        'name' => $bookprs->clientName,
        'address' => $address,
        'description' => $description,
        'price_per_unit' => $price,
        'quantity' => 1,
        'amount' => $price,
        'total_amount' => $price,
        'delivery_fee' => 0,
        'net_total' => $price,
        'phone' => $_POST['phone']
    ]);
    die();
}

function processPayPaidCreditCard()
{
    global $bookprs;
    global $bsiCore;
    
    $merchantId = ENV::get('PAYPAID_MERCHANT_ID');
    $websiteCode = ENV::get('PAYPAID_WEBSITE_CODE');
    $secretKey = ENV::get('PAYPAID_SECRET_KEY');
    $refTransactionId = $bookprs->bookingId;
    $price = $bookprs->totalPaymentAmount;

    $description = 'Order ' . $bookprs->bookingId ;
    $address = implode(' ', [$_POST['str_addr'], $_POST['city'], $_POST['state'], $_POST['zipcode'], $_POST['country']]);

    $paypaid = new PayPaid();
    $paypaid->setCredential([
        'merchant_id' => $merchantId,
        'website_code' => $websiteCode,
        'secret_key' => $secretKey
    ])
    ->setHost(ENV::get('PAYPAID_HOST'))
    ->setBank('tbank')
    ->setService(2) //Service ID 2 = Credit Card
    ->buildForm([
        'id' => $refTransactionId,
        'name' => $bookprs->clientName,
        'address' => $address,
        'description' => $description,
        'price_per_unit' => $price,
        'quantity' => 1,
        'amount' => $price,
        'total_amount' => $price,
        'delivery_fee' => 0,
        'net_total' => $price,
        'phone' => $_POST['phone']
    ]);
    die();
}

/* PAYPAL PAYMENT */
function processPayPal()
{
    global $bookprs;
    global $bsiCore;
    echo '<script language="JavaScript">';
    echo "document.write('<form action=\"paypal.php\" method=\"post\" name=\"formpaypal\">');";
    echo "document.write('<input type=\"hidden\" name=\"amount\"  value=\"" . (('1' == $bsiCore->config['conf_payment_currency']) ? $bsiCore->getExchangemoney($bookprs->totalPaymentAmount, $_SESSION['sv_currency']) : number_format($bookprs->totalPaymentAmount, 2)) . "\">');";
    echo "document.write('<input type=\"hidden\" name=\"invoice\"  value=\"" . $bookprs->bookingId . "\">');";
    echo "document.write('</form>');";
    echo 'setTimeout("document.formpaypal.submit()",500);';
    echo '</script>';
}
/* CREDIT CARD PAYMENT */
function processCreditCard()
{
    global $bookprs;
    global $bsiCore;
    $paymentAmount = number_format($bookprs->totalPaymentAmount, 2, '.', '');

    echo '<script language="javascript">';
    echo "document.write('<form action=\"offlinecc-payment.php\" method=\"post\" name=\"form2checkout\">');";
    echo "document.write('<input type=\"hidden\" name=\"x_invoice_num\" value=\"" . $bookprs->bookingId . "\"/>');";
    echo "document.write('<input type=\"hidden\" name=\"total\" value=\"" . (('1' == $bsiCore->config['conf_payment_currency']) ? $bsiCore->getExchangemoney($paymentAmount, $_SESSION['sv_currency']) : $paymentAmount) . "\">');";
    echo "document.write('</form>');";
    echo 'setTimeout("document.form2checkout.submit()",500);';
    echo '</script>';
}

function processAuthorizeNet()
{
    global $bookprs;
    global $bsiCore;
    $_SESSION['paymentAmount']=$bookprs->totalPaymentAmount;
    $_SESSION['bookingId']    =$bookprs->bookingId;
    header('Location: an_direct_post.php');
    die;
}

/* PAYPAL PAYMENT */
function process2Checkout()
{
    global $bookprs;
    global $bsiCore;
    $paymentGatewayDetails    = $bsiCore->loadPaymentGateways();
    $_SESSION['paymentAmount']=$bookprs->totalPaymentAmount;
    echo '<script language="JavaScript">';
    echo "document.write('<form action=\"https://www.2checkout.com/checkout/spurchase\" method=\"post\" name=\"twocopayment\">');";
    echo "document.write('<input type=\"hidden\" name=\"sid\" value=\"" . $paymentGatewayDetails['2co']['account'] . "\" >');";
    echo "document.write('<input type=\"hidden\" name=\"mode\" value=\"2CO\" >');";
    //echo "document.write('<input type=\"hidden\" name=\"demo\" value=\"N\"/>');";
    echo "document.write('<input type=\"hidden\" name=\"li_0_type\" value=\"product\" >');";
    echo "document.write('<input type=\"hidden\" name=\"li_0_name\" value=\"Booking : " . $bsiCore->config['conf_hotel_name'] . "\" >');";
    echo "document.write('<input type=\"hidden\" name=\"li_0_price\" value=\"" . $bookprs->totalPaymentAmount . "\" >');";
    echo "document.write('<input type=\"hidden\" name=\"invoice\"  value=\"" . $bookprs->bookingId . "\">');";
    echo "document.write('</form>');";
    echo 'setTimeout("document.twocopayment.submit()",500);';
    echo '</script>';
}

function processStripe()
{
    global $bookprs;
    global $bsiCore;
    $_SESSION['clientEmail']  =$bookprs->clientEmail;
    $_SESSION['paymentAmount']=$bookprs->totalPaymentAmount;
    $_SESSION['bookingId']    =$bookprs->bookingId;
    header('Location: stripe-processor.php');
    die;
}
/* OTHER PAYMENT */
function processOther()
{
    /* not implemented yet */
    header('Location: booking-failure.php?error_code=22');
    die;
}
