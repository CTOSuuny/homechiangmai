var RoomTypeForm = function() {

    this.el = {
        switcher: '#lang-switcher',
        container: '.multilang-textarea-container',
        multilangTextarea: '.multilang-textarea'
    }

}

RoomTypeForm.prototype = {
    constructor: RoomTypeForm,

    init: function() {
        this.bindEvent();
        this.initEditor();
    },

    bindEvent: function() {
        $(this.el.switcher).bind('change', this.switchLanguage.bind(this));
    },

    switchLanguage: function(e) {
        var lang = $(e.currentTarget).val();

        $(this.el.container).addClass('hide');
        $(this.el.container + '[data-language="' + lang + '"]').removeClass('hide');
    },

    setToTargetTextarea: function(e) {
        var target = $(e.currentTarget).data('target'),
            group = $(e.currentTarget).data('group');

        $(target).val(JSON.stringify(this.getContent(group)));
    },

    getContent: function(group) {
        var result = {};
        group = group || '';

        $(this.el.multilangTextarea + '[data-group="' + group + '"]').each(function() {

            var lang = $(this).data('language');
            result[lang] = $(this).val();
        });

        return result;
    },

    initEditor: function() {

        LANGS.forEach(function(lang, index) {
            CKEDITOR.replace('description[' + lang['lang_code'] + ']');
            CKEDITOR.replace('facilities[' + lang['lang_code'] + ']');
        });

    }
}
