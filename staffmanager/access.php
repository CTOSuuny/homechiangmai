<?php
session_start();
include '../includes/constant/user.php';
// Sends the user to the login-page if not logged in
if(!isset($_SESSION['cppassBSI'])) {
   header('Location:index.php?msg=requires_login');
   exit;
}

$dashboard = 'admin-home.php';

if ($_SESSION['cprole'] !== USER_TYPE['admin']) {

    $pageRelation = include 'page_relation.php';

    $allowedPages = json_decode($_SESSION['cpaccess']);
    $allowedPages = array_merge($allowedPages, [$dashboard]);
    $extPage = [];

    foreach ($allowedPages as $page) {
        if (!empty($pageRelation[$page])) {
            foreach ($pageRelation[$page] as $subpage) {
                $extPage[] = $subpage;
            }
        }
    }

    $allowedPages = array_merge($allowedPages, $extPage);

    $scriptName = explode('/', $_SERVER['SCRIPT_NAME']);
    $scriptName = $scriptName[count($scriptName) - 1];

    if (!in_array($scriptName, $allowedPages)) {
        header('location: ' . $dashboard);
    }
}

?>
