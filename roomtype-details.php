<?php
include("includes/db.conn.php");
include('includes/conf.class.php');
include('language.php');
$sql=$mysqli->query("SELECT * FROM `bsi_roomtype` where roomtype_ID=".$bsiCore->ClearInput($_GET['tid']));
$row=$sql->fetch_assoc();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Playfair+Display" rel="stylesheet">
    <style>
        .roomtype-title {
            font-family: 'Playfair Display', serif;
            font-size: 20px;
        }
    </style>
    <title>Room Type Details</title>
</head>

<body style="font-family:Arial, Helvetica, sans-serif;">
    <span class="roomtype-title" style="width:15px; font-weight:bold;"><?php echo trans('FACILITIES_TEXT', true) . ' : ' .$row['type_name']; ?></span>
    <p style="margin: 1em 0;"></p>
    <?php
        $language = !empty($_COOKIE['language']) ? $_COOKIE['language'] : $defaultLanguage["lang_code"];
        echo json_decode($row['facilities'], true)[$language];
    ?>
</body>
</html>
