<?php


if( !empty($_POST['sender']) &&
    !empty($_POST['sender_email']) &&
    !empty($_POST['subject']) &&
    !empty($_POST['emailBody'])
){

    $sender = $_POST['sender'];
    $senderEmail = $_POST['sender_email'];
    $subject = $_POST['subject'];
    $emailBody = $_POST['emailBody'];
    $emailBody .= "<p>sender name : $sender</p><p>email / tel sender : $senderEmail</p>";
    $smtpMail = new SMTPMail();
    $smtpConfig = [
        'smtp_host' => $bsiCore->config['conf_smtp_host'],
        'smtp_username' => $bsiCore->config['conf_smtp_username'],
        'smtp_password' => $bsiCore->config['conf_smtp_password'],
        'smtp_port' => $bsiCore->config['conf_smtp_port'],
        'smtp_encryption' => $bsiCore->config['conf_smtp_encryption']
    ];

    $smtpMail->setSubject($subject)
        ->setSystemConfig($smtpConfig)
        ->sender([$bsiCore->config['conf_hotel_email']])
        ->receiver([$bsiCore->config['conf_hotel_email']])
        ->setBody($emailBody)
        ->send();
}

?>
<div class="row show-contact-dialog room-modules">
    <div class="col-sm-4">
        <h2><?php trans('CONTACT_INFO') ?></h2>
        <br>
            <p><?php echo $bsiCore->config["conf_hotel_name"]; ?></p>
            <p><?php trans('ADDRESS') ?> :
                <?php echo $bsiCore->config["conf_hotel_streetaddr"]; ?>
                <?php echo $bsiCore->config["conf_hotel_city"]; ?>
                <?php echo $bsiCore->config["conf_hotel_state"]; ?>
                <?php echo $bsiCore->config["conf_hotel_country"]; ?>
            </p>
            <p>
                <?php trans('ZIP') ?> : <?php echo $bsiCore->config["conf_hotel_zipcode"]; ?>
            </p>
            <p><?php trans('TEL') ?> : <?php echo $bsiCore->config["conf_hotel_phone"]; ?></p>
            <p><?php trans('FAX_TEXT') ?> : <?php echo $bsiCore->config["conf_hotel_fax"]; ?></p>
            <p><?php trans('EMAIL_TEXT') ?> : <a href="#"><?php echo $bsiCore->config["conf_hotel_email"]; ?></a></p>
            <p><a href="/">www.contentvillachiangmai.com</a></p><p>
    </div>
    <div class="col-sm-8">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3778.772436104444!2d98.93238928623747!3d18.7190036160833!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30da3130cddfd935%3A0x938b88a2639d8eb5!2sContent+Villa+Chiang+Mai!5e0!3m2!1sth!2sth!4v1517370705132" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
<div class="row show-contact-dialog room-modules">
    <form class="form-horizontal" action="" method="post">
        <div class="form-group">
            <div class="col-sm-6">
                <input type="text" name="sender" class="form-control" required placeholder="<?php trans('YOUR_NAME') ?>">
            </div>
            <div class="col-sm-6">
                <input type="text" name="sender_email" class="form-control" required placeholder="<?php trans('EMAIL_OR_PHONE') ?>">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <input type="text" name="subject" class="form-control" required placeholder="<?php trans('SUBJECT') ?>">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <textarea type="text" name="emailBody" class="form-control" required placeholder="<?php trans('CONTENT') ?>" rows="10"></textarea>
            </div>
        </div>
        <div class="form-group div-contact" align="right">
            <button type="submit" class="btn btn-default btn-book"><?php trans('SEND') ?></button>
        </div>
    </form>
</div>
