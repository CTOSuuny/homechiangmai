<?php
    include("includes/db.conn.php");
    include("includes/conf.class.php");
    // include("includes/room.class.php");
    include('includes/csrf.class.php');
    // include('includes/mail.class.php');
    // include('includes/smtp-mail.class.php');

    $bsiCore->exchange_rate_update();
?>
<!DOCTYPE html>
<html>
<head>

	<meta charset='utf-8' />
	<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible' />
	<title>Home chiang mai Hotel</title>
	
	<meta content='Serene & Private Retreat - Lanna Colonial Boutique Hotel in the North of Thailand' name='description' />
	
	
	<meta name="distribution" content="global" />
	<meta name="language" content="en" />
	<meta content='width=device-width, initial-scale=1.0' name='viewport' />

	<!-- Inline CSS based on choices in "Settings" tab -->
	<style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: white !important;} .asteriskField{color: red;}</style>

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="icon" href="favicon.ico" sizes="16x16 32x32" type="image/png">

	<link rel="stylesheet" type="text/css" href="css/new_style.css">
	<link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css?family=Prompt:200" rel="stylesheet">
    <link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
        <link rel="stylesheet" href="css/style.css">



	<style type="text/css">
		body {
            font-family: Prompt;
            color: #333;
            background-color: inherit; 
            font-size: 20px;
        }
        
        #from,#to{
            text-align: center;
        }

        @media only screen and (max-width: 800px) {
            .booknow .inner .inputform .form .detail-inner .content{
                width: 100%;
            }
        }
        hr{
            margin-top: inherit;
            margin-bottom: inherit; 
            display: block;
            unicode-bidi: isolate;
            -webkit-margin-before: 0.5em;
            -webkit-margin-after: 0.5em;
            -webkit-margin-start: auto;
            -webkit-margin-end: auto;
            overflow: hidden;
            border-style: inset;
            border-width: 1px;
        }
        a:hover{
            color: white; 
            text-decoration: none; 
        }
	</style>
</head>
<body>

  <!-- Sidebar -->
  <div class="w3-sidebar w3-bar-block w3-animate-left" style="display:none;z-index: 4;" id="mySidebar">
    <a href="#" class="w3-bar-item w3-button" style="margin-top: 100px;"></a>
    <a href="index.php" class="w3-bar-item w3-button">Home</a>
    <a class="w3-bar-item w3-button" href="abouts.php">About Us</a>
    <a class="w3-bar-item w3-button" href="room.php">Rooms</a>
    <a class="w3-bar-item w3-button" href="gallery.php">Gallery</a>
    <a class="w3-bar-item w3-button" href="bar.php">Bars & Restaurants</a>
    
    <a class="w3-bar-item w3-button" href="workwithus.php">Work With Us</a>
<a class="w3-bar-item w3-button" href="calendar.php">Event</a>
<a class="w3-bar-item w3-button" href="contacts.php">Contact Us</a>
    <!-- <a class="w3-bar-item w3-button" href=""><span>Book Now</span></a> -->
  </div>
    
    <header role="banner" style="position: fixed;">
     
      <nav class="navbar2 navbar-expand-md navbar-dark bg-light" style="height: 100px;background-color: #fff !important">
        <div class="container">
          <a href="index.php"><img src="images/logo2.jpg" class="logox logo2" id="logo" style="width: inherit;"></a>
          <div>
            
            <!-- <ul class="navbar-nav ml-auto pl-lg-5 pl-0 activate-nav">
              <li class="nav-item">
                <a class="nav-link active" href="index.html">Home</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="rooms.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Rooms</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="rooms.html">Room Videos</a>
                  <a class="dropdown-item" href="rooms.html">Presidential Room</a>
                  <a class="dropdown-item" href="rooms.html">Luxury Room</a>
                  <a class="dropdown-item" href="rooms.html">Deluxe Room</a>
                </div>

              </li>
              <li class="nav-item">
                <a class="nav-link" href="">Event</a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="">Contact</a>
              </li>

               <li class="nav-item cta">
                <a class="nav-link" href=""><span>Book Now</span></a>
              </li>
            </ul> -->


            <!-- <a id="trigger" ></a> -->
            
            <!-- <button class="w3-button w3-xlarge" id="trigger" onclick="w3_open()"></button> -->
            <div class="container2" onclick="myFunction(this)">
              <div class="bar1 bar1-new" id="bar1"></div>
              <div class="bar3 bar3-new" id="bar3"></div>
            </div>
          </div>
        </div>
        <div class="book">
          <a class="a2" href="" id="book"><?php echo $manu["book"]; ?></a>
        </div>
      </nav>
    </header> 

    <!-- <div class="inner-bot"><a href="become.html"> Become </a></div>
    </div> -->

    <script>
              function myFunction(x) {
      var chklogo = document.getElementById("logo");
      if (chklogo.style.display === 'none') {
        document.getElementById("logo").style.display = "block"
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("book").style.color = "white";

      } else {
        document.getElementById("logo").style.display = "none"
        document.getElementById("mySidebar").style.width = "100%";
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("book").style.color = "black";

      }
      x.classList.toggle("change");
    }
    </script>

		<div class="booknow" style="margin-top: 120px;">
			<div class="inner">
				<p class="title">Check Rates & Availability</p>
				<div class="inputform">
					<form class="form" method="post" id="form" action="">  <!-- /booking-search.php?lang=en -->

                        <?php echo CSRF::tokenField() ?>
                        <div class="padding">
                            <div class="textdiv"><p>Check-in</p></div>
                            <!-- <input required type="text" name="check_in"  id="from" style="height:40px;width: 95%" disabled /> -->
                            <!-- <div class="form-group">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' name="check_in" class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div> -->
                            <input type="text" class="form-control checkIn" name="check_in" placeholder="Check-In" width="90" required autocomplete="off"> 
                        </div>
                        <div class="padding">
                            <div class="textdiv"><p>Check-out</p></div>
                            <!-- <input required type="text" name="check_out"  id="to" style="height:40px;width: 95%" disabled /> -->
                            <!-- <div class="form-group">
                                <div class='input-group date' id='datetimepicker2'>
                                    <input type='text' name="check_out" class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div> -->
                            <input type="text" class="form-control checkOut" name="check_out" placeholder="Check-Out" required autocomplete="off">   
                            <script type="text/javascript">
                                $(".checkIn").datepicker({
                                    format: "mm/dd/yyyy",
                                    todayBtn: true,
                                    autoclose: true,
                                    startDate: new Date()
                                  })
                                  .on("changeDate", function(e) {
                                    var checkInDate = e.date, $checkOut = $(".checkOut");    
                                    checkInDate.setDate(checkInDate.getDate() + 1);
                                    $checkOut.datepicker("setStartDate", checkInDate);
                                    $checkOut.datepicker("setDate", checkInDate).focus();
                                  });

                                $(".checkOut").datepicker({
                                  format: "mm/dd/yyyy",
                                  todayBtn: true,
                                  autoclose: true
                                });

                                
                            </script>
                        </div>
                        <div class="detail-inner">
                            <div class="content">
                                <div class="textdiv" style=""><p>Room(s)</p></div>
                                <select required style="height:40px;width: 50%">
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                                </select>
                            </div><div class="content">
                                <div class="textdiv" ><p>Adults</p></div>
                                <select required name="capacity" style="height:40px;width: 50%">
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                </select>
                            </div>
                            <div class="content">
                                <div class="textdiv" style="width: 180px;"><p>Children (0-12 yrs.)</p></div>
                                <select required name="child_per_room" style="height:40px;width: 50%">
                                  <option value="0">0</option>
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                                </select>
                            </div>
                            
                                <?php echo $bsiCore->get_currency_combo3($bsiCore->currency_code()); ?>
                                
                        </div>
                    </form>
				</div>

				<div class="highlight">
						<BUTTON class="button" type="submit" value="Submit" form="form" >Check Availability</BUTTON>
				</div>
			</div>
	        <div class="site-footer">
	           <div class="container">
                <div class="row mb-5">
                  <div class="col-md-4">
                    <p class="lead">Reservations <a href="tel://">+ 66 53 218 856</a></p>
                  </div>
                  <div class="col-md-4">
                    <h3>Connect With Us</h3>
                    <p>We are socialized. Follow us</p>
                    <p>
                      <a href="https://www.facebook.com/HomeChiangMaiHotel/" class="pl-0 p-3"><span class="fa fa-facebook"></span></a>
                      <a href="https://www.instagram.com/homechiangmaihotel/" class="p-3"><span class="fa fa-instagram"></span></a>
                    </p>
                  </div>
                  <div class="col-md-4">
                    <h3>Connect With Us</h3>
                    <p>“Home Chiang Mai Hotel
         Address:</p><p>
                      <a href="https://www.google.com/maps/place/'@homechiangmai'/@18.8029473,98.9800778,18z/data=!4m5!3m4!1s0x30da3a9296803ec7:0xada6e29a86472be!8m2!3d18.8031711!4d98.9807778?hl=en">3 Rasemeechan Alle, Sermsuk Rd. <br>Chang Phueak, Chiang Mai, Thailand 50300</a><br>
                      Phone:+66(0)53 218 856<br>
                      Email:gsa@homechiangmaihotel.com<br>
                      Fax:+66(0)53 218 856<br>
Website: www.homechiangmaihotel.com
                    </p>
                    <form action="#" class="subscribe">
                      <div class="form-group">
                        <button type="submit"><span class="ion-ios-arrow-thin-right"></span></button>
                        <input type="email" class="form-control" placeholder="Enter Email">
                      </div>
                      
                    </form>
                  </div>
                </div>
                <div class="row justify-content-center">
                  <div class="col-md-7 text-center">
                    &copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | 
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                  </div>
                </div>
              </div>

	        </div>
		</div>

		<script type="text/javascript">
			
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-733524-1']);
		  _gaq.push(['_trackPageview']);
			
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>

<script type="text/javascript" >var scrolltotop={setting:{startline:100,scrollto:0,scrollduration:1e3,fadeduration:[500,100]},controlHTML:'<img src="https://i1155.photobucket.com/albums/p559/scrolltotop/arrow27.png" />',controlattrs:{offsetx:5,offsety:5},anchorkeyword:"#top",state:{isvisible:!1,shouldvisible:!1},scrollup:function(){this.cssfixedsupport||this.$control.css({opacity:0});var t=isNaN(this.setting.scrollto)?this.setting.scrollto:parseInt(this.setting.scrollto);t="string"==typeof t&&1==jQuery("#"+t).length?jQuery("#"+t).offset().top:0,this.$body.animate({scrollTop:t},this.setting.scrollduration)},keepfixed:function(){var t=jQuery(window),o=t.scrollLeft()+t.width()-this.$control.width()-this.controlattrs.offsetx,s=t.scrollTop()+t.height()-this.$control.height()-this.controlattrs.offsety;this.$control.css({left:o+"px",top:s+"px"})},togglecontrol:function(){var t=jQuery(window).scrollTop();this.cssfixedsupport||this.keepfixed(),this.state.shouldvisible=t>=this.setting.startline?!0:!1,this.state.shouldvisible&&!this.state.isvisible?(this.$control.stop().animate({opacity:1},this.setting.fadeduration[0]),this.state.isvisible=!0):0==this.state.shouldvisible&&this.state.isvisible&&(this.$control.stop().animate({opacity:0},this.setting.fadeduration[1]),this.state.isvisible=!1)},init:function(){jQuery(document).ready(function(t){var o=scrolltotop,s=document.all;o.cssfixedsupport=!s||s&&"CSS1Compat"==document.compatMode&&window.XMLHttpRequest,o.$body=t(window.opera?"CSS1Compat"==document.compatMode?"html":"body":"html,body"),o.$control=t('<div id="topcontrol">'+o.controlHTML+"</div>").css({position:o.cssfixedsupport?"fixed":"absolute",bottom:o.controlattrs.offsety,right:o.controlattrs.offsetx,opacity:0,cursor:"pointer"}).attr({title:"Scroll to Top"}).click(function(){return o.scrollup(),!1}).appendTo("body"),document.all&&!window.XMLHttpRequest&&""!=o.$control.text()&&o.$control.css({width:o.$control.width()}),o.togglecontrol(),t('a[href="'+o.anchorkeyword+'"]').click(function(){return o.scrollup(),!1}),t(window).bind("scroll resize",function(t){o.togglecontrol()})})}};scrolltotop.init();</script>
</body>
</html>