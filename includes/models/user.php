<?php

require __DIR__ . '/model.php';

class User extends Model
{
    public $table = 'bsi_admin';

    /**
     * List users
     *
     * @return array
     */
    public function list()
    {
        $query = $this->db->query("SELECT id, username, f_name, l_name, email, last_login FROM {$this->table} ORDER BY id DESC");

        return $query->fetch_all(MYSQLI_ASSOC);
    }

    /**
     * Find by user id
     *
     * @param mixed $id user id
     *
     * @return array
     */
    public function findUserById($id)
    {
        $id = $this->filterNumber($id);

        $query = $this->db->query("SELECT * from {$this->table} WHERE id = {$id}");

        return $query->fetch_assoc();
    }

    /**
     * Create user
     *
     * @param array $user user data
     *
     * @return \Mysqli
     */
    public function createUser(array $user)
    {
        $user['pass'] = md5($user['pass']);
        $query        = $this->db->prepare("INSERT INTO {$this->table} (username, pass, f_name, l_name, email, access_id, designation, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

        $accessId    = 1;
        $status      = 1;

        $query->bind_param('sssssisi', $user['username'], $user['pass'], $user['f_name'], $user['l_name'], $user['email'], $accessId, $user['designation'], $status);

        return $query->execute();
    }

    /**
     * Update user
     *
     * @param array $user user data
     *
     * @return \Mysqli
     */
    public function updateUser(array $user)
    {
        $id = $this->filterNumber($user['id']);

        if (!empty($user['pass'])) {
            $user['pass'] = md5($user['pass']);

            $query = $this->db->prepare("UPDATE {$this->table} SET username = ?, pass = ?, f_name = ?, l_name = ?, email = ?, designation = ? WHERE id = ?");
            $query->bind_param('ssssssi', $user['username'], $user['pass'], $user['f_name'], $user['l_name'], $user['email'], $user['designation'], $id);
        } else {
            $query = $this->db->prepare("UPDATE {$this->table} SET username = ?, f_name = ?, l_name = ?, email = ?, designation = ? WHERE id = ?");
            $query->bind_param('sssssi', $user['username'], $user['f_name'], $user['l_name'], $user['email'], $user['designation'], $id);

            unset($user['pass']);
        }

        return $query->execute();
    }

    /**
     * Destroy user
     *
     * @param int $id user id
     *
     * @return \Mysqli
     */
    public function destroy($id)
    {
        $id = $this->filterNumber($id);

        $query = $this->db->prepare("DELETE FROM {$this->table} WHERE id = ?");
        $query->bind_param('i', $id);

        return $query->execute();
    }
}
