<!DOCTYPE html>
<html>
<head>

	<meta charset='utf-8' />
	<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible' />
	<title>Home Chiang Mai</title>
	
	<meta content='Serene & Private Retreat - Lanna Colonial Boutique Hotel in the North of Thailand' name='description' />
	
	
	<meta name="distribution" content="global" />
	<meta name="language" content="en" />
	<meta content='width=device-width, initial-scale=1.0' name='viewport' />
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="shortcut icon" href="images/favicon/favicon-16x16.png" />
	<link rel="apple-touch-icon" href="images/favicon/favicon-16x16.png" />
	
	<link rel="stylesheet" href="css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
	<link rel="stylesheet" href="css/screen.css?v=1.2" type="text/css" media="screen" charset="utf-8" />

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>

	<link rel="stylesheet" type="text/css" href="css/new_style.css">
	<link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css?family=Prompt:200" rel="stylesheet">

    <script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.all.min.js"></script>

	<style type="text/css">
		body {
		    font-family: Prompt;
		    font-size: 20px;
            background-color: inherit;
		}
		html{
			background-image: url("images/1.jpg");
			background-repeat: no-repeat;
			background-size: auto;
		}
		
		.pic-bot{
			width:30%; font-size: 25px;text-align: center;padding: 30px;
		}

		@media only screen and (max-width: 720px) {
           .promotion .inner .w3-row .w3-col{
           		width: 100%;
           }
           .promotion .inner{
           		margin: 40px;
		    	padding: initial;
           }
        }
	</style>
</head>
<body>
    <?php
        if ($_GET["col"] == '1') {
            echo "<script>
            window.onload = function() {
                            swal('ทำการจองสำเร็จ!','เจ้าหน้าที่จะทำการติดต่อกลับ เพื่อทำการยืนยัน','success');
                        }
                        </script>";
        }

    ?>
<div id="popup" class="w3-modal" style="padding-bottom: 100px;z-index: 9999;">
    <div class="w3-modal-content w3-animate-top w3-card-4">
      <header class="w3-container w3-teal"> 
        <span onclick="document.getElementById('popup').style.display='none'" 
        class="w3-button w3-display-topright">&times;</span>
        <h2 id="headerer" style="height: 100px;
                                text-align: center;
                                padding: 20px;
                                font-size: 30px;"></h2>
      </header>
      <form method="post" action="promotionbook.php" id="form1">
          <div class="w3-container">
            <input type="hidden" name="titlet" id="titlet">
            <div class="w3-row" style="padding: 16px;">
              <div class="w3-half w3-container">
                <p>Check-in</p> 
                <input type="text" name="chin" class="w3-input checkIn" autocomplete="off" required>
              </div>
              <div class="w3-half w3-container">
                <p>Check-out</p>
                <input type="text" name="chout" class="w3-input checkOut" autocomplete="off" required>
              </div>
                <script type="text/javascript">
                    $(".checkIn").datepicker({
                        format: "mm/dd/yyyy",
                        todayBtn: true,
                        autoclose: true,
                        startDate: new Date()
                      })
                      .on("changeDate", function(e) {
                        var checkInDate = e.date, $checkOut = $(".checkOut");    
                        checkInDate.setDate(checkInDate.getDate() + 1);
                        $checkOut.datepicker("setStartDate", checkInDate);
                        $checkOut.datepicker("setDate", checkInDate).focus();
                      });
                    $(".checkOut").datepicker({
                      format: "mm/dd/yyyy",
                      todayBtn: true,
                      autoclose: true
                    });  
                </script>
            </div>
            <div style="padding: 10px;">
                <p>คำนำหน้า</p>
                <select name="title">
                    <option value="นาย">นาย</option>
                    <option value="นาง">นาง</option>
                    <option value="นางสาว">นางสาว</option>
                </select>
            </div>
            <div style="padding: 10px;">
                <p>ชื่อ</p>
                <input required type="text" class="w3-input" name="firstname">
            </div>
            <div style="padding: 10px;">
                <p>นามสกุล</p>
                <input required type="text" class="w3-input" name="lastname">
            </div>
            <div style="padding: 10px;">
                <p>ที่อยู่</p>
                <input required type="text" class="w3-input" name="address">
            </div>
            <div style="padding: 10px;">
                <p>อำเภอ</p>
                <input required type="text" class="w3-input" name="distric">
            </div>
            <div style="padding: 10px;">
                <p>จังหวัด</p>
                <input required type="text" class="w3-input" name="province">
            </div>
            <div style="padding: 10px;">
                <p>รหัสไปรษณีย์</p>
                <input required type="text" class="w3-input" name="zip">
            </div>
            <div style="padding: 10px;">
                <p>ประเทศ</p>
                <input required type="text" class="w3-input" name="country">
            </div>
            <div style="padding: 10px;">
                <p>เบอร์โทรศัพท์</p>
                <input required type="text" class="w3-input" name="tel">
            </div>
            <div style="padding: 10px;">
                <p>หมายเลขบัตรประจำตัว</p>
                <input required type="text" class="w3-input" name="idcard">
            </div>
            <div style="padding: 10px;">
                <p>Credit card number</p>
                <input required type="text" class="w3-input" name="creditcard">
            </div>
            <div class="w3-row">
              <div class="w3-half w3-container">
                <p>Date expire</p> 
                <input required type="text" class="w3-input" name="expire">
              </div>
              <div class="w3-half w3-container">
                <p>cvc</p>
                <input required type="text" class="w3-input" name="cvc">
              </div>
            </div>
            <br>
            <br>
            <button class="w3-button w3-red" style="width: 100%" type="submit" form="form1" >จองทันที</button>
            <br>
            <br>

          </div>
        </form>
    </div>
  </div>
<div class="logo">
        <a href="/">
            <img src="images/logo.png" class="main">
        </a>
        <a href="javascript:void(0);" class="icon" onclick="myFunction()">
            <img src="images/menu.png">
        </a>
    </div>
    <div id="top_menu" class="top_menu">
        <div class="inner-top"><a href="index.html"> หน้าหลัก </a></div>
        <div class="inner-top"><a href="location.html"> สถานที่ตั้ง </a></div>
        <div class="inner-top"><a href="contact.php"> ติดต่อเรา </a></div>
        <div class="inner-top">
            <a href="#"><img src="images/thai.jpg"></a>
        </div>
        <div class="inner-top">
            <a href="eng/promotion.php "><img src="images/eng.png"></a>
        </div>
        <div class="inner-top">
            <a href="#"><img src="images/china.png"></a>
        </div>
            
    </div>
    <hr>
    <div id="under_menu" class="under_menu">
        <div class="inner-bot"><a href="accomudation.html"> ห้องพัก                
                <div class="dropdown-content">
                    <a href="premium-delux.html" > พรีเมียม ดีลักซ์ </a>
            <br>
            <a href="premium-delux-with-bath.html"> พรีเมียม ดีลักซ์ พร้อมอ่างอาบน้ำ </a>
            <br>
            <a href="premium-exclusive.html"> พรีเมียม เอ็กเซ็กคลูทีฟ </a>
            <br>
            <a href="premium-exclusive-with-bath.html"> พรีเมียม เอ็กเซ็กคลูทีฟ พร้อมอ่างอาบน้ำ </a>
        </div>
        </a>
    </div>


    <div class="inner-bot">
        <a href="dining.html"> ห้องอาหาร 
            <div class="dropdown-content">
                <a href="lemont.html" > ร้านอาหารเลอมองต์ </a>
                <br>
                <a href="contentrestuarent.html">ร้านอาหาร อิตาเลี่ยน/ไทย ซิโพลเล่</a>
             </div>
         </a>
    </div>
    <div class="inner-bot"><a href="promotion.php"> โปรโมชั่น </a></div>
    <div class="inner-bot">
        <a href="facilities.html"> สิ่งอำนวยความสะดวก 
                <div class="dropdown-content">
                    <a href="pool.html" > สระว่ายน้ำ </a>
                    <br>
                    <a href="fitness.html"> ห้องออกกำลังกาย 
                    </a>
                    <br>
                    <a href="contentgarden.html"> สวนคอนเทนท์ 
                    </a>
                    <br>

                    <a href="bus.html"> บริการรับส่งสนามบินและเข้าเมือง 
                    </a>
                    <br>
                    <a href="spa.html"> นวดไทย 
                    </a>
                    <br>
                </div>
        </a>
    </div>
    <div class="inner-bot"><a href="gallery.html"> รูปภาพ </a></div>
<div class="inner-bot"><a href="attraction.html"> สถานที่ท่องเที่ยว </a>
        <div class="dropdown-content">
            <a href="baanthawai.html">  บ้านถวาย </a><br>
            <a href="doisuthep.html">  ดอยสุเทพ </a><br>
            <a href="watintravas.html">  วัดอินทราวาส หรือวัดต้นแกว๋น </a><br>
            <a href="phathatdoikhom.html">  วัดพระธาตุดอยคํา </a><br>
            <a href="ratchaplue.html">  อุทยานหลวงราชพฤกษ์ </a><br>
            <a href="doiintanon.html">  ดอยอินทนนท์ </a><br>
            <a href="kadfarang.html">  กาดฝรั่ง </a><br>
            <a href="baanmuengging.html">  บ้านเหมืองกุง </a><br>
            <a href="nimman.html">  ถนนนิมมานเหมินทร์ </a><br>
            <a href="nightsafari.html">  เชียงใหม่ ไนท์ซาฟารี </a><br>
            <a href="nightbazaa.html">  เชียงใหม่ ไนท์พลาซ่า </a><br>
            <a href="grandcanyon.html">  สวนน้ํา แกรนด์แคนยอน </a><br>
            <a href="longpair.html">  เดินป่า ขี่ช้าง ล่องแพไม้ไผ่ อําเภอแม่วาง </a><br>
            <a href="maiaim.html">  พิพิธภัณฑ์ใหม่เอี่ยม เชียงใหม่ </a><br>
		</div>
    </div>
    <div class="inner-bot" >
<a href="booknow.php" style="color: inherit;
    text-shadow: inherit;">
<div class="bookicon" style="    transform: inherit;
    position: inherit;
    left: inherit;
    width: 90px;
    height: 35px;">
				<div class="booktext" style="line-height: 35px;">
                จองห้องพัก
            </div>
			</div>
			</a>
			</div>
        </div>
    </div>
    

    <script>
        function myFunction() {
            var top = document.getElementById("top_menu");
            var under = document.getElementById("under_menu");
            if (top.className === "top_menu") {
                top.className += " responsive";
            } else {
                top.className = "top_menu";
            }
            if (under.className === "under_menu") {
                under.className += " responsive";
            } else {
                under.className = "under_menu";
            }
        }
    </script>

			<div class="bookicon">
				<a href="booknow.php">
					<div class="booktext">
                        จองห้องพัก
                    </div>
				</a>
			</div>

		<div class="promotion">
			<div class="inner">
				<div  class="w3-row">
					<div class="w3-col pic-bot">
						<img src="images/Deluxe_Room/1532164226722.jpg"" style="width: 100%;"><br><br>
						<button class="w3-button w3-brown" style="width: 100%" onclick="upper('โปรโมชั่นคุ้มยิ่งกว่าคุ้ม');">จองห้องพัก</button>
					</div>
					<div class="w3-rest" style="padding: 35px;text-align: left">
						<h1 class="title">โปรโมชั่นคุ้มยิ่งกว่าคุ้ม</h1>
						<p class="detail-1">ระยะเวลาเข้าพัก : 22 กันยายน 2561 ถึง 14 ตุลาคม 2561</p><br>
						<p class="detail-2">พัก 3 คืน จ่ายเพียง 2 คืน รวมอาหารเช้า<br><br>											 <p style="color: red;font-weight: bold;"> ราคา 3,341/ห้อง/คืน เท่านั้น</p><br>
											<b>เงื่อนไขการใช้บริการ</b></p>
											<ul>
												<li>ต้องจองล่วงหน้าอย่างน้อย 3 วัน</li>
												<li>ต้องชำระเงินเต็มจำนวนในวันที่ทำการจองเท่านั้น</li>
												<li>ไม่สามารถยกเลิกหรือขอคืนเงินได้ทุกกรณี</li>
											</ul>
					</div>
				</div>

			</div>
			<div class="follow" >
	            <div class="inner-foll"><a href="https://www.facebook.com/contentvillacm/"><img src="images/facebook.png"></a></div>
	            <div class="inner-foll"><a href="https://www.instagram.com/contentvilla"><img src="images/inst.png"></a></div>
	            <div class="inner-foll"><a href="https://twitter.com/content_villa"><img src="images/twitter.png"></a></div>
	            <div class="inner-foll"><a href="https://plus.google.com/u/2/112129358189939629256"><img src="images/google.png"></a></div>
	            <div class="inner-foll"><a href="https://www.youtube.com/channel/UCSbmsuG8nmaTMU3QfRDWyJg"><img src="images/youtube.png"></a></div>
		    </div>
	        <div class="footer">
		         <h1>โรงแรม คอนเทนท์ วิลล่า เชียงใหม่</h1>
		         <p class="ft-text">59 หมู่ 6 ตำบลหนองควาย อำเภอหางดง จังหวัดเชียงใหม่ 50230 
		            <br>โทร : +66 53 125 220-221 โทรสาร : +66 53 125 227 <br>Email :  <a href="mailto:info.contentvilla@gmail.com"> info.contentvilla@gmail.com</a>
				<a href="http://www.contentvillachiangmai.com"> www.contentvillachiangmai.com</a></p>
		         <span style="color:white;font-size: 14px;">@ 2018 Home Chiang Mai</span>
	        </div>
		</div>
		




		<script src='https://www.google.com/recaptcha/api.js'></script>
		<!-- Extra JavaScript/CSS added manually in "Settings" tab -->
		<!-- Include jQuery -->

		
		<!-- DON'T USE THIS: Insert Google Analytics code here -->
		<script type="text/javascript">
			
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-733524-1']);
		  _gaq.push(['_trackPageview']);
			
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

        function upper(title){
            document.getElementById('popup').style.display='block';
            document.getElementById('headerer').innerHTML=title;
            document.getElementById('titlet').value=title;
        }

		</script>

<script type="text/javascript" >var scrolltotop={setting:{startline:100,scrollto:0,scrollduration:1e3,fadeduration:[500,100]},controlHTML:'<img src="https://i1155.photobucket.com/albums/p559/scrolltotop/arrow27.png" />',controlattrs:{offsetx:5,offsety:5},anchorkeyword:"#top",state:{isvisible:!1,shouldvisible:!1},scrollup:function(){this.cssfixedsupport||this.$control.css({opacity:0});var t=isNaN(this.setting.scrollto)?this.setting.scrollto:parseInt(this.setting.scrollto);t="string"==typeof t&&1==jQuery("#"+t).length?jQuery("#"+t).offset().top:0,this.$body.animate({scrollTop:t},this.setting.scrollduration)},keepfixed:function(){var t=jQuery(window),o=t.scrollLeft()+t.width()-this.$control.width()-this.controlattrs.offsetx,s=t.scrollTop()+t.height()-this.$control.height()-this.controlattrs.offsety;this.$control.css({left:o+"px",top:s+"px"})},togglecontrol:function(){var t=jQuery(window).scrollTop();this.cssfixedsupport||this.keepfixed(),this.state.shouldvisible=t>=this.setting.startline?!0:!1,this.state.shouldvisible&&!this.state.isvisible?(this.$control.stop().animate({opacity:1},this.setting.fadeduration[0]),this.state.isvisible=!0):0==this.state.shouldvisible&&this.state.isvisible&&(this.$control.stop().animate({opacity:0},this.setting.fadeduration[1]),this.state.isvisible=!1)},init:function(){jQuery(document).ready(function(t){var o=scrolltotop,s=document.all;o.cssfixedsupport=!s||s&&"CSS1Compat"==document.compatMode&&window.XMLHttpRequest,o.$body=t(window.opera?"CSS1Compat"==document.compatMode?"html":"body":"html,body"),o.$control=t('<div id="topcontrol">'+o.controlHTML+"</div>").css({position:o.cssfixedsupport?"fixed":"absolute",bottom:o.controlattrs.offsety,right:o.controlattrs.offsetx,opacity:0,cursor:"pointer"}).attr({title:"Scroll to Top"}).click(function(){return o.scrollup(),!1}).appendTo("body"),document.all&&!window.XMLHttpRequest&&""!=o.$control.text()&&o.$control.css({width:o.$control.width()}),o.togglecontrol(),t('a[href="'+o.anchorkeyword+'"]').click(function(){return o.scrollup(),!1}),t(window).bind("scroll resize",function(t){o.togglecontrol()})})}};scrolltotop.init();</script>
</body>
</html>