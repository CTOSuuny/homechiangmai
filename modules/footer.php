<!-- <div class="row footer">
    <div class="col-sm-2">
        <h3><?php trans('QUICK_LINKS') ?></h3>
        <br>
        <p>
            <a href="/"><?php trans('HOME_TEXT') ?></a>
        </p>
        <p>
            <a href="/page/about_hotel"><?php trans('ABOUT_US') ?></a>
        </p>
        <p>
            <a href="/page/rooms"><?php trans('ROOMS_TEXT') ?></a>
        </p>
        <p>
            <a href="/page/food-and-drink"><?php trans('FOOD_AND_DRINKS') ?></a>
        </p>
        <p>
            <a href="/page/gallery"><?php trans('GALLERY_TEXT') ?></a>
        </p>
        <p>
            <a href="/page/contact_us"><?php trans('CONTACT_US') ?></a>
        </p>
    </div>
    <div class="col-sm-3 footer-contact">
        <h3><?php trans('ADDRESS') ?></h3>
        <br>
            <p><?php echo $bsiCore->config["conf_hotel_name"]; ?></p>
            <p><?php trans('ADDRESS') ?> :
                <?php echo $bsiCore->config["conf_hotel_streetaddr"]; ?>
                <?php echo $bsiCore->config["conf_hotel_city"]; ?>
                <?php echo $bsiCore->config["conf_hotel_state"]; ?>
                <?php echo $bsiCore->config["conf_hotel_country"]; ?>
            </p>
            <p>
                <?php trans('ZIP') ?> : <?php echo $bsiCore->config["conf_hotel_zipcode"]; ?>
            </p>
            <p><?php trans('TEL') ?> : <?php echo $bsiCore->config["conf_hotel_phone"]; ?></p>
            <p><?php trans('FAX_TEXT') ?> : <?php echo $bsiCore->config["conf_hotel_fax"]; ?></p>
            <p><?php trans('EMAIL_TEXT') ?> : <a href="mailto:<?php echo $bsiCore->config["conf_hotel_email"]; ?>"><?php echo $bsiCore->config["conf_hotel_email"]; ?></a></p>
            <p><a class="footer-black-link" href="/">www.contentvillachiangmai.com</a></p><p>
    </div>
    <div class="col-sm-offset-2 col-sm-5" align="center">
        <h3><?php trans('CONNECT_WITH_US') ?></h3>
        <br>
        <a href="<?php echo $bsiCore->config["conf_hotel_url_facebook"]; ?>" class="link-connect"><i class="fa fa-facebook"></i></a>
        <a href="<?php echo $bsiCore->config["conf_hotel_url_twitter"]; ?>" class="link-connect"><i class="fa fa-twitter"></i></a>
        <a href="<?php echo $bsiCore->config["conf_hotel_url_instagram"]; ?>" class="link-connect"><i class="fa fa-instagram"></i></a>
        <a href="<?php echo $bsiCore->config["conf_hotel_url_googleplus"]; ?>" class="link-connect"><i class="fa fa-google-plus"></i></a>
        <a href="https://www.youtube.com/channel/UCSbmsuG8nmaTMU3QfRDWyJg" class="link-connect"><i class="fa fa-youtube"></i></a>
    </div>
    </div>
</div>
<div class="row all-rights-reserved">
    <p>© <?php echo date('Y') ?> Home Chiang Mai</p>
</div> -->
            <div class="site-footer">
               <div class="container">
                <div class="row mb-5">
                  <div class="col-md-4">
                    <h3>Reservations</h3>
                    <p class="lead"><a href="tel://+ 66 53 218 856">+ 66 53 218 856</a></p>
                  </div>
                  <div class="col-md-4">
                    <h3>Connect With Us</h3>
                    <p>We are socialized. Follow us</p>
                    <p>
                      <a href="https://www.facebook.com/HomeChiangMaiHotel/" class="pl-0 p-3"><span class="fa fa-facebook"></span></a>
                      <a href="https://www.instagram.com/homechiangmaihotel/" class="p-3"><span class="fa fa-instagram"></span></a>
                      <a href="#" class="p-3"><span class="fa fa-vimeo"></span></a>
                      <a href="#" class="p-3"><span class="fa fa-youtube-play"></span></a>
                    </p>
                  </div>
                  <div class="col-md-4">
                    <h3>Connect With Us</h3>
                    <p>
                      <a href="https://www.google.com/maps/place/'@homechiangmai'/@18.8029473,98.9800778,18z/data=!4m5!3m4!1s0x30da3a9296803ec7:0xada6e29a86472be!8m2!3d18.8031711!4d98.9807778?hl=en"><a href="https://www.google.com/maps/place/'@homechiangmai'/@18.8029473,98.9800778,18z/data=!4m5!3m4!1s0x30da3a9296803ec7:0xada6e29a86472be!8m2!3d18.8031711!4d98.9807778?hl=en">3 Rasemeechan Alle, Sermsuk Rd. <br>Chang Phueak, Chiang Mai, Thailand 50300</a></a><br>
                      Phone:+66(0)53 218 856<br>
                      Email:gsa@homechiangmaihotel.com<br>
                      Fax:+66(0)53 218 856<br>
Website: www.homechiangmaihotel.com
                    </p>
                    <form action="#" class="subscribe">
                      <div class="form-group">
                        <button type="submit"><span class="ion-ios-arrow-thin-right"></span></button>
                        <input type="email" class="form-control" placeholder="Enter Email">
                      </div>
                      
                    </form>
                  </div>
                </div>
                <div class="row justify-content-center">
                  <div class="col-md-7 text-center">
                    &copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | 
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                  </div>
                </div>
              </div>

            </div>
