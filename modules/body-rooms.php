<?php
    $room = new room;
?>
<div class="row show-room-dialog room-modules">
    <h2><?php trans('ROOMS') ?></h2>
    <br><br>
    <p>
        <?php trans('ROOMS_DESC'); ?>
    </p>
    <div class="pic-divider">
    </div>
    <hr class="hr-line">
    <?php

        $ik = 1;
        foreach($room->getRoomsType() as $rowRoom){
            echo "<div class='row show-room-dialog-rooms' >";

                echo '<script> $(document).ready(function() { ';
                    echo "var widthScreen = $( document ).width()";
                echo '
                    $("#iframe_details_'.str_replace(" ","",$rowRoom['roomtype_ID']).'").colorbox({iframe:true, width: (widthScreen <= 767 ? 90 : 60) + \'%\', height: "60%"});
                    $(".group_'.$rowRoom['roomtype_ID'].'").colorbox({rel:\'group_'.$rowRoom['roomtype_ID'].'\', maxWidth: $(\'.room-modules\').width(), maxHeight:"80%", slideshow:true, slideshowSpeed:5000});
                        ';
                echo '}); </script>';

    ?>
        <div class="col-sm-4">
            <div class="gallery-room">
                <div class="flexslider" style="cursor:pointer; cursor:hand;" >
                    <ul class="slides">
                        <?php  echo $bsiCore->roomtype_photos_only($rowRoom['roomtype_ID']); ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-8 rooms-detail">
            <h2><?php echo $rowRoom['type_name']; ?></h2>
            <p class="rooms-price"> <a style="color:#DFC27C" href="<?php echo asset("roomtype-details.php?tid=". $rowRoom['roomtype_ID']); ?>" id='iframe_details_<?php echo str_replace(" ","",$rowRoom['roomtype_ID']); ?>' style="font-weight:bold; color:#FFF;" ><?php echo VIEW_ROOM_FACILITIES_TEXT; ?></a></p>
            <div style="word-wrap: break-word;">
                <?php
                    $language = !empty($_COOKIE['language']) ? $_COOKIE['language'] : $defaultLanguage["lang_code"];
                    echo json_decode($rowRoom['description'], true)[$language];
                ?>
            </div>
            <br>
            <a class="pull-right btn btn-default btn-book room-form<?php echo $rowRoom['roomtype_ID']; ?>"><?php echo BOOK_YOUR_ROOM_NOW; ?></a>
            <script>
                $(window).load(function() {
                    var widthScreen = $( document ).width();

                    var colorboxOption = {
                        width: (widthScreen <= 767 ? 90 : 60) + '%',
                        height: '90%',
                        fixed: true,
                        href:"<?php echo asset("modules/modal-room-form.php?room_type_id_index=". $rowRoom['roomtype_ID']); ?>"
                    }

                    if (widthScreen > 767) {
                        colorboxOption.height = 'auto';
                    }

                    $('.room-form'+<?php echo $rowRoom['roomtype_ID']; ?>).colorbox(colorboxOption);
                });
            </script>
        </div>
    <?php
            echo "</div>";
            echo "<hr class='hr-line'>";
        }
    ?>
    <br><br><br><br><br>
</div>
